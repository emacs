;;; cherry.el --- Cherry pick commits from upstream master  -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library defines the command `cherry-pick-new-commits' that
;; iterates over new commits in the upstream master branch, and
;; suggests cherry-picking them one after the other.

;;; Code:

(defvar cherry-upstream-remote "upstream")

(defvar cherry-upstream-branch "master")

(defvar cherry-skip-list-file
  (expand-file-name "admin/cherry-skip-list" source-directory))

(defvar cherry-did-fetch nil)

(defvar cherry-upstream-commits nil)

(defun cherry--call-git (&rest args)
  (apply #'call-process "git" nil nil nil "-C" source-directory args))

(defun cherry-merge-base ()
  (car
   (string-lines
    (shell-command-to-string
     (concat "git -C " source-directory " merge-base "
             cherry-upstream-remote "/" cherry-upstream-branch
             " HEAD"))
    t)))

(defun cherry-upstream-commits ()
  (setq cherry-upstream-commits
        (or cherry-upstream-commits
            (string-lines
             (shell-command-to-string
              (concat "git -C " source-directory
                      " cherry HEAD "
                      cherry-upstream-remote "/" cherry-upstream-branch
                      " | grep -vE '^-' | cut -f 2 -d ' '"))
             t))))

(defun cherry-pick-new-commits (&optional force)
  "Pick or skip new commits in the upstream branch.

With non-nil optional argument FORCE (interactively, the prefix
argument), force fetching and scanning for upstream commits even if
already done in this session."
  (interactive "P")
  (message "Ensuring working directory is clean...")
  (unless (= 0 (cherry--call-git "diff-index" "--quiet" "HEAD" "--"))
    (user-error "Working directory is dirty, cannot start cherry picking"))
  (when force
    (setq cherry-did-fetch        nil
          cherry-upstream-commits nil))
  (unless cherry-did-fetch
    (message "Fetching from upstream...")
    (cherry--call-git "fetch" cherry-upstream-remote)
    (setq cherry-did-fetch t))
  (message "Listing upstream commits...")
  (let ((upstream-commits (cherry-upstream-commits))
        (new-commits nil)
        (old-commits (make-hash-table :test 'equal)))
    (message "Reading skip list...")
    (dolist (line (with-temp-buffer
                    (insert-file-contents cherry-skip-list-file)
                    (split-string (buffer-string) "\n" t)))
      (puthash (substring line 0 40) t old-commits))
    (message "Finding picked commits...")
    (dolist (commit
             (string-lines
              (shell-command-to-string
               (concat "git -C " source-directory
                       " log --grep '(cherry picked from commit' "
                       (cherry-merge-base) "..HEAD"
                       " | grep '(cherry picked from commit' | sed -e 's/^.*commit \\(.*\\)).*$/\\1/g'"))
              t))
      (puthash commit t old-commits))
    (dolist-with-progress-reporter (commit upstream-commits)
        "Checking for new commits"
      (or (gethash commit old-commits) (push commit new-commits)))
    (setq new-commits (nreverse new-commits))
    (if (null new-commits)
        (message "No new commits.")
      (let ((num-new (length new-commits))
            (current 0))
        (dolist (commit new-commits)
          (with-current-buffer (get-buffer-create "*cherry*")
            (setq buffer-read-only nil)
            (delete-region (point-min) (point-max))
            (fundamental-mode))
          (call-process "git" nil "*cherry*" t "-C" source-directory
                        "format-patch" "-1" commit "--stdout" "--no-encode-email-headers")
          (pop-to-buffer "*cherry*")
          (setq buffer-read-only t)
          (diff-mode)
          (bug-reference-mode)
          (goto-char (point-min))
          (let ((choice (read-multiple-choice
                         (format "[%d/%d] Pick?"
                                 (setq current (1+ current))
                                 num-new)
                         '((?p "pick")
                           (?s "skip")
                           (?q "quit")
                           ;; (?a "amend")
                           ))))
            (pcase (car choice)
              (?s
               (message "Skipping...")
               (unless (= 0 (shell-command
                             (concat "echo " commit
                                     (shell-quote-argument
                                      (read-string "Reason: " ""))
                                     ">>" cherry-skip-list-file)))
                 (user-error "Failed to write commit to skip list"))
               (unless (= 0 (cherry--call-git
                             "commit" "-m"
                             (concat "; Skip commit " commit)
                             (file-relative-name
                              cherry-skip-list-file source-directory)))
                 (user-error "Failed to commit updated skip list"))
               (message "Added to skip list and committed."))
              (?p
               (message "Picking...")
               (unless (= 0 (cherry--call-git "cherry-pick" "-x" commit))
                 (user-error "Cherry picking failed"))
               (message "Picked."))
              (?q (bury-buffer "*cherry*")
                  (user-error "Quit cherry picking"))))))
      (message "Done."))))

(provide 'cherry)
;;; cherry.el ends here
