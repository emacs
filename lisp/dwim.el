;;; dwim.el --- Do what I mean   -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

(defvar dwim-hook nil)

;;;###autoload
(defun dwim ()
  (interactive)
  (or (run-hook-with-args-until-success 'dwim-hook)
      (user-error "Cannot guess what you mean")))

(provide 'dwim)
;;; dwim.el ends here
