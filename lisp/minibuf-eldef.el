;;; minibuf-eldef.el --- Only show defaults in prompts when applicable  -*- lexical-binding: t -*-
;;
;; Copyright (C) 2000-2025 Free Software Foundation, Inc.
;;
;; Author: Miles Bader <miles@gnu.org>
;; Keywords: convenience

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Defines the mode `minibuffer-electric-default-mode'.
;;
;; When active, minibuffer prompts that show a default value only show
;; the default when it's applicable -- that is, when hitting RET would
;; yield the default value.  If the user modifies the input such that
;; hitting RET would enter a non-default value, the prompt is modified
;; to remove the default indication (which is restored if the input is
;; ever restore to the match the initial input).

;;; Code:

;;; Internal variables

(defvar minibuf-eldef-frobbed-minibufs nil
  "A list of minibuffers to which we've added a `post-command-hook'.")

;;; The following are all local variables in the minibuffer

(defvar-local minibuf-eldef-initial-input nil
  "Input pre-inserted into the minibuffer before the user can edit it.")

(defvar-local minibuf-eldef-initial-buffer-length nil
  "The length of the minibuffer with initial input inserted.")

(defvar-local minibuf-eldef-showing-default-in-prompt nil
  "Non-nil if the current minibuffer prompt contains the default spec.")

(defvar-local minibuf-eldef-overlay nil
  "An overlay covering the default portion of the prompt")


;;; Hook functions

;; This function goes on minibuffer-setup-hook
(defun minibuf-eldef-setup-minibuffer ()
  "Set up a minibuffer for `minibuffer-electric-default-mode'.
The prompt and initial input should already have been inserted."
  (save-excursion
    (goto-char (point-min))
    (if-let ((match (text-property-search-forward 'minibuffer-default)))
        (progn
          (setq minibuf-eldef-overlay
	        (make-overlay (prop-match-beginning match)
                              (prop-match-end match))
                minibuf-eldef-showing-default-in-prompt t
                minibuf-eldef-initial-input (minibuffer-contents-no-properties)
                minibuf-eldef-initial-buffer-length (point-max))
          (add-to-list 'minibuf-eldef-frobbed-minibufs (current-buffer))
          (add-hook 'post-command-hook #'minibuf-eldef-update-minibuffer nil t))
      (remove-hook 'post-command-hook #'minibuf-eldef-update-minibuffer t))))

;; post-command-hook to swap prompts when necessary
(defun minibuf-eldef-update-minibuffer ()
  "Update a minibuffer's prompt to include a default only when applicable.
This is intended to be used as a minibuffer `post-command-hook' for
`minibuffer-electric-default-mode'; the minibuffer should have already
been set up by `minibuf-eldef-setup-minibuffer'."
  (unless (eq minibuf-eldef-showing-default-in-prompt
	      (and (= (point-max) minibuf-eldef-initial-buffer-length)
		   (string-equal (minibuffer-contents-no-properties)
				 minibuf-eldef-initial-input)))
    ;; Swap state.
    (setq minibuf-eldef-showing-default-in-prompt
	  (not minibuf-eldef-showing-default-in-prompt))
    (overlay-put minibuf-eldef-overlay 'invisible
                 (not minibuf-eldef-showing-default-in-prompt))))


;;;###autoload
(define-minor-mode minibuffer-electric-default-mode
  "Toggle Minibuffer Electric Default mode.

Minibuffer Electric Default mode is a global minor mode.  When
enabled, minibuffer prompts that show a default value only show
the default when it's applicable -- that is, when hitting RET
would yield the default value.  If the user modifies the input
such that hitting RET would enter a non-default value, the prompt
is modified to remove the default indication."
  :global t
  :group 'minibuffer
  (if minibuffer-electric-default-mode
      ;; Enable the mode
      (add-hook 'minibuffer-setup-hook 'minibuf-eldef-setup-minibuffer)
    ;; Disable the mode
    (remove-hook 'minibuffer-setup-hook 'minibuf-eldef-setup-minibuffer)
    ;; Remove our entry from any post-command-hook variable's it's still in
    (dolist (minibuf minibuf-eldef-frobbed-minibufs)
      (with-current-buffer minibuf
	(remove-hook 'post-command-hook #'minibuf-eldef-update-minibuffer t)))
    (setq minibuf-eldef-frobbed-minibufs nil)))

(provide 'minibuf-eldef)
;;; minibuf-eldef.el ends here
