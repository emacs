;;; electric.el --- window maker and Command loop for `electric' modes  -*- lexical-binding: t; -*-

;; Copyright (C) 1985-1986, 1995, 2001-2025 Free Software Foundation,
;; Inc.

;; Author: K. Shane Hartman
;; Maintainer: emacs-devel@gnu.org
;; Keywords: extensions

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; "Electric" has been used in Emacs to refer to different things.
;; Among them:
;;
;; - electric modes and buffers: modes that typically pop-up in a modal kind of
;;   way a transient buffer that automatically disappears as soon as the user
;;   is done with it.
;;
;; - electric keys: self inserting keys which additionally perform some side
;;   operation which happens to be often convenient at that time.  Examples of
;;   such side operations are: reindenting code, inserting a newline,
;;   ... auto-fill-mode and abbrev-mode can be considered as built-in forms of
;;   electric key behavior.

;;; Code:

;;; Electric keys.

(defgroup electricity ()
  "Electric behavior for self inserting keys."
  :group 'editing)

(defun electric--after-char-pos ()
  "Return the position after the char we just inserted.
Returns nil when we can't find this char."
  (let ((pos (point)))
    (when (or (eq (char-before) last-command-event) ;; Sanity check.
              (save-excursion
                (or (progn (skip-chars-backward " \t")
                           (setq pos (point))
                           (eq (char-before) last-command-event))
                    (progn (skip-chars-backward " \n\t")
                           (setq pos (point))
                           (eq (char-before) last-command-event)))))
      pos)))

;;; Electric indentation.

;; Autoloading variables is generally undesirable, but major modes
;; should usually set this variable by adding elements to the default
;; value, which only works well if the variable is preloaded.
;;;###autoload
(defvar electric-indent-chars '(?\n)
  "Characters that should cause automatic reindentation.")

(defvar electric-indent-functions nil
  "Special hook run to decide whether to auto-indent.
Each function is called with one argument (the inserted char), with
point right after that char, and it should return t to cause indentation,
`no-indent' to prevent indentation or nil to let other functions decide.")

(defvar-local electric-indent-inhibit nil
  "If non-nil, reindentation is not appropriate for this buffer.
This should be set by major modes such as `python-mode' since
Python does not lend itself to fully automatic indentation.")

(defvar electric-indent-functions-without-reindent
  '(indent-relative indent-to-left-margin indent-relative-maybe
    indent-relative-first-indent-point py-indent-line coffee-indent-line
    org-indent-line yaml-indent-line haskell-indentation-indent-line
    haskell-indent-cycle haskell-simple-indent yaml-indent-line)
  "List of indent functions that can't reindent.
If `indent-line-function' is one of those, then `electric-indent-mode' will
not try to reindent lines.  It is normally better to make the major
mode set `electric-indent-inhibit', but this can be used as a workaround.")

(defun electric-indent-post-self-insert-function ()
  "Function that `electric-indent-mode' adds to `post-self-insert-hook'.
This indents if the hook `electric-indent-functions' returns non-nil,
or if a member of `electric-indent-chars' was typed; but not in a string
or comment."
  ;; FIXME: This reindents the current line, but what we really want instead is
  ;; to reindent the whole affected text.  That's the current line for simple
  ;; cases, but not all cases.  We do take care of the newline case in an
  ;; ad-hoc fashion, but there are still missing cases such as the case of
  ;; electric-pair-mode wrapping a region with a pair of parens.
  ;; There might be a way to get it working by analyzing buffer-undo-list, but
  ;; it looks challenging.
  (let (pos)
    (when (and
           electric-indent-mode
           ;; Don't reindent while inserting spaces at beginning of line.
           (or (not (memq last-command-event '(?\s ?\t)))
               (save-excursion (skip-chars-backward " \t") (not (bolp))))
           (setq pos (electric--after-char-pos))
           (save-excursion
             (goto-char pos)
             (let ((act (or (run-hook-with-args-until-success
                             'electric-indent-functions
                             last-command-event)
                            (memq last-command-event electric-indent-chars))))
               (not (memq act '(nil no-indent))))))
      ;; If we error during indent, silently give up since this is an
      ;; automatic action that the user didn't explicitly request.
      ;; But we don't want to suppress errors from elsewhere in *this*
      ;; function, hence the `condition-case' and `throw' (Bug#18764).
      (catch 'indent-error
        ;; For newline, we want to reindent both lines and basically
        ;; behave like reindent-then-newline-and-indent (whose code we
        ;; hence copied).
        (let ((at-newline (<= pos (line-beginning-position))))
          (when at-newline
            (let ((before (copy-marker (1- pos) t)))
              (save-excursion
                (unless
                    (or (memq indent-line-function
                              electric-indent-functions-without-reindent)
                        electric-indent-inhibit)
                  ;; Don't reindent the previous line if the
                  ;; indentation function is not a real one.
                  (goto-char before)
                  (condition-case-unless-debug ()
                      (indent-according-to-mode)
                    (error (throw 'indent-error nil))))
                (goto-char before)
                ;; We were at EOL in marker `before' before the call
                ;; to `indent-according-to-mode' but after we may
                ;; not be (Bug#15767).
                (when (and (eolp))
                  (delete-horizontal-space t)))))
          (unless (and electric-indent-inhibit
                       (not at-newline))
            (condition-case-unless-debug ()
                (indent-according-to-mode)
              (error (throw 'indent-error nil)))))))))

(defvar-keymap electric-indent-local-mode-map
  :doc "Keymap for `electric-indent-local-mode'."
  "<remap> <newline>" #'newline-and-indent)

;;;###autoload
(define-minor-mode electric-indent-local-mode
  "Toggle `electric-indent-mode' only in this buffer."
  :light nil
  (if electric-indent-local-mode
      (add-hook 'post-self-insert-hook
                #'electric-indent-post-self-insert-function 60 t)
    (remove-hook 'post-self-insert-hook
                 #'electric-indent-post-self-insert-function t)))

;;;###autoload
(define-globalized-minor-mode electric-indent-mode
  electric-indent-local-mode electric-indent-local-mode)

;;; Electric quoting.

(defcustom electric-quote-comment t
  "Non-nil means to use electric quoting in program comments."
  :version "25.1"
  :type 'boolean :safe 'booleanp :group 'electricity)

(defcustom electric-quote-string nil
  "Non-nil means to use electric quoting in program strings."
  :version "25.1"
  :type 'boolean :safe 'booleanp :group 'electricity)

;; The default :value-create produces "list of numbers" when given "list
;; of characters", this prints them as characters.
(declare-function widget-get "wid-edit" (widget property))
(defun electric--print-list-of-chars (widget)
  (let ((print-integers-as-characters t))
    (princ (widget-get widget :value) (current-buffer))))

;; This is just so we can make pairs print as characters.
(define-widget 'electric-char-pair 'const
  "Electric quote character pair."
  :group 'electricity
  :format "%t: %v\n"
  :inline t
  :value-create #'electric--print-list-of-chars
  :type '(list character character))

(define-widget 'electric-quote-chars-pairs 'lazy
  "Choose pair of electric quote chars."
  :group 'electricity
  :type '(radio
          (electric-char-pair :tag "Single" (?‘ ?’))
          (electric-char-pair :tag "Double" (?“ ?”))
          (electric-char-pair :tag "Guillemets" (?« ?»))
          (electric-char-pair :tag "Single guillemets" (?‹ ?›))
          (electric-char-pair :tag "Corners" (?「 ?」))
          (electric-char-pair :tag "Double corners" (?『 ?』))
          (electric-char-pair :tag "Low/high single left" (?‚ ?‘))
          (electric-char-pair :tag "Low/high double left" (?„  ?“))
          (electric-char-pair :tag "Low/high single right" (?‚ ?’))
          (electric-char-pair :tag "Low/high double right" (?„  ?”))
          (electric-char-pair :tag "Single inverted" (?’ ?‘))
          (electric-char-pair :tag "Right single only" (?’ ?’))
          (electric-char-pair :tag "Right double only" (?” ?”))
          (electric-char-pair :tag "Guillemets inverted" (?» ?«))
          (electric-char-pair :tag "Guillemets right only" (?» ?»))
          (electric-char-pair :tag "Single guillemets inverted" (?› ?‹))
          (electric-char-pair :tag "Mathematical double angle" (?⟪ ?⟫))
          (electric-char-pair :tag "Mathematical single angle" (?⟨ ?⟩))
          (electric-char-pair :tag "Double angle" (?《 ?》))
          (electric-char-pair :tag "Single angle" (?〈 ?〉))))

(defcustom electric-quote-chars '(?‘ ?’ ?“ ?”)
  "Curved quote characters for `electric-quote-mode'.
This list's members correspond to left single quote, right single
quote, left double quote, and right double quote, respectively.

Commonly used pairs are predefined, or you can define your own
completely custom style."
  :version "26.1"
  :type '(choice
          (const :format "%t: %v\n" :tag "Default"
                 :value-create electric--print-list-of-chars
                 (?‘ ?’ ?“ ?”))
          (list :tag "Predefined pairs"
                (electric-quote-chars-pairs :tag "Single quotes")
                (electric-quote-chars-pairs :tag "Double quotes"))
          (list :tag "Custom"
                (character ?‘) (character ?’)
                (character ?“) (character ?”)))
  :safe (lambda (x)
          (pcase x
            (`(,(pred characterp) ,(pred characterp)
               ,(pred characterp) ,(pred characterp))
             t)))
  :group 'electricity)

(defcustom electric-quote-paragraph t
  "Non-nil means to use electric quoting in text paragraphs."
  :version "25.1"
  :type 'boolean :safe 'booleanp :group 'electricity)

(defcustom electric-quote-context-sensitive nil
  "Non-nil means to replace \\=' with an electric quote depending on context.
If `electric-quote-context-sensitive' is non-nil, Emacs replaces
\\=' and \\='\\=' with an opening quote after a line break,
whitespace, opening parenthesis, or quote and leaves \\=` alone."
  :version "26.1"
  :type 'boolean :safe #'booleanp :group 'electricity)

(defcustom electric-quote-replace-double nil
  "Non-nil means to replace \" with an electric double quote.
Emacs replaces \" with an opening double quote after a line
break, whitespace, opening parenthesis, or quote, and with a
closing double quote otherwise."
  :version "26.1"
  :type 'boolean :safe #'booleanp :group 'electricity)

(defcustom electric-quote-replace-consecutive t
  "Non-nil means to replace a pair of single quotes with a double quote.
Two single quotes are replaced by the corresponding double quote
when the second quote of the pair is entered (i.e. by typing ` or
') by default.  If nil, the single quotes are not altered."
  :version "29.1"
  :type 'boolean
  :safe #'booleanp
  :group 'electricity)

(defvar electric-quote-inhibit-functions ()
  "List of functions that should inhibit electric quoting.
When the variable `electric-quote-mode' is non-nil, Emacs will
call these functions in order after the user has typed an \\=` or
\\=' character.  If one of them returns non-nil, electric quote
substitution is inhibited.  The functions are called after the
\\=` or \\=' character has been inserted with point directly
after the inserted character.  The functions in this hook should
not move point or change the current buffer.")

(defvar electric-pair-text-pairs)

(defun electric-quote-post-self-insert-function ()
  "Function that `electric-quote-mode' adds to `post-self-insert-hook'.
This requotes when a quoting key is typed."
  (when (and electric-quote-mode
             (or (eq last-command-event ?\')
                 (and (not electric-quote-context-sensitive)
                      (eq last-command-event ?\`))
                 (and electric-quote-replace-double
                      (eq last-command-event ?\")))
             (not (run-hook-with-args-until-success
                   'electric-quote-inhibit-functions))
             (if (derived-mode-p 'text-mode)
                 electric-quote-paragraph
               (and comment-start comment-use-syntax
                    (or electric-quote-comment electric-quote-string)
                    (let* ((syntax (syntax-ppss))
                           (beg (nth 8 syntax)))
                      (and beg
                           (or (and electric-quote-comment (nth 4 syntax))
                               (and electric-quote-string (nth 3 syntax)))
                           ;; Do not requote a quote that starts or ends
                           ;; a comment or string.
                           (eq beg (nth 8 (save-excursion
                                            (syntax-ppss (1- (point)))))))))))
    (pcase electric-quote-chars
      (`(,q< ,q> ,q<< ,q>>)
       (save-excursion
         (let ((backtick ?\`))
           (if (or (eq last-command-event ?\`)
                   (and (or electric-quote-context-sensitive
                            (and electric-quote-replace-double
                                 (eq last-command-event ?\")))
                        (save-excursion
                          (backward-char)
                          (skip-syntax-backward "\\")
                          (or (bobp) (bolp)
                              (memq (char-before) (list q< q<<))
                              (memq (char-syntax (char-before))
                                    '(?\s ?\())))
                        (setq backtick ?\')))
               (cond ((and electric-quote-replace-consecutive
                           (search-backward
                            (string q< backtick) (- (point) 2) t))
                      (replace-match (string q<<))
                      (when (and electric-pair-mode
                                 (eq (cdr-safe
                                      (assq q< electric-pair-text-pairs))
                                     (char-after)))
                        (delete-char 1))
                      (setq last-command-event q<<))
                     ((search-backward (string backtick) (1- (point)) t)
                      (replace-match (string q<))
                      (setq last-command-event q<))
                     ((search-backward "\"" (1- (point)) t)
                      (replace-match (string q<<))
                      (setq last-command-event q<<)))
             (cond ((and electric-quote-replace-consecutive
                         (search-backward (string q> ?') (- (point) 2) t))
                    (replace-match (string q>>))
                    (setq last-command-event q>>))
                   ((search-backward "'" (1- (point)) t)
                    (replace-match (string q>))
                    (setq last-command-event q>))
                   ((search-backward "\"" (1- (point)) t)
                    (replace-match (string q>>))
                    (setq last-command-event q>>))))))))))

;;;###autoload
(define-minor-mode electric-quote-mode
  "Toggle on-the-fly requoting (Electric Quote mode).

When enabled, as you type this replaces \\=` with \\=‘, \\=' with \\=’,
\\=`\\=` with “, and \\='\\=' with ”.  This occurs only in comments, strings,
and text paragraphs, and these are selectively controlled with
`electric-quote-comment', `electric-quote-string', and
`electric-quote-paragraph'.

Customize `electric-quote-chars' to use characters other than the
ones listed here.  Also see `electric-quote-replace-consecutive'.

This is a global minor mode.  To toggle the mode in a single buffer,
use `electric-quote-local-mode'."
  :global t :group 'electricity
  :initialize 'custom-initialize-delay
  :init-value nil
  (if (not electric-quote-mode)
      (unless (catch 'found
                (dolist (buf (buffer-list))
                  (with-current-buffer buf
                    (if electric-quote-mode (throw 'found t)))))
        (remove-hook 'post-self-insert-hook
                     #'electric-quote-post-self-insert-function))
    (add-hook 'post-self-insert-hook
              #'electric-quote-post-self-insert-function
              10)))

;;;###autoload
(define-minor-mode electric-quote-local-mode
  "Toggle `electric-quote-mode' only in this buffer."
  :variable ( electric-quote-mode .
              (lambda (val) (setq-local electric-quote-mode val)))
  (cond
   ((eq electric-quote-mode (default-value 'electric-quote-mode))
    (kill-local-variable 'electric-quote-mode))
   ((not (default-value 'electric-quote-mode))
    ;; Locally enabled, but globally disabled.
    (electric-quote-mode 1)                ; Setup the hooks.
    (setq-default electric-quote-mode nil) ; But keep it globally disabled.
    )))

(provide 'electric)

;;; electric.el ends here
