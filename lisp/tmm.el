;;; tmm.el --- text mode access to menu-bar  -*- lexical-binding: t -*-

;; Copyright (C) 1994-1996, 2000-2025 Free Software Foundation, Inc.

;; Author: Ilya Zakharevich <ilya@math.mps.ohio-state.edu>
;; Maintainer: emacs-devel@gnu.org
;; Keywords: convenience

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides text mode access to the menu bar.

;;; Code:

(defgroup tmm nil
  "Text mode access to menu-bar."
  :prefix "tmm-"
  :group 'menu)

;;;###autoload (define-key global-map "\M-`" 'tmm-menubar)

;;;###autoload
(defun tmm-menubar (&optional _ignore)
  "Text-mode emulation of looking and choosing from a menubar.

Note that \\[menu-bar-open] by default drops down TTY menus; if you want it
to invoke `tmm-menubar' instead, customize the variable
`tty-menu-open-use-tmm' to a non-nil value."
  (interactive)
  (run-hooks 'menu-bar-update-hook)
  (let ((help-complete-keys-method 'nest))
    (help-complete-keys [menu-bar])))

(provide 'tmm)

;;; tmm.el ends here
