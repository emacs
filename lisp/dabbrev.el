;;; dabbrev.el --- Dynamic abbreviations  -*- lexical-binding: t -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; Author: Eshel Yaron
;; Keywords: abbrev expand completion convenience

;; This is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your
;; option) any later version.

;; This is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.

;; You should have received a copy of the GNU General Public License
;; with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defvar dabbrev-maximum-expansions 16
  "Maximum number of dynamic abbreviation expansions to suggest.")

(defvar dabbrev-maximum-characters 65536
  "Maximum number of character to scan from dynamic expansions.")

(defvar dabbrev-buffer-search-condition
  '(not (or (derived-mode . special-mode) "^ "))
  "Condition for searching a buffer for dynamic abbreviation expansions.
Dabbrev calls `match-buffers' (which see) with the value of this
variable as the CONDITION argument, and searchs the matching buffers in
addition to the current buffer and the visible buffers.")

(defun dabbrev-expansions (&optional abbrev anchor)
  "Return dynamic expansions for ABBREV sorted by proximity to ANCHOR."
  (let* ((abbrev (or abbrev (thing-at-point 'symbol)))
         (anchor (or anchor (car (bounds-of-thing-at-point 'symbol))))
         (scanned 0) pos expansions more
         (search
          (lambda (buffer)
            (when (< scanned dabbrev-maximum-characters)
              (with-current-buffer buffer
                (save-excursion
                  (goto-char (point-min))
                  (setq pos (point))
                  (while (and (< scanned dabbrev-maximum-characters)
                              (re-search-forward
                               (rx symbol-start
                                   (literal abbrev)
                                   (one-or-more (or (syntax word) (syntax symbol))))
                               nil t))
                    (setq scanned (+ (- (point) pos))
                          pos (point))
                    (let ((match (match-string-no-properties 0)))
                      (unless (or (equal match abbrev)
                                  (member match more)
                                  (member match expansions))
                        (push match more))))))))))
    (when (and abbrev (not (string-empty-p abbrev)) anchor)
      ;; Search the current buffer.
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward (rx symbol-start
                                      (literal abbrev)
                                      (one-or-more (or (syntax word)
                                                       (syntax symbol))))
                                  nil t)
          (let ((match (match-string-no-properties 0)))
            (unless (equal match abbrev)
              (push (cons (abs (- anchor (match-beginning 0))) match)
                    expansions)))))
      (setq expansions (take dabbrev-maximum-expansions
                             (delete-consecutive-dups
                              (mapcar #'cdr (sort expansions :in-place t))))
            scanned (- (point-max) (point-min)))
      ;; Then all visible buffers.
      (when (< scanned dabbrev-maximum-characters)
        (walk-windows (compf [search] window-buffer) nil 'visible)
        (setq expansions (nconc expansions more) more nil))
      ;; Then try other buffers.
      (when (< scanned dabbrev-maximum-characters)
        (mapc search (match-buffers dabbrev-buffer-search-condition))
        (setq expansions (nconc expansions more)))
      expansions)))

;;;###autoload
(defun dabbrev-capf ()
  "Dabbrev completion function for `completion-at-point-functions'."
  (when-let ((bounds (bounds-of-thing-at-point 'symbol)))
    (list (car bounds) (cdr bounds)
          (completion-table-with-metadata
           (completion-table-with-cache #'dabbrev-expansions)
           '((sort-function . identity) (category . dabbrev)))
          :exclusive 'no)))

;;;###autoload
(defun dabbrev-expand ()
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'symbol)))
    (unless bounds (user-error "Nothing to expand at point"))
    (let ((beg (car bounds)) (expansions (dabbrev-expansions)))
      (unless expansions
        (user-error "No dynamic expansions for `%s'"
                    (buffer-substring-no-properties beg (cdr bounds))))
      (completion--replace beg (cdr bounds) (car expansions))
      (set-transient-map
       (let ((map (make-sparse-keymap)))
         (define-key map (vector last-command-event)
                     (lambda ()
                       (interactive)
                       (let ((cur (car expansions))
                             (last (last expansions)))
                         (setcdr last (cons cur (cdr last)))
                         (setq expansions (cdr expansions))
                         (completion--replace beg (+ beg (length cur))
                                              (car expansions)))))
         map)
       t))))

;;;###autoload
(defun dabbrev-completion ()
  (interactive)
  (let ((completion-at-point-functions '(dabbrev-capf))) (completion-at-point)))

;;;###autoload (define-key esc-map "/" 'dabbrev-expand)
;;;###autoload (define-key esc-map [?\C-/] 'dabbrev-completion)

(provide 'dabbrev)

;;; dabbrev.el ends here
