;;; search.el --- Interactive text search  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library provides advanced text searching commands, inspired by
;; Icicles search (https://www.emacswiki.org/emacs/Icicles).

;;; Todo:

;; - Support multi-buffer `search'.
;; - Search non-matches.
;; - Highlight subgroups in matches.
;; - Improve documentation.
;; - Support toggling search case sensitivity.
;; - Lazy-highlight matches while reading search target.

;;; Code:

(defgroup search nil "Text search." :group 'matching)

(defface search-highlight
  '((t :inherit highlight :foreground "black"))
  "Face for highlighting matches for current input during \\[search].")

(defun search-change ()
  "Change the current `search' context."
  (interactive)
  (throw 'search-change (minibuffer-contents)))

(defun search-read-target-mode-p (_symbol buffer)
  "Check whether `search-read-target-mode' is enabled in BUFFER."
  (buffer-local-value 'search-read-target-mode buffer))

(put 'search-change 'completion-predicate #'search-read-target-mode-p)

(defvar-keymap search-read-target-mode-map
  :doc "Keymap for `search-read-target-mode'."
  "M-s M-s" #'search-change)

(define-minor-mode search-read-target-mode
  "Minor mode for `search-read-target' minibuffer.")

(defun search--read-target (buffer beg end reg sfn init alt)
  "Prompt for search target in BUFFER between BEG and END matching REG.

SFN is the search function to use for finding matches.  INIT is the
initial minibuffer input."
  (let ((tab (make-hash-table)) (ind 0) ovs ovz cur trs rep)
    (save-excursion
      (goto-char beg)
      (let ((pos beg) done)
        (while (not done)
          (if (not (and (< (point) end) (funcall sfn)))
              (setq done t)
            (if (<= (point) pos)
                (forward-char)
              (push (format "%d:%s" (cl-incf ind) (match-string 0)) trs)
              (puthash ind
                       (list (copy-marker (match-beginning 0))
                             (copy-marker (match-end 0)))
                       tab)
              (push (make-overlay (match-beginning 0)
                                  (match-end 0))
                    ovs)
              (overlay-put (car ovs) 'face 'lazy-highlight)
              (overlay-put (car ovs) 'search t)
              (overlay-put (car ovs) 'priority '(nil . 1)))
            (setq pos (point))))))
    (setq trs (nreverse trs))
    (unwind-protect
        (minibuffer-with-setup-hook
            (lambda ()
              (search-read-target-mode)
              (setq minibuffer-action
                    (cons
                     (lambda (c)
                       (with-selected-window
                           (or (get-buffer-window buffer) (display-buffer buffer))
                         (goto-char (car (gethash (string-to-number c) tab)))
                         (when (overlayp cur) (overlay-put cur 'face 'lazy-highlight))
                         (setq cur (seq-some
                                    (lambda (ov) (and (overlay-get ov 'search) ov))
                                    (overlays-at (point))))
                         (overlay-put cur 'face 'isearch)))
                     "search"))
              (setq minibuffer-alternative-action
                    (if alt
                        (cons
                         (lambda (c)
                           (with-selected-window
                               (or (get-buffer-window buffer) (display-buffer buffer))
                             (goto-char (car (gethash (string-to-number c) tab)))
                             (when (overlayp cur) (overlay-put cur 'face 'lazy-highlight))
                             (setq cur (seq-some
                                        (lambda (ov) (and (overlay-get ov 'search) ov))
                                        (overlays-at (point))))
                             (overlay-put cur 'face 'isearch)
                             (funcall (car alt))))
                         (cdr alt))
                      (cons
                       (lambda (c)
                         (if-let ((n (string-to-number c))
                                  (d (gethash n tab)))
                             (with-selected-window
                                 (or (get-buffer-window buffer) (display-buffer buffer))
                               (when (overlayp cur) (overlay-put cur 'face 'lazy-highlight))
                               (set-match-data d)
                               (let ((ov (seq-some
                                          (lambda (ov) (and (overlay-get ov 'search) ov))
                                          (overlays-at (match-beginning 0)))))
                                 (unless rep
                                   (overlay-put ov 'face 'isearch)
                                   (goto-char (match-beginning 0))
                                   (setq rep (query-replace-read-to reg "Replace" t)))
                                 (setq ovs (delq ov ovs))
                                 (delete-overlay ov))
                               (setq trs (delete c trs))
                               (remhash n tab)
                               (replace-match rep))
                           (user-error "Already replaced")))
                       "replace")))
              (let ((hook-fn
                     (lambda (input)
                       (unless (string-empty-p input)
                         (mapc #'delete-overlay ovz)
                         (setq ovz nil)
                         (with-current-buffer buffer
                           (dolist (ov ovs)
                             (save-excursion
                               (goto-char (overlay-start ov))
                               (let ((r (regexp-quote input))
                                     (e (overlay-end ov)))
                                 (while (re-search-forward r e t)
                                   (push (make-overlay (match-beginning 0)
                                                       (match-end 0))
                                         ovz)
                                   (overlay-put (car ovz) 'face 'search-highlight)
                                   (overlay-put (car ovz) 'search-input t)
                                   (overlay-put (car ovz) 'priority '(nil . 10))
                                   (overlay-put (car ovz) 'evaporate t))))))))))
                (add-hook 'minibuffer-new-completion-input-hook
                          (lambda () (funcall hook-fn (caar completion-history)))
                          nil t)
                (add-hook 'completion-setup-hook
                          (lambda () (funcall hook-fn (minibuffer-contents)))
                          nil t)))
          (gethash (string-to-number
                    (completing-read
                     "Search: "
                     (completion-table-with-metadata
                      (completion-table-dynamic (lambda (_) trs))
                      `((category . search)
                        (group-function
                         . ,(lambda (string &optional transform)
                              (when transform
                                (substring string (1+ (string-search ":" string))))))))
                     nil t init t))
                   tab))
      (mapc #'delete-overlay ovs)
      (mapc #'delete-overlay ovz))))

(defun search-read-target (&optional beg end re-or-fn alt)
  "Prompt for \\[search] target between BEG and END matching RE-OR-FN."
  (let* ((buf (current-buffer))
         (beg (or beg (use-region-beginning) (point-min)))
         (end (or end (use-region-end)       (point-max)))
         reg res
         (sfn (if (functionp re-or-fn)
                  (prog1 re-or-fn (setq reg "match"))
                (setq reg (or re-or-fn (read-regexp "Search regular expression")))
                (lambda () (re-search-forward reg end t)))))
    (deactivate-mark)
    (while (stringp (setq res (catch 'search-change
                                (search--read-target buf beg end reg sfn res alt))))
      (setq reg (read-regexp "Search regular expression")
            sfn (lambda () (re-search-forward reg end t))))
    res))

;;;###autoload
(defun search (beg end)
  "Go to and pulse region starting at BEG and ending at END."
  (interactive (save-excursion (search-read-target)))
  (push-mark)
  (goto-char beg)
  (pulse-momentary-highlight-region beg end 'isearch))

;;;###autoload
(defun search-from-isearch (beg end)
  "Pivot to `search' between BEG and END from `isearch'."
  (interactive
   (if isearch-mode
       (save-excursion
         (isearch-done)
         (search-read-target nil nil
                             (if isearch-regexp
                                 isearch-string
                               (regexp-quote isearch-string))))
     (user-error "Isearch not in progress")))
  ;; TODO: Consider handling `isearch-recursive-edit' like
  ;; `isearch-query-replace' does.
  (search beg end))

;;;###autoload
(defun search-lines (beg end)
  "Go to and pulse line starting at BEG and ending at END."
  (interactive (save-excursion (search-read-target nil nil ".*")))
  (search beg end))

(defun search--property (prop)
  (lambda ()
    (when-let ((pm (text-property-search-forward prop)))
      (set-match-data (list (prop-match-beginning pm)
                            (prop-match-end pm)))
      (point))))

;;;###autoload
(defun search-property (beg end)
  "`search' for region from BEG to END with a given text property."
  (interactive
   (save-excursion
     (search-read-target nil nil (search--property (read-text-property)))))
  (search beg end))

;;;###autoload
(defun search-button (beg end)
  "`search' for button from BEG to END."
  (interactive
   (save-excursion
     (search-read-target nil nil (search--property 'button)
                         (cons #'push-button "push"))))
  (search beg end)
  (push-button))

(provide 'search)
;;; search.el ends here
