;;; filet.el --- Do things with copied file names   -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library provides a single command, `filet'.  It pops up a
;; transient menu that lets you apply file system operations to files
;; whose names you previously copied (killed).
;;
;; `filet' works best with Dired: say you're in a Dired buffer showing
;; and you want to copy some of the listed files to another location.
;; Maybe you don't know exactly where you want to put them yet, but
;; that's OK.  Simply mark some files and hit `0 w' to copy their
;; absolute names.  You can consult some resource or just think what you
;; want to do with those files, and then later, in another Dired buffer,
;; hit `C-y' to invoke `filet'.  It remembers the file names that you've
;; copied (even if you copy other things in between), and lets you copy,
;; move or link those files to the current Dired directory.
;;
;; Initial development of this library was inspired by this discussion:
;; https://lists.gnu.org/archive/html/emacs-devel/2024-10/msg00478.html

;;; Code:

(require 'transient)
(require 'dired-aux)

(defun filet-short-name (file)
  "Return a short name for FILE."
  (let ((rel (file-relative-name file))
        (abr (abbreviate-file-name (expand-file-name file))))
    (if (< (string-width rel) (string-width abr)) rel abr)))

(defun filet-read-files (prompt initial-input history)
  "Prompt with PROMPT for multiple file names.

INITIAL-INPUT is the minibuffer initial input, and HISTORY is the
minibuffer history variable to use."
  (mapcar #'filet-short-name
          (completing-read-multiple
           prompt #'completion-file-name-table nil t initial-input history)))

(defun filet-create-files (file-creator op-name files directory marker-char)
  "Call FILE-CREATOR for each file in FILES to create files in DIRECTORY.

OP-NAME is a string describing the operation, such as \"Copy\".
MARKER-CHAR is a character to mark created files with."
  (dired-create-files
   file-creator op-name files
   (lambda (file) (expand-file-name (file-name-nondirectory file) directory))
   marker-char))

(defun filet-args (op-name)
  "Return (FILES DIRECTORY) argument list for `filet' commands.

OP-NAME is a string describing the current operation, such as \"Copy\"."
  (let ((files nil) (directory nil))
    (dolist (arg (transient-args 'filet))
      (cond
       ((and (consp arg) (equal (car arg) "--files"))
        (setq files (mapcar #'expand-file-name (cdr arg))))
       ((string-match "--directory=\\(.+\\)" arg)
        (setq directory (expand-file-name (match-string 1 arg))))))
    (unless files
      (setq files
            (mapcar #'expand-file-name
                    (completing-read-multiple
                     (format "%s files: " op-name)
                     #'completion-file-name-table
                     nil t nil 'file-name-history))))
    (unless directory
      (setq directory
            (expand-file-name
             (read-directory-name (format "%s files to: " op-name)))))
    (list files directory)))

(defun filet-copy (files directory)
  "Copy FILES to DIRECTORY."
  (interactive (filet-args "Copy"))
  (filet-create-files
   #'copy-file "Copy" files directory dired-keep-marker-copy))

(defun filet-move (files directory)
  "Move FILES to DIRECTORY."
  (interactive (filet-args "Move"))
  (filet-create-files
   #'rename-file "Move" files directory dired-keep-marker-rename))

(defun filet-link (files directory)
  "Create symlinks to FILES in DIRECTORY."
  (interactive (filet-args "Link"))
  (filet-create-files
   #'make-symbolic-link "Link" files directory dired-keep-marker-symlink))

(defun filet-make-relative-symbolic-link (file new ok-flag)
  "Create a relative symbolic link to FILE at NEW.

OK-FLAG says whether it is OK to override an existing NEW file."
  (make-symbolic-link
   (file-relative-name file (file-name-directory new)) new ok-flag))

(defun filet-relk (files directory)
  "Create relative symlinks to FILES in DIRECTORY."
  (interactive (filet-args "Relk"))
  (filet-create-files
   #'filet-make-relative-symbolic-link "Relk" files directory
   dired-keep-marker-relsymlink))

;;;###autoload
(transient-define-prefix filet ()
  "Operate on file names in your `kill-ring'.

Use `0 w' in a Dired buffer to grab some absolute file names, then later
invoke this command in another Dired buffer to copy/move/link those
files to that directory."
  [ :description (lambda () (concat "In directory "
                                    (abbreviate-file-name
                                     (expand-file-name default-directory))))
    ("-f" "Files" "--files"
     :class transient-option :multi-value rest :reader filet-read-files
     ;; Align value with the next line.
     :format " %k %d     (%v)")
    ("-d" "Directory" "--directory=")]
  ["Actions"
   [("c" "Copy" filet-copy)]
   [("m" "Move" filet-move)]
   [("l" "Link" filet-link)]
   [("r" "Relk" filet-relk)]]
  (interactive)
  (transient-setup
   'filet nil nil :value
   (append
    (let ((files nil) (ring kill-ring))
      (while (and ring (not files))
        (setq files (ignore-errors (split-string-and-unquote (car ring) "[\n\t ]+"))
              ring  (cdr ring))
        (unless (seq-every-p #'file-name-absolute-p files)
          (setq files nil)))
      (when files
        (list (cons "--files" (mapcar #'filet-short-name files)))))
    (list (concat "--directory="
                  (filet-short-name (if (derived-mode-p 'dired-mode)
                                        (dired-current-directory)
                                      default-directory)))))))

(provide 'filet)
;;; filet.el ends here
