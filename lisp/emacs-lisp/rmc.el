;;; rmc.el --- read from a multiple choice question -*- lexical-binding: t -*-

;; Copyright (C) 2016-2025 Free Software Foundation, Inc.

;; Maintainer: emacs-devel@gnu.org

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defgroup read-multiple-choice nil
  "Customizations for `read-multiple-choice'."
  :group 'minibuffer)

(defun rmc-format-key-label (key &optional label)
  "Format KEY with optional LABEL for display as part of a prompt."
  (let ((pos (seq-position label key))
        (desc (key-description (vector key))))
    (if (or (not pos) (member desc '("ESC" "TAB" "RET" "DEL" "SPC")))
        ;; Not in the label string, or a special character.
        (format "%s %s" (propertize desc 'face 'read-multiple-choice) label)
      (setq label (copy-sequence label))
      (put-text-property pos (1+ pos) 'face 'read-multiple-choice label)
      label)))

(defun rmc--add-key-description (elem)
  (let ((char (car elem)))
    (cons char (rmc-format-key-label char (cadr elem)))))

(defun rmc-key-description (key name)
  (let ((pos (seq-position name key))
        (desc (key-description (vector key))))
    (if (or (not pos) (member desc '("ESC" "TAB" "RET" "DEL" "SPC")))
        ;; Not in the name string, or a special character.
        (format "%s %s" (propertize desc 'face 'read-multiple-choice) name)
      (setq name (copy-sequence name))
      (put-text-property pos (1+ pos) 'face 'read-multiple-choice name)
      name)))

(defcustom read-multiple-choice-help-buffer-name "*Multiple Choice Help*"
  "Name of the buffer showing help during `read-multiple-choice'."
  :type 'string
  :version "30.1")

(defun rmc--show-help (prompt help-string show-help choices altered-names)
  (let* ((buf-name (if (stringp show-help)
                       show-help
                     read-multiple-choice-help-buffer-name))
         (buf (get-buffer-create buf-name)))
    (if (stringp help-string)
        (with-help-window buf
          (with-current-buffer buf
            (insert help-string)))
      (with-help-window buf
        (with-current-buffer buf
          (erase-buffer)
          (pop-to-buffer buf)
          (insert prompt "\n\n")
          (let* ((columns (/ (window-width) 25))
                 (fill-column 21)
                 (times 0)
                 (start (point)))
            (dolist (elem choices)
              (goto-char start)
              (unless (zerop times)
                (if (zerop (mod times columns))
                    ;; Go to the next "line".
                    (goto-char (setq start (point-max)))
                  ;; Add padding.
                  (while (not (eobp))
                    (end-of-line)
                    (insert (make-string (max (- (* (mod times columns)
                                                    (+ fill-column 4))
                                                 (current-column))
                                              0)
                                         ?\s))
                    (forward-line 1))))
              (setq times (1+ times))
              (let ((text
                     (with-temp-buffer
                       (insert (format
                                "%c: %s\n"
                                (car elem)
                                (cdr (assq (car elem) altered-names))))
                       (fill-region (point-min) (point-max))
                       (when (nth 2 elem)
                         (let ((start (point)))
                           (insert (nth 2 elem))
                           (unless (bolp)
                             (insert "\n"))
                           (fill-region start (point-max))))
                       (buffer-string))))
                (goto-char start)
                (dolist (line (split-string text "\n"))
                  (end-of-line)
                  (if (not (bolp))
		      (insert line)
		    (insert (make-string
                             (max (- (* (mod (1- times) columns)
                                        (+ fill-column 4))
                                     (current-column))
                                  0)
			     ?\s))
                    (insert line "\n"))
                  (forward-line 1))))))))
    buf))

(defvar read-multiple-choice-assign-key-function
  #'read-multiple-choice-assign-key-default
  "Default function to use for assigning keys to choices.")

(defun read-multiple-choice-assign-key-default (name choices)
  "Assign the first key in NAME that is free in CHOICES, or another single key."
  (seq-find (lambda (c) (not (seq-some (lambda (elem)
                                         (and (characterp (car elem))
                                              (= c (car elem))))
                                       choices)))
            (concat name
                    "abcdefghijklmnpqrstuvwxyz"
                    "ABCDEFGHIJKLMNPQRSTUVWXYZ"
                    "01234567890"
                    " -=")))

;;;###autoload
(defun read-multiple-choice (prompt choices &optional help-string show-help
                                    long-form)
  "Ask user to select an entry from CHOICES, prompting with PROMPT.
This function is used to ask the user a question with multiple
choices.

CHOICES should be a list of the form (KEY NAME [DESCRIPTION]).  KEY is a
character the user should type to select the entry.  NAME is a short
name for the entry to be displayed while prompting (if there's no room,
it might be shortened).  Alternatively, KEY can be a function that
computes the character for the entry dynamically.  Such a key assignment
function is called with two arguments, NAME and CHOICES, and should
return either a character or nil to signal failure.  KEY can also be
nil, which says to use `read-multiple-choice-assign-key-function' as the
key assignment function for this entry.  DESCRIPTION is an optional
longer description of the entry; it will be displayed in a help buffer
if the user requests more help.  This help description has a fixed
format in columns.  For greater flexibility, instead of passing a
DESCRIPTION, the caller can pass the optional argument HELP-STRING.
This argument is a string that should contain a more detailed
description of all of the possible choices.  `read-multiple-choice' will
display that description in a help buffer if the user requests that.  If
optional argument SHOW-HELP is non-nil, show the help screen
immediately, before any user input.  If SHOW-HELP is a string, use it as
the name of the help buffer.

This function translates user input into responses by consulting
the bindings in `query-replace-map'; see the documentation of
that variable for more information.  The relevant bindings for the
purposes of this function are `recenter', `scroll-up', `scroll-down',
and `edit'.
If the user types the `recenter', `scroll-up', or `scroll-down'
responses, the function performs the requested window recentering or
scrolling, and then asks the question again.  If the user enters `edit',
the function starts a recursive edit.  When the user exit the recursive
edit, the multiple-choice prompt gains focus again.

When there are just a few CHOICES, `use-dialog-box' is t (the default),
and the command using this function was invoked via the mouse, this
function pops up a GUI dialog to collect the user input, but only if
Emacs is capable of using GUI dialogs.  Otherwise, the function will
always use text-mode dialogs.

The return value is the matching entry from the CHOICES list.

If LONG-FORM is non-nil, do a `completing-read' over the NAME elements
in CHOICES instead.  In this case, GUI dialog is not used, regardless
of the value of `use-dialog-box' and whether the function was invoked
via a mouse gesture.

Usage example:

\(read-multiple-choice \"Continue connecting?\"
                      \\='((?a \"always\")
                        (?s \"session only\")
                        (?n \"no\")))"
  (when inhibit-interaction (signal 'inhibited-interaction nil))
  (while-let ((cell (seq-find (lambda (elem)
                                (not (characterp (car elem))))
                              choices)))
    (setcar cell (funcall (or (car cell)
                              read-multiple-choice-assign-key-function)
                          (cadr cell) choices))
    (unless (car cell)
      (error "Failed to assign a key to choice \"%s\"" (cadr cell))))
  (if long-form
      (read-multiple-choice--long-answers prompt choices)
    (read-multiple-choice--short-answers
     prompt choices help-string show-help)))

(defface read-multiple-choice-prompt
  '((t :inherit minibuffer-prompt))
  "Face for highlighting the `read-multiple-choice' prompt."
  :version "30.1")

(defun rmc--format-prompt-one-line (prompt options)
  (format "%s %s"
          (propertize prompt 'face 'read-multiple-choice-prompt)
          (mapconcat #'identity options
                     (propertize " | " 'face 'shadow))))

(defun rmc--format-prompt-multi-line (prompt options)
  (let* ((col-width (seq-max (mapcar #'string-width options)))
         (total-width (frame-width))
         (num-colms (/ total-width (+ col-width 3)))
         (result (propertize prompt 'face 'read-multiple-choice-prompt))
         (column 0))
    (dolist (option options)
      (setq result (concat
                    result
                    (if (zerop column) "\n" (propertize " | " 'face 'shadow))
                    option
                    (make-string (- col-width (string-width option)) ?\s))
            column (mod (1+ column) num-colms)))
    result))

(defun rmc--format-prompt (prompt options)
  (let ((one-line (rmc--format-prompt-one-line prompt options)))
    (if (< (frame-text-width) (string-pixel-width one-line))
        (rmc--format-prompt-multi-line prompt options)
      one-line)))

(defun read-multiple-choice--short-answers (prompt choices help-string show-help)
  (let* ((altered-names (mapcar #'rmc--add-key-description choices))
         (full-prompt (rmc--format-prompt prompt (mapcar #'cdr altered-names)))
         tchar buf result invalid-choice)
    (when show-help (setq buf (rmc--show-help prompt help-string show-help
                                              choices altered-names)))
    (unwind-protect
        (if (and (use-dialog-box-p)
                 ;; Guard against "Too many dialog items" errors.
                 (< (length choices) 9))
            (x-popup-dialog
             t
             (cons prompt
                   (mapcar
                    (lambda (elem)
                      (cons (capitalize (cadr elem)) elem))
                    choices)))
          (while (not result)
            (setq invalid-choice nil)
            (when tchar
              (pcase (lookup-key query-replace-map (vector tchar) t)
                ('help
                 (setq buf (rmc--show-help prompt help-string show-help
                                           choices altered-names)))
                ('recenter
                 (let ((this-command 'recenter-top-bottom)
		       (last-command 'recenter-top-bottom))
	           (recenter-top-bottom)))
                ('edit
                 (save-match-data
                   (save-excursion
                     (save-window-excursion
                       (message
                        (substitute-command-keys
                         "Recursive edit; \\[exit-recursive-edit] to resume"))
                       (recursive-edit)))))
                ('scroll-up
                 (ignore-errors (scroll-up-command)))
                ('scroll-down
                 (ignore-errors (scroll-down-command)))
                ('scroll-other-window
                 (ignore-errors (scroll-other-window)))
                ('scroll-other-window-down
                 (ignore-errors (scroll-other-window-down)))
                (_                      ; Invalid input.
                 (ding)
                 (setq invalid-choice
                       (concat "Invalid choice "
                               (propertize (key-description (vector tchar))
                                           'face 'read-multiple-choice)
                               ", choose one of the following ("
                               (propertize "C-h" 'face 'read-multiple-choice)
                               " for help, "
                               (propertize "C-g" 'face 'read-multiple-choice)
                               " to quit, "
                               (propertize "C-r" 'face 'read-multiple-choice)
                               " to pause):\n")))))
            (message (concat invalid-choice full-prompt))
            (setq tchar
                  (condition-case nil
                      (let ((cursor-in-echo-area t) (key (read-key)))
                        (when (eq key ?\C-g) (signal 'quit nil))
                        key)
                    (error nil)))
            (setq result (assq tchar choices)))
          result)
      (when-let ((win (and buf (get-buffer-window buf)))) (quit-window nil win)))))

(defun read-multiple-choice--long-answers (prompt choices)
  (let* ((cands
          (mapcar (lambda (elem)
                    (concat (propertize (key-description (char-to-string
                                                          (car elem)))
                                        'face 'read-multiple-choice)
                            " " (cadr elem)))
                  choices))
         (answer
          (completing-read
           (format-prompt prompt cands)
           (completion-table-with-metadata
            cands
            `((category . multiple-choice)
              (eager-display . t)
              (affixation-function
               . ,(lambda (cands)
                    (let ((max (1+ (seq-max (mapcar #'string-width cands)))))
                      (mapcar
                       (lambda (cand)
                         (list cand ""
                               (when-let ((desc (caddr (assq (aref cand 0)
                                                             choices))))
                                 (concat
                                  (make-string (- max (string-width cand))
                                               ?\s)
                                  (propertize desc 'face
                                              'completions-annotations)))))
                       cands))))))
           nil t nil nil cands)))
    (unless (string-empty-p answer) (assq (aref answer 0) choices))))

(provide 'rmc)

;;; rmc.el ends here
