;;; scope.el --- Semantic analysis for ELisp symbols  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords: lisp, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library implements an analysis that determines the role of each
;; symbol in ELisp code.  The entry point for the analysis is the
;; function `scope', see its docstring for usage information.

;;; Code:

(defvar scope-counter nil)

(defvar scope-local-functions nil)

(defvar scope--local nil)

(defvar scope-callback #'ignore)

(defvar scope-current-let-alist-form nil)

(defvar scope-gen-id-alist nil)

(defsubst scope-local-new (sym pos &optional local)
  "Return new local context with SYM bound at POS.

Optional argument LOCAL is a local context to extend."
  (cons (cons sym (or pos (cons 'gen (incf scope-counter)))) local))

(defsubst scope-sym-pos (sym)
  (when (symbol-with-pos-p sym) (symbol-with-pos-pos sym)))

(defsubst scope-sym-bare (sym)
  (cond
   ((symbolp sym) sym)
   ((symbol-with-pos-p sym) (bare-symbol sym))))

(defsubst scope-report (type beg len &optional id def)
  (funcall scope-callback type beg len id (or def (and (numberp id) id))))

(defun scope-s (local sym)
  (let* ((beg (scope-sym-pos sym))
         (bare (bare-symbol sym))
         (name (symbol-name bare))
         (len (length name))
         (scope--local local))
    (when (and beg (not (booleanp bare)))
      (cond
       ((keywordp bare) (scope-report 'constant beg len))
       ((and scope-current-let-alist-form (= (aref name 0) ?.))
        (if (and (length> name 1) (= (aref name 1) ?.))
            ;; Double dot espaces `let-alist'.
            (let* ((unescaped (intern (substring name 1)))
                   (id (alist-get unescaped local)))
              (scope-report 'variable beg len id))
          (scope-report 'variable beg len
                        (list 'let-alist (car scope-current-let-alist-form) bare)
                        (cdr scope-current-let-alist-form))))
       (t
        (let ((id (alist-get bare local)))
          (scope-report 'variable beg len id)))))))

(defun scope-let-1 (local0 local bindings body)
  (if bindings
      (let* ((binding (ensure-list (car bindings)))
             (sym (car binding))
             (bare (bare-symbol sym))
             (len (length (symbol-name bare)))
             (beg (scope-sym-pos sym)))
        (when beg (scope-report 'variable beg len beg))
        (scope-1 local0 (cadr binding))
        (scope-let-1 local0 (scope-local-new bare beg local)
                     (cdr bindings) body))
    (scope-n local body)))

(defun scope-let (local bindings body)
  (scope-let-1 local local bindings body))

(defun scope-let* (local bindings body)
  (if bindings
      (let* ((binding (ensure-list (car bindings)))
             (sym (car binding))
             (bare (bare-symbol sym))
             (len (length (symbol-name bare)))
             (beg (scope-sym-pos sym)))
        (when beg (scope-report 'variable beg len beg))
        (scope-1 local (cadr binding))
        (scope-let*
         (scope-local-new bare beg local) (cdr bindings) body))
    (scope-n local body)))

(defun scope-interactive (local intr spec modes)
  (when (symbol-with-pos-p intr)
    (scope-report 'special-form
                  (symbol-with-pos-pos intr)
                  (length (symbol-name (scope-sym-bare intr)))))
  (scope-1 local spec)
  (mapc #'scope-major-mode-name modes))

(defun scope-lambda (local args body)
  "Analyze (lambda ARGS BODY) function definition in LOCAL context."
  (let ((l local))
    (dolist (arg args)
      (when-let ((bare (bare-symbol arg))
                 (beg (scope-sym-pos arg)))
        (unless (memq bare '(&optional &rest))
          (setq l (scope-local-new bare beg l)))))
    ;; Handle docstring.
    (cond
     ((and (consp (car body))
           (or (symbol-with-pos-p (caar body))
               (symbolp (caar body)))
           (eq (bare-symbol (caar body)) :documentation))
      (scope-s local (caar body))
      (scope-1 local (cadar body))
      (setq body (cdr body)))
     ((stringp (car body)) (setq body (cdr body))))
    ;; Handle `declare'.
    (when-let ((form (car body))
               (decl (car-safe form))
               ((or (symbol-with-pos-p decl)
                    (symbolp decl)))
               ((eq (bare-symbol decl) 'declare)))
      (when (symbol-with-pos-p decl)
        (scope-report 'macro
                      (symbol-with-pos-pos decl)
                      (length (symbol-name (bare-symbol decl)))))
      (dolist (spec (cdr form))
        (when-let ((head (car-safe spec))
                   (bare (scope-sym-bare head)))
          (when (symbol-with-pos-p head)
            (scope-report 'declaration
                          (symbol-with-pos-pos head)
                          (length (symbol-name bare))))
          (case bare
            (completion (scope-sharpquote local (cadr spec)))
            (interactive-only
             (when-let ((bare (scope-sym-bare (cadr spec)))
                        ((not (eq bare t))))
               (scope-sharpquote local (cadr spec))))
            (obsolete
             (when-let ((bare (scope-sym-bare (cadr spec))))
               (scope-sharpquote local (cadr spec))))
            ((compiler-macro gv-expander gv-setter)
             ;; Use the extended lexical environment `l'.
             (scope-sharpquote l (cadr spec)))
            (modes (mapc #'scope-major-mode-name (cdr spec)))
            (interactive-args
             (dolist (arg-form (cdr spec))
               (when-let ((arg (car-safe arg-form)))
                 (scope-s l arg)
                 (when (consp (cdr arg-form))
                   (scope-1 local (cadr arg-form)))))))))
      (setq body (cdr body)))
    ;; Handle `interactive'.
    (when-let ((form (car body))
               (intr (car-safe form))
               ((or (symbol-with-pos-p intr)
                    (symbolp intr)))
               ((eq (bare-symbol intr) 'interactive)))
      (scope-interactive local intr (cadar body) (cddar body))
      (setq body (cdr body)))
    ;; Handle ARGS.
    (dolist (arg args)
      (and (symbol-with-pos-p arg)
           (let* ((beg (symbol-with-pos-pos arg))
                  (bare (bare-symbol arg))
                  (len (length (symbol-name bare))))
             (when (and beg (not (eq bare '_)))
               (if (memq bare '(&optional &rest))
                   (scope-report 'ampersand beg len)
                 (scope-report 'variable beg len beg))))))
    ;; Handle BODY.
    (let ((l local))
      (dolist (arg args)
        (when-let ((bare (bare-symbol arg))
                   (beg (scope-sym-pos arg)))
          (unless (memq bare '(&optional &rest))
            (setq l (scope-local-new bare beg l)))))
      (scope-n l body))))

(defun scope-defun (local name args body)
  (when-let ((beg (scope-sym-pos name))
             (bare (scope-sym-bare name)))
    (scope-report 'defun beg (length (symbol-name bare))))
  (scope-lambda local args body))

(defun scope-cond (local clauses)
  (dolist (clause clauses) (scope-n local clause)))

(defun scope-setq (local args)
  (let ((var nil) (val nil))
    (while args
      (setq var  (car  args)
            val  (cadr args)
            args (cddr args))
      (scope-s local var)
      (scope-1 local val))))

(defun scope-defvar (local name init)
  (when-let ((beg (scope-sym-pos name))
             (bare (scope-sym-bare name)))
    (scope-report 'defvar beg (length (symbol-name bare))))
  (scope-1 local init))

(defun scope-condition-case (local var bodyform handlers)
  (let* ((bare (bare-symbol var))
         (beg (when (symbol-with-pos-p var) (symbol-with-pos-pos var)))
         (l (scope-local-new bare beg local)))
    (when beg
      (scope-report 'variable beg (length (symbol-name bare)) beg))
    (scope-1 local bodyform)
    (dolist (handler handlers)
      (dolist (cond-name (ensure-list (car-safe handler)))
        (when-let* ((cbeg (scope-sym-pos cond-name))
                    (cbare (scope-sym-bare cond-name))
                    (clen (length (symbol-name cbare))))
          (cond
           ((booleanp cbare))
           ((keywordp cbare) (scope-report 'constant cbeg clen))
           (t                (scope-report 'condition cbeg clen)))))
      (scope-n l (cdr handler)))))

(defvar scope-flet-alist nil)

(defun scope-flet (local defs body)
  (if defs
      (let* ((def (car defs))
             (func (car def))
             (exps (cdr def))
             (beg (scope-sym-pos func))
             (bare (bare-symbol func)))
        (when beg
          (scope-report 'function beg (length (symbol-name bare)) beg))
        (if (cdr exps)
            ;; def is (FUNC ARGLIST BODY...)
            (scope-lambda local (car exps) (cdr exps))
          ;; def is (FUNC EXP)
          (scope-1 local (car exps)))
        (let ((scope-flet-alist (scope-local-new bare beg scope-flet-alist)))
          (scope-flet local (cdr defs) body)))
    (scope-n local body)))

(defun scope-labels (local defs forms)
  (if defs
      (let* ((def (car defs))
             (func (car def))
             (args (cadr def))
             (body (cddr def))
             (beg (scope-sym-pos func))
             (bare (bare-symbol func)))
        (when beg
          (scope-report 'function beg (length (symbol-name bare)) beg))
        (let ((scope-flet-alist (scope-local-new bare beg scope-flet-alist)))
          (scope-lambda local args body)
          (scope-flet local (cdr defs) forms)))
    (scope-n local forms)))

(defvar scope-block-alist nil)

(defun scope-block (local name body)
  (if name
      (let* ((beg (scope-sym-pos name))
             (bare (bare-symbol name)))
        (when beg
          (scope-report 'block beg (length (symbol-name bare)) beg))
        (let ((scope-block-alist (scope-local-new bare beg scope-block-alist)))
          (scope-n local body)))
    (scope-n local body)))

(defun scope-return-from (local name result)
  (when-let ((bare (and (symbol-with-pos-p name) (bare-symbol name)))
             (pos (alist-get bare scope-block-alist)))
    (scope-report 'block
                  (symbol-with-pos-pos name) (length (symbol-name bare)) pos))
  (scope-1 local result))

(defvar scope-assume-func-p nil)

(defun scope-sharpquote (local arg)
  (cond
   ((or (symbol-with-pos-p arg) (symbolp arg))
    (let ((bare (bare-symbol arg))
          (beg (scope-sym-pos arg)))
      (cond
       ((or (functionp bare) scope-assume-func-p)
        (when beg
          (scope-report 'function beg (length (symbol-name bare)))))
       ((or (assq bare scope-flet-alist) (consp arg))
        (scope-1 local arg)))))
   ((consp arg) (scope-1 local arg))))

(defun scope-declare-function (local fn _file arglist _fileonly)
  (scope-defun local fn arglist nil))

(defun scope-loop-for-and (local rest)
  (if (eq (scope-sym-bare (car rest)) 'and)
      (scope-loop-for local local (cadr rest) (cddr rest))
    (scope-loop local rest)))

(defun scope-loop-for-by (local0 local expr rest)
  (scope-1 local0 expr)
  (scope-loop-for-and local rest))

(defun scope-loop-for-to (local0 local expr rest)
  (scope-1 local0 expr)
  (when-let ((bare (scope-sym-bare (car rest)))
             (more (cdr rest)))
    (cond
     ((eq bare 'by)
      (scope-loop-for-by local0 local (car more) (cdr more)))
     (t (scope-loop-for-and local rest)))))

(defun scope-loop-for-from (local0 local expr rest)
  (scope-1 local0 expr)
  (when-let ((bare (scope-sym-bare (car rest)))
             (more (cdr rest)))
    (cond
     ((memq bare '(to upto downto below above))
      (scope-loop-for-to local0 local (car more) (cdr more)))
     ((eq bare 'by)
      (scope-loop-for-by local0 local (car more) (cdr more)))
     (t (scope-loop-for-and local rest)))))

(defun scope-loop-for-= (local0 local expr rest)
  (scope-1 local0 expr)
  (when-let ((bare (scope-sym-bare (car rest)))
             (more (cdr rest)))
    (cond
     ((eq bare 'then)
      (scope-loop-for-by local0 local (car more) (cdr more)))
     (t (scope-loop-for-and local rest)))))

(defun scope-loop-for-being-the-hash-keys-of-using (local form rest)
  (let* ((var (cadr form))
         (bare (scope-sym-bare var))
         (beg (scope-sym-pos var)))
    (when beg
      (scope-report 'variable beg (length (symbol-name bare)) beg))
    (scope-loop-for-and (scope-local-new bare beg local) rest)))

(defun scope-loop-for-being-the-hash-keys-of (local0 local expr rest)
  (scope-1 local0 expr)
  (when-let ((bare (scope-sym-bare (car rest)))
             (more (cdr rest)))
    (cond
     ((eq bare 'using)
      (scope-loop-for-being-the-hash-keys-of-using local (car more) (cdr more)))
     (t (scope-loop-for-and local rest)))))

(defun scope-loop-for-being-the-hash-keys (local0 local word rest)
  (when-let ((bare (scope-sym-bare word)))
    (cond
     ((eq bare 'of)
      (scope-loop-for-being-the-hash-keys-of local0 local (car rest) (cdr rest))))))

(defun scope-loop-for-being-the (local0 local word rest)
  (when-let ((bare (scope-sym-bare word)))
    (cond
     ((memq bare '(buffer buffers))
      (scope-loop-for-and local rest))
     ((memq bare '( hash-key hash-keys
                    hash-value hash-values
                    key-code key-codes
                    key-binding key-bindings))
      (scope-loop-for-being-the-hash-keys local0 local (car rest) (cdr rest))))))

(defun scope-loop-for-being (local0 local next rest)
  (scope-loop-for-being-the
   local0 local (car rest)
   (if (memq (scope-sym-bare next) '(the each)) (cdr rest) rest)))

(defun scope-loop-for (local0 local vars rest)
  (if vars
      (let* ((var (car (ensure-list vars)))
             (bare (bare-symbol var))
             (beg (scope-sym-pos var)))
        (when beg
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (scope-loop-for local0 (scope-local-new bare beg local) (cdr-safe vars) rest))
    (when-let ((bare (scope-sym-bare (car rest)))
               (more (cdr rest)))
      (cond
       ((memq bare '(from upfrom downfrom))
        (scope-loop-for-from local0 local (car more) (cdr more)))
       ((memq bare '( to upto downto below above
                      in on in-ref))
        (scope-loop-for-to local0 local (car more) (cdr more)))
       ((memq bare '(by
                     across across-ref))
        (scope-loop-for-by local0 local (car more) (cdr more)))
       ((eq bare '=)
        (scope-loop-for-= local0 local (car more) (cdr more)))
       ((eq bare 'being)
        (scope-loop-for-being local0 local (car more) (cdr more)))))))

(defun scope-loop-repeat (local form rest)
  (scope-1 local form)
  (scope-loop local rest))

(defvar scope-loop-into-vars nil)

(defun scope-loop-collect (local expr rest)
  (scope-1 local expr)
  (let ((bw (scope-sym-bare (car rest)))
        (more (cdr rest)))
    (if (eq bw 'into)
        (let* ((var (car more))
               (bare (scope-sym-bare var))
               (beg (scope-sym-pos var)))
          (if (memq bare scope-loop-into-vars)
              (progn
                (scope-s local var)
                (scope-loop local (cdr more)))
            (when beg
              (scope-report 'variable
                            beg (length (symbol-name bare)) beg))
            (let ((scope-loop-into-vars (cons bare scope-loop-into-vars)))
              (scope-loop (scope-local-new bare beg local) (cdr more)))))
      (scope-loop local rest))))

(defun scope-loop-with-and (local rest)
  (if (eq (scope-sym-bare (car rest)) 'and)
      (scope-loop-with local (cadr rest) (cddr rest))
    (scope-loop local rest)))

(defun scope-loop-with (local var rest)
  (let* ((bare (scope-sym-bare var))
         (beg (symbol-with-pos-pos var))
         (l (scope-local-new bare beg local))
         (eql (car rest)))
    (when beg
      (scope-report 'variable beg (length (symbol-name bare)) beg))
    (if (eq (scope-sym-bare eql) '=)
        (let* ((val (cadr rest)) (more (cddr rest)))
          (scope-1 local val)
          (scope-loop-with-and l more))
      (scope-loop-with-and l rest))))

(defun scope-loop-do (local form rest)
  (scope-1 local form)
  (if (consp (car rest))
      (scope-loop-do local (car rest) (cdr rest))
    (scope-loop local rest)))

(defun scope-loop-named (local name rest)
  (let* ((beg (scope-sym-pos name))
         (bare (scope-sym-bare name)))
    (when beg
      (scope-report 'block beg (length (symbol-name bare)) beg))
    (let ((scope-block-alist (scope-local-new bare beg scope-block-alist)))
      (scope-loop local rest))))

(defun scope-loop-finally (local next rest)
  (if-let ((bare (scope-sym-bare next)))
      (cond
       ((eq bare 'do)
        (scope-loop-do local (car rest) (cdr rest)))
       ((eq bare 'return)
        (scope-1 local (car rest))
        (scope-loop local (cdr rest))))
    (if (eq (scope-sym-bare (car-safe next)) 'return)
        (progn
          (scope-1 local (cadr next))
          (scope-loop local (cdr rest)))
      (scope-loop-do local next rest))))

(defun scope-loop-initially (local next rest)
  (if (eq (scope-sym-bare next) 'do)
      (scope-loop-do local (car rest) (cdr rest))
    (scope-loop-do local next rest)))

(defvar scope-loop-if-depth 0)

(defun scope-loop-if (local keyword condition rest)
  (scope-1 local condition)
  (let ((scope-loop-if-depth (1+ scope-loop-if-depth)))
    (scope-loop
     ;; `if' binds `it'.
     (scope-local-new 'it (scope-sym-pos keyword) local)
     rest)))

(defun scope-loop-end (local rest)
  (let ((scope-loop-if-depth (1- scope-loop-if-depth)))
    (unless (minusp scope-loop-if-depth)
      (scope-loop local rest))))

(defun scope-loop-and (local rest)
  (when (plusp scope-loop-if-depth) (scope-loop local rest)))

(defun scope-loop (local forms)
  (when forms
    (let* ((kw (car forms))
           (bare (scope-sym-bare kw))
           (rest (cdr forms)))
      (cond
       ((memq bare '(for as))
        (scope-loop-for local local (car rest) (cdr rest)))
       ((memq bare '( repeat while until always never thereis iter-by
                      return))
        (scope-loop-repeat local (car rest) (cdr rest)))
       ((memq bare '(collect append nconc concat vconcat count sum maximize minimize))
        (scope-loop-collect local (car rest) (cdr rest)))
       ((memq bare '(with))
        (scope-loop-with local (car rest) (cdr rest)))
       ((memq bare '(do)) (scope-loop-do local (car rest) (cdr rest)))
       ((memq bare '(named)) (scope-loop-named local (car rest) (cdr rest)))
       ((memq bare '(finally)) (scope-loop-finally local (car rest) (cdr rest)))
       ((memq bare '(initially)) (scope-loop-initially local (car rest) (cdr rest)))
       ((memq bare '(if when unless)) (scope-loop-if local kw (car rest) (cdr rest)))
       ((memq bare '(end)) (scope-loop-end local rest))
       ((memq bare '(and else)) (scope-loop-and local rest))))))

(defun scope-named-let (local name bindings body)
  (let ((bare (scope-sym-bare name))
        (beg (scope-sym-pos name)))
    (when beg
      (scope-report 'function beg (length (symbol-name bare)) beg))
    (dolist (binding bindings)
      (let* ((sym (car (ensure-list binding)))
             (beg (symbol-with-pos-pos sym))
             (bare (bare-symbol sym)))
        (when beg
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (scope-1 local (cadr binding))))
    (let ((l local))
      (dolist (binding bindings)
        (when-let ((sym (car (ensure-list binding)))
                   (bare (scope-sym-bare sym)))
          (setq l (scope-local-new bare (scope-sym-pos sym) l))))
      (let ((scope-flet-alist (scope-local-new bare beg scope-flet-alist)))
        (scope-n l body)))))

(defun scope-with-slots (local spec-list object body)
  (scope-1 local object)
  (scope-let local spec-list body))

(defun scope-rx (local regexps)
  (dolist (regexp regexps) (scope-rx-1 local regexp)))

(defvar scope-rx-alist nil)

(defun scope-rx-1 (local regexp)
  (if (consp regexp)
      (let* ((head (car regexp))
             (bare (scope-sym-bare head)))
        (when bare
          (scope-report 'rx-construct
                        (symbol-with-pos-pos head) (length (symbol-name bare))
                        (alist-get bare scope-rx-alist)))
        (cond
         ((memq bare '(literal regex regexp eval))
          (scope-1 local (cadr regexp)))
         ((memq bare '( seq sequence and :
                        or |
                        zero-or-more 0+ * *?
                        one-or-more 1+ + +?
                        zero-or-one optional opt \? \??
                        = >= ** repeat
                        minimal-match maximal-match
                        group submatch
                        group-n submatch-n))
          (scope-rx local (cdr regexp)))))
    (when-let ((bare (scope-sym-bare regexp)))
      (scope-report 'rx-construct
                    (symbol-with-pos-pos regexp) (length (symbol-name bare))
                    (alist-get bare scope-rx-alist)))))

(defun scope-rx-define (local name rest)
  (when-let ((bare (scope-sym-bare name)))
    (scope-report 'rx-construct
                  (symbol-with-pos-pos name) (length (symbol-name bare)) nil))
  (if (not (cdr rest))
      (scope-rx-1 local (car rest))
    (let ((l scope-rx-alist)
          (args (car rest))
          (rx (cadr rest)))
      (dolist (arg args)
        (and (symbol-with-pos-p arg)
             (let* ((beg (symbol-with-pos-pos arg))
                    (bare (bare-symbol arg))
                    (len (length (symbol-name bare))))
               (when beg
                 (if (memq (bare-symbol arg) '(&optional &rest _))
                     (scope-report 'ampersand beg len)
                   (scope-report 'rx-construct beg len beg))))))
      (dolist (arg args)
        (when-let ((bare (bare-symbol arg))
                   (beg (scope-sym-pos arg)))
          (unless (memq bare '(&optional &rest))
            (setq l (scope-local-new bare beg l)))))
      (let ((scope-rx-alist l))
        (scope-rx-1 local rx)))))

(defun scope-rx-let (local bindings body)
  (if-let ((binding (car bindings)))
      (let ((name (car binding)) (rest (cdr binding)))
        (when-let ((bare (scope-sym-bare name))
                   (beg (symbol-with-pos-pos name)))
          (scope-report 'rx-construct
                        beg (length (symbol-name bare)) beg))
        (if (cdr rest)
            (let ((l scope-rx-alist)
                  (args (car rest))
                  (rx (cadr rest)))
              (dolist (arg args)
                (and (symbol-with-pos-p arg)
                     (let* ((beg (symbol-with-pos-pos arg))
                            (bare (bare-symbol arg))
                            (len (length (symbol-name bare))))
                       (when beg
                         (if (memq (bare-symbol arg) '(&optional &rest _))
                             (scope-report 'ampersand beg len)
                           (scope-report 'rx-construct beg len beg))))))
              (dolist (arg args)
                (when-let ((bare (bare-symbol arg))
                           (beg (scope-sym-pos arg)))
                  (unless (memq bare '(&optional &rest))
                    (setq l (scope-local-new bare beg l)))))
              (let ((scope-rx-alist l))
                (scope-rx-1 local rx))
              (let ((scope-rx-alist (scope-local-new (scope-sym-bare name)
                                                     (scope-sym-pos name)
                                                     scope-rx-alist)))
                (scope-rx-let local (cdr bindings) body)))
          (scope-rx-1 local (car rest))
          (let ((scope-rx-alist (scope-local-new (scope-sym-bare name)
                                                 (scope-sym-pos name)
                                                 scope-rx-alist)))
            (scope-rx-let local (cdr bindings) body))))
    (scope-n local body)))

(defun scope-gv-define-expander (local name handler)
  (when-let* ((beg (scope-sym-pos name)) (bare (scope-sym-bare name)))
    (scope-report 'defun beg (length (symbol-name bare))))
  (scope-1 local handler))

(defun scope-gv-define-simple-setter (local name setter rest)
  (when-let* ((beg (scope-sym-pos name)) (bare (scope-sym-bare name)))
    (scope-report 'defun beg (length (symbol-name bare))))
  (when-let* ((beg (scope-sym-pos setter)) (bare (scope-sym-bare setter)))
    (scope-report 'function beg (length (symbol-name bare))))
  (scope-n local rest))

(defun scope-catch (local tag body)
  (when-let* (((memq (car-safe tag) '(quote \`)))
              (sym (cadr tag))
              (beg (scope-sym-pos sym))
              (bare (scope-sym-bare sym)))
    (scope-report 'throw-tag beg (length (symbol-name bare))))
  (scope-n local body))

(defun scope-face (face)
  (if (or (scope-sym-bare face)
          (keywordp (scope-sym-bare (car-safe face))))
      (scope-face-1 face)
    (mapc #'scope-face-1 face)))

(defun scope-face-1 (face)
  (cond
   ((symbol-with-pos-p face)
    (when-let ((beg (scope-sym-pos face)) (bare (scope-sym-bare face)))
      (scope-report 'face beg (length (symbol-name bare)))))
   ((keywordp (scope-sym-bare (car-safe face)))
    (let ((l face))
      (while l
        (let ((kw (car l))
              (vl (cadr l)))
          (setq l (cddr l))
          (when-let ((bare (scope-sym-bare kw))
                     ((keywordp bare)))
            (when-let ((beg (scope-sym-pos kw))
                       (len (length (symbol-name bare))))
              (scope-report 'constant beg len))
            (when (eq bare :inherit)
              (when-let ((beg (scope-sym-pos vl)) (fbare (scope-sym-bare vl)))
                (scope-report 'face beg (length (symbol-name fbare))))))))))))

(defun scope-deftype (local name args body)
  (when-let* ((beg (scope-sym-pos name)) (bare (scope-sym-bare name)))
    (scope-report 'type beg (length (symbol-name bare))))
  (scope-lambda local args body))

(defun scope-widget-type (_local form)
  (when-let (((memq (scope-sym-bare (car-safe form)) '(quote \`)))
             (type (cadr form)))
    (scope-widget-type-1 type)))

(defun scope-widget-type-1 (type)
  (cond
   ((symbol-with-pos-p type)
    (when-let* ((beg (scope-sym-pos type)) (bare (scope-sym-bare type)))
      (scope-report 'widget-type
                    (symbol-with-pos-pos type)
                    (length (symbol-name (bare-symbol type))))))
   ((consp type)
    (let ((head (car type)))
      (when-let ((beg (scope-sym-pos head)) (bare (scope-sym-bare head)))
        (scope-report 'widget-type beg (length (symbol-name bare))))
      (when-let ((bare (scope-sym-bare head)))
        (scope-widget-type-arguments bare (cdr type)))))))

(defun scope-widget-type-keyword-arguments (head kw args)
  (when-let ((beg (scope-sym-pos kw))
             (len (length (symbol-name (bare-symbol kw)))))
    (scope-report 'constant beg len))
  (cond
   ((and (memq head '(plist alist))
         (memq kw   '(:key-type :value-type)))
    (scope-widget-type-1 (car args)))
   ((memq kw '(:action :match :match-inline :validate))
    (when-let* ((fun (car args))
                (beg (scope-sym-pos fun))
                (bare (scope-sym-bare fun)))
      (scope-report 'function beg (length (symbol-name bare)))))
   ((memq kw '(:args))
    (mapc #'scope-widget-type-1 (car args))))
  ;; TODO: (restricted-sexp :match-alternatives CRITERIA)
  (scope-widget-type-arguments head (cdr args)))

(defun scope-widget-type-arguments (head args)
  (let* ((arg (car args))
         (bare (scope-sym-bare arg)))
    (if (keywordp bare)
        (scope-widget-type-keyword-arguments head bare (cdr args))
      (scope-widget-type-arguments-1 head args))))

(defun scope-widget-type-arguments-1 (head args)
  (case head
    ((list cons group vector choice radio set repeat checklist)
     (mapc #'scope-widget-type-1 args))
    ((function-item)
     (when-let* ((fun (car args))
                 (beg (scope-sym-pos fun))
                 (bare (scope-sym-bare fun)))
       (scope-report 'function beg (length (symbol-name bare)))))
    ((variable-item)
     (when-let* ((var (car args))
                 (beg (scope-sym-pos var))
                 (bare (scope-sym-bare var)))
       (scope-report 'variable beg (length (symbol-name bare)))))))

(defun scope-quoted-group (_local sym-form)
  (when-let* (((eq (scope-sym-bare (car-safe sym-form)) 'quote))
              (sym (cadr sym-form))
              (beg (scope-sym-pos sym))
              (bare (scope-sym-bare sym)))
    (scope-report 'group beg (length (symbol-name bare)))))

(defun scope-defmethod-1 (local0 local args body)
  (if args
      (let ((arg (car args)) (bare nil))
        (cond
         ((consp arg)
          (let* ((var (car arg))
                 (spec (cadr arg)))
            (cond
             ((setq bare (scope-sym-bare var))
              (when-let* ((beg (scope-sym-pos var))
                          (len (length (symbol-name bare))))
                (scope-report 'variable beg len beg))
              (cond
               ((consp spec)
                (let ((head (car spec)) (form (cadr spec)))
                  (and (eq 'eql (scope-sym-bare head))
                       (not (or (symbolp form) (symbol-with-pos-p form)))
                       (scope-1 local0 form))))
               ((symbol-with-pos-p spec)
                (when-let* ((beg (symbol-with-pos-pos spec))
                            (bare (bare-symbol spec))
                            (len (length (symbol-name bare))))
                  (scope-report 'type beg len))))
              (scope-defmethod-1
               local0 (scope-local-new bare (scope-sym-pos var) local)
               (cdr args) body)))))
         ((setq bare (scope-sym-bare arg))
          (cond
           ((memq bare '(&optional &rest &body _))
            (when-let ((beg (scope-sym-pos arg)))
              (scope-report 'ampersand beg (length (symbol-name bare))))
            (scope-defmethod-1 local0 local (cdr args) body))
           ((eq bare '&context)
            (let* ((expr-type (cadr args))
                   (expr (car expr-type))
                   (spec (cadr expr-type))
                   (more (cddr args)))
              (when-let ((beg (scope-sym-pos arg)))
                (scope-report 'ampersand beg (length (symbol-name bare))))
              (scope-1 local0 expr)
              (cond
               ((consp spec)
                (let ((head (car spec)) (form (cadr spec)))
                  (and (eq 'eql (scope-sym-bare head))
                       (not (or (symbolp form) (symbol-with-pos-p form)))
                       (scope-1 local0 form))))
               ((symbol-with-pos-p spec)
                (when-let* ((beg (symbol-with-pos-pos spec))
                            (bare (bare-symbol spec))
                            (len (length (symbol-name bare))))
                  (scope-report 'type beg len beg))))
              (scope-defmethod-1 local0 local more body)))
           (t
            (when-let* ((beg (scope-sym-pos arg))
                        (len (length (symbol-name bare))))
              (scope-report 'variable beg len beg))
            (scope-defmethod-1
             local0 (scope-local-new bare (scope-sym-pos arg) local)
             (cdr args) body))))))
    (scope-n local body)))

;; (defun scope-defmethod (local name rest)
;;   (when (and (symbol-with-pos-p (car rest))
;;              (eq (bare-symbol (car rest)) :extra))
;;     (setq rest (cddr rest)))
;;   (when (and (symbol-with-pos-p (car rest))
;;              (memq (bare-symbol (car rest)) '(:before :after :around)))
;;     (setq rest (cdr rest)))
;;   (scope-defmethod-1 local local name (car rest)
;;                      (if (stringp (cadr rest)) (cddr rest) (cdr rest))))

(defun scope-defmethod (local name rest)
  "Analyze method definition for NAME with args REST in LOCAL context."
  (when-let* ((beg (scope-sym-pos name)) (bare (scope-sym-bare name)))
    (scope-report 'defun beg (length (symbol-name bare))))
  ;; [EXTRA]
  (when (eq (scope-sym-bare (car rest)) :extra)
    (scope-s local (car rest))
    (setq rest (cddr rest)))
  ;; [QUALIFIER]
  (when (keywordp (scope-sym-bare (car rest)))
    (scope-s local (car rest))
    (setq rest (cdr rest)))
  ;; ARGUMENTS
  (scope-defmethod-1 local local (car rest) (cdr rest)))

(defun scope-cl-defun (local name arglist body)
  (when-let ((beg (scope-sym-pos name))
             (bare (scope-sym-bare name)))
    (scope-report 'defun beg (length (symbol-name bare))))
  (scope-cl-lambda local arglist body))

(defun scope-cl-lambda (local arglist body)
  (scope-cl-lambda-1 local arglist nil body))

(defun scope-cl-lambda-1 (local arglist more body)
  (cond
   (arglist
    (if (consp arglist)
        (let ((head (car arglist)))
          (if (consp head)
              (scope-cl-lambda-1 local head (cons (cdr arglist) more) body)
            (let ((bare (scope-sym-bare head)))
              (if (memq bare '(&optional &rest &body &key &aux &whole))
                  (progn
                    (when-let ((beg (scope-sym-pos head)))
                      (scope-report 'ampersand beg (length (symbol-name bare))))
                    (case bare
                      (&optional (scope-cl-lambda-optional local (cadr arglist) (cddr arglist) more body))
                      ((&rest &body) (scope-cl-lambda-rest local (cadr arglist) (cddr arglist) more body))
                      (&key (scope-cl-lambda-key local (cadr arglist) (cddr arglist) more body))
                      (&aux (scope-cl-lambda-aux local (cadr arglist) (cddr arglist) more body))
                      (&whole (scope-cl-lambda-1 local (cdr arglist) more body))))
                (when-let ((beg (scope-sym-pos head)))
                  (scope-report 'variable beg (length (symbol-name bare)) beg))
                (scope-cl-lambda-1 (scope-local-new bare (scope-sym-pos head) local)
                                   (cdr arglist) more body)))))
      (scope-cl-lambda-1 local (list '&rest arglist) more body)))
   (more (scope-cl-lambda-1 local (car more) (cdr more) body))
   (t (scope-lambda local nil body))))

(defun scope-cl-lambda-optional (local arg arglist more body)
  (let* ((a (ensure-list arg))
         (var (car a))
         (l local)
         (init (cadr a))
         (svar (caddr a)))
    (scope-1 local init)
    (if (consp var)
        (scope-cl-lambda-1 l var (cons (append (when svar (list svar))
                                               (cons '&optional arglist))
                                       more)
                           body)
      (when-let ((bare (scope-sym-bare svar)))
        (when-let ((beg (scope-sym-pos svar)))
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (setq l (scope-local-new bare (scope-sym-pos svar) l)))
      (when-let ((bare (scope-sym-bare var)))
        (when-let ((beg (scope-sym-pos var)))
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (setq l (scope-local-new bare (scope-sym-pos var) l)))
      (cond
       (arglist
        (let ((head (car arglist)))
          (if-let ((bare (scope-sym-bare head))
                   ((memq bare '(&rest &body &key &aux))))
              (progn
                (when-let ((beg (scope-sym-pos head)))
                  (scope-report 'ampersand beg (length (symbol-name bare))))
                (case bare
                  ((&rest &body) (scope-cl-lambda-rest l (cadr arglist) (cddr arglist) more body))
                  (&key (scope-cl-lambda-key l (cadr arglist) (cddr arglist) more body))
                  (&aux (scope-cl-lambda-aux l (cadr arglist) (cddr arglist) more body))))
            (scope-cl-lambda-optional l head (cdr arglist) more body))))
       (more (scope-cl-lambda-1 l (car more) (cdr more) body))
       (t (scope-lambda l nil body))))))

(defun scope-cl-lambda-rest (local var arglist more body)
  (let* ((l local))
    (if (consp var)
        (scope-cl-lambda-1 l var (cons arglist more) body)
      (when-let ((bare (scope-sym-bare var)))
        (when-let ((beg (scope-sym-pos var)))
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (setq l (scope-local-new bare (scope-sym-pos var) l)))
      (cond
       (arglist
        (let ((head (car arglist)))
          (if-let ((bare (scope-sym-bare head))
                   ((memq bare '(&key &aux))))
              (progn
                (when-let ((beg (scope-sym-pos head)))
                  (scope-report 'ampersand beg (length (symbol-name bare))))
                (case bare
                  (&key (scope-cl-lambda-key l (cadr arglist) (cddr arglist) more body))
                  (&aux (scope-cl-lambda-aux l (cadr arglist) (cddr arglist) more body))))
            (scope-cl-lambda-1 l (car more) (cdr more) body))))
       (more (scope-cl-lambda-1 l (car more) (cdr more) body))
       (t (scope-lambda l nil body))))))

(defun scope-cl-lambda-key (local arg arglist more body)
  (let* ((a (ensure-list arg))
         (var (car a))
         (l local)
         (init (cadr a))
         (svar (caddr a))
         (kw (car-safe var)))
    (scope-1 local init)
    (and kw (or (symbolp kw) (symbol-with-pos-p kw))
         (cadr var)
         (not (cddr var))
         ;; VAR is (KEYWORD VAR)
         (setq var (cadr var)))
    (when-let ((bare (scope-sym-bare kw))
               ((keywordp bare)))
      (when-let ((beg (scope-sym-pos kw)))
        (scope-report 'constant beg (length (symbol-name bare))))
      (setq l (scope-local-new bare (scope-sym-pos svar) l)))
    (if (consp var)
        (scope-cl-lambda-1 l var (cons (append (when svar (list svar))
                                               (cons '&key arglist))
                                       more)
                           body)
      (when-let ((bare (scope-sym-bare svar)))
        (when-let ((beg (scope-sym-pos svar)))
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (setq l (scope-local-new bare (scope-sym-pos svar) l)))
      (when-let ((bare (scope-sym-bare var)))
        (when-let ((beg (scope-sym-pos var)))
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (setq l (scope-local-new bare (scope-sym-pos var) l)))
      (cond
       (arglist
        (let ((head (car arglist)))
          (if-let ((bare (scope-sym-bare head))
                   ((memq bare '(&aux &allow-other-keys))))
              (progn
                (when-let ((beg (scope-sym-pos head)))
                  (scope-report 'ampersand beg (length (symbol-name bare))))
                (case bare
                  (&aux (scope-cl-lambda-aux l (cadr arglist) (cddr arglist) more body))
                  (&allow-other-keys (scope-cl-lambda-1 l (car more) (cdr more) body))))
            (scope-cl-lambda-key l head (cdr arglist) more body))))
       (more (scope-cl-lambda-1 l (car more) (cdr more) body))
       (t (scope-lambda l nil body))))))

(defun scope-cl-lambda-aux (local arg arglist more body)
  (let* ((a (ensure-list arg))
         (var (car a))
         (l local)
         (init (cadr a)))
    (scope-1 local init)
    (if (consp var)
        (scope-cl-lambda-1 l var (cons arglist more) body)
      (when-let ((bare (scope-sym-bare var)))
        (when-let ((beg (scope-sym-pos var)))
          (scope-report 'variable beg (length (symbol-name bare)) beg))
        (setq l (scope-local-new bare (scope-sym-pos var) l)))
      (cond
       (arglist (scope-cl-lambda-aux l (car arglist) (cdr arglist) more body))
       (more (scope-cl-lambda-1 l (car more) (cdr more) body))
       (t (scope-lambda l nil body))))))

(defvar scope-macrolet-alist nil)

(defun scope-cl-macrolet (local bindings body)
  (if-let ((b (car bindings)))
      (let ((name (car b))
            (arglist (cadr b))
            (mbody (cddr b)))
        (scope-cl-lambda local arglist mbody)
        (when-let ((bare (scope-sym-bare name)))
          (when-let ((beg (scope-sym-pos name)))
            (scope-report 'macro beg (length (symbol-name bare)) beg))
          (let ((scope-macrolet-alist (scope-local-new bare (scope-sym-pos name) scope-macrolet-alist)))
            (scope-cl-macrolet local (cdr bindings) body))))
    (scope-n local body)))

(defun scope-define-minor-mode (local mode _doc body)
  (let ((explicit-var nil))
    (while-let ((kw (car-safe body))
                (bkw (scope-sym-bare kw))
                ((keywordp bkw)))
      (when-let ((beg (scope-sym-pos kw)))
        (scope-report 'constant beg (length (symbol-name bkw))))
      (case bkw
        ((:init-value :keymap :after-hook :initialize)
         (scope-1 local (cadr body)))
        (:lighter (scope-mode-line-construct local (cadr body)))
        ((:interactive)
         (let ((val (cadr body)))
           (when (consp val) (mapc #'scope-major-mode-name val))))
        ((:variable)
         (let* ((place (cadr body))
                (tail (cdr-safe place)))
           (if (and tail (let ((symbols-with-pos-enabled t))
                           (or (symbolp tail) (functionp tail))))
               (progn
                 (scope-1 local (car place))
                 (scope-sharpquote local tail))
             (scope-1 local place)))
         (setq explicit-var t))
        ((:group)
         (scope-quoted-group local (cadr body)))
        ((:predicate)                   ;For globalized minor modes.
         (scope-global-minor-mode-predicate (cadr body))))
      (setq body (cddr body)))
    (when-let ((bare (scope-sym-bare mode)) (beg (scope-sym-pos mode)))
      (scope-report 'defun beg (length (symbol-name bare)))
      (unless explicit-var
        (scope-report 'defvar beg (length (symbol-name bare)))))
    (scope-n local body)))

(defun scope-global-minor-mode-predicate (pred)
  (if (consp pred)
      (if (eq 'not (scope-sym-bare (car pred)))
          (mapc #'scope-global-minor-mode-predicate (cdr pred))
        (mapc #'scope-global-minor-mode-predicate pred))
    (scope-major-mode-name pred)))

(defun scope-major-mode-name (mode)
  (when-let* ((beg (scope-sym-pos mode))
              (bare (bare-symbol mode))
              ((not (booleanp bare)))
              (len (length (symbol-name bare))))
    (scope-report 'major-mode beg len)))

(defun scope-mode-line-construct (_local format)
  (scope-mode-line-construct-1 format))

(defun scope-mode-line-construct-1 (format)
  (cond
   ((symbol-with-pos-p format)
    (scope-report 'variable
                  (symbol-with-pos-pos format)
                  (length (symbol-name (bare-symbol format)))))
   ((consp format)
    (let ((head (car format)))
      (cond
       ((or (stringp head) (consp head) (integerp head))
        (mapc #'scope-mode-line-construct-1 format))
       ((or (symbolp head) (symbol-with-pos-p head))
        (scope-s nil head)
        (case (bare-symbol head)
          (:eval
           (scope-1 nil (cadr format)))
          (:propertize
           (scope-mode-line-construct-1 (cadr format))
           (when-let* ((props (cdr format))
                       (symbols-with-pos-enabled t)
                       (val-form (plist-get props 'face)))
             (scope-face-1 val-form)))
          (otherwise
           (scope-mode-line-construct-1 (cadr format))
           (scope-mode-line-construct-1 (caddr format))))))))))

(defvar scope-safe-macros t
  "Specify which macros are safe to expand.

If this is t, macros are considered safe by default.  Otherwise, this is
a (possibly empty) list of safe macros.")

(defvar scope-unsafe-macros
  '( static-if cl-eval-when eval-when-compile eval-and-compile let-when-compile
     rx cl-macrolet))

(defun scope-safe-macro-p (macro)
  (and (not (memq macro scope-unsafe-macros))
       (or (eq scope-safe-macros t)
           (memq macro scope-safe-macros)
           (get macro 'safe-macro)
           (trusted-content-p))))

(defvar warning-minimum-log-level)

(defmacro scope-define-analyzer (fsym args &rest body)
  (declare (indent defun))
  (let ((analyzer (intern (concat "scope--analyze-" (symbol-name fsym)))))
    `(progn
       (defun ,analyzer ,args ,@body)
       (put ',fsym 'scope-analyzer #',analyzer))))

(defmacro scope-define-function-analyzer (fsym args &rest body)
  (declare (indent defun))
  (let* ((helper (intern (concat "scope--analyze-" (symbol-name fsym) "-1"))))
    `(progn
       (defun ,helper ,args ,@body)
       (scope-define-analyzer ,fsym (l f &rest args)
         (scope-report-s f 'function)
         (apply #',helper args)
         (scope-n l args)))))

(defmacro scope-define-macro-analyzer (fsym args &rest body)
  (declare (indent defun))
  (let* ((helper (intern (concat "scope--analyze-" (symbol-name fsym) "-1"))))
    `(progn
       (defun ,helper ,args ,@body)
       (scope-define-analyzer ,fsym (l f &rest args)
         (scope-report-s f 'macro)
         (apply #',helper l args)))))

(defmacro scope-define-special-form-analyzer (fsym args &rest body)
  (declare (indent defun))
  (let* ((helper (intern (concat "scope--analyze-" (symbol-name fsym) "-1"))))
    `(progn
       (defun ,helper ,args ,@body)
       (scope-define-analyzer ,fsym (l f &rest args)
         (scope-report-s f 'macro)
         (apply #',helper l args)))))

(defun scope--unqoute (form)
  (when (memq (scope-sym-bare (car-safe form)) '(quote function \`))
    (cadr form)))

(scope-define-analyzer with-suppressed-warnings (l f warnings &rest body)
  (scope-report-s f 'macro)
  (dolist (warning warnings)
    (when-let* ((wsym (car-safe warning)))
      (scope-report-s wsym 'warning-type)))
  (scope-n l body))

(scope-define-analyzer eval (l f form &optional lexical)
  (scope-report-s f 'function)
  (if-let ((quoted (scope--unqoute form)))
      (scope-1 l quoted)
    (scope-1 l form))
  (scope-1 l lexical))

(scope-define-function-analyzer defalias (sym _def &optional _docstring)
  (when-let ((quoted (scope--unqoute sym))) (scope-report-s quoted 'defun)))

(scope-define-function-analyzer thing-at-point (thing &optional _)
  (when-let ((quoted (scope--unqoute thing))) (scope-report-s quoted 'thing)))

(dolist (sym '( forward-thing
                beginning-of-thing
                end-of-thing
                bounds-of-thing-at-point))
  (put sym 'scope-analyzer #'scope--analyze-thing-at-point))

(scope-define-function-analyzer bounds-of-thing-at-mouse (_event thing)
  (when-let ((quoted (scope--unqoute thing))) (scope-report-s quoted 'thing)))

(scope-define-function-analyzer thing-at-mouse (_event thing &optional _)
  (when-let ((quoted (scope--unqoute thing))) (scope-report-s quoted 'thing)))

(scope-define-function-analyzer custom-declare-variable (sym _default _doc &rest args)
  (when-let ((quoted (scope--unqoute sym))) (scope-report-s quoted 'defvar))
  (while-let ((kw (car-safe args))
              (bkw (scope-sym-bare kw))
              ((keywordp bkw)))
    (case bkw
      (:type
       (when-let ((quoted (scope--unqoute (cadr args)))) (scope-widget-type-1 quoted)))
      (:group
       (when-let ((quoted (scope--unqoute (cadr args)))) (scope-report-s quoted 'group))))
    (setq args (cddr args))))

(scope-define-function-analyzer custom-declare-group (sym _members _doc &rest args)
  (when-let ((quoted (scope--unqoute sym))) (scope-report-s quoted 'group))
  (while-let ((kw (car-safe args))
              (bkw (scope-sym-bare kw))
              ((keywordp bkw)))
    (case bkw
      (:group
       (when-let ((quoted (scope--unqoute (cadr args)))) (scope-report-s quoted 'group))))
    (setq args (cddr args))))

(scope-define-function-analyzer custom-declare-face (face spec _doc &rest args)
  (when-let ((q (scope--unqoute face))) (scope-report-s q 'defface))
  (when-let ((q (scope--unqoute spec)))
    (when (consp q) (dolist (s q) (scope-face (cdr s)))))
  (while-let ((kw (car-safe args))
              (bkw (scope-sym-bare kw))
              ((keywordp bkw)))
    (case bkw
      (:group
       (when-let ((q (scope--unqoute (cadr args)))) (scope-report-s q 'group))))
    (setq args (cddr args))))

(scope-define-function-analyzer cl-typed (_val type)
  (when-let ((q (scope--unqoute type)) ((not (booleanp q))))
    (scope-report-s q 'type)))

(scope-define-function-analyzer pulse-momentary-highlight-region (_start _end &optional face)
  (when-let ((q (scope--unqoute face))) (scope-face q)))

(scope-define-function-analyzer throw (tag _value)
  (when-let ((q (scope--unqoute tag))) (scope-report-s q 'throw-tag)))

(scope-define-function-analyzer run-hooks (&rest hooks)
  (dolist (hook hooks)
    (when-let ((q (scope--unqoute hook))) (scope-report-s q 'variable))))

(scope-define-function-analyzer signal (error-symbol &optional _data)
  (when-let ((q (scope--unqoute error-symbol))) (scope-report-s q 'condition)))

(scope-define-function-analyzer fboundp (symbol)
  (when-let ((q (scope--unqoute symbol))) (scope-report-s q 'function)))

(scope-define-function-analyzer overlay-put (_ov prop val)
  (when-let ((q (scope--unqoute prop))
             ((eq (scope-sym-bare q) 'face))
             (face (scope--unqoute val)))
    (scope-face face)))

(scope-define-function-analyzer boundp (var &rest _)
  (when-let ((q (scope--unqoute var))) (scope-report-s q 'variable)))

(dolist (sym '( set symbol-value define-abbrev-table
                special-variable-p local-variable-p
                local-variable-if-set-p
                default-value set-default make-local-variable
                buffer-local-value add-to-list add-to-history
                add-hook remove-hook run-hook-with-args run-hook-wrapped))
  (put sym 'scope-analyzer #'scope--analyze-boundp))

(scope-define-function-analyzer defvaralias (new base &optional _docstring)
  (when-let ((q (scope--unqoute new))) (scope-report-s q 'defvar))
  (when-let ((q (scope--unqoute base))) (scope-report-s q 'variable)))

(scope-define-function-analyzer define-error (name _message &optional parent)
  (when-let ((q (scope--unqoute name))) (scope-report-s q 'condition))
  (when-let ((q (scope--unqoute parent)))
    (dolist (p (ensure-list q)) (scope-report-s p 'condition))))

(scope-define-function-analyzer featurep (feature &rest _)
  (when-let ((q (scope--unqoute feature))) (scope-report-s q 'feature)))

(put 'provide 'scope-analyzer #'scope--analyze-featurep)
(put 'require 'scope-analyzer #'scope--analyze-featurep)

(scope-define-function-analyzer put-text-property (&optional _ _ prop val)
  (when (memq (scope-sym-bare (scope--unqoute prop)) '(mouse-face face))
    (when-let ((q (scope--unqoute val))) (scope-face q))))

(put 'remove-overlays 'scope-analyzer #'scope--analyze-put-text-property)

(scope-define-function-analyzer propertize (_string &rest props)
  (while props
    (case (scope-sym-bare (scope--unqoute (car props)))
      ((face mouse-face)
       (when-let ((q (scope--unqoute (cadr props)))) (scope-face q))))
    (setq props (cddr props))))

(scope-define-function-analyzer eieio-defclass-internal (name superclasses _ _)
  (when-let ((q (scope--unqoute name))) (scope-report-s q 'type))
  (when-let ((q (scope--unqoute superclasses)))
    (dolist (sup q) (scope-report-s sup 'type))))

(scope-define-function-analyzer cl-struct-define
  (name _doc parent _type _named _slots _children _tab _print)
  (when-let ((q (scope--unqoute name)))   (scope-report-s q 'type))
  (when-let ((q (scope--unqoute parent))) (scope-report-s q 'type)))

(scope-define-function-analyzer define-widget (name class _doc &rest args)
  (when-let ((q (scope--unqoute name)))  (scope-report-s q 'widget-type))
  (when-let ((q (scope--unqoute class))) (scope-report-s q 'widget-type))
  (while-let ((kw (car-safe args))
              (bkw (scope-sym-bare kw))
              ((keywordp bkw)))
    (case bkw
      (:type
       (when-let ((q (scope--unqoute (cadr args)))) (scope-widget-type-1 q)))
      (:args
       (when-let ((q (scope--unqoute (cadr args)))) (mapc #'scope-widget-type-1 q))))
    (setq args (cddr args))))

(scope-define-function-analyzer provide-theme (name &rest _)
  (when-let ((q (scope--unqoute name))) (scope-report-s q 'theme)))

(put 'custom-declare-theme 'scope-analyzer #'scope--analyze-provide-theme)

(scope-define-function-analyzer eieio-oref (_obj slot)
  (when-let ((q (scope--unqoute slot))) (scope-report-s q 'slot)))

(dolist (fun '(slot-boundp slot-makeunbound slot-exists-p eieio-oref-default))
  (put fun 'scope-analyzer #'scope--analyze-eieio-oref))

(scope-define-function-analyzer eieio-oset (_obj slot _value)
  (when-let ((q (scope--unqoute slot))) (scope-report-s q 'slot)))

(put 'eieio-oset-default 'scope-analyzer #'scope--analyze-eieio-oset)

(scope-define-macro-analyzer define-globalized-minor-mode (l global mode turn-on &rest body)
  (scope-report-s mode 'function)
  (scope-report-s turn-on 'function)
  (scope-define-minor-mode l global nil body))

(scope-define-macro-analyzer lambda (l args &rest body)
  (scope-lambda l args body))

(scope-define-macro-analyzer cl-loop (l &rest clauses)
  (scope-loop l clauses))

(scope-define-macro-analyzer named-let (l name bindings &rest body)
  (scope-named-let l name bindings body))

(scope-define-macro-analyzer cl-flet (l bindings &rest body)
  (scope-flet l bindings body))

(scope-define-macro-analyzer cl-labels (l bindings &rest body)
  (scope-labels l bindings body))

(scope-define-macro-analyzer with-slots (l spec-list object &rest body)
  (scope-with-slots l spec-list object body))

(scope-define-macro-analyzer cl-defmethod (l name &rest rest)
  (scope-defmethod l name rest))

(scope-define-macro-analyzer cl-destructuring-bind (l args expr &rest body)
  (scope-1 l expr)
  (scope-cl-lambda l args body))

(scope-define-macro-analyzer declare-function (l fn file &optional arglist fileonly)
  (scope-declare-function l fn file arglist fileonly))

(scope-define-macro-analyzer cl-block (l name &rest body)
  (scope-block l name body))

(scope-define-macro-analyzer cl-return-from (l name &optional result)
  (scope-return-from l name result))

(scope-define-macro-analyzer rx (l &rest regexps)
  ;; Unsafe macro!
  (scope-rx l regexps))

(scope-define-macro-analyzer cl-tagbody (l &rest body)
  (let (labels statements)
    (while body
      (let ((head (pop body)))
        (if (consp head)
            (push head statements)
          (push head labels))))
    (scope-cl-tagbody l (nreverse labels) (nreverse statements))))

(defvar scope-label-alist nil)

(defun scope-cl-tagbody (l labels statements)
  (if labels
      (let* ((label (car labels))
             (bare (scope-sym-bare label)))
        (when-let ((beg (scope-sym-pos label)))
          (scope-report 'label beg (length (symbol-name bare)) beg))
        (let ((scope-label-alist
               (if bare
                   (scope-local-new bare (scope-sym-pos label) scope-label-alist)
                 scope-label-alist)))
          (scope-cl-tagbody l (cdr labels) statements)))
    (scope-n l statements)))

(scope-define-macro-analyzer go (_l label)
  ;; TODO: Change to a local macro defintion induced by `cl-tagbody'.
  (when-let ((bare (scope-sym-bare label))
             (pos (alist-get bare scope-label-alist))
             (beg (scope-sym-pos label)))
    (scope-report 'label beg (length (symbol-name bare)) pos)))

(scope-define-macro-analyzer rx-define (l name &rest rest)
  (scope-rx-define l name rest))

(scope-define-macro-analyzer rx-let (l bindings &rest body)
  (scope-rx-let l bindings body))

(scope-define-macro-analyzer let-when-compile (l bindings &rest body)
  ;; Unsafe macro!
  (scope-let* l bindings body))

(scope-define-macro-analyzer cl-eval-when (l _when &rest body)
  ;; Unsafe macro!
  (scope-n l body))

(scope-define-macro-analyzer cl-macrolet (l bindings &rest body)
  ;; Unsafe macro!
  (scope-cl-macrolet l bindings body))

(scope-define-macro-analyzer gv-define-expander (l name handler)
  (scope-gv-define-expander l name handler))

(scope-define-macro-analyzer gv-define-simple-setter (l name setter &rest rest)
  (scope-gv-define-simple-setter l name setter rest))

(scope-define-macro-analyzer cl-deftype (l name arglist &rest body)
  (scope-deftype l name arglist body))

(scope-define-macro-analyzer define-minor-mode (l mode doc &rest body)
  (scope-define-minor-mode l mode doc body))

(scope-define-macro-analyzer setq-local (l &rest args)
  (scope-setq l args))

(put 'setq-default 'scope-analyzer #'scope--analyze-setq-local)

(scope-define-macro-analyzer cl-defun (l name arglist &rest body)
  (scope-cl-defun l name arglist body))

(put 'cl-defmacro 'scope-analyzer #'scope--analyze-cl-defun)

(scope-define-macro-analyzer defun (l name arglist &rest body)
  (scope-defun l name arglist body))

(put 'defmacro 'scope-analyzer #'scope--analyze-defun)
(put 'ert-deftest 'scope-analyzer #'scope--analyze-defun)

(scope-define-macro-analyzer cl-letf (l bindings &rest body)
  (let ((l0 l))
    (dolist (binding bindings)
      (let ((place (car binding)))
        (if (or (symbol-with-pos-p place) (symbolp place))
            (let* ((bare (bare-symbol place))
                   (len (length (symbol-name bare)))
                   (beg (scope-sym-pos place)))
              (when beg (scope-report 'variable beg len beg))
              (setq l (scope-local-new bare beg l)))
          (scope-1 l0 place))
        (scope-1 l0 (cadr binding))))
    (scope-n l body)))

(scope-define-macro-analyzer setf (l &rest args)
  (scope-n l args))

(dolist (sym '( pop push with-memoization cl-pushnew incf decf
                ;; The following macros evaluate unsafe code.
                ;; Never expand them!
                static-if eval-when-compile eval-and-compile))
  (put sym 'scope-analyzer #'scope--analyze-setf))

(scope-define-macro-analyzer seq-let (l args sequence &rest body)
  (scope-1 l sequence)
  (dolist (arg args)
    (let* ((bare (scope-sym-bare arg))
           (len (length (symbol-name bare)))
           (beg (scope-sym-pos arg)))
      (if (eq bare '&rest)
          (scope-report 'ampersand beg len)
        (when beg (scope-report 'variable beg len beg))
        (setq l (scope-local-new bare beg l)))))
  (scope-n l body))

(scope-define-analyzer let-alist (l f alist &rest body)
  (scope-report-s f 'macro)
  (scope-1 l alist)
  (let ((scope-current-let-alist-form
         (cons (or (scope-sym-pos f) (cons 'gen (incf scope-counter)))
               (scope-sym-pos f))))
    (scope-n l body)))

(scope-define-special-form-analyzer let (l bindings &rest body)
  (scope-let-1 l l bindings body))

(scope-define-special-form-analyzer let* (l bindings &rest body)
  (scope-let* l bindings body))

(scope-define-special-form-analyzer cond (l &rest clauses)
  (scope-cond l clauses))

(scope-define-special-form-analyzer setq (l &rest args)
  (scope-setq l args))

(scope-define-special-form-analyzer defvar (l sym &optional init _doc)
  (scope-defvar l sym init))

(put 'defconst 'scope-analyzer #'scope--analyze-defvar)

(scope-define-special-form-analyzer condition-case (l var bodyform &rest handlers)
  (scope-condition-case l var bodyform handlers))

(scope-define-special-form-analyzer function (l arg)
  (scope-sharpquote l arg))

(scope-define-special-form-analyzer quote (_l _arg)) ;Do nothing.

(scope-define-special-form-analyzer catch (l tag &rest body)
  (scope-catch l tag body))

(defun scope-report-s (sym type)
  (when-let* ((beg (scope-sym-pos sym)) (bare (bare-symbol sym)))
    (scope-report type beg (length (symbol-name bare)))))

(defun scope-1 (local form)
  (cond
   ((consp form)
    (let* ((f (car form)) (bare (scope-sym-bare f))
           (forms (cdr form)) (this nil))
      (when bare
        (cond
         ((setq this (assq bare scope-flet-alist))
          (scope-report
           'function (symbol-with-pos-pos f) (length (symbol-name bare)) this)
          (scope-n local forms))
         ((setq this (assq bare scope-macrolet-alist))
          (scope-report
           'macro (symbol-with-pos-pos f) (length (symbol-name bare)) this)
          ;; Local macros can be unsafe, so we do not expand them.
          ;; Hence we cannot interpret their arguments.
          )
         ((setq this (function-get bare 'scope-analyzer)) (apply this local form))
         ((special-form-p bare) (scope-report-s f 'special-form) (scope-n local forms))
         ((macrop bare) (scope-report-s f 'macro)
          (cond
           ((eq (get bare 'edebug-form-spec) t) (scope-n local forms))
           ((scope-safe-macro-p bare)
            (let* ((warning-minimum-log-level :emergency)
                   (macroexp-inhibit-compiler-macros t)
                   (symbols-with-pos-enabled t)
                   (message-log-max nil)
                   (inhibit-message t)
                   (macroexpand-all-environment
                    (append (mapcar #'list scope-unsafe-macros) macroexpand-all-environment))
                   (expanded (ignore-errors (macroexpand-1 form macroexpand-all-environment))))
              (scope-1 local expanded)))))
         ((or (functionp bare) (memq bare scope-local-functions) scope-assume-func-p)
          (scope-report-s f 'function) (scope-n local forms))))))
   ((symbol-with-pos-p form) (scope-s local form))))

(defun scope-n (local body) (dolist (form body) (scope-1 local form)))

;;;###autoload
(defun scope (callback &optional stream)
  "Read and analyze code from STREAM, reporting findings via CALLBACK.

Call CALLBACK for each analyzed symbol SYM with arguments TYPE, POS,
LEN, ID and DEF, where TYPE is a symbol that specifies the semantics of
SYM; POS is the position of SYM in STREAM; LEN is SYM's length; ID is an
object that uniquely identifies (co-)occurrences of SYM in the current
defun; and DEF is the position in which SYM is locally defined, or nil.
If SYM is itself a binding occurrence, then POS and BINDER are equal.
If SYM is not lexically bound, then BINDER is nil.  This function
ignores `read-symbol-shorthands', so SYM and LEN always correspond to
the symbol as it appears in STREAM.

If STREAM is nil, it defaults to the current buffer.

This function recursively analyzes Lisp forms (HEAD . TAIL), usually
starting with a top-level form, by inspecting HEAD at each level:

- If HEAD satisfies `functionp', which means it is a function in the
  running Emacs session, analzye the form as a function call.

- Special forms such as `if', and `let', along with some standard macros
  like `lambda', `setf' and backquotes, are handled specially according
  to their particular semantics.  Other macros are expanded.

- If HEAD has the symbol property `scope-function', the value of this
  property is used to analyze TAIL.  It should be a function that takes
  two arguments, LOCAL and TAIL, and calls `scope-callback' to report on
  analyzed symbols in TAIL.  `scope-callback' is let-bound to CALLBACK.
  LOCAL represents the local context around the current form; the
  `scope-function' can pass LOCAL to functions such as `scope-1' and
  `scope-n' to analyze sub-forms.  See also `scope-local-new' for
  extending LOCAL with local bindings while analyzing TAIL.

- If within the code under analysis HEAD is a `cl-flet'-bound local
  function name, analyze the form as a function call.

- Otherwise, HEAD is unknown.  If the HEAD of the top-level form that
  this function reads from STREAM is unknown, then this function ignores
  it and returns nil.  If an unknown HEAD occurs in a nested form, then
  by default it is similarly ignored, but if `scope-assume-func-p' is
  non-nil, then this function assumes that such HEADs are functions."
  (let ((scope-counter 0)
        (scope-callback callback)
        (read-symbol-shorthands nil))
    (scope-1 nil (read-positioning-symbols (or stream (current-buffer))))))

(provide 'scope)
;;; scope.el ends here
