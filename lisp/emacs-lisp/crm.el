;;; crm.el --- read multiple strings with completion  -*- lexical-binding: t; -*-

;; Copyright (C) 1985-1986, 1993-2025 Free Software Foundation, Inc.

;; Author: Sen Nagata <sen@eccosys.com>
;; Keywords: completion, minibuffer, multiple elements

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This code defines a function, `completing-read-multiple', which
;; provides the ability to read multiple strings in the minibuffer,
;; with completion.  See that function's documentation for details.

;; For the moment, I have decided to not bind any special behavior to
;; the separator key.  In the future, the separator key might be used
;; to provide completion in certain circumstances.  One of the reasons
;; why this functionality is not yet provided is that it is unclear to
;; the author what the precise circumstances are, under which
;; separator-invoked completion should be provided.

;; Design note: `completing-read-multiple' is modeled after
;; `completing-read'.  They should be similar -- it was intentional.

;; Some of this code started out as translation from C code in
;; src/minibuf.c to Emacs Lisp code.  After this code was rewritten in Elisp
;; and made to operate on any field, this file was completely rewritten to
;; just reuse that code.

;; Thanks to Sen Nagata <sen@eccosys.com> for the original version of the
;; code, and sorry for throwing it all out.  --Stef

;; Thanks to Richard Stallman for all of his help (many of the good
;; ideas in here are from him), Gerd Moellmann for his attention,
;; Stefan Monnier for responding with a code sample and comments very
;; early on, and Kai Grossjohann & Soren Dayton for valuable feedback.

;;; Questions and Thoughts:

;; -should `completing-read-multiple' allow a trailing separator in
;; a return value when REQUIRE-MATCH is t?  if not, should beep when a user
;; tries to exit the minibuffer via RET?

;; -tip: use M-f and M-b for ease of navigation among elements.

;; - the difference between minibuffer-completion-table and
;;   crm-completion-table is just crm--collection-fn.  In most cases it
;;   shouldn't make any difference.  But if a non-CRM completion function
;;   happens to be used, it will use minibuffer-completion-table and
;;   crm--collection-fn will try to make it do "more or less the right
;;   thing" by making it complete on the last element, which is about as
;;   good as we can hope for right now.
;;   I'm not sure if it's important or not.  Maybe we could just throw away
;;   crm-completion-table and crm--collection-fn, but there doesn't seem to
;;   be a pressing need for it, and since Sen did bother to write it, we may
;;   as well keep it, in case it helps.

;;; History:
;;
;; 2000-04-10:
;;
;;   first revamped version
;;
;; 2024-01-03:
;;
;;   second revamped version

;;; Code:

(define-obsolete-variable-alias 'crm-default-separator 'crm-separator "29.1")

(defvar crm-separator "[ \t]*,[ \t]*"
  "Separator regexp used for separating strings in `completing-read-multiple'.
It should be a regexp that does not match the list of completion candidates.

This can also be a cons cell (REGEXP . CANONICAL), where REGEXP
is the separator regexp used for matching input separators, and
CANONICAL is a canonical separator string that Emacs uses when it
inserts a separator for you.  If CANONICAL does not match REGEXP,
it is ignored.  See also `crm-complete-and-insert-separator'.")

(defvar crm-common-separators '(",")
  "List of strings often used to separate multiple minibuffer inputs.

See also `crm-complete-and-insert-separator'.")

(defvar crm-current-separator nil
  "The value of `crm-separator' for the current minibuffer.")

(defvar crm-canonical-separator nil
  "Canonical separator for `completing-read-multiple'.

This can either a string that matches `crm-current-separator', or
nil when there is no canonical separator.")

(defun crm-complete-and-exit ()
  "If all of the minibuffer elements are valid completions then exit.
All elements in the minibuffer must match.  If there is a mismatch, move point
to the location of mismatch and do not exit.

This function is modeled after `minibuffer-complete-and-exit'."
  (interactive "" minibuffer-mode)
  (let ((bob (minibuffer--completion-prompt-end))
        (doexit t))
    (goto-char bob)
    (while
        (and doexit
             (let* ((beg (save-excursion
                           (if (re-search-backward crm-current-separator bob t)
                               (match-end 0)
                             bob)))
                    (end (copy-marker
                          (save-excursion
                            (if (re-search-forward crm-current-separator nil t)
                                (match-beginning 0)
                              (point-max)))
                          t)))
               (goto-char end)
               (setq doexit nil)
               (completion-complete-and-exit beg end
                                             (lambda () (setq doexit t)))
               (goto-char end)
               (not (eobp)))
             (looking-at crm-current-separator))
      (when doexit
        (goto-char (match-end 0))))
    (if doexit (exit-minibuffer))))

(defvar-local read-string-matching-regexp nil
  "Regular expression that minibuffer input must match.")

(defun read-string-matching-try-exit ()
  "Exit minibuffer only if the input matches `read-string-matching-regexp'."
  (interactive nil minibuffer-mode)
  (if (string-match-p read-string-matching-regexp (minibuffer-contents))
      (exit-minibuffer)
    (user-error "Input does not match \"%s\"" read-string-matching-regexp)))

(defvar-keymap read-string-matching-mode-map
  :doc "Keymap for `read-string-matching-mode'."
  "<remap> <exit-minibuffer>" #'read-string-matching-try-exit)

(define-minor-mode read-string-matching-mode
  "Minor mode for reading a string matching some regular expression.

`read-string-matching' enables this minor mode in the minibuffer."
  :lighter nil)

(defun read-string-matching (regexp prompt &optional
                                    initial-input history
                                    default-value inherit-input-method)
  "Read a string matching REGEXP in the minibufffer.

This function calls `read-string' with arguments PROMPT,
INITIAL-INPUT, HISTORY, DEFAULT-VALUE and INHERIT-INPUT-METHOD."
  (minibuffer-with-setup-hook
      (lambda ()
        (read-string-matching-mode)
        (setq read-string-matching-regexp regexp))
    (read-string prompt initial-input history default-value
                 inherit-input-method)))

(defun crm-change-separator (sep &optional rep)
  "Set the current `crm-separator' to SEP.

Non-nil optional argument REP says to replace occurrences of the
old `crm-separator' in the current minibuffer contents with REP.

Interactively, prompt for SEP.  With a prefix argument, prompt
for REP as well."
  (interactive
   (let* ((sep (read-regexp
                (format-prompt "New separator" crm-current-separator)
                crm-current-separator)))
     (list sep
           (when current-prefix-arg
             (read-string-matching sep "Replace existing separators with: "))))
   minibuffer-mode)
  (save-excursion
    (when rep
      (goto-char (minibuffer-prompt-end))
      (while (re-search-forward crm-current-separator nil t)
        (replace-match rep t t))))
  (setq crm-current-separator sep crm-canonical-separator rep)
  (crm-highlight-separators (minibuffer-prompt-end) (point-max))
  (when (get-buffer-window completions-buffer-name 0)
    ;; Update *Completions* to avoid stale `completion-base-position'.
    (minibuffer-completion-help)))

(defun crm-complete-and-insert-separator ()
  "Complete partial inputs and then insert a new input separator.

If `crm-canonical-separator' is non-nil and matches the regular
expression `crm-current-separator', then this command uses
`crm-canonical-separator' as the separator.  Otherwise, this
command tries to find an appropriate separator by matching
`crm-current-separator' against your current input and against
the list of common separators in `crm-common-separators', and if
that fails this command prompts you for the separator to use."
  (interactive "" minibuffer-mode)
  (let ((bob (minibuffer--completion-prompt-end))
        (all-complete t))
    ;; Establish a canonical separator string, so we can insert it.
    (setq crm-canonical-separator
          (or
           ;; If `crm-canonical-separator' matches, use it.
           (and (stringp crm-canonical-separator)
                (string-match-p crm-current-separator
                                crm-canonical-separator)
                crm-canonical-separator)
           ;; If there's some separator already, use that.
           (and (save-excursion
                  (goto-char bob)
                  (re-search-forward crm-current-separator nil t))
                (buffer-substring-no-properties (match-beginning 0)
                                                (match-end 0)))
           ;; If any common separator matches, use it.
           (seq-some (lambda (sep)
                       (and (string-match-p crm-current-separator sep)
                            sep))
                     crm-common-separators)
           ;; Ask the user for help.
           (read-string-matching crm-current-separator
                                 "Separate inputs with: ")))
    (while
        (and all-complete
             (let* ((beg (save-excursion
                           (if (re-search-backward crm-current-separator bob t)
                               (match-end 0)
                             bob)))
                    (end (copy-marker
                          (save-excursion
                            (if (re-search-forward crm-current-separator nil t)
                                (match-beginning 0)
                              (point-max)))
                          t)))
               (goto-char end)
               (setq all-complete nil)
               (completion-complete-and-exit
                beg end (lambda () (setq all-complete t)))
               (goto-char end)
               (not (eobp)))
             (looking-at crm-current-separator))
      (when all-complete
        (goto-char (match-end 0))))
    (when all-complete
      (if (looking-back crm-current-separator bob)
          ;; Separator already present, show completion candidates.
          (minibuffer-completion-help)
        (insert crm-canonical-separator)))))

(define-minor-mode completions-multi-mode
  "Minor mode for reading multiple strings in the minibuffer."
  :interactive nil
  (if completions-multi-mode
      (setq-local completions-header-extra
                  (cons
                   '(:eval
                     (let ((canonical
                            (buffer-local-value 'crm-canonical-separator
                                                completion-reference-buffer)))
                       (propertize
                        (concat
                         "Multi"
                         (when canonical (concat "[" crm-canonical-separator "]")))
                        'help-echo
                        (concat
                         "Insert multiple inputs by separating them with \""
                         (or canonical
                             (buffer-local-value 'crm-current-separator
                                                 completion-reference-buffer))
                         "\""))))
                   completions-header-extra))))

(defun crm-completions-setup ()
  "Enable `completions-multi-mode' in *Completions* buffer."
  (with-current-buffer standard-output (completions-multi-mode)))

(define-obsolete-variable-alias 'crm-local-completion-map
  'completing-read-multiple-mode-map "30.1")

(define-obsolete-variable-alias 'crm-local-must-match-map
  'completing-read-multiple-mode-map "30.1")

(defface crm-separator
  '((t :inherit minibuffer-prompt))
  "Face for highlighting input separators in multi-input minibuffers."
  :version "30.1"
  :group 'minibuffer)

(defface crm-prompt-indicator-highlight
  '((t :inherit mode-line-highlight))
  "Face for minibuffer multi-inputs prompt indicator when mouse is over it."
  :group 'minibuffer)

(defun crm-highlight-separators (beg end &optional _)
  "Highlight current minibuffer input separators between BEG and END."
  (let* ((bob (minibuffer-prompt-end))
         (beg (or (save-excursion
                    (goto-char beg)
                    (re-search-backward crm-current-separator bob t))
                  bob))
         (end (or (save-excursion
                    (goto-char end)
                    (re-search-forward crm-current-separator nil t))
                  (point-max)))
         (ovs (seq-filter (lambda (ov) (overlay-get ov 'crm-separator))
                          (overlays-in beg end))))
    (mapc #'delete-overlay ovs)
    (save-excursion
      (goto-char beg)
      (save-match-data
        (while (re-search-forward crm-current-separator nil t)
          (let ((ov (make-overlay (match-beginning 0) (match-end 0))))
            (overlay-put ov 'face 'crm-separator)
            (overlay-put ov 'crm-separator t)
            (overlay-put ov 'evaporate t)))))))

(defvar-keymap completing-read-multiple-mode-map
  :doc "Keymap for `completing-read-multiple-mode'."
  "<remap> <minibuffer-complete-and-exit>" #'crm-complete-and-exit
  "C-x ," #'crm-change-separator
  "C-," #'crm-complete-and-insert-separator)

(defcustom crm-prompt-indicator "+"
  "String to show in minibuffer prompt when reading multiple inputs."
  :type 'string
  :version "31.1"
  :group 'minibuffer
  :risky t)

(defvar crm-prompt-indicator-format
  '(completing-read-multiple-mode
    (:propertize crm-prompt-indicator
                 help-echo "Reading multiple inputs"
                 mouse-face crm-prompt-indicator-highlight)))

(put 'crm-prompt-indicator-format 'risky-local-variable t)

(define-minor-mode completing-read-multiple-mode
  "Minor mode for reading multiple strings in the minibuffer."
  :interactive nil
  (if completing-read-multiple-mode
      (progn
        (add-hook 'completion-setup-hook #'crm-completions-setup 10 t)
        (add-hook 'after-change-functions #'crm-highlight-separators nil t)
        (setq-local minibuffer-extra-prompt-indicators-format
                    (list "" 'crm-prompt-indicator-format
                          minibuffer-extra-prompt-indicators-format))
        (crm-highlight-separators (minibuffer-prompt-end) (point-max)))
    (remove-hook 'completion-setup-hook #'crm-completions-setup t)
    (remove-hook 'after-change-functions #'crm-highlight-separators t)
    (mapc #'delete-overlay
          (seq-filter (lambda (ov) (overlay-get ov 'crm-separator))
                      (overlays-in (minibuffer-prompt-end) (point-max)))))
  (minibuffer-update-prompt-indicators))

(defun crm--table (table s p a)
  (let ((beg 0))
    (while (string-match crm-current-separator s beg)
      (setq beg (match-end 0)))
    (pcase a
      (`(boundaries . ,suffix)
       (let ((bounds (completion-boundaries
                      (substring s beg) table p
                      (substring suffix 0
                                 (string-match crm-current-separator
                                               suffix)))))
         `(boundaries ,(+ (car bounds) beg) . ,(cdr bounds))))
      ('metadata
       ;; Adjust `minibuffer-completion-base' for annotation functions.
       (let ((md (completion-metadata (substring s beg) table p)))
         (cons 'metadata
               (cons (cons 'adjust-base-function
                           (lambda (base)
                             ;; When TABLE is nil and point is after
                             ;; separator, we may get empty `base' from
                             ;; `completion-all-sorted-completions',
                             ;; because `completion-all-completions'
                             ;; returns nil with no base length.
                             (if (string-empty-p base) base
                               (funcall (or (alist-get 'adjust-base-function md)
                                            #'identity)
                                        (substring base beg)))))
                     (cdr-safe md)))))
      ('nil (let ((comp (complete-with-action a table (substring s beg) p)))
              (if (stringp comp) (concat (substring s 0 beg) comp) comp)))
      ('completion--unquote
       (let ((qbeg 0) (qend nil))
         (while (and (string-match crm-current-separator s qbeg)
                     ;; P is Point here, not Predicate.
                     (<= (match-end 0) p))
           (setq qbeg (match-end 0)))
         (setq qend (or (string-match crm-current-separator s qbeg)
                        (length s)))
         (seq-let (ustring utable upoint requote)
             (complete-with-action a table (substring s qbeg qend) (- p qbeg))
           (list ustring utable upoint
                 (lambda (uresult op)
                   (let ((result (funcall requote uresult op)))
                     (pcase op
                       (1 ;;try
                        (if (stringp (car-safe result))
                            (cons (concat (substring s 0 qbeg)
                                          (car result)
                                          (substring s qend))
                                  (+ qbeg (cdr result)))
                          result))
                       (2 ;;all
                        (when-let ((last (last result))
                                   (base (or (cdr last) 0)))
                          (setcdr last (+ base qbeg))
                          result)))))))))
      (_ (complete-with-action a table (substring s beg) p)))))

;;;###autoload
(defun completing-read-multiple
  (prompt table &optional predicate require-match initial-input
	  hist def inherit-input-method)
  "Read multiple strings in the minibuffer, with completion.
The arguments are the same as those of `completing-read'.
\\<minibuffer-local-completion-map>
Input multiple strings by separating each one with a string that
matches the regexp `crm-separator'.  For example, if the separator
regexp is \",\", entering \"alice,bob,eve\" specifies the strings
\"alice\", \"bob\", and \"eve\".

We refer to contiguous strings of non-separator-characters as
\"elements\".  In this example there are three elements.

Completion is available on a per-element basis.  For example, if the
contents of the minibuffer are \"alice,bob,eve\" and point is between
\"l\" and \"i\", pressing \\[minibuffer-complete] operates on the element \"alice\".

This function returns a list of the strings that were read,
with empty strings removed."
  (let* ((crm-current-separator
          (if (consp crm-separator)
              (car crm-separator)
            crm-separator))
         (crm-canonical-separator (cdr-safe crm-separator))
         (string (minibuffer-with-setup-hook
                     (lambda ()
                       (setq-local history-add-new-input nil)
                       (completing-read-multiple-mode))
                   (completing-read prompt (apply-partially #'crm--table table)
                                    predicate require-match initial-input hist
                                    def inherit-input-method)))
         (results (split-string string crm-current-separator t)))
    (when-let ((hist-var (and history-add-new-input
                              hist (not (eq hist t))
                              (if (consp hist) (car hist) hist))))
      (dolist (res results) (add-to-history hist-var res)))
    results))

(provide 'crm)

;;; crm.el ends here
