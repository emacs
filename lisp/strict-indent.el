;;; strict-indent.el --- Keep code indented at all times  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'track-changes)

(defvar-local strict-indent--change-tracker nil)

(defun strict-indent-region (beg end &optional _bef-len)
  (let (b e)
    (save-excursion
      (goto-char beg)
      (beginning-of-defun)
      (setq b (point))
      (goto-char end)
      (end-of-defun)
      (setq e (point)))
    (indent-region b e)))

;;;###autoload
(define-minor-mode strict-indent-mode
  "Keep code indented as you edit."
  :lighter nil
  (if strict-indent-mode
      (unless strict-indent--change-tracker
        (setq strict-indent--change-tracker
              (track-changes-register
               (lambda (id) (track-changes-fetch id #'strict-indent-region))
               :nobefore t)))
    (when strict-indent--change-tracker
      (track-changes-unregister strict-indent--change-tracker)
      (setq strict-indent--change-tracker nil))))

(provide 'strict-indent)
;;; strict-indent.el ends here
