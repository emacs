;;; minibuffer.el --- Minibuffer and completion functions -*- lexical-binding: t -*-

;; Copyright (C) 2008-2025 Free Software Foundation, Inc.

;; Author: Stefan Monnier <monnier@iro.umontreal.ca>
;; Package: emacs

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Names with "--" are for functions and variables that are meant to be for
;; internal use only.

;; Functional completion tables have an extended calling conventions:
;; The `action' can be (additionally to nil, t, and lambda) of the form
;; - (boundaries . SUFFIX) in which case it should return
;;   (boundaries START . END).  See `completion-boundaries'.
;;   Any other return value should be ignored (so we ignore values returned
;;   from completion tables that don't know about this new `action' form).
;; - `metadata' in which case it should return (metadata . ALIST) where
;;   ALIST is the metadata of this table.  See `completion-metadata'.
;;   Any other return value should be ignored (so we ignore values returned
;;   from completion tables that don't know about this new `action' form).

;;; Bugs:

;; - choose-completion can't automatically figure out the boundaries
;;   corresponding to the displayed completions because we only
;;   provide the start info but not the end info in
;;   completion-base-position.
;; - C-x C-f ~/*/sr ? should not list "~/./src".
;; - minibuffer-force-complete completes ~/src/emacs/t<!>/lisp/minibuffer.el
;;   to ~/src/emacs/trunk/ and throws away lisp/minibuffer.el.

;;; Todo:

;; - Make *Completions* readable even if some of the completion
;;   entries have LF chars or spaces in them (including at
;;   beginning/end) or are very long.
;; - case-sensitivity currently confuses two issues:
;;   - whether or not a particular completion table should be case-sensitive
;;     (i.e. whether strings that differ only by case are semantically
;;     equivalent)
;;   - whether the user wants completion to pay attention to case.
;;   e.g. we may want to make it possible for the user to say "first try
;;   completion case-sensitively, and if that fails, try to ignore case".
;;   Maybe the trick is that we should distinguish completion-ignore-case in
;;   try/all-completions (obey user's preference) from its use in
;;   test-completion (obey the underlying object's semantics).
;; - add support for ** to pcm.
;; - Add vc-file-name-completion-table to read-file-name-internal.

;;; Code:

(eval-when-compile (require 'cl-lib))

(declare-function widget-put "wid-edit" (widget property value))

;;; Completion table manipulation

;; New completion-table operation.
(defun completion-boundaries (string collection pred suffix)
  "Return the boundaries of text on which COLLECTION will operate.
STRING is the string on which completion will be performed.
SUFFIX is the string after point.
If COLLECTION is a function, it is called with 3 arguments: STRING,
PRED, and a cons cell of the form (boundaries . SUFFIX).

The result is of the form (START . END) where START is the position
in STRING of the beginning of the completion field and END is the position
in SUFFIX of the end of the completion field.
E.g. for simple completion tables, the result is always (0 . (length SUFFIX))
and for file names the result is the positions delimited by
the closest directory separators."
  (let ((boundaries (if (functionp collection)
                        (funcall collection string pred
                                 (cons 'boundaries suffix)))))
    (if (not (eq (car-safe boundaries) 'boundaries))
        (setq boundaries nil))
    (cons (or (cadr boundaries) 0)
          (or (cddr boundaries) (length suffix)))))

(defun completion-metadata (string table pred)
  "Return the metadata of elements to complete at the end of STRING.
This metadata is an alist.  Currently understood keys are:
- `category': the kind of objects returned by `all-completions'.
   Used by `completion-category-overrides'.
- `annotation-function': function to add annotations in *Completions*.
   Takes one argument (STRING), which is a possible completion and
   returns a string to append to STRING.
- `affixation-function': function to prepend/append a prefix/suffix to
   entries.  Takes one argument (COMPLETIONS) and should return a list
   of annotated completions.  The elements of the list must be
   three-element lists: completion, its prefix and suffix.  This
   function takes priority over `annotation-function' when both are
   provided, so only this function is used.
- `group-function': function for grouping the completion candidates.
   Takes two arguments: a completion candidate (COMPLETION) and a
   boolean flag (TRANSFORM).  If TRANSFORM is nil, the function
   returns the group title of the group to which the candidate
   belongs.  The returned title may be nil.  Otherwise the function
   returns the transformed candidate.  The transformation can remove a
   redundant prefix, which is displayed in the group title.
- `sort-function': function to sort completion candidates.
   Takes one argument (COMPLETIONS) and should return a new list
   of completions.  Can operate destructively.
- `narrow-completions-function': function for
  narrowing (restricting) the completions list.  This function
  overrides `minibuffer-narrow-completions-function', which see.
- `adjust-base-function': function used to adjust the completion
  base, which is the part of the minibuffer input that is elided
  from all completion candidates, such as the directory part
  during file name completion.  This function should take one
  argument, the original completion base, and return a new base.
The metadata of a completion table should be constant between two boundaries."
  (let ((metadata (if (functionp table)
                      (funcall table string pred 'metadata))))
    (cons 'metadata
          (if (eq (car-safe metadata) 'metadata)
              (cdr metadata)))))

(defun completion--field-metadata (field-start)
  (completion-metadata (buffer-substring-no-properties field-start (point))
                       minibuffer-completion-table
                       minibuffer-completion-predicate))

(defun completion--metadata-get-1 (metadata prop)
  (or (alist-get prop metadata)
      (plist-get completion-extra-properties
                 ;; Cache the keyword
                 (or (get prop 'completion-extra-properties--keyword)
                     (put prop 'completion-extra-properties--keyword
                          (intern (concat ":" (symbol-name prop))))))))

(defun completion-metadata-get (metadata prop)
  "Get property PROP from completion METADATA.
If the metadata specifies a completion category, the variables
`completion-category-overrides' and
`completion-category-defaults' take precedence for
category-specific overrides.  If the completion metadata does not
specify the property, the `completion-extra-properties' plist is
consulted.  Note that the keys of the
`completion-extra-properties' plist are keyword symbols, not
plain symbols."
  (let ((cat (completion--metadata-get-1 metadata 'category)))
    (cond
     ((eq prop 'category) cat)
     ((eq prop 'sort-function)
      (or (cdr (completion--category-override cat 'sort-function))
          (completion--metadata-get-1 metadata 'sort-function)
          (cdr (completion--category-override cat 'cycle-sort-function))
          (completion--metadata-get-1 metadata 'cycle-sort-function)
          (cdr (completion--category-override cat 'display-sort-function))
          (completion--metadata-get-1 metadata 'display-sort-function)))
     (t (or (cdr (completion--category-override cat prop))
            (completion--metadata-get-1 metadata prop))))))

(defun complete-with-action (action collection string predicate)
  "Perform completion according to ACTION.
STRING, COLLECTION and PREDICATE are used as in `try-completion'.

If COLLECTION is a function, it will be called directly to
perform completion, no matter what ACTION is.

If ACTION is `metadata' or a list where the first element is
`boundaries', return nil.  If ACTION is nil, this function works
like `try-completion'; if it is t, this function works like
`all-completion'; and any other value makes it work like
`test-completion'."
  (cond
   ((functionp collection) (funcall collection string predicate action))
   ((eq (car-safe action) 'boundaries) nil)
   ((eq action 'metadata) nil)
   (t
    (funcall
     (cond
      ((null action) 'try-completion)
      ((eq action t) 'all-completions)
      (t 'test-completion))
     string collection predicate))))

(defun completion-table-dynamic (fun &optional switch-buffer)
  "Use function FUN as a dynamic completion table.
FUN is called with one argument, the string for which completion is requested,
and it should return a completion table containing all the intended possible
completions.
This table is allowed to include elements that do not actually match the
string: they will be automatically filtered out.
The completion table returned by FUN can use any of the usual formats of
completion tables such as lists, alists, and hash-tables.

If SWITCH-BUFFER is non-nil and completion is performed in the
minibuffer, FUN will be called in the buffer from which the minibuffer
was entered.

The result of the `completion-table-dynamic' form is a function
that can be used as the COLLECTION argument to `try-completion' and
`all-completions'.  See Info node `(elisp)Programmed Completion'.
The completion table returned by `completion-table-dynamic' has empty
metadata and trivial boundaries.

See also the related function `completion-table-with-cache'."
  (lambda (string pred action)
    (if (or (eq (car-safe action) 'boundaries) (eq action 'metadata))
        ;; `fun' is not supposed to return another function but a plain old
        ;; completion table, whose boundaries are always trivial.
        nil
      (with-current-buffer (if (not switch-buffer) (current-buffer)
                             (let ((win (minibuffer-selected-window)))
                               (if (window-live-p win) (window-buffer win)
                                 (current-buffer))))
        (complete-with-action action (funcall fun string) string pred)))))

(defun completion-table-with-cache (fun &optional ignore-case)
  "Create dynamic completion table from function FUN, with cache.
This is a wrapper for `completion-table-dynamic' that saves the last
argument-result pair from FUN, so that several lookups with the
same argument (or with an argument that starts with the first one)
only need to call FUN once.  This can be useful when FUN performs a
relatively slow operation, such as calling an external process.

When IGNORE-CASE is non-nil, FUN is expected to be case-insensitive."
  ;; See eg bug#11906.
  (let* (last-arg last-result
         (new-fun
          (lambda (arg)
            (if (and last-arg (string-prefix-p last-arg arg ignore-case))
                last-result
              (prog1
                  (setq last-result (funcall fun arg))
                (setq last-arg arg))))))
    (completion-table-dynamic new-fun)))

(defmacro lazy-completion-table (var fun)
  "Initialize variable VAR as a lazy completion table.
If the completion table VAR is used for the first time (e.g., by passing VAR
as an argument to `try-completion'), the function FUN is called with no
arguments.  FUN must return the completion table that will be stored in VAR.
If completion is requested in the minibuffer, FUN will be called in the buffer
from which the minibuffer was entered.  The return value of
`lazy-completion-table' must be used to initialize the value of VAR.

You should give VAR a non-nil `risky-local-variable' property."
  (declare (debug (symbolp lambda-expr)))
  (let ((str (make-symbol "string")))
    `(completion-table-dynamic
      (lambda (,str)
        (when (functionp ,var)
          (setq ,var (funcall #',fun)))
        ,var)
      'do-switch-buffer)))

(defun completion-table-case-fold (table &optional dont-fold)
  "Return new completion TABLE that is case insensitive.
If DONT-FOLD is non-nil, return a completion table that is
case sensitive instead."
  (lambda (string pred action)
    (let ((completion-ignore-case (not dont-fold)))
      (complete-with-action action table string pred))))

(defun completion-table-subvert (table s1 s2)
  "Return a completion table from TABLE with S1 replaced by S2.
The result is a completion table which completes strings of the
form (concat S1 S) in the same way as TABLE completes strings of
the form (concat S2 S)."
  (lambda (string pred action)
    (let* ((str (if (string-prefix-p s1 string completion-ignore-case)
                    (concat s2 (substring string (length s1)))))
           (res (if str (complete-with-action action table str pred))))
      (when (or res (eq (car-safe action) 'boundaries))
        (cond
         ((eq (car-safe action) 'boundaries)
          (let ((beg (or (and (eq (car-safe res) 'boundaries) (cadr res)) 0)))
            `(boundaries
              ,(min (length string)
                    (max (length s1)
                         (+ beg (- (length s1) (length s2)))))
              . ,(and (eq (car-safe res) 'boundaries) (cddr res)))))
         ((stringp res)
          (if (string-prefix-p s2 res completion-ignore-case)
              (concat s1 (substring res (length s2)))))
         ((eq action t)
          (let ((bounds (completion-boundaries str table pred "")))
            (if (>= (car bounds) (length s2))
                res
              (let ((re (concat "\\`"
                                (regexp-quote (substring s2 (car bounds))))))
                (delq nil
                      (mapcar (lambda (c)
                                (if (string-match re c)
                                    (substring c (match-end 0))))
                              res))))))
         ;; E.g. action=nil and it's the only completion.
         (res))))))

(defun completion-table-with-context (prefix table string pred action)
  ;; TODO: add `suffix' maybe?
  (let ((pred
         (if (not (functionp pred))
             ;; Notice that `pred' may not be a function in some abusive cases.
             pred
           ;; Predicates are called differently depending on the nature of
           ;; the completion table :-(
           (cond
            ((obarrayp table)
             (lambda (sym) (funcall pred (concat prefix (symbol-name sym)))))
            ((hash-table-p table)
             (lambda (s _v) (funcall pred (concat prefix s))))
            ((functionp table)
             (lambda (s) (funcall pred (concat prefix s))))
            (t                          ;Lists and alists.
             (lambda (s)
               (funcall pred (concat prefix (if (consp s) (car s) s)))))))))
    (if (eq (car-safe action) 'boundaries)
        (let* ((len (length prefix))
               (bound (completion-boundaries string table pred (cdr action))))
          `(boundaries ,(+ (car bound) len) . ,(cdr bound)))
      (let ((comp (complete-with-action action table string pred)))
        (cond
         ;; In case of try-completion, add the prefix.
         ((stringp comp) (concat prefix comp))
         (t comp))))))

(defun completion-table-with-terminator (terminator table string pred action)
  "Construct a completion table like TABLE but with an extra TERMINATOR.
This is meant to be called in a curried way by first passing TERMINATOR
and TABLE only (via `apply-partially').
TABLE is a completion table, and TERMINATOR is a string appended to TABLE's
completion if it is complete.  TERMINATOR is also used to determine the
completion suffix's boundary.
TERMINATOR can also be a cons cell (TERMINATOR . TERMINATOR-REGEXP)
in which case TERMINATOR-REGEXP is a regular expression whose submatch
number 1 should match TERMINATOR.  This is used when there is a need to
distinguish occurrences of the TERMINATOR strings which are really terminators
from others (e.g. escaped).  In this form, the car of TERMINATOR can also be,
instead of a string, a function that takes the completion and returns the
\"terminated\" string."
  ;; FIXME: This implementation is not right since it only adds the terminator
  ;; in try-completion, so any completion-style that builds the completion via
  ;; all-completions won't get the terminator, and selecting an entry in
  ;; *Completions* won't get the terminator added either.
  (cond
   ((eq (car-safe action) 'boundaries)
    (let* ((suffix (cdr action))
           (bounds (completion-boundaries string table pred suffix))
           (terminator-regexp (if (consp terminator)
                                  (cdr terminator) (regexp-quote terminator)))
           (max (and terminator-regexp
                     (string-match terminator-regexp suffix))))
      `(boundaries ,(car bounds)
                   . ,(min (cdr bounds) (or max (length suffix))))))
   ((eq action nil)
    (let ((comp (try-completion string table pred)))
      (if (consp terminator) (setq terminator (car terminator)))
      (if (eq comp t)
          (if (functionp terminator)
              (funcall terminator string)
            (concat string terminator))
        (if (and (stringp comp) (not (zerop (length comp)))
                 ;; Try to avoid the second call to try-completion, since
                 ;; it may be very inefficient (because `comp' made us
                 ;; jump to a new boundary, so we complete in that
                 ;; boundary with an empty start string).
                 (let ((newbounds (completion-boundaries comp table pred "")))
                   (< (car newbounds) (length comp)))
                 (eq (try-completion comp table pred) t))
            (if (functionp terminator)
                (funcall terminator comp)
              (concat comp terminator))
          comp))))
   ;; completion-table-with-terminator is always used for
   ;; "sub-completions" so it's only called if the terminator is missing,
   ;; in which case `test-completion' should return nil.
   ((eq action 'lambda) nil)
   (t
    ;; FIXME: We generally want the `try' and `all' behaviors to be
    ;; consistent so pcm can merge the `all' output to get the `try' output,
    ;; but that sometimes clashes with the need for `all' output to look
    ;; good in *Completions*.
    ;; (mapcar (lambda (s) (concat s terminator))
    ;;         (all-completions string table pred))))
    (complete-with-action action table string pred))))

(defun completion-table-with-predicate (table pred1 strict string pred2 action)
  "Make a completion table equivalent to TABLE but filtered through PRED1.
PRED1 is a function of one argument which returns non-nil if and
only if the argument is an element of TABLE which should be
considered for completion.  STRING, PRED2, and ACTION are the
usual arguments to completion tables, as described in
`try-completion', `all-completions', and `test-completion'.  If
STRICT is non-nil, the predicate always applies; if nil it only
applies if it does not reduce the set of possible completions to
nothing.  Note: TABLE needs to be a proper completion table which
obeys predicates."
  (cond
   ((and (not strict) (eq action 'lambda))
    ;; Ignore pred1 since it doesn't really have to apply anyway.
    (test-completion string table pred2))
   (t
    (or (complete-with-action action table string
                              (if (not (and pred1 pred2))
                                  (or pred1 pred2)
                                (lambda (x)
                                  ;; Call `pred1' first, so that `pred2'
                                  ;; really can't tell that `x' is in table.
                                  (and (funcall pred1 x) (funcall pred2 x)))))
        ;; If completion failed and we're not applying pred1 strictly, try
        ;; again without pred1.
        (and (not strict) pred1
             (complete-with-action action table string pred2))))))

(defun completion-table-in-turn (&rest tables)
  "Create a completion table that tries each table in TABLES in turn."
  ;; FIXME: the boundaries may come from TABLE1 even when the completion list
  ;; is returned by TABLE2 (because TABLE1 returned an empty list).
  ;; Same potential problem if any of the tables use quoting.
  (lambda (string pred action)
    (seq-some (lambda (table)
                (complete-with-action action table string pred))
              tables)))

(defun completion-table-merge (&rest tables)
  "Create a completion table that collects completions from all TABLES."
  ;; FIXME: same caveats as in `completion-table-in-turn'.
  (lambda (string pred action)
    (cond
     ((null action)
      (let ((retvals (mapcar (lambda (table)
                               (try-completion string table pred))
                             tables)))
        (if (member string retvals)
            string
          (try-completion string
                          (mapcar (lambda (value)
                                    (if (eq value t) string value))
                                  (delq nil retvals))
                          pred))))
     ((eq action t)
      (apply #'append (mapcar (lambda (table)
                                (all-completions string table pred))
                              tables)))
     (t
      (seq-some (lambda (table)
                  (complete-with-action action table string pred))
                tables)))))

(defun completion-table-with-quoting (table unquote requote)
  ;; A difficult part of completion-with-quoting is to map positions in the
  ;; quoted string to equivalent positions in the unquoted string and
  ;; vice-versa.  There is no efficient and reliable algorithm that works for
  ;; arbitrary quote and unquote functions.
  ;; So to map from quoted positions to unquoted positions, we simply assume
  ;; that `concat' and `unquote' commute (which tends to be the case).
  ;; And we ask `requote' to do the work of mapping from unquoted positions
  ;; back to quoted positions.
  ;; FIXME: For some forms of "quoting" such as the truncation behavior of
  ;; substitute-in-file-name, it would be desirable not to requote completely.
  "Return a new completion table operating on quoted text.
TABLE operates on the unquoted text.
UNQUOTE is a function that takes a string and returns a new unquoted string.
REQUOTE is a function of 2 args (UPOS QSTR) where
  QSTR is a string entered by the user (and hence indicating
  the user's preferred form of quoting); and
  UPOS is a position within the unquoted form of QSTR.
REQUOTE should return a pair (QPOS . QFUN) such that QPOS is the
position corresponding to UPOS but in QSTR, and QFUN is a function
of one argument (a string) which returns that argument appropriately quoted
for use at QPOS."
  ;; FIXME: One problem with the current setup is that `qfun' doesn't know if
  ;; its argument is "the end of the completion", so if the quoting used double
  ;; quotes (for example), we end up completing "fo" to "foobar and throwing
  ;; away the closing double quote.
  (lambda (string pred action)
    (cond
     ((eq action 'metadata)
      (append (completion-metadata string table pred)
              '((completion--unquote-requote . t))))

     ((eq action 'lambda) ;;test-completion
      (let ((ustring (funcall unquote string)))
        (test-completion ustring table pred)))

     ((eq (car-safe action) 'boundaries)
      (let* ((ustring (funcall unquote string))
             (qsuffix (cdr action))
             (ufull (if (zerop (length qsuffix)) ustring
                      (funcall unquote (concat string qsuffix))))
             ;; If (not (string-prefix-p ustring ufull)) we have a problem:
             ;; unquoting the qfull gives something "unrelated" to ustring.
             ;; E.g. "~/" and "/" where "~//" gets unquoted to just "/" (see
             ;; bug#47678).
             ;; In that case we can't even tell if we're right before the
             ;; "/" or right after it (aka if this "/" is from qstring or
             ;; from qsuffix), thus which usuffix to use is very unclear.
             (usuffix (if (string-prefix-p ustring ufull)
                          (substring ufull (length ustring))
                        ;; FIXME: Maybe "" is preferable/safer?
                        qsuffix))
             (boundaries (completion-boundaries ustring table pred usuffix))
             (qlboundary (car (funcall requote (car boundaries) string)))
             (qrboundary (if (zerop (cdr boundaries)) 0 ;Common case.
                           (let* ((urfullboundary
                                   (+ (cdr boundaries) (length ustring))))
                             (- (car (funcall requote urfullboundary
                                              (concat string qsuffix)))
                                (length string))))))
        `(boundaries ,qlboundary . ,qrboundary)))

     ;; In "normal" use a c-t-with-quoting completion table should never be
     ;; called with action in (t nil) because `completion--unquote' should have
     ;; been called before and would have returned a different completion table
     ;; to apply to the unquoted text.  But there's still a lot of code around
     ;; that likes to use all/try-completions directly, so we do our best to
     ;; handle those calls as well as we can.

     ((eq action nil) ;;try-completion
      (let* ((ustring (funcall unquote string))
             (completion (try-completion ustring table pred)))
        ;; Most forms of quoting allow several ways to quote the same string.
        ;; So here we could simply requote `completion' in a kind of
        ;; "canonical" quoted form without paying attention to the way
        ;; `string' was quoted.  But since we have to solve the more complex
        ;; problems of "pay attention to the original quoting" for
        ;; all-completions, we may as well use it here, since it provides
        ;; a nicer behavior.
        (if (not (stringp completion)) completion
          (car (completion--twq-try
                string ustring completion 0 unquote requote)))))

     ((eq action t) ;;all-completions
      ;; When all-completions is used for completion-try/all-completions
      ;; (e.g. for `pcm' style), we can't do the job properly here because
      ;; the caller will match our output against some pattern derived from
      ;; the user's (quoted) input, and we don't have access to that
      ;; pattern, so we can't know how to requote our output so that it
      ;; matches the quoting used in the pattern.  It is to fix this
      ;; fundamental problem that we have to introduce the new
      ;; unquote-requote method so that completion-try/all-completions can
      ;; pass the unquoted string to the style functions.
      (pcase-let*
          ((ustring (funcall unquote string))
           (completions (all-completions ustring table pred))
           (boundary (car (completion-boundaries ustring table pred "")))
           (completions
            (completion--twq-all
             string ustring completions boundary unquote requote))
           (last (last completions)))
        (when (consp last) (setcdr last nil))
        completions))

     ((eq action 'completion--unquote)
      ;; PRED is really a POINT in STRING.
      ;; We should return a new set (STRING TABLE POINT REQUOTE)
      ;; where STRING is a new (unquoted) STRING to match against the new TABLE
      ;; using a new POINT inside it, and REQUOTE is a requoting function which
      ;; should reverse the unquoting, (i.e. it receives the completion result
      ;; of using the new TABLE and should turn it into the corresponding
      ;; quoted result).
      (let* ((qpos pred)
	     (ustring (funcall unquote string))
	     (uprefix (funcall unquote (substring string 0 qpos)))
	     ;; FIXME: we really should pass `qpos' to `unquote' and have that
	     ;; function give us the corresponding `uqpos'.  But for now we
	     ;; presume (more or less) that `concat' and `unquote' commute.
	     (uqpos (if (string-prefix-p uprefix ustring)
			;; Yay!!  They do seem to commute!
			(length uprefix)
		      ;; They don't commute this time!  :-(
		      ;; Maybe qpos is in some text that disappears in the
		      ;; ustring (bug#17239).  Let's try a second chance guess.
		      (let ((usuffix (funcall unquote (substring string qpos))))
			(if (string-suffix-p usuffix ustring)
			    ;; Yay!!  They still "commute" in a sense!
			    (- (length ustring) (length usuffix))
			  ;; Still no luck!  Let's just choose *some* position
			  ;; within ustring.
			  (/ (+ (min (length uprefix) (length ustring))
				(max (- (length ustring) (length usuffix)) 0))
			     2))))))
        (list ustring table uqpos
              (lambda (unquoted-result op)
                (pcase op
                  (1 ;;try
                   (if (not (stringp (car-safe unquoted-result)))
                       unquoted-result
                     (completion--twq-try
                      string ustring
                      (car unquoted-result) (cdr unquoted-result)
                      unquote requote)))
                  (2 ;;all
                   (let* ((last (last unquoted-result))
                          (base (or (cdr last) 0)))
                     (when last
                       (setcdr last nil)
                       (completion--twq-all string ustring
                                            unquoted-result base
                                            unquote requote))))))))))))

(defun completion-table-with-metadata (table metadata)
  "Return completion TABLE with additional METADATA.

METADATA is a completion metatdata alist.  See
`completion-metadata' for a description of its possible values.
METADATA can also be a function that takes two arguments, STRING
and PRED, and returns a metadata alist appropriate for completing
STRING subject to predicate PRED.

METADATA takes precedence over any metadata that TABLE provides."
  (let ((md-fun (if (functionp metadata)
                    metadata
                  (lambda (&rest _) metadata))))
    (lambda (string pred action)
      (cond
       ((eq action 'metadata)
        (cons 'metadata
              (append (funcall md-fun string pred)
                      (cdr-safe (completion-metadata string table pred)))))
       ((eq (car-safe action) 'boundaries)
        (completion-boundaries string table pred (cdr action)))
       (t
        (complete-with-action action table string pred))))))

(defun completion--twq-try (string ustring completion point
                                   unquote requote)
  ;; Basically two cases: either the new result is
  ;; - commonprefix1 <point> morecommonprefix <qpos> suffix
  ;; - commonprefix <qpos> newprefix <point> suffix
  (pcase-let*
      ((prefix (fill-common-string-prefix ustring completion))
       (suffix (substring completion (max point (length prefix))))
       (`(,qpos . ,qfun) (funcall requote (length prefix) string))
       (qstr1 (if (> point (length prefix))
                  (funcall qfun (substring completion (length prefix) point))))
       (qsuffix (funcall qfun suffix))
       (qstring (concat (substring string 0 qpos) qstr1 qsuffix))
       (qpoint
        (cond
         ((zerop point) 0)
         ((> point (length prefix)) (+ qpos (length qstr1)))
         (t (car (funcall requote point string))))))
    ;; Make sure `requote' worked.
    (if (equal (funcall unquote qstring) completion)
	(cons qstring qpoint)
      ;; If requote failed (e.g. because sifn-requote did not handle
      ;; Tramp's "/foo:/bar//baz -> /foo:/baz" truncation), then at least
      ;; try requote properly.
      (let ((qstr (funcall qfun completion)))
	(cons qstr (length qstr))))))

(defun completion--twq-all (string ustring completions boundary
                                   _unquote requote)
  (when completions
    (pcase-let*
        ((prefix
          (try-completion "" (cons (substring ustring boundary)
                                   completions)))
         (`(,qfullpos . ,qfun)
          (funcall requote (+ boundary (length prefix)) string))
         (qfullprefix (substring string 0 qfullpos))
	 ;; FIXME: This assertion can be wrong, e.g. in Cygwin, where
	 ;; (unquote "c:\bin") => "/usr/bin" but (unquote "c:\") => "/".
         ;;(cl-assert (string-equal-ignore-case
         ;;            (funcall unquote qfullprefix)
         ;;            (concat (substring ustring 0 boundary) prefix))
         ;;           t))
         (qboundary (car (funcall requote boundary string)))
         (_ (cl-assert (<= qboundary qfullpos)))
         ;; FIXME: this split/quote/concat business messes up the carefully
         ;; placed completions-common-part and completions-first-difference
         ;; faces.  We could try within the mapcar loop to search for the
         ;; boundaries of those faces, pass them to `requote' to find their
         ;; equivalent positions in the quoted output and re-add the faces:
         ;; this might actually lead to correct results but would be
         ;; pretty expensive.
         ;; The better solution is to not quote the *Completions* display,
         ;; which nicely circumvents the problem.  The solution I used here
         ;; instead is to hope that `qfun' preserves the text-properties and
         ;; presume that the `first-difference' is not within the `prefix';
         ;; this presumption is not always true, but at least in practice it is
         ;; true in most cases.
         (qprefix (propertize (substring qfullprefix qboundary)
                              'face 'completions-common-part)))

      ;; Here we choose to quote all elements returned, but a better option
      ;; would be to return unquoted elements together with a function to
      ;; requote them, so that *Completions* can show nicer unquoted values
      ;; which only get quoted when needed by choose-completion.
      (nconc
       (mapcar (lambda (completion)
                 (cl-assert (string-prefix-p prefix completion 'ignore-case) t)
                 (let* ((new (substring completion (length prefix)))
                        (qnew (funcall qfun new))
 			(qprefix
                         (if (not completion-ignore-case)
                             qprefix
                           ;; Make qprefix inherit the case from `completion'.
                           (let* ((rest (substring completion
                                                   0 (length prefix)))
                                  (qrest (funcall qfun rest)))
                             (if (string-equal-ignore-case qprefix qrest)
                                 (propertize qrest 'face
                                             'completions-common-part)
                               qprefix))))
                        (qcompletion (concat qprefix qnew)))
                   ;; Some completion tables (including this one) pass
                   ;; along necessary information as text properties
                   ;; on the first character of the completion.  Make
                   ;; sure the quoted completion has these properties
                   ;; too.
                   (add-text-properties 0 1 (text-properties-at 0 completion)
                                        qcompletion)
                   ;; Attach unquoted completion string, which is needed
                   ;; to score the completion in `completion--flex-score'.
                   (put-text-property 0 1 'completion--unquoted
                                      completion qcompletion)
		   ;; FIXME: Similarly here, Cygwin's mapping trips this
		   ;; assertion.
                   ;;(cl-assert
                   ;; (string-equal-ignore-case
		   ;;  (funcall unquote
		   ;;           (concat (substring string 0 qboundary)
		   ;;                   qcompletion))
		   ;;  (concat (substring ustring 0 boundary)
		   ;;          completion))
		   ;; t)
                   qcompletion))
               completions)
       qboundary))))

;;; Minibuffer completion

(defgroup minibuffer nil
  "Controlling the behavior of the minibuffer."
  :link '(custom-manual "(emacs)Minibuffer")
  :group 'environment)

(defvar minibuffer-message-properties nil
  "Text properties added to the text shown by `minibuffer-message'.")

(defvar minibuffer--message-overlay nil)

(defvar minibuffer--message-timer nil)

(defun minibuffer--delete-message-overlay ()
  (when (overlayp minibuffer--message-overlay)
    (delete-overlay minibuffer--message-overlay)
    (setq minibuffer--message-overlay nil))
  (when (timerp minibuffer--message-timer)
    (cancel-timer minibuffer--message-timer)
    (setq minibuffer--message-timer nil))
  (remove-hook 'pre-command-hook #'minibuffer--delete-message-overlay))

(defun minibuffer-message (message &rest args)
  "Temporarily display MESSAGE at the end of minibuffer text.
This function is designed to be called from the minibuffer, i.e.,
when Emacs prompts the user for some input, and the user types
into the minibuffer.  If called when the current buffer is not
the minibuffer, this function just calls `message', and thus
displays MESSAGE in the echo-area.
When called from the minibuffer, this function displays MESSAGE
at the end of minibuffer text for `minibuffer-message-timeout'
seconds, or until the next input event arrives, whichever comes first.
It encloses MESSAGE in [...] if it is not yet enclosed.
The intent is to show the message without hiding what the user typed.
If ARGS are provided, then the function first passes MESSAGE
through `format-message'.
If some of the minibuffer text has the `minibuffer-message' text
property, MESSAGE is shown at that position instead of EOB."
  (if (not (minibufferp (current-buffer) t))
      (if args
          (apply #'message message args)
        (message "%s" message))
    ;; Clear out any old echo-area message to make way for our new thing.
    (minibuffer--delete-message-overlay)
    (message nil)
    (setq message (if (and (null args)
                           (string-match-p "\\` *\\[.+\\]\\'" message))
                      ;; Make sure we can put-text-property.
                      (copy-sequence message)
                    (concat " [" message "]")))
    (when args (setq message (apply #'format-message message args)))
    (unless (or (null minibuffer-message-properties)
                ;; Don't overwrite the face properties the caller has set
                (text-properties-at 0 message))
      (setq message (apply #'propertize message minibuffer-message-properties)))
    ;; Put overlay either on `minibuffer-message' property, or at EOB.
    (let* ((ovpos (minibuffer--message-overlay-pos))
           (ol (make-overlay ovpos ovpos nil t t)))
      (unless (zerop (length message))
        ;; The current C cursor code doesn't know to use the overlay's
        ;; marker's stickiness to figure out whether to place the cursor
        ;; before or after the string, so let's spoon-feed it the pos.
        (put-text-property 0 1 'cursor t message))
      (overlay-put ol 'after-string message)
      ;; Make sure the overlay with the message is displayed before
      ;; any other overlays in that position, in case they have
      ;; resize-mini-windows set to nil and the other overlay strings
      ;; are too long for the mini-window width.  This makes sure the
      ;; temporary message will always be visible.
      (overlay-put ol 'priority 1100)
      (setq minibuffer--message-overlay ol
            minibuffer--message-timer
            (run-at-time (or minibuffer-message-timeout 1000000) nil
                         #'minibuffer--delete-message-overlay))
      (add-hook 'pre-command-hook #'minibuffer--delete-message-overlay))))

(defcustom minibuffer-message-clear-timeout nil
  "How long to display an echo-area message when the minibuffer is active.
If the value is a number, it is the time in seconds after which to
remove the echo-area message from the active minibuffer.
If the value is not a number, such messages are never removed,
and their text is displayed until the next input event arrives.
Unlike `minibuffer-message-timeout' used by `minibuffer-message',
this option affects the pair of functions `set-minibuffer-message'
and `clear-minibuffer-message' called automatically via
`set-message-function' and `clear-message-function'."
  :type '(choice (const :tag "Never time out" nil)
                 (integer :tag "Wait for the number of seconds" 2))
  :version "27.1")

(defvar minibuffer-message-timer nil)
(defvar minibuffer-message-overlay nil)

(defun minibuffer--message-overlay-pos ()
  "Return position where minibuffer message functions shall put message overlay.
The minibuffer message functions include `minibuffer-message' and
`set-minibuffer-message'."
  ;; Starting from point, look for non-nil `minibuffer-message'
  ;; property, and return its position.  If none found, return the EOB
  ;; position.
  (let* ((pt (point))
         (propval (get-text-property pt 'minibuffer-message)))
    (if propval pt
      (next-single-property-change pt 'minibuffer-message nil (point-max)))))

(defun set-minibuffer-message (message)
  "Temporarily display MESSAGE at the end of the active minibuffer window.
If some part of the minibuffer text has the `minibuffer-message' property,
the message will be displayed before the first such character, instead of
at the end of the minibuffer.
The text is displayed for `minibuffer-message-clear-timeout' seconds
\(if the value is a number), or until the next input event arrives,
whichever comes first.
Unlike `minibuffer-message', this function is called automatically
via `set-message-function'."
  (let* ((minibuf-window (active-minibuffer-window))
         (minibuf-frame (and (window-live-p minibuf-window)
                             (window-frame minibuf-window))))
    (when (and (not noninteractive)
               (window-live-p minibuf-window)
               (or (eq (window-frame) minibuf-frame)
                   (eq (frame-parameter minibuf-frame 'minibuffer) 'only)))
      (with-current-buffer (window-buffer minibuf-window)
        (setq message (if (string-match-p "\\` *\\[.+\\]\\'" message)
                          ;; Make sure we can put-text-property.
                          (copy-sequence message)
                        (concat " [" message "]")))
        (unless (or (null minibuffer-message-properties)
                    ;; Don't overwrite the face properties the caller has set
                    (text-properties-at 0 message))
          (setq message
                (apply #'propertize message minibuffer-message-properties)))

        (clear-minibuffer-message)

        (let ((ovpos (minibuffer--message-overlay-pos)))
          (setq minibuffer-message-overlay
                (make-overlay ovpos ovpos nil t t)))
        (unless (zerop (length message))
          ;; The current C cursor code doesn't know to use the overlay's
          ;; marker's stickiness to figure out whether to place the cursor
          ;; before or after the string, so let's spoon-feed it the pos.
          (put-text-property 0 1 'cursor t message))
        (overlay-put minibuffer-message-overlay 'after-string message)
        ;; Make sure the overlay with the message is displayed before
        ;; any other overlays in that position, in case they have
        ;; resize-mini-windows set to nil and the other overlay strings
        ;; are too long for the mini-window width.  This makes sure the
        ;; temporary message will always be visible.
        (overlay-put minibuffer-message-overlay 'priority 1100)

        (when (numberp minibuffer-message-clear-timeout)
          (setq minibuffer-message-timer
                (run-with-timer minibuffer-message-clear-timeout nil
                                #'clear-minibuffer-message)))

        ;; Return t telling the caller that the message
        ;; was handled specially by this function.
        t))))

(setq set-message-function 'set-message-functions)

(defcustom set-message-functions '(set-minibuffer-message)
  "List of functions to handle display of echo-area messages.
Each function is called with one argument that is the text of a message.
If a function returns nil, a previous message string is given to the
next function in the list, and if the last function returns nil, the
last message string is displayed in the echo area.
If a function returns a string, the returned string is given to the
next function in the list, and if the last function returns a string,
it's displayed in the echo area.
If a function returns any other non-nil value, no more functions are
called from the list, and no message will be displayed in the echo area.

Useful functions to add to this list are:

 `inhibit-message'        -- if this function is the first in the list,
                             messages that match the value of
                             `inhibit-message-regexps' will be suppressed.
 `set-multi-message'      -- accumulate multiple messages and display them
                             together as a single message.
 `set-minibuffer-message' -- if the minibuffer is active, display the
                             message at the end of the minibuffer text
                             (this is the default)."
  :type '(choice (const :tag "No special message handling" nil)
                 (repeat
                  (choice (function-item :tag "Inhibit some messages"
                                         inhibit-message)
                          (function-item :tag "Accumulate messages"
                                         set-multi-message)
                          (function-item :tag "Handle minibuffer"
                                         set-minibuffer-message)
                          (function :tag "Custom function"))))
  :version "29.1")

(defun set-message-functions (message)
  (run-hook-wrapped 'set-message-functions
                    (lambda (fun)
                      (when (stringp message)
                        (let ((ret (funcall fun message)))
                          (when ret (setq message ret))))
                      nil))
  message)

(defcustom inhibit-message-regexps nil
  "List of regexps that inhibit messages by the function `inhibit-message'.
When the list in `set-message-functions' has `inhibit-message' as its
first element, echo-area messages which match the value of this variable
will not be displayed."
  :type '(repeat regexp)
  :version "29.1")

(defun inhibit-message (message)
  "Don't display MESSAGE when it matches the regexp `inhibit-message-regexps'.
This function is intended to be added to `set-message-functions'.
To suppress display of echo-area messages that match `inhibit-message-regexps',
make this function be the first element of `set-message-functions'."
  (or (and (consp inhibit-message-regexps)
           (string-match-p (mapconcat #'identity inhibit-message-regexps "\\|")
                           message))
      message))

(defcustom multi-message-timeout 2
  "Number of seconds between messages before clearing the accumulated list."
  :type 'number
  :version "29.1")

(defcustom multi-message-max 8
  "Max size of the list of accumulated messages."
  :type 'number
  :version "29.1")

(defvar multi-message-separator "\n")

(defvar multi-message-list nil)

(defun set-multi-message (message)
  "Return recent messages as one string to display in the echo area.
Individual messages will be separated by a newline.
Up to `multi-message-max' messages can be accumulated, and the
accumulated messages are discarded when `multi-message-timeout'
seconds have elapsed since the first message.
Note that this feature works best only when `resize-mini-windows'
is at its default value `grow-only'."
  (let ((last-message (car multi-message-list)))
    (unless (and last-message (equal message (aref last-message 1)))
      (when last-message
        (cond
         ((> (float-time) (+ (aref last-message 0) multi-message-timeout))
          (setq multi-message-list nil))
         ((or
           ;; `message-log-max' was nil, potential clutter.
           (aref last-message 2)
           ;; Remove old message that is substring of the new message
           (string-prefix-p (aref last-message 1) message))
          (setq multi-message-list (cdr multi-message-list)))))
      (push (vector (float-time) message (not message-log-max)) multi-message-list)
      (when (> (length multi-message-list) multi-message-max)
        (setf (nthcdr multi-message-max multi-message-list) nil)))
    (mapconcat (lambda (m) (aref m 1))
               (reverse multi-message-list)
               multi-message-separator)))

(defvar touch-screen-current-tool)

(defun clear-minibuffer-message ()
  "Clear message temporarily shown in the minibuffer.
Intended to be called via `clear-message-function'."
  (when (not noninteractive)
    (when (timerp minibuffer-message-timer)
      (cancel-timer minibuffer-message-timer)
      (setq minibuffer-message-timer nil))
    (when (overlayp minibuffer-message-overlay)
      (delete-overlay minibuffer-message-overlay)
      (setq minibuffer-message-overlay nil)))
  ;; Don't clear the message if touch screen drag-to-select is in
  ;; progress, because a preview message might currently be displayed
  ;; in the echo area.  FIXME: find some way to place this in
  ;; touch-screen.el.
  (if (and (bound-and-true-p touch-screen-preview-select)
           (eq (nth 3 touch-screen-current-tool) 'drag))
      'dont-clear-message
    ;; Return nil telling the caller that the message
    ;; should be also handled by the caller.
    nil))

(setq clear-message-function 'clear-minibuffer-message)

(defun minibuffer-completion-contents ()
  "Return the user input in a minibuffer before point as a string.
In Emacs 22, that was what completion commands operated on.
If the current buffer is not a minibuffer, return everything before point."
  (declare (obsolete nil "24.4"))
  (buffer-substring (minibuffer-prompt-end) (point)))

(defun delete-minibuffer-contents ()
  "Delete all user input in a minibuffer.
If the current buffer is not a minibuffer, erase its entire contents."
  (interactive "" minibuffer-mode)
  ;; We used to do `delete-field' here, but when file name shadowing
  ;; is on, the field doesn't cover the entire minibuffer contents.
  (delete-region (minibuffer-prompt-end) (point-max)))

(defun minibuffer--completion-prompt-end ()
  (let ((end (minibuffer-prompt-end)))
    (if (< (point) end)
        (user-error "Can't complete in prompt")
      end)))

(defvar completion-show-inline-help t
  "If non-nil, print helpful inline messages during completion.")

(defcustom completion-eager-display 'auto
  "Whether completion commands should display *Completions* buffer eagerly.

If the variable is set to t, completion commands show the *Completions*
buffer always immediately.  Setting the variable to nil disables the
eager *Completions* display for all commands.

For the value `auto', completion commands show the *Completions* buffer
immediately only if requested by the completion command.  Completion
tables can request eager display via the `eager-display' metadata.

See also the variables `completion-category-overrides' and
`completion-extra-properties' for the `eager-display' completion
metadata."
  :type '(choice (const :tag "Never show *Completions* eagerly" nil)
                 (const :tag "Always show *Completions* eagerly" t)
                 (const :tag "If requested by the completion command" auto))
  :version "31.1")

(defcustom completion-auto-help t
  "Non-nil means automatically provide help for invalid completion input.
If the value is t, the *Completions* buffer is displayed whenever completion
is requested but cannot be done.
If the value is `lazy', the *Completions* buffer is only displayed after
the second failed attempt to complete.
If the value is `always', the *Completions* buffer is always shown
after a completion attempt, and the list of completions is updated if
already visible.
If the value is `visible', the *Completions* buffer is displayed
whenever completion is requested but cannot be done for the first time,
but remains visible thereafter, and the list of completions in it is
updated for subsequent attempts to complete."
  :type '(choice (const :tag "Don't show" nil)
                 (const :tag "Show only when cannot complete" t)
                 (const :tag "Show after second failed completion attempt" lazy)
                 (const :tag
                        "Leave visible after first failed completion" visible)
                 (const :tag "Always visible" always)))

(defvar completion-styles-alist
  '((emacs21
     completion-emacs21-try-completion completion-emacs21-all-completions
     "Simple prefix-based completion.
I.e. when completing \"foo_bar\" (where _ is the position of point),
it will consider all completions candidates matching the glob
pattern \"foobar*\".")
    (emacs22
     completion-emacs22-try-completion completion-emacs22-all-completions
     "Prefix completion that only operates on the text before point.
I.e. when completing \"foo_bar\" (where _ is the position of point),
it will consider all completions candidates matching the glob
pattern \"foo*\" and will add back \"bar\" to the end of it.")
    (basic
     completion-basic-try-completion completion-basic-all-completions
     "Completion of the prefix before point and the suffix after point.
I.e. when completing \"foo_bar\" (where _ is the position of point),
it will consider all completions candidates matching the glob
pattern \"foo*bar*\".")
    (partial-completion
     completion-pcm-try-completion completion-pcm-all-completions
     "Completion of multiple words, each one taken as a prefix.
I.e. when completing \"l-co_h\" (where _ is the position of point),
it will consider all completions candidates matching the glob
pattern \"l*-co*h*\".
Furthermore, for completions that are done step by step in subfields,
the method is applied to all the preceding fields that do not yet match.
E.g. C-x C-f /u/mo/s TAB could complete to /usr/monnier/src.
Additionally the user can use the char \"*\" as a glob pattern.")
    (substring
     completion-substring-try-completion completion-substring-all-completions
     "Completion of the string taken as a substring.
I.e. when completing \"foo_bar\" (where _ is the position of point),
it will consider all completions candidates matching the glob
pattern \"*foo*bar*\".")
    (flex
     completion-flex-try-completion completion-flex-all-completions
     "Completion of an in-order subset of characters.
When completing \"foo\" the glob \"*f*o*o*\" is used, so that
\"foo\" can complete to \"frodo\".")
    (initials
     completion-initials-try-completion completion-initials-all-completions
     "Completion of acronyms and initialisms.
E.g. can complete M-x lch to list-command-history
and C-x C-f ~/sew to ~/src/emacs/work.")
    (shorthand
     completion-shorthand-try-completion completion-shorthand-all-completions
     "Completion of symbol shorthands setup in `read-symbol-shorthands'.
E.g. can complete \"x-foo\" to \"xavier-foo\" if the shorthand
((\"x-\" . \"xavier-\")) is set up in the buffer of origin.")
    (regexp
     completion-regexp-try-completion completion-regexp-all-completions
     "Regular expression matching."))
  "List of available completion styles.
Each element has the form (NAME TRY-COMPLETION ALL-COMPLETIONS DOC):
where NAME is the name that should be used in `completion-styles',
TRY-COMPLETION is the function that does the completion (it should
follow the same calling convention as `completion-try-completion'),
ALL-COMPLETIONS is the function that lists the completions (it should
follow the calling convention of `completion-all-completions'),
and DOC describes the way this style of completion works.")

(defun completion--update-styles-options (widget)
  "Function to keep updated the options in `completion-category-overrides'."
  (let ((lst (mapcar (lambda (x)
                       (list 'const (car x)))
		     completion-styles-alist)))
    (widget-put widget :args (mapcar #'widget-convert lst))
    widget))

(defconst completion--styles-type
  `(repeat :tag "insert a new menu to add more styles"
           (choice :convert-widget completion--update-styles-options)))

(defcustom completion-styles
  ;; First, use `basic' because prefix completion has been the standard
  ;; for "ever" and works well in most cases, so using it first
  ;; ensures that we obey previous behavior in most cases.
  '(basic
    ;; Then use `partial-completion' because it has proven to
    ;; be a very convenient extension.
    partial-completion
    ;; Finally use `emacs22' so as to maintain (in many/most cases)
    ;; the previous behavior that when completing "foobar" with point
    ;; between "foo" and "bar" the completion try to complete "foo"
    ;; and simply add "bar" to the end of the result.
    emacs22)
  "List of completion styles to use.
The available styles are listed in `completion-styles-alist'.

Note that `completion-category-overrides' may override these
styles for specific categories, such as files, buffers, etc."
  :type completion--styles-type
  :version "23.1")

(defvar completion-category-defaults
  '((buffer (styles basic substring)
            (export-function . minibuffer-export-list-buffers))
    (file (export-function . minibuffer-export-dired))
    (unicode-name (styles basic substring))
    ;; A new style that combines substring and pcm might be better,
    ;; e.g. one that does not anchor to bos.
    (project-file (styles substring))
    (dabbrev (styles basic))
    (search (styles regexp))
    (xref-location (styles substring))
    (info-menu (styles basic substring))
    (symbol-help (styles basic partial-completion shorthand substring))
    (multiple-choice (styles basic substring) (sort-function . identity))
    (calendar-month (sort-function . identity))
    (predicate-description (sort-function . identity))
    (search (sort-function . identity))
    (keybinding (sort-function . minibuffer-sort-alphabetically)
                (eager-display . t))
    (function (styles partial-completion substring)
              (sort-function . minibuffer-sort-alphabetically)
              (affixation-function . minibuffer-function-affixation))
    (library (sort-function . minibuffer-sort-alphabetically)))
  "Default settings for specific completion categories.

Each entry has the shape (CATEGORY . ALIST) where ALIST is
an association list that can specify properties such as:
- `styles': the list of `completion-styles' to use for that category.
- `sort-function': function to sort entries when cycling.
- `group-function': function for grouping the completion candidates.
- `annotation-function': function to add annotations in *Completions*.
- `affixation-function': function to prepend/append a prefix/suffix.
- `narrow-completions-function': function to restrict the completions list.

Categories are symbols such as `buffer' and `file', used when
completing buffer and file names, respectively.

Also see `completion-category-overrides'.")

(defcustom completion-category-overrides nil
  "List of category-specific user overrides for completion metadata.

Each override has the shape (CATEGORY . ALIST) where ALIST is
an association list that can specify properties such as:
- `styles': the list of `completion-styles' to use for that category.
- `sort-function': function to sort entries when cycling.
function from metadata, or if that is nil, fall back to `completions-sort';
`identity' disables sorting and keeps the original order; and other
possible values are the same as in `completions-sort'.
- `group-function': function for grouping the completion candidates.
- `annotation-function': function to add annotations in *Completions*.
- `affixation-function': function to prepend/append a prefix/suffix.
- `narrow-completions-function': function for narrowing the completions list.
See more description of metadata in `completion-metadata'.

Categories are symbols such as `buffer' and `file', used when
completing buffer and file names, respectively.

If a property in a category is specified by this variable, it
overrides the default specified in `completion-category-defaults'."
  :version "25.1"
  :type `(alist
          :key-type (choice :tag "Category"
			    (const buffer)
                            (const file)
                            (const unicode-name)
			    (const bookmark)
                            symbol)
          :value-type (set :tag "Properties to override"
	                   (cons :tag "Completion Styles"
		                 (const styles) ,completion--styles-type)
                           (cons :tag "Sort order"
                                 (const sort-function)
                                 (choice
                                  (const :tag "Default sorting" nil)
                                  (const :tag "No sorting" identity)
                                  (const :tag "Alphabetical"
                                         minibuffer-sort-alphabetically)
                                  (const :tag "Historical"
                                         minibuffer-sort-by-history)
                                  (function :tag "Custom function")))
                           (cons :tag "Grouping"
                                 (const group-function) function)
                           (cons :tag "Annotation"
                                 (const annotation-function) function)
                           (cons :tag "Affixation"
                                 (const affixation-function) function)
                           (cons :tag "Restriction"
                                 (const narrow-completions-function)
                                 function))))

(defun completion--category-override (category tag)
  (or (assq tag (cdr (assq category completion-category-overrides)))
      (assq tag (cdr (assq category completion-category-defaults)))))

(defvar completion--matching-style nil
  "Last completion style to match user input.")

(defvar completion-local-styles nil
  "List of completion styles local to the current minibuffer.

You manipulate this variable with command \
\\<minibuffer-local-completion-map>\\[minibuffer-set-completion-styles]
in the minibuffer.  When it is non-nil, it takes precedence over
the global `completion-styles' user option and the completion
styles that the completion category may prescribe.")

(defun completion--styles (metadata)
  "Return current list of completion styles, considering completion METADATA."
  (or completion-local-styles
      (let* ((cat (completion-metadata-get metadata 'category))
             (over (completion--category-override cat 'styles)))
        (if over (cdr over) completion-styles))))

(defcustom completions-exclude-exceptional-candidates t
  "Whether to exclude exceptional minibuffer completion candidates."
  :type 'boolean
  :version "30.1")

(defvar completions--only-exceptional-candidates nil
  "Whether last completion operation produced only exceptional candidates.")

(defcustom completion-ignored-extensions
  (append
   (cond ((memq system-type '(ms-dos windows-nt))
	  (mapcar 'purecopy
	          '(".o" "~" ".bin" ".bak" ".obj" ".map" ".ico" ".pif" ".lnk"
		    ".a" ".ln" ".blg" ".bbl" ".dll" ".drv" ".vxd" ".386")))
	 (t
	  (mapcar 'purecopy
	          '(".o" "~" ".bin" ".lbin" ".so"
		    ".a" ".ln" ".blg" ".bbl"))))
   (mapcar 'purecopy
           '(".elc" ".lof"
	     ".glo" ".idx" ".lot"
             "./"
             ".DS_Store"
	     ;; VCS metadata directories
	     ".svn/" ".hg/" ".git/" ".bzr/" "CVS/" "_darcs/" "_MTN/"
	     ;; TeX-related
	     ".fmt" ".tfm"
	     ;; Java compiled
	     ".class"
	     ;; CLISP
	     ".fas" ".lib" ".mem"
	     ;; CMUCL
	     ".x86f" ".sparcf"
	     ;; OpenMCL / Clozure CL
	     ".dfsl" ".pfsl" ".d64fsl" ".p64fsl" ".lx64fsl" ".lx32fsl"
	     ".dx64fsl" ".dx32fsl" ".fx64fsl" ".fx32fsl" ".sx64fsl"
	     ".sx32fsl" ".wx64fsl" ".wx32fsl"
             ;; Other CL implementations (Allegro, LispWorks)
             ".fasl" ".ufsl" ".fsl" ".dxl"
	     ;; Libtool
	     ".lo" ".la"
	     ;; Gettext
	     ".gmo" ".mo"
	     ;; Texinfo-related
	     ;; This used to contain .log, but that's commonly used for log
	     ;; files you do want to see, not just TeX stuff.  -- fx
	     ".toc" ".aux"
	     ".cp" ".fn" ".ky" ".pg" ".tp" ".vr"
	     ".cps" ".fns" ".kys" ".pgs" ".tps" ".vrs"
	     ;; Python byte-compiled
	     ".pyc" ".pyo")))
  "Completion normally ignores file names ending with strings in this list.
It does not ignore them if all possible completions end.  Completion
ignores directory names if they match any string in this list that ends
in a slash."
  :type '(repeat string)
  :version "30.1")

(defun completion--nth-completion (n string table pred point metadata)
  "Call the Nth method of completion styles."
  ;; We provide special support for quoting/unquoting here because it cannot
  ;; reliably be done within the normal completion-table routines: Completion
  ;; styles such as `substring' or `partial-completion' need to match the
  ;; output of all-completions with the user's input, and since most/all
  ;; quoting mechanisms allow several equivalent quoted forms, the
  ;; completion-style can't do this matching (e.g. `substring' doesn't know
  ;; that "\a\b\e" is a valid (quoted) substring of "label").
  ;; The quote/unquote function needs to come from the completion table (rather
  ;; than from completion-extra-properties) because it may apply only to some
  ;; part of the string (e.g. substitute-in-file-name).
  (setq completions--only-exceptional-candidates nil)
  (let* ((md (or metadata
                 (completion-metadata (substring string 0 point) table pred)))
         (requote
          (when (and
                 (completion-metadata-get md 'completion--unquote-requote)
                 ;; Sometimes a table's metadata is used on another
                 ;; table (typically that other table is just a list taken
                 ;; from the output of `all-completions' or something
                 ;; equivalent, for progressive refinement).
                 ;; See bug#28898 and bug#16274.
                 ;; FIXME: Rather than do nothing, we should somehow call
                 ;; the original table, in that case!
                 (functionp table))
            (let ((new (funcall table string point 'completion--unquote)))
              (setq string (pop new))
              (setq table (pop new))
              (setq point (pop new))
              (cl-assert (<= point (length string)))
              (pop new))))
         (defp (completion-metadata-get md 'normal-predicate))
         (func (lambda (predicate)
                 (seq-some
                  (lambda (style)
                    (when-let ((probe (funcall
                                       (nth n (assq style completion-styles-alist))
                                       string table predicate point)))
                      (cons probe style)))
                  (completion--styles md))))
         (result-and-style
          (cond
           ((and completions-exclude-exceptional-candidates defp pred)
            (or (funcall func (lambda (cand)
                                (and (funcall defp cand)
                                     (funcall pred cand))))
                (setq completions--only-exceptional-candidates
                      (funcall func pred))))
           ((and completions-exclude-exceptional-candidates defp)
            (or (funcall func defp)
                (setq completions--only-exceptional-candidates
                      (funcall func pred))))
           (t (funcall func pred)))))
    (setq completion--matching-style (cdr result-and-style))
    (if requote
        (funcall requote (car result-and-style) n)
      (car result-and-style))))

(defun completion-try-completion (string table pred point &optional metadata)
  "Try to complete STRING using completion table TABLE.
Only the elements of table that satisfy predicate PRED are considered.
POINT is the position of point within STRING.
The return value can be either nil to indicate that there is no completion,
t to indicate that STRING is the only possible completion,
or a pair (NEWSTRING . NEWPOINT) of the completed result string together with
a new position for point."
  (completion--nth-completion 1 string table pred point metadata))

(defun completion-all-completions (string table pred point &optional metadata)
  "List the possible completions of STRING in completion table TABLE.
Only the elements of table that satisfy predicate PRED are considered.
POINT is the position of point within STRING.
The return value is a list of completions and may contain the base-size
in the last `cdr'."
  (setq completion-lazy-hilit-fn nil)
  ;; FIXME: We need to additionally return the info needed for the
  ;; second part of completion-base-position.
  (completion--nth-completion 2 string table pred point metadata))

(defun minibuffer--bitset (modified completions exact)
  (logior (if modified    4 0)
          (if completions 2 0)
          (if exact       1 0)))

(defun completion--replace (beg end newtext)
  "Replace the buffer text between BEG and END with NEWTEXT.
Moves point to the end of the new text."
  ;; The properties on `newtext' include things like the
  ;; `completions-first-difference' face, which we don't want to
  ;; include upon insertion.
  (setq newtext (copy-sequence newtext))
  (remove-text-properties 0 (length newtext)
                          '(face nil display nil completion--unquoted nil)
                          newtext)
  ;; Maybe this should be in subr.el.
  ;; You'd think this is trivial to do, but details matter if you want
  ;; to keep markers "at the right place" and be robust in the face of
  ;; after-change-functions that may themselves modify the buffer.
  (let ((prefix-len 0))
    ;; Don't touch markers in the shared prefix (if any).
    (while (and (< prefix-len (length newtext))
                (< (+ beg prefix-len) end)
                (eq (char-after (+ beg prefix-len))
                    (aref newtext prefix-len)))
      (setq prefix-len (1+ prefix-len)))
    (unless (zerop prefix-len)
      (setq beg (+ beg prefix-len))
      (setq newtext (substring newtext prefix-len))))
  (let ((suffix-len 0))
    ;; Don't touch markers in the shared suffix (if any).
    (while (and (< suffix-len (length newtext))
                (< beg (- end suffix-len))
                (eq (char-before (- end suffix-len))
                    (aref newtext (- (length newtext) suffix-len 1))))
      (setq suffix-len (1+ suffix-len)))
    (unless (zerop suffix-len)
      (setq end (- end suffix-len))
      (setq newtext (substring newtext 0 (- suffix-len))))
    (goto-char beg)
    (let ((length (- end beg)))         ;Read `end' before we insert the text.
      (insert-and-inherit newtext)
      (delete-region (point) (+ (point) length)))
    (forward-char suffix-len)))

(defvar completion-in-region-mode nil
  "This variable is obsolete and no longer used.")

(make-obsolete-variable 'completion-in-region-mode nil "31.1")

(defvar completion-cycle-threshold nil
  "This variable is obsolete and no longer used.")

(make-obsolete-variable 'completion-cycle-threshold nil "30.1")

(defcustom completions-sort 'alphabetical
  "Sort candidates in the *Completions* buffer.

Completion candidates in the *Completions* buffer are sorted
depending on the value.

If it's nil, sorting is disabled.
If it's the symbol `alphabetical', candidates are sorted by
`minibuffer-sort-alphabetically'.
If it's the symbol `historical', candidates are sorted by
`minibuffer-sort-by-history', which first sorts alphabetically,
and then rearranges the order according to the order of the
candidates in the minibuffer history.
If it's a function, the function is called to sort the candidates.
The sorting function takes a list of completion candidate
strings, which it may modify; it should return a sorted list,
which may be the same.

If the completion-specific metadata provides a `sort-function', that
function overrides the value of this variable."
  :type '(choice (const :tag "No sorting" nil)
                 (const :tag "Alphabetical sorting" alphabetical)
                 (const :tag "Historical sorting" historical)
                 (function :tag "Custom function"))
  :version "30.1")

(defcustom completions-group nil
  "Enable grouping of completion candidates in the *Completions* buffer.
See also `completions-group-format' and `completions-group-sort'."
  :type 'boolean
  :version "28.1")

(defcustom completions-group-sort nil
  "Sort groups in the *Completions* buffer.

The value can either be nil to disable sorting, `alphabetical' for
alphabetical sorting or a custom sorting function.  The sorting
function takes and returns an alist of groups, where each element is a
pair of a group title string and a list of group candidate strings."
  :type '(choice (const :tag "No sorting" nil)
                 (const :tag "Alphabetical sorting" alphabetical)
                 function)
  :version "28.1")

(defcustom completions-group-format
  (concat
   (propertize "    " 'face 'completions-group-separator)
   (propertize " %s " 'face 'completions-group-title)
   (propertize " " 'face 'completions-group-separator
               'display '(space :align-to right)))
  "Format string used for the group title."
  :type 'string
  :version "28.1")

(defface completions-group-title
  '((t :inherit shadow :slant italic))
  "Face used for the title text of the candidate group headlines."
  :version "28.1")

(defface completions-group-separator
  '((t :inherit shadow :strike-through t))
  "Face used for the separator lines between the candidate groups."
  :version "28.1")

(defvar-local completion-all-sorted-completions nil)
(defvar-local completion-history nil)

(defvar completion-fail-discreetly nil
  "If non-nil, stay quiet when there is no match.")

(defun minibuffer-completing-remote-file-p ()
  "Check if currenlty completing remote file names."
  (and minibuffer-completing-file-name (file-remote-p (minibuffer-contents))))

(defun minibuffer-not-completing-remote-file-p ()
  "Negation of `minibuffer-completing-remote-file-p'."
  (not (minibuffer-completing-remote-file-p)))

(defface minibuffer-completion-fail
  '((t :inherit isearch-fail))
  "Face for highlighting minibuffer input part that prevents completion."
  :version "30.1")

(defcustom minibuffer-pulse-failing-completion
  #'minibuffer-not-completing-remote-file-p
  "Whether to pulse the part of your input that prevents completion.

If this option is non-nil and your minibuffer input does not match any
of the possible completion candidates, Emacs momentarily
highlights (pulses) the non-matching part of your input; if you delete
that part, your input would match at least one completion candidate.

When pulsing, Emacs places a mark at each end of the non-matching part,
so you can quickly select it with \\[exchange-point-and-mark] and kill
it with \\[kill-region]."
  :type '(choice (const :tag "Never pulse" nil)
                 (const :tag "Always pulse" t)
                 (const :tag "Pulse unless completing remote file names"
                        minibuffer-not-remote-file-completion-p)
                 (function :tag "Custom predicate"))
  :version "30.1")

(defun completion--fail ()
  "Indicate completion failure."
  (unless completion-fail-discreetly
    (ding)
    (when (and (minibufferp) minibuffer-pulse-failing-completion)
      (while-no-input
        (let* ((beg-end (minibuffer--completion-boundaries))
               (beg (car beg-end)) (end (cdr beg-end))
               (ref (minibuffer-prompt-end))
               (md (completion--field-metadata beg))
               (test (lambda (pos)
                       (let* ((completion-lazy-hilit t))
                         (completion-all-completions
                          (buffer-substring ref pos)
                          minibuffer-completion-table
                          minibuffer-completion-predicate
                          (- pos ref) md))))
               (cur nil))
          (when (and (< beg end)
                     (or (eq minibuffer-pulse-failing-completion t)
                         (funcall minibuffer-pulse-failing-completion)))
            (setq cur
                  (cond
                   ((or (funcall test (1- end)) (= beg (1- end))) (1- end))
                   ((funcall test (- end 2)) (- end 2))
                   ((named-let search ((beg beg)
                                       (cur (+ beg (floor (- end beg) 2)))
                                       (end (- end 2)))
                      ;; BEG passes TEST, END doesn't.
                      (if (or (= beg cur) (funcall test cur))
                          (if (and (< (1+ cur) end) (funcall test (1+ cur)))
                              (search cur (+ cur (floor (- end cur) 2)) end)
                            cur)
                        (search beg (+ beg (floor (- cur beg) 2)) cur))))))
            (push-mark end)
            (push-mark cur)
            (pulse-momentary-highlight-region
             cur end
             ;; TODO: Select face based on `minibuffer--require-match'.
             'minibuffer-completion-fail)))))
    (completion--message
     (concat
      (propertize "No match" 'face 'error)
      (when (minibuffer-narrow-completions-p)
        (substitute-command-keys
         (concat
          ", \\[minibuffer-widen-completions] to clear restrictions ("
          (completions-predicate-description minibuffer-completion-predicate)
          ")")))))))

(defvar-local completions-buffer-name "*Completions*"
  "Name of the completions list buffer for the current (mini)buffer.")

(defun completion--message (msg)
  (if completion-show-inline-help
      (minibuffer-message msg)))

(defun completion--do-completion (beg end &optional
                                      try-completion-function expect-exact)
  "Do the completion and return a summary of what happened.
M = completion was performed, the text was Modified.
C = there were available Completions.
E = after completion we now have an Exact match.

 MCE
 000  0 no possible completion
 001  1 was already an exact and unique completion
 010  2 no completion happened
 011  3 was already an exact completion
 100  4 ??? impossible
 101  5 ??? impossible
 110  6 some completion happened
 111  7 completed to an exact completion

TRY-COMPLETION-FUNCTION is a function to use in place of `try-completion'.
EXPECT-EXACT, if non-nil, means that there is no need to tell the user
when the buffer's text is already an exact match."
  (let* ((string (buffer-substring beg end))
         (md (completion--field-metadata beg))
         (comp (funcall (or try-completion-function
                            #'completion-try-completion)
                        string
                        minibuffer-completion-table
                        minibuffer-completion-predicate
                        (- (point) beg)
                        md)))
    (cond
     ((null comp)
      (minibuffer-hide-completions)
      (completion--fail)
      (minibuffer--bitset nil nil nil))
     ((eq t comp)
      (minibuffer-hide-completions)
      (goto-char end)
      (completion--done string 'finished
                        (unless expect-exact "Sole completion"))
      (minibuffer--bitset nil nil t))   ;Exact and unique match.
     (t
      ;; `completed' should be t if some completion was done, which doesn't
      ;; include simply changing the case of the entered string.  However,
      ;; for appearance, the string is rewritten if the case changes.
      (let* ((comp-pos (cdr comp))
             (completion (car comp))
             (completed (not (string-equal-ignore-case completion string)))
             (unchanged (string-equal completion string)))
        (if unchanged
	    (goto-char end)
          ;; Insert in minibuffer the chars we got.
          (completion--replace beg end completion)
          (setq end (+ beg (length completion))))
	;; Move point to its completion-mandated destination.
	(forward-char (- comp-pos (length completion)))

        (if (not (or unchanged completed))
            ;; The case of the string changed, but that's all.  We're not sure
            ;; whether this is a unique completion or not, so try again using
            ;; the real case (this shouldn't recurse again, because the next
            ;; time try-completion will return either t or the exact string).
            (completion--do-completion beg end
                                       try-completion-function expect-exact)

          ;; It did find a match.  Do we match some possibility exactly now?
          (let* ((exact (test-completion completion
                                         minibuffer-completion-table
                                         minibuffer-completion-predicate)))
            (cond
             (completed
              (if (pcase completion-auto-help
                    ('visible (get-buffer-window completions-buffer-name 0))
                    ('always t))
                  (minibuffer-completion-help beg end)
                (minibuffer-hide-completions)
                (when exact
                  ;; If completion did not put point at end of field,
                  ;; it's a sign that completion is not finished.
                  (completion--done completion
                                    (if (< comp-pos (length completion))
                                        'exact 'unknown)))))
             ;; Show the completion table, if requested.
             ((not exact)
	      (if (pcase completion-auto-help
                    ('lazy (eq this-command last-command))
                    (_ completion-auto-help))
                  (minibuffer-completion-help beg end)
                (completion--message "Next char not unique")))
             ;; If the last exact completion and this one were the same, it
             ;; means we've already given a "Complete, but not unique" message
             ;; and the user's hit TAB again, so now we give him help.
             (t
              (if (and (eq this-command last-command) completion-auto-help)
                  (minibuffer-completion-help beg end))
              (completion--done completion 'exact
                                (unless (or expect-exact
                                            (and completion-auto-select
                                                 (eq this-command last-command)
                                                 completion-auto-help))
                                  "Complete, but not unique"))))

            (minibuffer--bitset completed t exact))))))))

(defun minibuffer-restore-completion-input ()
  "Restore the state of the minibuffer prior to last completion command."
  (interactive "" minibuffer-mode)
  (if-let ((record (pop completion-history))
           (contents (car record))
           (point (cdr record)))
      (if (and (equal contents (minibuffer-contents))
               (equal point (point)))
          (minibuffer-message "Popped completion history")
        (completion--replace (minibuffer-prompt-end)
                             (point-max)
                             contents)
        (goto-char point)
        (when (get-buffer-window completions-buffer-name 0)
          ;; Refresh *Completions* buffer, if already visible.
          (minibuffer-completion-help)))
    (user-error "Empty completion history")))

(defmacro minibuffer-record-completion-input (&rest body)
  "Execute BODY and record the prior minibuffer state if BODY changed it."
  (declare (indent 0) (debug t))
  (let ((cnt (make-symbol "contents"))
        (pos (make-symbol "position"))
        (res (make-symbol "result")))
    `(let ((,cnt (minibuffer-contents))
           (,pos (point))
           (,res (progn ,@body)))
       (unless (and (equal ,cnt (minibuffer-contents))
                    (equal ,pos (point)))
         (prog1 (push (cons ,cnt ,pos) completion-history)
           (run-hooks 'minibuffer-new-completion-input-hook)))
       ,res)))

(defun minibuffer-complete ()
  "Complete the minibuffer contents as far as possible.
Return nil if there is no valid completion, else t.
If no characters can be completed, display a list of possible completions.
If you repeat this command after it displayed such a list,
scroll the window of possible completions."
  (interactive "" minibuffer-mode)
  (minibuffer-record-completion-input
    (completion-in-region (minibuffer--completion-prompt-end) (point-max)
                          minibuffer-completion-table
                          minibuffer-completion-predicate)))

(define-obsolete-function-alias 'minibuffer-complete-word
  'minibuffer-complete "30.1")

(defun completion--in-region-1 (beg end)
  (prog1 (pcase (completion--do-completion beg end)
           (#b000 nil)
           (_     t))
    (if (window-live-p minibuffer-scroll-window)
        (and (eq completion-auto-select t)
             (eq t (frame-visible-p (window-frame minibuffer-scroll-window)))
             ;; When the completion list window was displayed, select it.
             (switch-to-completions)))))

(defun completion--metadata (string base md-at-point table pred)
  ;; Like completion-metadata, but for the specific case of getting the
  ;; metadata at `base', which tends to trigger pathological behavior for old
  ;; completion tables which don't understand `metadata'.
  (let ((bounds (completion-boundaries string table pred "")))
    (if (eq (car bounds) base) md-at-point
      (completion-metadata (substring string 0 base) table pred))))

(defun minibuffer--sort-by-length-alpha (elems)
  "Sort ELEMS first by length, then alphabetically."
  (sort elems :key (lambda (c) (cons (length c) c))))

(defun minibuffer-sort-by-length (completions)
  "Sort COMPLETIONS by length."
  (sort completions :key #'length))

(defun minibuffer-sort-alphabetically (completions)
  "Sort COMPLETIONS alphabetically.

COMPLETIONS are sorted alphabetically by `string-lessp'.

This is a suitable function to use for `completions-sort' or to
include as `sort-function' in completion metadata."
  (sort completions))

(defvar minibuffer-completion-base nil
  "The base for the current completion.

This is the part of the current minibuffer input which comes
before the current completion field, as determined by
`completion-boundaries'.  This is primarily relevant for file
names, where this is the directory component of the file name.")

(defun minibuffer--sort-by-history-key-default (hist)
  (let ((hash (make-hash-table :test #'equal :size (length hist)))
        (index 0))
    (dolist (c hist)
      (unless (gethash c hash)
        (puthash c index hash))
      (cl-incf index))
    (lambda (x) (list (gethash x hash most-positive-fixnum) x))))

(defvar minibuffer-sort-by-history-key-function
  #'minibuffer--sort-by-history-key-default)

(defun minibuffer-sort-by-history (completions)
  "Sort COMPLETIONS by their position in the minibuffer history.

This is a suitable function to use for `completions-sort' or to
include as `sort-function' in completion metadata."
  (sort completions
        :key (funcall minibuffer-sort-by-history-key-function
                      (and (not (eq minibuffer-history-variable t))
                           (symbol-value minibuffer-history-variable)))))

(defun minibuffer--group-by (group-fun sort-fun elems)
  "Group ELEMS by GROUP-FUN and sort groups by SORT-FUN."
  (let ((groups))
    (dolist (cand elems)
      (let* ((key (funcall group-fun cand nil))
             (group (assoc key groups)))
        (if group
            (setcdr group (cons cand (cdr group)))
          (push (list key cand) groups))))
    (setq groups (nreverse groups)
          groups (mapc (lambda (x)
                         (setcdr x (nreverse (cdr x))))
                       groups)
          groups (funcall sort-fun groups))
    (mapcan #'cdr groups)))

(defun completion-all-sorted-completions (&optional start end)
  (let* ((start (or start (minibuffer-prompt-end)))
         (end (or end (point-max)))
         all base-size)
    (unless (and (get-buffer-window completions-buffer-name 0)
                 (with-current-buffer completions-buffer-name
                   (when-let ((cur (get-text-property (point) 'completion--string)))
                     (let ((tail nil))
                       (while (and completions-candidates
                                   (not (eq cur (car completions-candidates))))
                         (push (pop completions-candidates) tail))
                       (setq base-size (- (car completion-base-position) start)
                             completions-candidates
                             (nconc completions-candidates
                                    (nreverse tail))
                             all (copy-sequence completions-candidates))))))
      (let* ((string (buffer-substring start end))
             (md (completion--field-metadata start))
             (sub (completion-all-completions
                   (buffer-substring start end)
                   minibuffer-completion-table
                   minibuffer-completion-predicate
                   (- (point) start)
                   (completion--field-metadata start)))
             (last (last sub))
             (size (or (cdr last) 0))
             (sort-fun
              (or minibuffer-completions-sort-function
                  (completion-metadata-get md 'sort-function)
                  (pcase completions-sort
                    ('nil #'identity)
                    ('alphabetical #'minibuffer-sort-alphabetically)
                    ('historical #'minibuffer-sort-by-history)
                    (_ completions-sort))))
             (full-base (substring string 0 size))
             (minibuffer-completion-base
              (funcall (or (alist-get 'adjust-base-function md) #'identity)
                       full-base)))
        (when last
          (setcdr last nil)
          (when sort-fun (setq sub (funcall sort-fun sub)))
          (setq all sub base-size size))))
    (setq completion-all-sorted-completions (nconc all base-size))))

(defun minibuffer-toggle-completion-ignore-case ()
  "Toggle completion case-sensitively for the current minibuffer."
  (interactive "" minibuffer-mode)
  (setq-local completion-ignore-case (not completion-ignore-case))
  (when (get-buffer-window completions-buffer-name 0) (minibuffer-completion-help))
  (minibuffer-message "Completion is now case-%ssensitive"
                      (if completion-ignore-case "in" "")))

(defun minibuffer-force-complete-and-exit ()
  "Exit the minibuffer with the first matching completion.

If the current input does not match any completion candidate,
report that and do nothing else.
\\<minibuffer-local-completion-map>
When there are multiple matching completions, this command
chooses the first one according to the same sorting order that
you use for cycling.  You can think of this command as a quick
way to cycle to the next completion and exit immediately: use it
instead of \\[minibuffer-cycle-completion] followed by \
\\[exit-minibuffer] when you know you want the first
completion even before cycling to it."
  (interactive "" minibuffer-mode)
  (if-let ((beg (minibuffer-prompt-end))
           (end (cdr (minibuffer--completion-boundaries)))
           (all (completion-all-sorted-completions beg end)))
      (progn
        (completion--replace (+ (minibuffer-prompt-end)
                                (or (cdr (last all)) 0))
                             end (car all))
        (completion--done (buffer-substring-no-properties beg (point))
                          'finished)
        (exit-minibuffer))
    (completion--fail)))

(defun completion-switch-cycling-direction ()
  "Switch completion cycling from forward to backward and vice versa."
  (let* ((all completion-all-sorted-completions)
         (last (last all))
         (base (cdr last)))
    (when last (setcdr last nil))
    (setq all (nreverse all))
    (setq last (last all))
    (when last (setcdr last (cons (car all) base)))
    (setq completion-all-sorted-completions (cdr all))))

(defun minibuffer-cycle-completion (arg)
  "Cycle minibuffer input to the ARGth next completion.

If ARG is negative, cycle back that many completion candidates.
If ARG is 0, change cycling direction.

Interactively, ARG is the prefix argument, and it defaults to 1."
  (interactive "p" minibuffer-mode)
  (let ((times (abs arg)))
    (when (< arg 1) (completion-switch-cycling-direction))
    (if (< 0 times)
        (dotimes (_ times) (minibuffer-cycle-completion-further))
      (completion--message "Switched cycling direction"))
    (when (< arg 0) (completion-switch-cycling-direction))))

(defun minibuffer--highlight-in-completions (cand)
  (when-let ((win (get-buffer-window completions-buffer-name 0))
             (pm (with-current-buffer completions-buffer-name
                   (save-excursion
                     (goto-char (point-min))
                     (when-let ((pm (text-property-search-forward
                                     'completion--string cand t)))
                       (setq-local
                        cursor-face-highlight-nonselected-window t)
                       (goto-char (prop-match-beginning pm))
                       (text-property-search-forward 'cursor-face))))))
    (set-window-point win (prop-match-beginning pm))))

(defun minibuffer-cycle-completion-further ()
  "Cycle to next completion candidate."
  (if-let* ((all completion-all-sorted-completions)
            (cur (car all))
            (beg (minibuffer-prompt-end)))
      (progn
        (completion--replace (+ beg (or (cdr (last all)) 0))
                             (point-max) cur)
        (completion--done (buffer-substring-no-properties beg (point-max)) 'sole)
        (minibuffer--highlight-in-completions cur)
        ;; Rotate cached `completion-all-sorted-completions'.
        (let ((last (last all)))
          (setcdr last (cons (car all) (cdr last)))
          (setq completion-all-sorted-completions (cdr all))))
    (minibuffer-force-complete)))

(defun minibuffer-force-complete (&optional start end _)
  "Complete text between START and END to an exact match."
  (declare (advertised-calling-convention (&optional start end) "30.1"))
  (interactive "" minibuffer-mode)
  ;; FIXME: Need to deal with the extra-size issue here as well.
  ;; FIXME: ~/src/emacs/t<M-TAB>/lisp/minibuffer.el completes to
  ;; ~/src/emacs/trunk/ and throws away lisp/minibuffer.el.
  (let* ((start (or start (minibuffer-prompt-end)))
         (end (or end (point-max)))
         (all (completion-all-sorted-completions start end))
         (cur (car all))
         (base (+ start (or (cdr (last all)) 0))))
    (minibuffer-record-completion-input
      (cond
       ((atom all) (completion--message "No completions"))
       ((atom (cdr all))
        (let ((done (equal cur (buffer-substring-no-properties base end))))
          (unless done (completion--replace base end cur))
          (completion--done (buffer-substring-no-properties start (point))
                            'finished (if done "Sole completion" "."))
          (setq completion-all-sorted-completions nil)))
       (t
        (completion--replace base end cur)
        (setq end (+ base (length cur)))
        (completion--done (buffer-substring-no-properties start (point)) 'sole
                          ;; TODO: Maybe use prompt indicator instead.
                          (propertize
                           (concat "/" (number-to-string
                                        (let ((all all) (n 1))
                                          (while (consp (cdr all))
                                            (setq n (1+ n)
                                                  all (cdr all)))
                                          n)))
                           'face 'shadow))
        (minibuffer--highlight-in-completions cur)
        (let ((last (last all)))
          (setcdr last (cons cur (cdr last)))
          (setq completion-all-sorted-completions (cdr all)))
        (set-transient-map
         (let ((map (make-sparse-keymap)))
           (define-key map (vector last-command-event) this-command)
           map)
         (lambda ()
           (member this-command
                   '(universal-argument
                     negative-argument
                     digit-argument
                     minibuffer-cycle-completion
                     minibuffer-apply-and-cycle-completion
                     minibuffer-apply-alt-and-cycle-completion
                     minibuffer-cycle-completion-and-apply
                     minibuffer-cycle-completion-and-apply-alt
                     minibuffer-apply
                     minibuffer-apply-alt
                     minibuffer-set-action
                     minibuffer-exchange-actions)))
         (lambda ()
           (setq completion-all-sorted-completions nil))))))))

(defun minibuffer-apply-and-cycle-completion (n)
  "Apply minibuffer action to current input and cycle N candidates forward."
  (interactive "p" minibuffer-mode)
  (let ((input-prefix (minibuffer-current-input)))
    (minibuffer-apply (car input-prefix) (cdr input-prefix)))
  (minibuffer-cycle-completion n))

(defun minibuffer-apply-alt-and-cycle-completion (n)
  "Apply minibuffer action to current input and cycle N candidates forward."
  (interactive "p" minibuffer-mode)
  (let ((input-prefix (minibuffer-current-input)))
    (minibuffer-apply-alt (car input-prefix) (cdr input-prefix)))
  (minibuffer-cycle-completion n))

(defun minibuffer-cycle-completion-and-apply (n)
  "Cycle N candidates forward and apply minibuffer action to that candidate."
  (interactive "p" minibuffer-mode)
  (minibuffer-cycle-completion n)
  (let ((input-prefix (minibuffer-current-input)))
    (minibuffer-apply (car input-prefix) (cdr input-prefix))))

(defun minibuffer-cycle-completion-and-apply-alt (n)
  "Cycle N candidates forward and apply alternative minibuffer action."
  (interactive "p" minibuffer-mode)
  (minibuffer-cycle-completion n)
  (let ((input-prefix (minibuffer-current-input)))
    (minibuffer-apply-alt (car input-prefix) (cdr input-prefix))))

(defvar minibuffer-confirm-exit-commands
  '(completion-at-point minibuffer-complete)
  "List of commands which cause an immediately following
`minibuffer-complete-and-exit' to ask for extra confirmation.")

(defvar minibuffer--require-match nil
  "Value of REQUIRE-MATCH passed to `completing-read'.")

(defvar minibuffer--original-buffer nil
  "Buffer that was current when `completing-read' was called.")

(defun minibuffer-complete-and-exit ()
  "Exit if the minibuffer contains a valid completion.
Otherwise, try to complete the minibuffer contents.  If
completion leads to a valid completion, a repetition of this
command will exit.

If `minibuffer-completion-confirm' is `confirm', do not try to
 complete; instead, ask for confirmation and accept any input if
 confirmed.
If `minibuffer-completion-confirm' is `confirm-after-completion',
 do not try to complete; instead, ask for confirmation if the
 preceding minibuffer command was a member of
 `minibuffer-confirm-exit-commands', and accept the input
 otherwise."
  (interactive "" minibuffer-mode)
  (completion-complete-and-exit (minibuffer--completion-prompt-end) (point-max)
                                #'exit-minibuffer))

(defun completion-complete-and-exit (beg end exit-function)
  (minibuffer-record-completion-input
    (completion--complete-and-exit
     beg end exit-function
     (lambda ()
       (pcase (condition-case nil
                  (completion--do-completion beg end
                                             nil 'expect-exact)
                (error 1))
         ((or #b001 #b011) (funcall exit-function))
         (#b111 (if (not minibuffer-completion-confirm)
                    (funcall exit-function)
                  (minibuffer-message "Confirm")
                  nil))
         (_ nil))))))

(defun completion--complete-and-exit (beg end
                                          exit-function completion-function)
  "Exit from `require-match' minibuffer.
COMPLETION-FUNCTION is called if the current buffer's content does not
appear to be a match."
  (cond
   ;; Allow user to specify null string
   ((= beg end) (funcall exit-function))
   ;; The CONFIRM argument is a predicate.
   ((functionp minibuffer-completion-confirm)
    (if (funcall minibuffer-completion-confirm
                 (buffer-substring beg end))
        (funcall exit-function)
      (completion--message "Can't exit minibuffer with current input")))
   ;; See if we have a completion from the table.
   ((test-completion (buffer-substring beg end)
                     minibuffer-completion-table
                     minibuffer-completion-predicate)
    ;; FIXME: completion-ignore-case has various slightly
    ;; incompatible meanings.  E.g. it can reflect whether the user
    ;; wants completion to pay attention to case, or whether the
    ;; string will be used in a context where case is significant.
    ;; E.g. usually try-completion should obey the first, whereas
    ;; test-completion should obey the second.
    (when completion-ignore-case
      ;; Fixup case of the field, if necessary.
      (let* ((string (buffer-substring beg end))
             (compl (try-completion
                     string
                     minibuffer-completion-table
                     minibuffer-completion-predicate)))
        (when (and (stringp compl) (not (equal string compl))
                   ;; If it weren't for this piece of paranoia, I'd replace
                   ;; the whole thing with a call to do-completion.
                   ;; This is important, e.g. when the current minibuffer's
                   ;; content is a directory which only contains a single
                   ;; file, so `try-completion' actually completes to
                   ;; that file.
                   (= (length string) (length compl)))
          (completion--replace beg end compl))))
    (funcall exit-function))
   ;; The user is permitted to exit with an input that's rejected
   ;; by test-completion, after confirming her choice.
   ((memq minibuffer-completion-confirm '(confirm confirm-after-completion))
    (if (or (eq last-command this-command)
            ;; For `confirm-after-completion' we only ask for confirmation
            ;; if trying to exit immediately after typing TAB (this
            ;; catches most minibuffer typos).
            (and (eq minibuffer-completion-confirm 'confirm-after-completion)
                 (not (memq last-command minibuffer-confirm-exit-commands))))
        (funcall exit-function)
      (minibuffer-message "Confirm")
      nil))

   (t
    ;; Call do-completion, but ignore errors.
    (funcall completion-function))))

(defface completions-annotations '((t :inherit (italic shadow)))
  "Face to use for annotations in the *Completions* buffer.
This face is only used if the strings used for completions
doesn't already specify a face.")

(defface completions-highlight
  '((t :inherit highlight))
  "Default face for highlighting the current completion candidate."
  :version "29.1")

(defcustom completions-highlight-face 'completions-highlight
  "A face name to highlight the current completion candidate.
If the value is nil, no highlighting is performed."
  :type '(choice (const nil) face)
  :version "29.1")

(defcustom completions-format 'horizontal
  "Define the appearance and sorting of completions.
If the value is `vertical', display completions sorted vertically in
columns in the *Completions* buffer.  If the value is `horizontal',
display completions sorted in columns horizontally, rather than down the
screen.  If the value is `one-column', display completions down the
screen in one column."
  :type '(choice (const horizontal) (const vertical) (const one-column))
  :version "23.2")

(defcustom completions-detailed nil
  "When non-nil, display completions with details added as prefix/suffix.
This makes some commands (for instance, \\[describe-symbol]) provide a
detailed view with more information prepended or appended to
completions."
  :type 'boolean
  :version "28.1")

(defvar-local completions-base-prefix nil)
(defvar-local completions-candidates nil)
(defvar-local completions-annotations nil)
(defvar-local completions-group-function nil)
(defvar-local completions-category nil)
(defvar-local completions-sort-function nil)
(defvar-local completions-sort-orders nil)
(defvar-local completions-predicate nil)
(defvar-local completions-exceptional-candidates nil)
(defvar-local completions-ignore-case nil)
(defvar-local completions-action nil)
(defvar-local completions-alternative-action nil)
(defvar-local completions-style nil)
(defvar-local completions-minibuffer-state nil)

(defvar completions-header-count
  '(completions-candidates
    ((-3 "%p") "/"
     (:eval (file-size-human-readable (length completions-candidates) 'si)) " ")))

(defvar completions-header-category
  '(completions-category
    ("" (:eval (propertize (symbol-name completions-category)
                           'help-echo "Candidates category"))
     " ")))

(defun completions-header-sort (e)
  (interactive "e")
  (with-current-buffer
      (buffer-local-value
       'completion-reference-buffer
       (window-buffer (posn-window (event-end e))))
    (minibuffer-sort-completions 1)))

(defun completions-header-reverse (e)
  (interactive "e")
  (with-current-buffer
      (buffer-local-value
       'completion-reference-buffer
       (window-buffer (posn-window (event-end e))))
    (minibuffer-sort-completions -1)))

(defvar-keymap completions-header-order-map
  :doc "Keymap for modifying completions sorting with the mouse."
  (key-description [header-line mouse-2]) #'completions-header-sort
  (key-description [header-line mouse-3]) #'completions-header-reverse
  "<follow-link>" 'mouse-face)

(defvar completions-header-order
  '(""
    (:eval
     (propertize
      (let ((sd (or (nth 4 (seq-find
                            (lambda (order)
                              (eq
                               (nth 3 order)
                               (advice--cd*r
                                completions-sort-function)))
                            completions-sort-orders))
                    "default"))
            (rv (advice-function-member-p #'reverse completions-sort-function)))
        (concat (if rv "↑" "↓")
                sd
                (if rv "↑" "↓")))
      'mouse-face 'mode-line-highlight
      'help-echo "mouse-2: Change sort order\nmouse-3: Reverse order"
      'keymap completions-header-order-map))
    " "))

(defface completions-header-restriction-separator-highlight
  '((t :inherit mode-line-highlight))
  "Face for restriction separator in completions header when mouse is on it."
  :version "31.1")

(defvar completions-header-restriction
  '(:eval (let* ((neg (advice-function-member-p #'not completions-predicate))
                 (sep
                  (propertize (if neg "\\" "/")
                              'help-echo (concat (when neg "Negated\n")
                                                 "mouse-2: Toggle negation")
                              'mouse-face 'completions-header-restriction-separator-highlight
                              'keymap
                              (let ((map (make-sparse-keymap)))
                                (define-key map [header-line mouse-2]
                                            (lambda (e)
                                              (interactive "e")
                                              (with-current-buffer
                                                  (buffer-local-value
                                                   'completion-reference-buffer
                                                   (window-buffer (posn-window (event-end e))))
                                                (minibuffer-negate-completion-predicate))))
                                (define-key map [header-line follow-link] 'mouse-face)
                                map))))
            (concat
             sep
             (or (completions-predicate-description completions-predicate)
                 (and completions-predicate
                      (symbolp completions-predicate)
                      (not (eq completions-predicate 'always))
                      (symbol-name completions-predicate))
                 "all")
             sep
             " "))))

(defvar completions-header-action
  '(completions-action
    ("+"
     (:eval (cdr completions-action))
     (completions-alternative-action
      ("[" (:eval (cdr completions-alternative-action)) "]"))
     "+ ")))

(defvar completions-header-style
  '(completions-style
    ("" (:eval (propertize (symbol-name completions-style)
                           'help-echo "Completion style"))
     " ")))

(defvar completions-header-ignore-case
  `(:propertize (completions-ignore-case "a" "A")
                mouse-face mode-line-highlight
                help-echo "Toggle case sensitivity"
                keymap
                ,(let ((map (make-sparse-keymap)))
                   (define-key map [header-line mouse-2]
                               (lambda (e)
                                 (interactive "e")
                                 (with-current-buffer
                                     (buffer-local-value
                                      'completion-reference-buffer
                                      (window-buffer (posn-window (event-end e))))
                                   (minibuffer-toggle-completion-ignore-case))))
                   (define-key map [header-line follow-link] 'mouse-face)
                   map)))

(defvar completions-header-exceptional-candidates
  `(:propertize (completions-exceptional-candidates "~" "!")
                mouse-face mode-line-highlight
                help-echo "Toggle exceptional candidates"
                keymap
                ,(let ((map (make-sparse-keymap)))
                   (define-key map [header-line mouse-2]
                               (lambda (e)
                                 (interactive "e")
                                 (with-current-buffer
                                     (buffer-local-value
                                      'completion-reference-buffer
                                      (window-buffer (posn-window (event-end e))))
                                   (minibuffer-toggle-exceptional-candidates))))
                   (define-key map [header-line follow-link] 'mouse-face)
                   map)))

(defvar completions-header-annotations
  `(:propertize (completions-annotations "@" "-")
                mouse-face mode-line-highlight
                help-echo "Toggle annotations"
                keymap
                ,(let ((map (make-sparse-keymap)))
                   (define-key map [header-line mouse-2]
                               (lambda (e)
                                 (interactive "e")
                                 (with-current-buffer
                                     (buffer-local-value
                                      'completion-reference-buffer
                                      (window-buffer (posn-window (event-end e))))
                                   (minibuffer-toggle-completions-annotations))))
                   (define-key map [header-line follow-link] 'mouse-face)
                   map)))

(defvar completions-header-extra nil)

(dolist (sym '(completions-header-count
               completions-header-category
               completions-header-annotations
               completions-header-order
               completions-header-restriction
               completions-header-action
               completions-header-style
               completions-header-ignore-case
               completions-header-exceptional-candidates
               completions-header-extra))
  (put sym 'risky-local-variable t))

(defvar completions-header-format
  '(" "
    completions-header-count
    completions-header-category
    completions-header-style
    "%b | "
    completions-header-order
    completions-header-restriction
    completions-header-action
    completions-header-ignore-case
    completions-header-exceptional-candidates
    completions-header-annotations
    (completions-header-extra (" | " completions-header-extra)))
  "Header line format of the *Completions* buffer.")

(defun completion--insert-strings (strings &optional group-fun)
  "Insert a list of STRINGS into the current buffer.
The candidate strings are inserted into the buffer depending on the
completions format as specified by the variable `completions-format'.
Runs of equal candidate strings are eliminated.

Optional argument GROUP-FUN, if non-nil, is a completions grouping
function as described in the documentation of `completion-metadata'."
  (when (consp strings)
    (let* ((length (apply #'max
			  (mapcar (lambda (s)
				    (if (consp s)
				        (apply #'+ (mapcar #'string-width s))
				      (string-width s)))
				  strings)))
	   (window (get-buffer-window (current-buffer) 0))
	   (wwidth (if window (1- (window-width window)) (1- (frame-width))))
	   (columns (min
		     ;; At least 2 spaces between columns.
		     (max 1 (/ wwidth (+ 2 length)))
		     ;; Don't allocate more columns than we can fill.
		     ;; Windows can't show less than 3 lines anyway.
		     (max 1 (/ (length strings) 2))))
	   (colwidth (/ wwidth columns)))
      (funcall (intern (format "completion--insert-%s" completions-format))
               strings group-fun length wwidth colwidth columns))))

(defun completion--insert-horizontal (strings group-fun
                                              length wwidth
                                              colwidth _columns)
  (let ((column 0)
        (first t)
	(last-title nil)
        (colid 0))
    (dolist (str strings)
      (when group-fun
        (let ((title (funcall group-fun (if (consp str) (car str) str) nil)))
          (unless (equal title last-title)
            (setq last-title title)
            (when title
              (insert (if first "" "\n") (format completions-group-format title) "\n")
              (setq column 0 colid 0
                    first t)))))
      (unless first
        ;; FIXME: `string-width' doesn't pay attention to
        ;; `display' properties.
	(if (< wwidth (+ column (max colwidth
                                     (if (consp str)
                                         (apply #'+ (mapcar #'string-width str))
                                       (string-width str)))))
	    ;; No space for `str' at point, move to next line.
	    (progn (insert "\n") (setq column 0 colid 0))
	  (insert " \t")
	  ;; Leave the space unpropertized so that in the case we're
	  ;; already past the goal column, there is still
	  ;; a space displayed.
	  (set-text-properties (1- (point)) (point)
			       `(display (space :align-to ,column)))
	  nil))
      (setq first nil)
      (completion--insert str group-fun colid)
      ;; Next column to align to.
      (setq column (+ column
		      ;; Round up to a whole number of columns.
		      (* colwidth (ceiling length colwidth)))
            colid (1+ colid)))))

(defun completion--insert-vertical (strings group-fun
                                            _length _wwidth
                                            colwidth columns)
  (while strings
    (let ((group nil)
          (column 0)
          (colid 0)
	  (row 0)
          (rows))
      (if group-fun
          (let* ((str (car strings))
                 (title (funcall group-fun (if (consp str) (car str) str) nil)))
            (while (and strings
                        (equal title (funcall group-fun
                                              (if (consp (car strings))
                                                  (car (car strings))
                                                (car strings))
                                              nil)))
              (push (car strings) group)
              (pop strings))
            (setq group (nreverse group)))
        (setq group strings
              strings nil))
      (setq rows (/ (length group) columns))
      (when group-fun
        (let* ((str (car group))
               (title (funcall group-fun (if (consp str) (car str) str) nil)))
          (when title
            (goto-char (point-max))
            (insert (format completions-group-format title) "\n"))))
      (dolist (str group)
	(when (> row rows)
          (forward-line (- -1 rows))
	  (setq row 0 column (+ column colwidth) colid (1+ colid)))
	(when (> column 0)
	  (end-of-line)
	  (while (> (current-column) column)
	    (if (eobp)
		(insert "\n")
	      (forward-line 1)
	      (end-of-line)))
	  (insert " \t")
	  (set-text-properties (1- (point)) (point)
			       `(display (space :align-to ,column))))
        (completion--insert str group-fun colid)
	(if (> column 0)
	    (forward-line)
	  (insert "\n"))
	(setq row (1+ row))))))

(defun completion--insert-one-column (strings group-fun &rest _)
  (let ((last-title nil))
    (dolist (str strings)
      (when group-fun
        (let ((title (funcall group-fun (if (consp str) (car str) str) nil)))
          (unless (equal title last-title)
            (setq last-title title)
            (when title
              (insert (format completions-group-format title) "\n")))))
      (completion--insert str group-fun 0)
      (insert "\n"))
    (delete-char -1)))

(defun completion--insert (str group-fun &optional column)
  (if (not (consp str))
      (add-text-properties
       (point)
       (progn
         (insert
          (if group-fun
              (funcall group-fun str 'transform)
            str))
         (point))
       `( mouse-face highlight
          cursor-face ,completions-highlight-face
          completion--string ,str
          completion--column ,column))
    ;; If `str' is a list that has 2 elements,
    ;; then the second element is a suffix annotation.
    ;; If `str' has 3 elements, then the second element
    ;; is a prefix, and the third element is a suffix.
    (let* ((prefix (when (nth 2 str) (nth 1 str)))
           (suffix (or (nth 2 str) (nth 1 str))))
      (when prefix
        (let ((beg (point))
              (end (progn (insert prefix) (point))))
          (add-text-properties beg end `(mouse-face nil completion--string ,(car str)))))
      (completion--insert (car str) group-fun column)
      (let ((beg (point))
            (end (progn (insert suffix) (point))))
        (add-text-properties beg end `(mouse-face nil completion--string ,(car str)))
        ;; Put the predefined face only when suffix
        ;; is added via annotation-function without prefix,
        ;; and when the caller doesn't use own face.
        (unless (or prefix (text-property-not-all
                            0 (length suffix) 'face nil suffix))
          (font-lock-prepend-text-property
           beg end 'face 'completions-annotations))))))

(defvar completion-setup-hook nil
  "Normal hook run at the end of setting up a completion list buffer.
When this hook is run, the current buffer is the one in which the
command to display the completion list buffer was run.
The completion list buffer is available as the value of `standard-output'.
See also `display-completion-list'.")

(defface completions-first-difference
  '((t (:inherit bold)))
  "Face for the first character after point in completions.
See also the face `completions-common-part'.")

(defface completions-common-part
  '((((class color) (min-colors 16) (background light)) :foreground "blue3")
    (((class color) (min-colors 16) (background dark)) :foreground "lightblue"))
  "Face for the parts of completions which matched the pattern.
See also the face `completions-first-difference'.")

(defface completions-regexp-match-1
  '((t :foreground "red"))
  "Face for first submatch of matching completions.")

(defface completions-regexp-match-2
  '((t :foreground "dark green"))
  "Face for second submatch of matching completions.")

(defun completion-hilit-commonality (completions prefix-len &optional base-size)
  "Apply font-lock highlighting to a list of completions, COMPLETIONS.
PREFIX-LEN is an integer.  BASE-SIZE is an integer or nil (meaning zero).

This adds the face `completions-common-part' to the first
\(PREFIX-LEN - BASE-SIZE) characters of each completion, and the face
`completions-first-difference' to the first character after that.

It returns a list with font-lock properties applied to each element,
and with BASE-SIZE appended as the last element."
  (when completions
    (let* ((com-str-len (- prefix-len (or base-size 0)))
           (hilit-fn
            (lambda (str)
              (font-lock-prepend-text-property
               0
               ;; If completion-boundaries returns incorrect values,
               ;; all-completions may return strings that don't contain
               ;; the prefix.
               (min com-str-len (length str))
               'face 'completions-common-part str)
              (when (> (length str) com-str-len)
                (font-lock-prepend-text-property
                 com-str-len (1+ com-str-len)
                 'face 'completions-first-difference str))
              str)))
      (if completion-lazy-hilit
          (setq completion-lazy-hilit-fn hilit-fn)
        (setq completions
              (mapcar
               (lambda (elem)
                 ;; Don't modify the string itself, but a copy, since
                 ;; the string may be read-only or used for other
                 ;; purposes.  Furthermore, since `completions' may come
                 ;; from display-completion-list, `elem' may be a list.
                 (funcall hilit-fn
                          (if (consp elem)
                              (car (setq elem (cons (copy-sequence (car elem))
                                                    (cdr elem))))
                            (setq elem (copy-sequence elem))))
                 elem)
               completions)))
      (nconc completions base-size))))

(defun completions-predicate-description (pred)
  "Return string describing predicate PRED, or nil."
  (and (functionp pred)
       (let ((descs nil))
         (advice-function-mapc
          (lambda (_ alist)
            (when-let ((description (alist-get 'description alist)))
              (push description descs)))
          pred)
         (when descs (mapconcat
                      (lambda (desc)
                        (propertize
                         desc
                         'mouse-face 'mode-line-highlight
                         'help-echo "mouse-2: Negate\nmouse-3: Remove"
                         'keymap
                         (let ((map (make-sparse-keymap)))
                           (define-key map [header-line mouse-2]
                                       (lambda (e)
                                         (interactive "e")
                                         (with-current-buffer
                                             (buffer-local-value
                                              'completion-reference-buffer
                                              (window-buffer (posn-window (event-end e))))
                                           (minibuffer-negate-completion-predicate desc))))
                           (define-key map [header-line mouse-3]
                                       (lambda (e)
                                         (interactive "e")
                                         (with-current-buffer
                                             (buffer-local-value
                                              'completion-reference-buffer
                                              (window-buffer (posn-window (event-end e))))
                                           (minibuffer-widen-completions desc))))
                           (define-key map [header-line follow-link] 'mouse-face)
                           map)))
                      descs ", ")))))

(defvar minibuffer-completions-sort-function nil
  "Function for sorting minibuffer completion candidates, or nil.

When the value of this variable is a function,
`minibuffer-completion-help' uses that function to sort the
completions list instead of using the `sort-function'
from the completion table or the value of `completions-sort'.

`minibuffer-sort-completions' sets the value of this variable to
temporarily override the default completions sorting.")

(defcustom minibuffer-completions-sort-orders
  '((?a "alphabetical" "Sort alphabetically"
        minibuffer-sort-alphabetically "alphabetically")
    (?h "historical" "Sort by position in minibuffer history"
        minibuffer-sort-by-history "position in minibuffer history")
    (?i "identity" "Disable sorting" identity nil)
    (?d "default" "Default sort order" nil nil)
    (?l "length" "Sort by length" minibuffer-sort-by-length "length"))
  "List of minibuffer completions sort orders.
Each element is a list of the form (CHAR NAME HELP FUNC DESC),
where CHAR is a character that you type to select this sort order
in `minibuffer-sort-completions', NAME is the name of the sort
order, HELP is a short help string that explains what this sort
order does, FUNC is the completions sorting function, and DESC is
a desription that is shown in the *Completions* buffer when the
sort order is in effect.

FUNC can also be nil, which says to use the default sort order
when you select this sort order."
  :version "30.1"
  :type '(repeat
          (list character string string
                (choice function
                        (const :tag "No sorting" nil)
                        (const :tag "Use default sort order" identity))
                (choice string
                        (const :tag "No description" nil)))))

(defface completions-heading '((t :background "light cyan" :underline "black"))
  "Face for the completions headling line.")

(defface completions-previous-input '((t :underline "violet"))
  "Face for highlighting previous inputs in the *Completions* buffer.")

(defface completions-exceptional-candidate
  '((t :underline (:style wave :color "grey")))
  "Face for highlighting exceptional candidates in the *Completions* buffer.")

(defface completions-used-input '((t :inherit link-visited))
  "Face for highlighting used inputs in the *Completions* buffer.")

(defface completions-used-input-alt
  '((t :foreground "blue" :inherit completions-used-input))
  "Face for candidates to which you applied the alternative minibuffer action.")

(defcustom completions-highlight-previous-inputs t
  "Whether to highlight previously used inputs in the *Completions* buffer."
  :version "30.1"
  :type 'boolean)

(defcustom completions-highlight-exceptional-candidates t
  "Whether to highlight exceptional candidates in the *Completions* buffer."
  :version "30.1"
  :type 'boolean)

(defvar completion-extra-properties nil
  "Property list of extra properties of the current completion job.
These include:

`:category': the kind of objects returned by `all-completions'.
   Used by `completion-category-overrides'.

`:annotation-function': Function to annotate the completions buffer.
   The function must accept one argument, a completion string,
   and return either nil or a string which is to be displayed
   next to the completion (but which is not part of the
   completion).  The function can access the completion data via
   `minibuffer-completion-table' and related variables.

`:affixation-function': Function to prepend/append a prefix/suffix to
   completions.  The function must accept one argument, a list of
   completions, and return a list of annotated completions.  The
   elements of the list must be three-element lists: completion, its
   prefix and suffix.  This function takes priority over
   `:annotation-function' when both are provided, so only this
   function is used.

`:group-function': Function for grouping the completion candidates.

`:sort-function': Function to sort completion candidates.

`:narrow-completions-function': function for narrowing the completions list.

`:eager-display': Show the *Completions* buffer eagerly.

See more information about these functions above
in `completion-metadata'.

`:exit-function': Function to run after completion is performed.

   The function must accept two arguments, STRING and STATUS.
   STRING is the text to which the field was completed, and
   STATUS indicates what kind of operation happened:
     `finished' - text is now complete
     `sole'     - text cannot be further completed but
                  completion is not finished
     `exact'    - text is a valid completion but may be further
                  completed.")

(defun completion--done (string &optional finished message)
  (let* ((exit-fun (plist-get completion-extra-properties :exit-function))
         (pre-msg (and exit-fun (current-message))))
    (cl-assert (memq finished '(exact sole finished unknown)))
    (when exit-fun
      (when (eq finished 'unknown)
        (setq finished
              (if (eq (try-completion string
                                      minibuffer-completion-table
                                      minibuffer-completion-predicate)
                      t)
                  'finished 'exact)))
      (funcall exit-fun string finished))
    (when (and message
               ;; Don't output any message if the exit-fun already did so.
               (equal pre-msg (and exit-fun (current-message))))
      (completion--message message))))

(defcustom completions-max-height nil
  "Maximum height for *Completions* buffer window."
  :type '(choice (const nil) natnum)
  :version "29.1")

(defcustom completions-min-height 2
  "Minimum height for *Completions* buffer window."
  :type '(choice (const nil) natnum)
  :version "30.1")

(defun completions--fit-window-to-buffer (&optional win &rest _)
  "Resize *Completions* buffer window."
  (if temp-buffer-resize-mode
      (let ((temp-buffer-max-height (or completions-max-height
                                        temp-buffer-max-height)))
        (resize-temp-buffer-window win))
    (fit-window-to-buffer win completions-max-height completions-min-height)))

(defcustom minibuffer-read-sort-order-with-completion nil
  "Whether to use completion for reading minibuffer completions sort order.
If this user options is nil (the default),
`minibuffer-sort-completions' lets you to select a sort order by
typing a single key, which is usually the first letter of the
name of the sort order.  If you set this user option to non-nil,
`minibuffer-sort-completions' instead reads the sort order name
in the minibuffer, with completion."
  :type 'boolean
  :version "30.1")

(defun minibuffer-sort-completions (arg)
  "Sort the list of minibuffer completion candidates.
Prompt for a sort order among
`minibuffer-completions-sort-orders' and apply it to the current
completions list.  With negative prefix argument ARG, reverse the
current order instead."
  (interactive "p" minibuffer-mode)
  (if (< arg 0)
      (if (advice-function-member-p
           #'reverse minibuffer-completions-sort-function)
          (remove-function
           (local 'minibuffer-completions-sort-function) #'reverse)
        (unless minibuffer-completions-sort-function
          (setq-local minibuffer-completions-sort-function
                      (or (completion-metadata-get
                           (completion--field-metadata
                            (minibuffer-prompt-end))
                           'sort-function)
                          (pcase completions-sort
                            ('nil #'identity)
                            ('alphabetical #'minibuffer-sort-alphabetically)
                            ('historical #'minibuffer-sort-by-history)
                            ;; It's already a function, use it.
                            (_ completions-sort)))))
        (add-function
         :filter-return
         (local 'minibuffer-completions-sort-function) #'reverse))
    (setq-local
     minibuffer-completions-sort-function
     (nth 3 (read-multiple-choice
             "Sort order" minibuffer-completions-sort-orders
             nil nil minibuffer-read-sort-order-with-completion))))
  (when completion-auto-help (minibuffer-completion-help)))

(defun completion-styles-affixation (names)
  "Return completion affixations for completion styles list NAMES."
  (let ((max-name (seq-max (cons 0 (mapcar #'string-width names)))))
    (mapcar
     (lambda (name)
       (list name
             ""
             (if-let ((desc (nth 3 (assoc (intern name)
                                          completion-styles-alist))))
                 (concat (make-string (- (+ max-name 4)
                                         (string-width name))
                                      ?\s)
                         (propertize
                          ;; Only use the first line.
                          (substring desc 0 (string-search "\n" desc))
                          'face 'completions-annotations))
               "")))
     names)))

(defvar minibuffer-completion-styles-history nil
  "Minibuffer history list for `minibuffer-set-completion-styles'.")

(defun minibuffer-set-completion-styles (styles)
  "Set the completion styles for the current minibuffer to STYLES.

STYLES is a list of completion styles (symbols).  If STYLES is
nil, this discards any completion styles changes that you have
made with this commmand in the current minibuffer.

Interactively, with no prefix argument, prompt for a list of
completion styles, with completion.  With plain prefix
\\[universal-argument], discard all changes that you made with
this commmand in the current minibuffer.  Zero prefix argument
(C-0 C-x /) says to disable the completion style that produced
the current completions list.  Prefix argument one (C-1 C-x /)
says to keep only the completion style that produced the current
completions list."
  (interactive
   (list (let ((styles (completion--styles (completion--field-metadata
                                            (minibuffer-prompt-end))))
               (current (when-let ((buf (get-buffer completions-buffer-name)))
                          (buffer-local-value 'completions-style buf))))
           (pcase current-prefix-arg
             (`(,_ . ,_) nil)           ; \\[universal-argument]
             (0 (unless current
                  (user-error "No current completion style"))
                (or (remove current styles)
                    (user-error "Cannot disable sole competion style")))
             (1 (unless current
                  (user-error "No current completion style"))
                (list current))
             (_ (mapcar
                 #'intern
                 (minibuffer-with-setup-hook
                     (lambda ()
                       (require 'crm)
                       (setq-local crm-separator "[ \t]*,[ \t]*"))
                   (completing-read-multiple
                    "Set completion styles: "
                    (completion-table-with-metadata
                     completion-styles-alist
                     '((category . completion-style)
                       (affixation-function . completion-styles-affixation)))
                    nil t
                    (concat (mapconcat #'symbol-name styles ",") ",")
                    'minibuffer-completion-styles-history)))))))
   minibuffer-mode)
  (setq-local completion-local-styles styles)
  (when (get-buffer-window completions-buffer-name 0)
    (minibuffer-completion-help))
  (message (format "Using completion style%s `%s'"
                   (ngettext "" "s" (length styles))
                   (mapconcat #'symbol-name styles "', `"))))

(defcustom minibuffer-completion-annotations t
  "Whether to display annotations for completion candidates."
  :type 'boolean
  :version "30.1")

(defvar-local minibuffer-action nil)

(defvar-local minibuffer-alternative-action nil)

(defun minibuffer-completion-help (&optional start end)
  "Display a list of possible completions of the current minibuffer contents."
  (interactive "" minibuffer-mode)
  (let* ((start (or start (minibuffer--completion-prompt-end)))
         (end (or end (point-max)))
         (string (buffer-substring start end))
         (md (completion--field-metadata start))
         (completions (completion-all-completions
                       string
                       minibuffer-completion-table
                       minibuffer-completion-predicate
                       (- (point) start)
                       md))
         (last (last completions))
         (base-size (or (cdr last) 0)))
    (message nil)
    (if (or (null completions)
            (and (not (consp (cdr completions)))
                 (equal (car completions) string)))
        (progn
          ;; If there are no completions, or if the current input is already
          ;; the sole completion, then hide (previous&stale) completions.
          (minibuffer-hide-completions)
          (if completions
              (completion--message "Sole completion")
            (completion--fail)))

      (let* ((prefix (unless (zerop base-size) (substring string 0 base-size)))
             (full-base (substring string 0 base-size))
             (base-prefix (buffer-substring (minibuffer--completion-prompt-end)
                                            (+ start base-size)))
             (field-end
              (save-excursion
                (forward-char
                 (cdr (completion-boundaries (buffer-substring start (point))
                                             minibuffer-completion-table
                                             minibuffer-completion-predicate
                                             (buffer-substring (point) end))))
                (point)))
             (field-char (and (< field-end end) (char-after field-end)))
             (base-position (list (+ start base-size) field-end))
             (style completion--matching-style)
             (exceptional-candidates
              (or (not completions-exclude-exceptional-candidates)
                  completions--only-exceptional-candidates))
             (all-md (completion--metadata (buffer-substring-no-properties
                                            start (point))
                                           base-size md
                                           minibuffer-completion-table
                                           minibuffer-completion-predicate))
             (ann-fun (completion-metadata-get all-md 'annotation-function))
             (aff-fun (completion-metadata-get all-md 'affixation-function))
             (sort-fun (completion-metadata-get all-md 'sort-function))
             (group-fun (completion-metadata-get all-md 'group-function))
             (def-pred (completion-metadata-get all-md 'normal-predicate))
             (category (completion-metadata-get all-md 'category))
             (minibuffer-completion-base
              (funcall (or (alist-get 'adjust-base-function all-md) #'identity)
                       full-base))
             (explicit-sort-function minibuffer-completions-sort-function)
             (sort-orders minibuffer-completions-sort-orders)
             (cpred minibuffer-completion-predicate)
             (ctable minibuffer-completion-table)
             (action (minibuffer-action)))

        (when last (setcdr last nil))

        ;; Maybe highilight previously used completions.
        (when-let
            ((hist (and completions-highlight-previous-inputs
                        (not (eq minibuffer-history-variable t))
                        (symbol-value minibuffer-history-variable))))
          (setq completions
                (mapcar
                 (lambda (comp)
                   (if (member (concat minibuffer-completion-base comp) hist)
                       ;; Avoid modifying the original string.
                       (let ((copy (copy-sequence comp)))
                         (font-lock-append-text-property
                          0 (length copy)
                          'face 'completions-previous-input copy)
                         copy)
                     comp))
                 completions)))

        ;; Maybe highilight exceptional completion candidates.
        (unless (or
                 ;; Exceptional candidate highilighting is disabled.
                 (not completions-highlight-exceptional-candidates)
                 ;; All candidates are normal.
                 (not def-pred)
                 ;; Exceptional candidates are excluded.
                 completions-exclude-exceptional-candidates
                 ;; All candidates are exceptional.
                 completions--only-exceptional-candidates)
          ;; Otherwise, we have a mix of normal and exceptional
          ;; candidates, and the mandate to highlight exceptions.
          (setq completions
                (mapcar
                 (lambda (comp)
                   (if (not (funcall def-pred comp))
                       ;; COMP is exceptional.  Return highlighted COPY.
                       (let ((copy (copy-sequence comp)))
                         (font-lock-append-text-property
                          0 (length copy)
                          'face 'completions-exceptional-candidate copy)
                         copy)
                     ;; COMP is normal.  Return it as-is.
                     comp))
                 completions)))

        ;; Sort first using the `sort-function'.
        (setq completions
              (cond
               (explicit-sort-function
                (funcall explicit-sort-function
                         completions))
               (sort-fun
                (funcall sort-fun completions))
               (t
                (pcase completions-sort
                  ('nil completions)
                  ('alphabetical (minibuffer-sort-alphabetically completions))
                  ('historical (minibuffer-sort-by-history completions))
                  (_ (funcall completions-sort completions))))))

        ;; Group the candidates using the `group-function'.
        (when group-fun
          (setq completions
                (minibuffer--group-by
                 group-fun
                 (pcase completions-group-sort
                   ('nil #'identity)
                   ('alphabetical #'sort)
                   (_ completions-group-sort))
                 completions)))

        (when minibuffer-completion-annotations
          (cond
           (aff-fun
            (setq completions
                  (funcall aff-fun completions)))
           (ann-fun
            (setq completions
                  (mapcar (lambda (s)
                            (let ((ann (funcall ann-fun s)))
                              (if ann (list s ann) s)))
                          completions)))))
        (setq minibuffer-scroll-window
              (let ((standard-output (get-buffer-create completions-buffer-name)))
                (completions-display
                 completions
                 :group-function group-fun
                 :style style
                 :category category
                 :sort-function explicit-sort-function
                 :sort-orders sort-orders
                 :predicate cpred
                 :exceptional-candidates exceptional-candidates
                 :action action
                 :alt-action minibuffer-alternative-action
                 :base-position base-position
                 :base-prefix base-prefix
                 :ignore-case completion-ignore-case
                 :annotations minibuffer-completion-annotations
                 :minibuffer-state (when (minibufferp)
                                     (cons (minibuffer-contents) (point)))
                 :insert-choice-function
                 (let ((cprops completion-extra-properties))
                   (lambda (start end choice)
                     (unless (or (zerop (length prefix))
                                 (equal prefix
                                        (buffer-substring-no-properties
                                         (max (point-min)
                                              (- start (length prefix)))
                                         start)))
                       (message "*Completions* out of date"))
                     (when (> (point) end)
                       ;; Completion suffix has changed, have to adapt.
                       (setq end (+ end
                                    (cdr (completion-boundaries
                                          (concat prefix choice) ctable cpred
                                          (buffer-substring end (point))))))
                       ;; Stopped before some field boundary.
                       (when (> (point) end)
                         (setq field-char (char-after end))))
                     (when (and field-char
                                (= (aref choice (1- (length choice)))
                                   field-char))
                       (setq end (1+ end)))
                     (cl-decf (nth 1 base-position) (- end start (length choice)))
                     ;; FIXME: Use `md' to do quoting&terminator here.
                     (completion--replace start (min end (point-max)) choice)
                     (let* ((minibuffer-completion-table ctable)
                            (minibuffer-completion-predicate cpred)
                            (completion-extra-properties cprops)
                            (result (concat prefix choice))
                            (bounds (completion-boundaries
                                     result ctable cpred "")))
                       ;; If the completion introduces a new field, then
                       ;; completion is not finished.
                       (completion--done result
                                         (if (eq (car bounds) (length result))
                                             'exact 'finished))))))))
        (unless (minibufferp)
          (set-transient-map completion-in-region-mode-map t #'minibuffer-hide-completions))))))

(defun completions-display (completions &rest plist)
  "Display COMPLETIONS in the buffer specified by `standard-output'.

PLIST is a property list with optional extra information about COMPLETIONS."
  (let* ((mainbuf (current-buffer))
         (buf standard-output)
         (group-fun (plist-get plist :group-function))
         (current
          (when-let ((win (get-buffer-window buf 0)))
            (get-text-property (window-point win) 'completion--string buf)))
         (prev-next
          (when current
            (with-current-buffer buf
              (save-excursion
                (goto-char (point-min))
                (text-property-search-forward 'completion--string current t)
                (cons
                 (save-excursion
                   (when-let ((pm (text-property-search-backward
                                   'completion--string current)))
                     (goto-char (prop-match-end pm))
                     (when-let ((pm (text-property-search-backward
                                     'cursor-face nil)))
                       (goto-char (prop-match-beginning pm))
                       (get-text-property (point) 'completion--string))))
                 (save-excursion
                   (when-let ((pm (text-property-search-forward
                                   'cursor-face nil t)))
                     (goto-char (prop-match-end pm))
                     (get-text-property (point) 'completion--string)))))))))
    (with-current-buffer buf
      (completion-list-mode)
      (let ((inhibit-read-only t))
        (erase-buffer)
        (delete-all-overlays)
        (completion--insert-strings completions group-fun))
      (goto-char (point-min))
      (when-let
          ((pm
            (or (and current (text-property-search-forward 'completion--string current t))
                (when-let ((next (cdr prev-next)))
                  (text-property-search-forward 'completion--string next t))
                (when-let ((prev (car prev-next)))
                  (text-property-search-forward 'completion--string prev t)))))
        (goto-char (prop-match-beginning pm))
        (setq pm (text-property-search-forward 'cursor-face))
        (setq-local cursor-face-highlight-nonselected-window t)
        (set-window-point (get-buffer-window) (prop-match-beginning pm)))
      (setq-local
       completion-reference-buffer mainbuf
       completions-candidates (mapcar (lambda (c) (if (atom c) c (car c))) completions)
       completions-group-function group-fun
       completions-annotations (plist-get plist :annotations)
       completions-style (plist-get plist :style)
       completion-base-position (plist-get plist :base-position)
       completions-base-prefix (plist-get plist :base-prefix)
       completion-list-insert-choice-function (plist-get plist :insert-choice-function)
       completions-category (plist-get plist :category)
       completions-sort-function (plist-get plist :sort-function)
       completions-sort-orders (plist-get plist :sort-orders)
       completions-predicate (plist-get plist :predicate)
       completions-exceptional-candidates (plist-get plist :exceptional-candidates)
       completions-ignore-case (plist-get plist :ignore-case)
       completions-action (plist-get plist :action)
       completions-alternative-action (plist-get plist :alt-action)
       completions-minibuffer-state (plist-get plist :minibuffer-state)))
    (run-hooks 'completion-setup-hook)
    (display-buffer buf
                    `((display-buffer-reuse-window display-buffer-at-bottom)
                      (window-height . completions--fit-window-to-buffer)
                      (preserve-size . (nil . t))
                      (dedicated     . soft)
                      (window-parameters
                       (mode-line-format   . none)
                       (header-line-format . ,completions-header-format)
                       (split-window       . ignore))))))

(defun display-completion-list (completions &optional group-fun)
  "Display the list of completions, COMPLETIONS, using `standard-output'.
Each element may be just a symbol or string
or may be a list of two strings to be printed as if concatenated.
If it is a list of two strings, the first is the actual completion
alternative, the second serves as annotation.
`standard-output' must be a buffer.
The actual completion alternatives, as inserted, are given `mouse-face'
properties of `highlight'.
At the end, this runs the normal hook `completion-setup-hook'.
It can find the completion buffer in `standard-output'.

Optional argument GROUP-FUN, if non-nil, is a completions grouping
function as described in the documentation of `completion-metadata'."
  (completions-display completions :group-function group-fun))

(defun minibuffer-hide-completions ()
  "Get rid of an out-of-date *Completions* buffer."
  (interactive "" minibuffer-mode)
  (let ((win (get-buffer-window completions-buffer-name 0)))
    (if win (with-selected-window win (bury-buffer)))))

(defun exit-minibuffer ()
  "Terminate this minibuffer argument."
  (interactive "" minibuffer-mode)
  (when (minibufferp)
    (when (not (minibuffer-innermost-command-loop-p))
      (error "%s" "Not in most nested command loop"))
    (when (not (innermost-minibuffer-p))
      (error "%s" "Not in most nested minibuffer")))
  ;; If the command that uses this has made modifications in the minibuffer,
  ;; we don't want them to cause deactivation of the mark in the original
  ;; buffer.
  ;; A better solution would be to make deactivate-mark buffer-local
  ;; (or to turn it into a list of buffers, ...), but in the mean time,
  ;; this should do the trick in most cases.
  (setq deactivate-mark nil)
  (throw 'exit nil))

(defun minibuffer-quit-recursive-edit (&optional levels)
  "Quit the command that requested this recursive edit or minibuffer input.
Do so without terminating keyboard macro recording or execution.
LEVELS specifies the number of nested recursive edits to quit.
If nil, it defaults to 1."
  (unless levels
    (setq levels 1))
  (if (> levels 1)
      ;; See Info node `(elisp)Recursive Editing' for an explanation
      ;; of throwing a function to `exit'.
      (throw 'exit (lambda () (minibuffer-quit-recursive-edit (1- levels))))
    (throw 'exit (lambda () (signal 'minibuffer-quit nil)))))

(defvar completion-in-region-function #'completion--in-region
  "Function to perform the job of `completion-in-region'.
The function is called with 4 arguments: START END COLLECTION PREDICATE.
The arguments and expected return value are as specified for
`completion-in-region'.")

(defun completion-in-region (start end collection &optional predicate)
  "Complete the text between START and END using COLLECTION.
Point needs to be somewhere between START and END.
PREDICATE (a function called with no arguments) says when to exit.
This calls the function that `completion-in-region-function' specifies
\(passing the same four arguments that it received) to do the work,
and returns whatever it does.  The return value should be nil
if there was no valid completion, else t."
  (cl-assert (<= start (point)) (<= (point) end))
  (funcall completion-in-region-function start end collection predicate))

(defcustom read-file-name-completion-ignore-case
  (if (memq system-type '(ms-dos windows-nt darwin cygwin))
      t nil)
  "Non-nil means when reading a file name completion ignores case."
  :type 'boolean
  :version "22.1")

(defun completion--in-region (start end collection &optional predicate)
  "Default function to use for `completion-in-region-function'.
Its arguments and return value are as specified for `completion-in-region'."
  (let ((minibuffer-completion-table collection)
        (minibuffer-completion-predicate predicate))
    (completion--in-region-1 start end)))

(defvar-keymap completion-in-region-mode-map
  :doc "Keymap activated during `completion-in-region'."
  "M-?"       #'completion-help-at-point
  "M-<up>"    #'minibuffer-previous-line-completion
  "M-<down>"  #'minibuffer-next-line-completion
  "M-<left>"  #'minibuffer-previous-completion
  "M-<right>" #'minibuffer-next-completion
  "M-RET"     #'minibuffer-choose-completion)

(defvar completion-at-point-functions nil
  "Special hook to find the completion table for the entity at point.
Each function on this hook is called in turn without any argument and
should return either nil, meaning it is not applicable at point,
or a list of the form (START END COLLECTION . PROPS), where:
 START and END delimit the entity to complete and should include point,
 COLLECTION is the completion table to use to complete the entity, and
 PROPS is a property list for additional information.
Currently supported properties are all the properties that can appear in
`completion-extra-properties' plus:
 `:predicate'	a predicate that completion candidates need to satisfy.
 `:exclusive'	value of `no' means that if the completion table fails to
   match the text at point, then instead of reporting a completion
   failure, the completion should try the next completion function.
As is the case with most hooks, the functions are responsible for
preserving things like point and current buffer.

NOTE: These functions should be cheap to run since they're sometimes
run from `post-command-hook'; and they should ideally only choose
which kind of completion table to use, and not pre-filter it based
on the current text between START and END (e.g., they should not
obey `completion-styles').")

(defun completion--capf-wrapper (fun &optional _)
  (let ((res (funcall fun)))
    (cond
     ((and (consp res) (not (functionp res)))
      (and (eq 'no (plist-get (nthcdr 3 res) :exclusive))
           (null (completion-try-completion (buffer-substring-no-properties
                                             (car res) (point))
                                            (nth 2 res)
                                            (plist-get (nthcdr 3 res) :predicate)
                                            (- (point) (car res))))
           (setq res nil))))
    res))

(defun completion-at-point-function-with-frecency-sorting
    (capf &optional num-ts)
  (let ((cache (make-hash-table :test #'equal))
        (num-ts (or num-ts 64)))
    (lambda ()
      (let ((res (ignore-errors (funcall capf))))
        (and (consp res)
             (not (functionp res))
             (seq-let (beg end table &rest plist) res
               (let* ((pred (plist-get plist :predicate))
                      (completion-extra-properties plist)
                      (md (completion-metadata (buffer-substring beg end) table pred))
                      (sf (completion-metadata-get md 'sort-function)))
                 `( ,beg ,end
                    ,(completion-table-with-metadata
                      table
                      `((sort-function
                         . ,(lambda (completions)
                              (sort (if sf (funcall sf completions) completions)
                                    :key (let ((now (float-time)))
                                           (lambda (c)
                                             (if-let ((ts (gethash c cache)))
                                                 (- (log (- now (car ts))) (length ts))
                                               1.0e+INF)))
                                    :in-place t)))))
                    ,@(plist-put
                       (copy-sequence plist)
                       :exit-function
                       (lambda (str _sts)
                         (let* ((str (substring-no-properties str))
                                (ts (gethash str cache)))
                           (setf (gethash str cache)
                                 (cons (float-time) (take num-ts ts))))))))))))))

(defun completion-at-point ()
  "Perform completion on the text around point.
The completion method is determined by `completion-at-point-functions'."
  (interactive)
  (pcase (run-hook-wrapped 'completion-at-point-functions #'completion--capf-wrapper)
    (`(,start ,end ,collection . ,plist)
     (unless (markerp start) (setq start (copy-marker start)))
     (let* ((completion-extra-properties plist))
       (completion-in-region start end collection
                             (plist-get plist :predicate))))))

(defun completion-help-at-point ()
  "Display the completions on the text around point.
The completion method is determined by `completion-at-point-functions'."
  (interactive)
  (pcase (run-hook-wrapped 'completion-at-point-functions #'completion--capf-wrapper)
    (`(,start ,end ,collection . ,plist)
     (unless (markerp start) (setq start (copy-marker start)))
     (let* ((minibuffer-completion-table collection)
            (minibuffer-completion-predicate (plist-get plist :predicate))
            (completion-extra-properties plist))
       (minibuffer-completion-help start end)))))

;;; Key bindings.

(let ((map minibuffer-local-map))
  (define-key map "\C-g" 'abort-minibuffers)
  (define-key map "\M-<" 'minibuffer-beginning-of-buffer)
  (define-key map "\C-x\M-k" 'minibuffer-kill-from-history)
  (define-key map "\C-x\M-h" 'minibuffer-alternate-history)
  (define-key map "\C-x\C-w" 'minibuffer-insert-symbol-at-point)
  (define-key map "\C-xj" 'minibuffer-set-action)
  (define-key map "\C-x\M-j" 'minibuffer-exchange-actions)
  (define-key map "\n" 'minibuffer-apply)
  (define-key map (kbd "C-S-j") 'minibuffer-apply-alt)
  (define-key map "\r" 'exit-minibuffer))

(defvar-keymap minibuffer-local-completion-map
  :doc "Local keymap for minibuffer input with completion."
  :parent minibuffer-local-map
  "TAB"       #'minibuffer-complete
  "C-o"       #'minibuffer-cycle-completion
  "C-M-o"     #'minibuffer-apply-and-cycle-completion
  "C-M-S-o"   #'minibuffer-apply-alt-and-cycle-completion
  "M-o"       #'minibuffer-cycle-completion-and-apply
  "M-O"       #'minibuffer-cycle-completion-and-apply-alt
  "M-S-o"     #'minibuffer-cycle-completion-and-apply-alt
  "C-l"       #'minibuffer-restore-completion-input
  "C-S-a"     #'minibuffer-toggle-completion-ignore-case
  "?"         #'minibuffer-completion-help
  "<prior>"   #'switch-to-completions
  "M-g M-c"   #'switch-to-completions
  "M-v"       #'switch-to-completions
  "C-v"       #'minibuffer-hide-completions
  "M-<up>"    #'minibuffer-previous-line-completion
  "M-<down>"  #'minibuffer-next-line-completion
  "M-<left>"  #'minibuffer-previous-completion
  "M-<right>" #'minibuffer-next-completion
  "M-RET"     #'minibuffer-choose-completion
  "M-j"       #'minibuffer-force-complete-and-exit
  "C-x C-v"   #'minibuffer-sort-completions
  "C-x C-<"   #'minibuffer-first-completion
  "C-x C->"   #'minibuffer-last-completion
  "C-x n"     'minibuffer-narrow-completions-map
  "C-x /"     #'minibuffer-set-completion-styles
  "C-x ~"     #'minibuffer-toggle-exceptional-candidates
  "C-x C-a"   #'minibuffer-toggle-completions-annotations
  "C-x C-."   #'minibuffer-auto-completion-mode
  "C-x M-e"   #'minibuffer-export
  "C-p"       #'minibuffer-previous-line-or-completion
  "C-n"       #'minibuffer-next-line-or-completion
  "C-%"       #'minibuffer-query-apply)

(defvar-keymap minibuffer-local-must-match-map
  :doc "Local keymap for minibuffer input with completion, for exact match."
  :parent minibuffer-local-completion-map
  "RET" #'minibuffer-complete-and-exit)

(defvar-keymap minibuffer-local-filename-completion-map
  :doc "This variable is obsolete and no longer used.")

(make-obsolete-variable 'minibuffer-local-filename-completion-map
                        nil "30.1")

(defvar-keymap minibuffer-narrow-completions-map
  :doc "Keymap for completions narrowing commands."
  :prefix 'minibuffer-narrow-completions-map
  "n" #'minibuffer-narrow-completions-to-current
  "h" #'minibuffer-narrow-completions-to-history
  "m" #'minibuffer-narrow-completions
  "g" #'minibuffer-narrow-completions-by-regexp
  "p" #'minibuffer-add-completion-predicate
  "-" #'minibuffer-negate-completion-predicate
  "w" #'minibuffer-widen-completions)

(defvar-keymap minibuffer-local-ns-map
  :doc "This variable is obsolete and no longer used.")

(make-obsolete-variable 'minibuffer-local-ns-map nil "30.1")

(define-obsolete-function-alias 'read-no-blanks-input 'read-string "30.1")

;;; Major modes for the minibuffer

(defvar-keymap minibuffer-inactive-mode-map
  :doc "Keymap for use in the minibuffer when it is not active.
The non-mouse bindings in this keymap can only be used in minibuffer-only
frames, since the minibuffer can normally not be selected when it is
not active."
  :full t
  :suppress t
  "e" #'find-file-other-frame
  "f" #'find-file-other-frame
  "b" #'switch-to-buffer-other-frame
  "i" #'info
  "m" #'mail
  "n" #'make-frame
  "<mouse-1>"      #'view-echo-area-messages
  ;; So the global down-mouse-1 binding doesn't clutter the execution of the
  ;; above mouse-1 binding.
  "<down-mouse-1>" #'ignore)

(define-derived-mode minibuffer-inactive-mode nil "InactiveMinibuffer"
  ;; Note: this major mode is called from minibuf.c.
  "Major mode to use in the minibuffer when it is not active.
This is only used when the minibuffer area has no active minibuffer.

Note that the minibuffer may change to this mode more often than
you might expect.  For instance, typing \\`M-x' may change the
buffer to this mode, then to a different mode, and then back
again to this mode upon exit.  Code running from
`minibuffer-inactive-mode-hook' has to be prepared to run
multiple times per minibuffer invocation.  Also see
`minibuffer-exit-hook'.")

(defvaralias 'minibuffer-mode-map 'minibuffer-local-map)

(define-derived-mode minibuffer-mode nil "Minibuffer"
  "Major mode used for active minibuffers.

For customizing this mode, it is better to use
`minibuffer-setup-hook' and `minibuffer-exit-hook' rather than
the mode hook of this mode."
  :syntax-table nil
  :interactive nil
  ;; Enable text conversion, but always make sure `RET' does
  ;; something.
  (setq text-conversion-style 'action))


;;; Completion tables.

(defun minibuffer--double-dollars (str)
  ;; Reuse the actual "$" from the string to preserve any text-property it
  ;; might have, such as `face'.
  (replace-regexp-in-string "\\$" (lambda (dollar) (concat dollar dollar))
                            str))

(defun minibuffer-maybe-quote-filename (filename)
  "Protect FILENAME from `substitute-in-file-name', as needed.
Useful to give the user default values that won't be substituted."
  (if (and (not (file-name-quoted-p filename))
           (file-name-absolute-p filename)
           (string-match-p (if (memq system-type '(windows-nt ms-dos))
                               "[/\\]~" "/~")
                           (file-local-name filename)))
      (file-name-quote filename)
    (minibuffer--double-dollars filename)))

(defun completion--make-envvar-table ()
  (mapcar (lambda (enventry)
            (substring enventry 0 (string-search "=" enventry)))
          process-environment))

(defconst completion--embedded-envvar-re
  ;; We can't reuse env--substitute-vars-regexp because we need to match only
  ;; potentially-unfinished envvars at end of string.
  (concat "\\(?:^\\|[^$]\\(?:\\$\\$\\)*\\)"
          "\\$\\([[:alnum:]_]*\\|{\\([^}]*\\)\\)\\'"))

(defun completion--embedded-envvar-table (string _pred action)
  "Completion table for envvars embedded in a string.
The envvar syntax (and escaping) rules followed by this table are the
same as `substitute-in-file-name'."
  ;; We ignore `pred', because the predicates passed to us via
  ;; read-file-name-internal are not 100% correct and fail here:
  ;; e.g. we get predicates like file-directory-p there, whereas the filename
  ;; completed needs to be passed through substitute-in-file-name before it
  ;; can be passed to file-directory-p.
  (when (string-match completion--embedded-envvar-re string)
    (let* ((beg (or (match-beginning 2) (match-beginning 1)))
           (table (completion--make-envvar-table))
           (prefix (substring string 0 beg)))
      (cond
       ((eq action 'lambda)
        ;; This table is expected to be used in conjunction with some
        ;; other table that provides the "main" completion.  Let the
        ;; other table handle the test-completion case.
        nil)
       ((or (eq (car-safe action) 'boundaries) (eq action 'metadata))
        ;; Only return boundaries/metadata if there's something to complete,
        ;; since otherwise when we're used in
        ;; completion-table-in-turn, we could return boundaries and
        ;; let some subsequent table return a list of completions.
        ;; FIXME: Maybe it should rather be fixed in
        ;; completion-table-in-turn instead, but it's difficult to
        ;; do it efficiently there.
        (when (try-completion (substring string beg) table nil)
          ;; Compute the boundaries of the subfield to which this
          ;; completion applies.
          (if (eq action 'metadata)
              '(metadata (category . environment-variable))
            (let ((suffix (cdr action)))
              `(boundaries
                ,(or (match-beginning 2) (match-beginning 1))
                . ,(when (string-match "[^[:alnum:]_]" suffix)
                     (match-beginning 0)))))))
       (t
        (if (eq (aref string (1- beg)) ?{)
            (setq table (apply-partially #'completion-table-with-terminator
                                         "}" table)))
        ;; Even if file-name completion is case-insensitive, we want
        ;; envvar completion to be case-sensitive.
        (let ((completion-ignore-case nil))
          (completion-table-with-context
           prefix table (substring string beg) nil action)))))))

(defun completion-file-name-affixation (files)
  "Return completion affixations for file list FILES."
  (let ((max-file (seq-max (cons 0 (mapcar #'string-width files)))))
    (mapcar
     (lambda (file)
       (let ((full (substitute-in-file-name
                    (concat minibuffer-completion-base file))))
         (list file ""
               (if-let ((ann (file-name-completion-annotation full)))
                   (concat (make-string (- (+ max-file 2)
                                           (string-width file))
                                        ?\s)
                           (propertize ann 'face 'completions-annotations))
                 ""))))
     files)))

(defun completion-file-name-normal-predicate (file-name)
  "Return non-nil if FILE-NAME is a normal file name completion candidate.

Normal candidates are all file names that don't end in one of the
strings in `completion-ignored-extensions'."
  (not (seq-some (lambda (ext) (string-suffix-p ext file-name))
                 completion-ignored-extensions)))

(defun completion-file-name-table (string pred action)
  "Completion table for file names."
  (condition-case nil
      (cond
       ((eq action 'metadata)
        `(metadata
          (category . file)
          (normal-predicate . completion-file-name-normal-predicate)
          ,@(when completions-detailed
              '((affixation-function . completion-file-name-affixation)))))
       ((string-match-p "\\`~[^/\\]*\\'" string)
        (completion-table-with-context "~"
                                       (mapcar (lambda (u) (concat u "/"))
                                               (system-users))
                                       (substring string 1)
                                       pred action))
       ((eq (car-safe action) 'boundaries)
        (let ((start (length (file-name-directory string)))
              (end (string-search "/" (cdr action))))
          `(boundaries
            ;; if `string' is "C:" in w32, (file-name-directory string)
            ;; returns "C:/", so `start' is 3 rather than 2.
            ;; Not quite sure what is The Right Fix, but clipping it
            ;; back to 2 will work for this particular case.  We'll
            ;; see if we can come up with a better fix when we bump
            ;; into more such problematic cases.
            ,(min start (length string)) . ,end)))

       (t
        (let* ((name (file-name-nondirectory string))
               (specdir (file-name-directory string))
               (realdir (or specdir default-directory)))

          (cond
           ((null action)
            (let ((comp (file-name-completion name realdir pred)))
              (if (stringp comp)
                  (concat specdir comp)
                comp)))

           ((eq action 'lambda)
            (or (not pred)
                (let ((default-directory (expand-file-name realdir)))
                  (funcall pred name))))

           ((eq action t)
            (let ((all (file-name-all-completions name realdir)))
              (when pred
                (setq all
                      (let ((default-directory (expand-file-name realdir)))
                        (seq-filter pred all))))
              all))))))
    (file-error nil)))               ;PCM often calls with invalid directories.

(defun completion--sifn-requote (upos qstr)
  ;; We're looking for (the largest) `qpos' such that:
  ;; (equal (substring (substitute-in-file-name qstr) 0 upos)
  ;;        (substitute-in-file-name (substring qstr 0 qpos)))
  ;; Big problem here: we have to reverse engineer substitute-in-file-name to
  ;; find the position corresponding to UPOS in QSTR, but
  ;; substitute-in-file-name can do anything, depending on file-name-handlers.
  ;; substitute-in-file-name does the following kind of things:
  ;; - expand env-var references.
  ;; - turn backslashes into slashes.
  ;; - truncate some prefix of the input.
  ;; - rewrite some prefix.
  ;; Some of these operations are written in external libraries and we'd rather
  ;; not hard code any assumptions here about what they actually do.  IOW, we
  ;; want to treat substitute-in-file-name as a black box, as much as possible.
  ;; Kind of like in rfn-eshadow-update-overlay, only worse.
  ;; Example of things we need to handle:
  ;; - Tramp (substitute-in-file-name "/foo:~/bar//baz") => "/scpc:foo:/baz".
  ;; - Cygwin (substitute-in-file-name "C:\bin") => "/usr/bin"
  ;;          (substitute-in-file-name "C:\") => "/"
  ;;          (substitute-in-file-name "C:\bi") => "/bi"
  (let* ((ustr (substitute-in-file-name qstr))
         (uprefix (substring ustr 0 upos))
         qprefix)
    (if (eq upos (length ustr))
        ;; Easy and common case.  This not only speed things up in a very
        ;; common case but it also avoids problems in some cases (bug#53053).
        (cons (length qstr) #'minibuffer-maybe-quote-filename)
      ;; Main assumption: nothing after qpos should affect the text before upos,
      ;; so we can work our way backward from the end of qstr, one character
      ;; at a time.
      ;; Second assumption: If qpos is far from the end this can be a bit slow,
      ;; so we speed it up by doing a first loop that skips a word at a time.
      ;; This word-sized loop is careful not to cut in the middle of env-vars.
      (while (let ((boundary (string-match "\\(\\$+{?\\)?\\w+\\W*\\'" qstr)))
               (and boundary
                    ;; Try and make sure we keep the largest `qpos' (bug#72176).
                    (not (string-match-p "/[/~]" qstr boundary))
                    (progn
                      (setq qprefix (substring qstr 0 boundary))
                      (string-prefix-p uprefix
                                       (substitute-in-file-name qprefix)))))
        (setq qstr qprefix))
      (let ((qpos (length qstr)))
        (while (and (> qpos 0)
                    (string-prefix-p uprefix
                                     (substitute-in-file-name
                                      (substring qstr 0 (1- qpos)))))
          (setq qpos (1- qpos)))
        (cons qpos #'minibuffer-maybe-quote-filename)))))

(defalias 'completion--file-name-table
  (completion-table-with-quoting #'completion-file-name-table
                                 #'substitute-in-file-name
                                 #'completion--sifn-requote)
  "Internal subroutine for `read-file-name'.  Do not call this.
This is a completion table for file names, like `completion-file-name-table'
except that it passes the file name through `substitute-in-file-name'.")

(defalias 'read-file-name-internal
  (completion-table-in-turn #'completion--embedded-envvar-table
                            #'completion--file-name-table)
  "Internal subroutine for `read-file-name'.  Do not call this.")

(defvar read-file-name-function #'read-file-name-default
  "The function called by `read-file-name' to do its work.
It should accept the same arguments as `read-file-name'.")

(defcustom insert-default-directory t
  "Non-nil means when reading a filename start with default dir in minibuffer.

When the initial minibuffer contents show a name of a file or a directory,
typing RETURN without editing the initial contents is equivalent to typing
the default file name.

If this variable is non-nil, the minibuffer contents are always
initially non-empty, and typing RETURN without editing will fetch the
default name, if one is provided.  Note however that this default name
is not necessarily the same as initial contents inserted in the minibuffer,
if the initial contents is just the default directory.

If this variable is nil, the minibuffer often starts out empty.  In
that case you may have to explicitly fetch the next history element to
request the default name; typing RETURN without editing will leave
the minibuffer empty.

For some commands, exiting with an empty minibuffer has a special meaning,
such as making the current buffer visit no file in the case of
`set-visited-file-name'."
  :type 'boolean)

(defcustom minibuffer-beginning-of-buffer-movement nil
  "Control how the \\<minibuffer-local-map>\\[minibuffer-beginning-of-buffer] \
command in the minibuffer behaves.
If non-nil, the command will go to the end of the prompt (if
point is after the end of the prompt).  If nil, it will behave
like the `beginning-of-buffer' command."
  :version "27.1"
  :type 'boolean)

;; Not always defined, but only called if next-read-file-uses-dialog-p says so.
(declare-function x-file-dialog "xfns.c"
                  (prompt dir &optional default-filename mustmatch only-dir-p))

(defun read-file-name--defaults (&optional dir initial)
  (let ((default
	  (cond
	   ;; With non-nil `initial', use `dir' as the first default.
	   ;; Essentially, this mean reversing the normal order of the
	   ;; current directory name and the current file name, i.e.
	   ;; 1. with normal file reading:
	   ;; 1.1. initial input is the current directory
	   ;; 1.2. the first default is the current file name
	   ;; 2. with non-nil `initial' (e.g. for `find-alternate-file'):
	   ;; 2.2. initial input is the current file name
	   ;; 2.1. the first default is the current directory
	   (initial (abbreviate-file-name dir))
	   ;; In file buffers, try to get the current file name
	   (buffer-file-name
	    (abbreviate-file-name buffer-file-name))))
	(file-name-at-point
	 (run-hook-with-args-until-success 'file-name-at-point-functions)))
    (when file-name-at-point
      (setq default (delete-dups
		     (delete "" (delq nil (list file-name-at-point default))))))
    ;; Append new defaults to the end of existing `minibuffer-default'.
    (append
     (if (listp minibuffer-default) minibuffer-default (list minibuffer-default))
     (if (listp default) default (list default)))))

(defun read-file-name (prompt &optional dir default-filename mustmatch initial predicate)
  "Read file name, prompting with PROMPT and completing in directory DIR.
The return value is not expanded---you must call `expand-file-name' yourself.

DIR is the directory to use for completing relative file names.
It should be an absolute directory name, or nil (which means the
current buffer's value of `default-directory').

DEFAULT-FILENAME specifies the default file name to return if the
user exits the minibuffer with the same non-empty string inserted
by this function.  If DEFAULT-FILENAME is a string, that serves
as the default.  If DEFAULT-FILENAME is a list of strings, the
first string is the default.  If DEFAULT-FILENAME is omitted or
nil, then if INITIAL is non-nil, the default is DIR combined with
INITIAL; otherwise, if the current buffer is visiting a file,
that file serves as the default; otherwise, the default is simply
the string inserted into the minibuffer.

If the user exits with an empty minibuffer, return an empty
string.  (This happens only if the user erases the pre-inserted
contents, or if `insert-default-directory' is nil.)

Fourth arg MUSTMATCH can take the following values:
- nil means that the user can exit with any input.
- t means that the user is not allowed to exit unless
  the input is (or completes to) an existing file.
- `confirm' means that the user can exit with any input, but she needs
  to confirm her choice if the input is not an existing file.
- `confirm-after-completion' means that the user can exit with any
  input, but she needs to confirm her choice if she called
  `minibuffer-complete' right before `minibuffer-complete-and-exit'
  and the input is not an existing file.
- a function, which will be called with a single argument, the
  input unquoted by `substitute-in-file-name', which see.  If the
  function returns a non-nil value, the minibuffer is exited with
  that argument as the value.
- anything else behaves like t except that typing RET does not exit if it
  does non-null completion.

Fifth arg INITIAL specifies text to start with.  It will be
interpreted as the trailing part of DEFAULT-FILENAME, so using a
full file name for INITIAL will usually lead to surprising
results.

Sixth arg PREDICATE, if non-nil, should be a function of one
argument; then a file name is considered existing (and hence an
acceptable completion alternative) only if PREDICATE returns
non-nil with the file name as its argument.  PREDICATE is called
with `default-directory' bound to the direcotry part of the
candidate file name, while the argument passed to PREDICATE does
not include a directory part.  If PREDICATE is omitted or nil, it
defaults to `file-exists-p'.

If this command was invoked with the mouse, use a graphical file
dialog if `use-dialog-box' is non-nil, and the window system or X
toolkit in use provides a file dialog box, and DIR is not a
remote file.  For graphical file dialogs, any of the special values
of MUSTMATCH `confirm' and `confirm-after-completion' are
treated as equivalent to nil.  Some graphical file dialogs respect
a MUSTMATCH value of t, and some do not (or it only has a cosmetic
effect, and does not actually prevent the user from entering a
non-existent file).

See also `read-file-name-completion-ignore-case'
and `read-file-name-function'."
  ;; If x-gtk-use-old-file-dialog = t (xg_get_file_with_selection),
  ;; then MUSTMATCH is enforced.  But with newer Gtk
  ;; (xg_get_file_with_chooser), it only has a cosmetic effect.
  ;; The user can still type a non-existent file name.
  (funcall (or read-file-name-function #'read-file-name-default)
           prompt dir default-filename mustmatch initial predicate))

(defvar minibuffer-local-filename-syntax
  (let ((table (make-syntax-table))
	(punctuation (car (string-to-syntax "."))))
    ;; Convert all punctuation entries to symbol.
    (map-char-table (lambda (c syntax)
		      (when (eq (car syntax) punctuation)
			(modify-syntax-entry c "_" table)))
		    table)
    (mapc
     (lambda (c)
       (modify-syntax-entry c "." table))
     '(?/ ?: ?\\))
    table)
  "Syntax table used when reading a file name in the minibuffer.")

(defvar-local minibuffer-completing-file-name nil
  "Whether the current minibuffer reads a file name.")

(defun minibuffer--sort-file-names-by-last-modified-time (files)
  "Sort file name completion candidates FILES by last modified time."
  (sort files
        :key (lambda (f)
               (list (or (file-attribute-modification-time
                          (ignore-errors
                            (file-attributes
                             (substitute-in-file-name
                              (concat minibuffer-completion-base f)))))
                         `(,most-positive-fixnum))
                     f))
        :reverse t))

(defun minibuffer--file-name-sort-by-history-key (hist)
  (let ((expanded-hist (mapcar #'expand-file-name hist)))
    (lambda (f)
      (list (or (seq-position expanded-hist
                              (expand-file-name f minibuffer-completion-base)
                              (lambda (h c)
                                (or (and (string= (file-name-directory c) c)
                                         (string-prefix-p c h))
                                    (string= h c))))
                most-positive-fixnum)
            f))))

(defun read-file-name-default (prompt &optional dir default-filename mustmatch initial predicate)
  "Default method for reading file names.
See `read-file-name' for the meaning of the arguments."
  (unless dir (setq dir (or default-directory "~/")))
  (unless (file-name-absolute-p dir) (setq dir (expand-file-name dir)))
  (unless default-filename
    (setq default-filename
          (cond
           ((null initial) buffer-file-name)
           ;; Special-case "" because (expand-file-name "" "/tmp/") returns
           ;; "/tmp" rather than "/tmp/" (bug#39057).
           ((equal "" initial) dir)
           (t (expand-file-name initial dir)))))
  ;; If dir starts with user's homedir, change that to ~.
  (setq dir (abbreviate-file-name dir))
  ;; Likewise for default-filename.
  (if default-filename
      (setq default-filename
	    (if (consp default-filename)
		(mapcar 'abbreviate-file-name default-filename)
	      (abbreviate-file-name default-filename))))
  (let ((insdef (cond
                 ((and insert-default-directory (stringp dir))
                  (if initial
                      (cons (minibuffer-maybe-quote-filename (concat dir initial))
                            (length (minibuffer-maybe-quote-filename dir)))
                    (minibuffer-maybe-quote-filename dir)))
                 (initial (cons (minibuffer-maybe-quote-filename initial) 0)))))

    (let ((ignore-case read-file-name-completion-ignore-case)
          (pred (or predicate 'file-exists-p))
          (add-to-history nil)
          (require-match (if (functionp mustmatch)
                             (lambda (input)
                               (funcall mustmatch
                                        ;; User-supplied MUSTMATCH expects an unquoted filename
                                        (substitute-in-file-name input)))
                           mustmatch)))

      (let* ((val
              (if (or (not (next-read-file-uses-dialog-p))
                      ;; Graphical file dialogs can't handle remote
                      ;; files (Bug#99).
                      (file-remote-p dir))
                  ;; We used to pass `dir' to `read-file-name-internal' by
                  ;; abusing the `predicate' argument.  It's better to
                  ;; just use `default-directory', but in order to avoid
                  ;; changing `default-directory' in the current buffer,
                  ;; we don't let-bind it.
                  (let ((dir (file-name-as-directory
                              (expand-file-name dir))))
                    (minibuffer-with-setup-hook
                        (lambda ()
                          (setq default-directory dir)
                          (setq-local minibuffer-completing-file-name t)
                          ;; When the first default in `minibuffer-default'
                          ;; duplicates initial input `insdef',
                          ;; reset `minibuffer-default' to nil.
                          (when (equal (or (car-safe insdef) insdef)
                                       (or (car-safe minibuffer-default)
                                           minibuffer-default))
                            (setq minibuffer-default
                                  (cdr-safe minibuffer-default)))
                          (setq-local completion-ignore-case ignore-case)
                          ;; On the first request on `M-n' fill
                          ;; `minibuffer-default' with a list of defaults
                          ;; relevant for file-name reading.
                          (setq-local minibuffer-default-add-function
                               (lambda ()
                                 (with-current-buffer
                                     (window-buffer (minibuffer-selected-window))
                                   (read-file-name--defaults dir initial))))
                          (setq-local
                           minibuffer-completions-sort-orders
                           (cons '(?m "modified" "Sort by last modified time"
                                      minibuffer--sort-file-names-by-last-modified-time
                                      "latest modified first")
                                 minibuffer-completions-sort-orders)
                           minibuffer-sort-by-history-key-function
                           #'minibuffer--file-name-sort-by-history-key)
			  (set-syntax-table minibuffer-local-filename-syntax))
                      (completing-read prompt 'read-file-name-internal
                                       pred require-match insdef
                                       'file-name-history default-filename)))
                ;; If DEFAULT-FILENAME not supplied and DIR contains
                ;; a file name, split it.
                (let ((file (file-name-nondirectory dir))
                      ;; When using a dialog, revert to nil and non-nil
                      ;; interpretation of mustmatch. confirm options
                      ;; need to be interpreted as nil, otherwise
                      ;; it is impossible to create new files using
                      ;; dialogs with the default settings.
                      (dialog-mustmatch
                       (not (memq mustmatch
                                  '(nil confirm confirm-after-completion)))))
                  (when (and (not default-filename)
                             (not (zerop (length file))))
                    (setq default-filename file)
                    (setq dir (file-name-directory dir)))
                  (when default-filename
                    (setq default-filename
                          (expand-file-name (if (consp default-filename)
                                                (car default-filename)
                                              default-filename)
                                            dir)))
                  (setq add-to-history t)
                  (x-file-dialog prompt dir default-filename
                                 dialog-mustmatch
                                 (eq predicate 'file-directory-p)))))

             (replace-in-history (eq (car-safe file-name-history) val)))
        ;; If completing-read returned the inserted default string itself
        ;; (rather than a new string with the same contents),
        ;; it has to mean that the user typed RET with the minibuffer empty.
        ;; In that case, we really want to return ""
        ;; so that commands such as set-visited-file-name can distinguish.
        (when (consp default-filename)
          (setq default-filename (car default-filename)))
        (when (eq val default-filename)
          ;; In this case, completing-read has not added an element
          ;; to the history.  Maybe we should.
          (if (not replace-in-history)
              (setq add-to-history t))
          (setq val ""))
        (unless val (error "No file name specified"))

        (if (and default-filename
                 (string-equal val (if (consp insdef) (car insdef) insdef)))
            (setq val default-filename))
        (setq val (substitute-in-file-name val))

        (if replace-in-history
            ;; Replace what Fcompleting_read added to the history
            ;; with what we will actually return.  As an exception,
            ;; if that's the same as the second item in
            ;; file-name-history, it's really a repeat (Bug#4657).
            (let ((val1 (minibuffer-maybe-quote-filename val)))
              (if history-delete-duplicates
                  (setcdr file-name-history
                          (delete val1 (cdr file-name-history))))
              (if (string= val1 (cadr file-name-history))
                  (pop file-name-history)
                (setcar file-name-history val1)))
          (when add-to-history
            (add-to-history 'file-name-history
                            (minibuffer-maybe-quote-filename val))))
	val))))

(defun minibuffer-narrow-buffer-completions ()
  "Restrict buffer name completions by candidate major mode.

`completion-buffer-name-table' uses this function as its
`narrow-completions-function'."
  (let* ((names
          (let* ((beg (minibuffer-prompt-end))
                 (end (point-max))
                 (input (buffer-substring beg end))
                 (all (completion-all-completions
                       input
                       minibuffer-completion-table
                       minibuffer-completion-predicate
                       (- (point) beg)
                       (completion--field-metadata beg)))
                 (last (last all)))
            (when (consp last) (setcdr last nil))
            all))
         (modes (delete-dups
                 (mapcar
                  (apply-partially #'string-replace "-mode" "")
                  (mapcar
                   #'symbol-name
                   (mapcar
                    (apply-partially #'buffer-local-value 'major-mode)
                    (mapcar #'get-buffer names))))))
         (max (seq-max (cons 0 (mapcar #'string-width modes))))
         (name
          (completing-read
           "Restrict to mode: "
           (completion-table-with-metadata
            modes (list (cons
                         'annotation-function
                         (lambda (cand)
                           (let* ((sym (intern (concat cand "-mode")))
                                  (doc (ignore-errors (documentation sym))))
                             (when doc
                               (concat
                                (make-string
                                 (- (+ max 2) (string-width cand)) ?\s)
                                (propertize
                                 (substring doc 0 (string-search "\n" doc))
                                 'face 'completions-annotations))))))))
           nil t))
         (mode (intern (concat name "-mode"))))
    (cons
     (lambda (cand)
       (eq mode (buffer-local-value 'major-mode (get-buffer cand))))
     (format "mode=%s" (capitalize name)))))

(defun completion-buffer-name-affixation (names)
  "Return completion affixations for buffer name list NAMES."
  (let ((max-name (seq-max (cons 0 (mapcar #'string-width names))))
        (max-mode
         (seq-max
          (cons 0 (mapcar #'string-width
                          (mapcar #'symbol-name
                                  (mapcar (apply-partially #'buffer-local-value
                                                           'major-mode)
                                          (delete nil (mapcar #'get-buffer names)))))))))
    (mapcar
     (lambda (name)
       (if-let* ((buf (get-buffer name))
                 (mode (capitalize
                        (string-replace
                         "-mode" ""
                         (symbol-name (buffer-local-value 'major-mode buf))))))
           (list name
                 (concat (if (and (buffer-modified-p buf)
                                  (buffer-file-name buf))
                             (propertize "*" 'face 'completions-annotations) " ")
                         " ")
                 (propertize
                  (concat (make-string (- (+ max-name 2)
                                          (string-width name))
                                       ?\s)
                          mode
                          (make-string (- (+ max-mode 2)
                                          (string-width mode))
                                       ?\s)
                          (if-let ((file-name (buffer-file-name buf)))
                              (abbreviate-file-name file-name)
                            (if-let ((proc (get-buffer-process buf)))
                                (format "%s (%s)"
                                        (process-name proc)
                                        (process-status proc))
                              (abbreviate-file-name
                               (buffer-local-value 'default-directory buf)))))
                  'face 'completions-annotations))
         (list name (concat (propertize "X" 'face 'completions-annotations)
                            " ")
               "")))
     names)))

(defun completion-buffer-name-normal-predicate (buffer-name)
  "Return non-nil if BUFFER-NAME is a normal buffer name completion candidate.

Normal candidates are buffer names that don't start with a space.
Buffer names that begin with a space are for internal buffers, so in
other words, this function returns nil for internal buffer names."
  (not (or (string-empty-p buffer-name) (eq (aref buffer-name 0) ?\s))))

(defun completion-buffer-name-table ()
  "Return a completion table for buffer names.

When the value of variable `read-buffer-to-switch-current-buffer' is a
buffer, the completion table excludles that buffer from the list of
possible completions."
  (completion-table-with-metadata
   (completion-table-dynamic
    (lambda (&rest _)
      (mapcar #'buffer-name
              (remove read-buffer-to-switch-current-buffer (buffer-list)))))
   `((category . buffer)
     (sort-function . identity)
     (narrow-completions-function . minibuffer-narrow-buffer-completions)
     (normal-predicate . completion-buffer-name-normal-predicate)
     ,@(when completions-detailed
         '((affixation-function . completion-buffer-name-affixation))))))

(define-obsolete-function-alias 'internal-complete-buffer-except
  'completion-buffer-name-table "30.1")

(define-obsolete-function-alias 'internal-complete-buffer
  'completion-buffer-name-table "30.1")

(defun minibuffer-current-input ()
  (let* ((beg-end (minibuffer--completion-boundaries))
         (beg (car beg-end)) (end (cdr beg-end))
         (str (buffer-substring beg end))
         (prf (buffer-substring (minibuffer-prompt-end) beg)))
    (when-let ((adjust-fn (alist-get 'adjust-base-function
                                     (completion-metadata
                                      (concat prf str)
                                      minibuffer-completion-table
                                      minibuffer-completion-predicate))))
      (setq prf (funcall adjust-fn prf)))
    (and minibuffer-completing-file-name
         (string-empty-p str) (string-suffix-p "/" prf)
         (let ((tmp prf))
           (setq prf (file-name-parent-directory prf)
                 str (file-relative-name tmp prf))))
    (cons str prf)))

(defun minibuffer--get-action (symbol)
  (when-let ((action (get symbol 'minibuffer-action)))
    (cond
     ((consp action) action)
     ((symbolp action) (minibuffer--get-action action))
     (t (cons symbol action)))))

(defvar-local current-minibuffer-command nil
  "Command that invoked the current minibuffer.")

(defun minibuffer-record-command ()
  (setq-local current-minibuffer-command this-command))

(defvar-local minibuffer-prompt-indicators-overlay nil)

(defface minibuffer-action-prompt-indicator-highlight
  '((t :inherit mode-line-highlight))
  "Face for minibuffer action prompt indicator when mouse is over it.")

(defface minibuffer-alt-action-prompt-indicator-highlight
  '((t :inherit mode-line-highlight))
  "Face for minibuffer action prompt indicator when mouse is over it.")

(defface minibuffer-completion-prompt-indicator-highlight
  '((t :inherit mode-line-highlight))
  "Face for minibuffer completion prompt indicator when mouse is over it.")

(defcustom minibuffer-action-prompt-indicator "<"
  "String to show in minibuffer prompt when there's an available action."
  :type 'string
  :version "31.1"
  :group 'minibuffer
  :risky t)

(defcustom minibuffer-alt-action-prompt-indicator "<"
  "String to show in minibuffer prompt when there's an alternative action."
  :type 'string
  :version "31.1"
  :group 'minibuffer
  :risky t)

(defcustom minibuffer-strict-prompt-indicator "=>"
  "String to show in minibuffer prompt to indicate strict completion."
  :type 'string
  :version "31.1"
  :group 'minibuffer
  :risky t)

(defcustom minibuffer-lax-with-confirmation-prompt-indicator ">"
  "String to show in minibuffer prompt for lax completion with confirmation."
  :type 'string
  :version "31.1"
  :group 'minibuffer
  :risky t)

(defcustom minibuffer-lax-prompt-indicator ">"
  "String to show in minibuffer prompt to indicate lax completion."
  :type 'string
  :version "31.1"
  :group 'minibuffer
  :risky t)

(defvar minibuffer-action-prompt-indicator-format
  '(""
    (:eval
     (when-let ((desc (cdr (minibuffer-action))))
       (propertize
        minibuffer-action-prompt-indicator
        'help-echo (concat "\\<minibuffer-local-map>\\[minibuffer-apply]: " desc)
        'mouse-face 'minibuffer-action-prompt-indicator-highlight)))
    (minibuffer-alternative-action
     (:eval
      (propertize
       minibuffer-alt-action-prompt-indicator
       'help-echo (concat "\\<minibuffer-local-map>\\[minibuffer-apply-alt]: "
                          (cdr minibuffer-alternative-action))
       'mouse-face 'minibuffer-alt-action-prompt-indicator-highlight)))))

(defvar minibuffer-extra-prompt-indicators-format nil)

(defvar minibuffer-completion-prompt-indicator-format
  '(minibuffer-completion-table
    (minibuffer--require-match
     (minibuffer-completion-confirm
      (:propertize minibuffer-lax-with-confirmation-prompt-indicator
                   help-echo "Lax completion with confirmation"
                   mouse-face minibuffer-completion-prompt-indicator-highlight)
      (:propertize minibuffer-strict-prompt-indicator
                   help-echo "Strict completion"
                   mouse-face minibuffer-completion-prompt-indicator-highlight))
     (:propertize minibuffer-lax-prompt-indicator
                  help-echo "Lax completion"
                  mouse-face minibuffer-completion-prompt-indicator-highlight))))

(dolist (sym '(minibuffer-action-prompt-indicator-format
               minibuffer-extra-prompt-indicators-format
               minibuffer-completion-prompt-indicator-format))
  (put sym 'risky-local-variable t))

(defvar minibuffer-prompt-indicators-format
  '(""
    minibuffer-action-prompt-indicator-format
    minibuffer-extra-prompt-indicators-format
    minibuffer-completion-prompt-indicator-format)
  "Mode line construct used for formatting minibuffer prompt indicators.")

(defun minibuffer-update-prompt-indicators ()
  (unless (overlayp minibuffer-prompt-indicators-overlay)
    (setq-local minibuffer-prompt-indicators-overlay
                (make-overlay (point-min) (point-min))))
  (let ((ind (format-mode-line minibuffer-prompt-indicators-format 'minibuffer-prompt)))
    (unless (or (string-empty-p ind) (string-suffix-p " " ind))
      (setq ind (concat ind " ")))
    (overlay-put minibuffer-prompt-indicators-overlay 'before-string ind)))

(add-hook 'minibuffer-setup-hook #'minibuffer-record-command)
(add-hook 'minibuffer-setup-hook #'minibuffer-update-prompt-indicators 95)

(defun minibuffer-action (&optional alt)
  "Return the minibuffer action function for the current minibuffer.

If optional argument ALT is non-nil and there is an alternative
minibuffer action, return the alternative action instead."
  (or (and alt minibuffer-alternative-action)
      minibuffer-action
      (when-let* ((cmd current-minibuffer-command)
                  (cmd (car (last (cons cmd (function-alias-p cmd))))))
        (when (symbolp cmd) (minibuffer--get-action cmd)))))

(defun minibuffer-apply (input &optional prefix alt)
  "Apply minibuffer action to current INPUT.

Optional argument PREFIX, if non-nil, is a string to prepend to INPUT
before passing it to the action function.  If optional argument ALT is
non-nil and there is an alternative minibuffer action, apply the
alternative action instead.

Interactively, INPUT is the current minibuffer input sans the completion
base, PREFIX is the completion base, and ALT is nil."
  (interactive (let* ((input-prefix (minibuffer-current-input))
                      (input (car input-prefix))
                      (prefix (cdr input-prefix)))
                 (list input prefix))
               minibuffer-mode)
  (funcall
   (or (car (minibuffer-action alt)) (user-error "No applicable action"))
   (concat prefix input))
  (when-let ((buf (get-buffer completions-buffer-name))
             (win (get-buffer-window buf 0)))
    (with-current-buffer buf
      (save-excursion
        (goto-char (point-min))
        (when-let ((pm (or (and
                            (get-text-property 0 'completion-identity input)
                            (text-property-search-forward
                             'completion-identity
                             (get-text-property 0 'completion-identity input)
                             #'eq))
                           (text-property-search-forward
                            'completion--string input t))))
          (goto-char (prop-match-beginning pm))
          (setq pm (text-property-search-forward 'cursor-face))
          (let ((inhibit-read-only t))
            (add-face-text-property (prop-match-beginning pm) (point)
                                    (if alt 'completions-used-input-alt
                                      'completions-used-input))))))))

(defun minibuffer-apply-alt (input &optional prefix)
  "Apply alternative minibuffer action to current INPUT.

If there is no alternative action, apply the regular (non-alternative)
action instead.

Optional argument PREFIX, if non-nil, is a string to prepend to INPUT
before passing it to the action function.

Interactively, INPUT is the current minibuffer input sans the completion
base, and PREFIX is the completion base."
  (interactive (let* ((input-prefix (minibuffer-current-input))
                      (input (car input-prefix))
                      (prefix (cdr input-prefix)))
                 (list input prefix))
               minibuffer-mode)
  (minibuffer-apply input prefix t))

(defun minibuffer-query-apply (&optional alt)
  "Suggest applying the minibuffer action to each completion candidate in turn.

If optional argument ALT is non-nil and there is an alternative
minibuffer action, apply the alternative action instead."
  (interactive "P" minibuffer-mode)
  (with-minibuffer-completions-window
    (let (prev all done)
      (goto-char (point-min))
      ;; Clear `first-completion' property from beginning of buffer such
      ;; that first call to `minibuffer-next-completion' below "selects"
      ;; the first completion, instead of moving forward to the second.
      (when (get-text-property (point) 'first-completion)
        (let ((inhibit-read-only t))
          (remove-text-properties (point) (1+ (point)) '(first-completion))))
      (while (not done)
        (setq prev (point))
        (with-current-buffer completion-reference-buffer
          (minibuffer-next-completion))
        (if (<= prev (point))
            (pcase
                (or all (car (read-multiple-choice
                              (format "Apply \"%s\" to input?"
                                      (propertize (if alt
                                                      (cdr completions-alternative-action)
                                                    (cdr completions-action))
                                                  'face 'bold))
                              '((?y  "yes"  "Apply")
                                (?n  "no"   "Skip")
                                (?q  "quit" "Quit")
                                (?!  "all"  "Apply to all")))))
              (?y                       ; Apply.
               (with-current-buffer completion-reference-buffer
                 (call-interactively (if alt #'minibuffer-apply-alt
                                       #'minibuffer-apply))))
              (?n)                      ; Skip.
              (?q (setq done t))        ; Quit.
              (?!                       ; Apply to all.
               (with-current-buffer completion-reference-buffer
                 (call-interactively #'minibuffer-apply))
               (setq all ?y)))
          ;; We're back at the first candidate, stop.
          (setq done t)
          (message "Done"))))))

(defun minibuffer-function-affixation (cands)
  "Annotate completion candidates CANDS with their documentation strings."
  (let ((max (seq-max (cons 0 (mapcar #'string-width cands)))))
    (mapcar
     (lambda (cand)
       (let ((sym (intern cand)))
         (list cand ""
               (if-let ((doc (ignore-errors
                               (documentation sym))))
                   (concat (make-string (1+ (- max (string-width cand))) ?\s)
                           (propertize
                            (substring doc 0 (string-search "\n" doc))
                            'face 'completions-annotations))
                 ""))))
     cands)))

(defvar minibuffer-action-history nil
  "History list for `minibuffer-set-action'.")

(defun minibuffer-set-action (action-fn &optional alt)
  "Set minibuffer (ALT) action function of current minibuffer to ACTION-FN."
  (interactive
   (list (completing-read (format "Set %saction function: "
                                  (if current-prefix-arg "alternative " ""))
                          (completion-table-with-metadata
                           obarray '((category . function)))
                          #'fboundp
                          nil nil 'minibuffer-action-history)
         current-prefix-arg)
   minibuffer-mode)
  (when (stringp action-fn) (setq action-fn (read action-fn)))
  (let ((action (cons action-fn
                      (or (and (symbolp action-fn)
                               (cdr (minibuffer--get-action action-fn)))
                          "custom action"))))
    (if alt
        (setq-local minibuffer-alternative-action action)
      (setq-local minibuffer-action action)))
  (minibuffer-update-prompt-indicators))

(defun minibuffer-exchange-actions ()
  "Exchange minibuffer primary and alternative actions."
  (interactive "" minibuffer-mode)
  (if-let ((prm (minibuffer-action))
           (alt minibuffer-alternative-action))
      (progn
        (setq minibuffer-alternative-action prm
              minibuffer-action alt)
        (if (get-buffer-window completions-buffer-name 0)
            (minibuffer-completion-help)
          (minibuffer-message
           "Minibuffer action in now `%s', alternative is `%s'"
           (cdr minibuffer-action) (cdr minibuffer-alternative-action))))
    (user-error "No current alternative minibuffer action"))
  (minibuffer-update-prompt-indicators))

;;; Old-style completion, used in Emacs-21 and Emacs-22.

(defun completion-emacs21-try-completion (string table pred _point)
  (let ((completion (try-completion string table pred)))
    (if (stringp completion)
        (cons completion (length completion))
      completion)))

(defun completion-emacs21-all-completions (string table pred _point)
  (completion-hilit-commonality
   (all-completions string table pred)
   (length string)
   (car (completion-boundaries string table pred ""))))

(defun completion-emacs22-try-completion (string table pred point)
  (let ((suffix (substring string point))
        (completion (try-completion (substring string 0 point) table pred)))
    (cond
     ((eq completion t)
      (if (equal "" suffix)
          t
        (cons string point)))
     ((not (stringp completion)) completion)
     (t
      ;; Merge a trailing / in completion with a / after point.
      ;; We used to only do it for word completion, but it seems to make
      ;; sense for all completions.
      ;; Actually, claiming this feature was part of Emacs-22 completion
      ;; is pushing it a bit: it was only done in minibuffer-completion-word,
      ;; which was (by default) not bound during file completion, where such
      ;; slashes are most likely to occur.
      (if (and (not (zerop (length completion)))
               (eq ?/ (aref completion (1- (length completion))))
               (not (zerop (length suffix)))
               (eq ?/ (aref suffix 0)))
          ;; This leaves point after the / .
          (setq suffix (substring suffix 1)))
      (cons (concat completion suffix) (length completion))))))

(defun completion-emacs22-all-completions (string table pred point)
  (let ((beforepoint (substring string 0 point)))
    (completion-hilit-commonality
     (all-completions beforepoint table pred)
     point
     (car (completion-boundaries beforepoint table pred "")))))

;;; Basic completion.

(defun completion--merge-suffix (completion point suffix)
  "Merge end of COMPLETION with beginning of SUFFIX.
Simple generalization of the \"merge trailing /\" done in Emacs-22.
Return the new suffix."
  (if (and (not (zerop (length suffix)))
           (string-match "\\(.+\\)\n\\1" (concat completion "\n" suffix)
                         ;; Make sure we don't compress things to less
                         ;; than we started with.
                         point)
           ;; Just make sure we didn't match some other \n.
           (eq (match-end 1) (length completion)))
      (substring suffix (- (match-end 1) (match-beginning 1)))
    ;; Nothing to merge.
    suffix))

(defun completion-basic--pattern (beforepoint afterpoint bounds)
  (list (substring beforepoint (car bounds))
        'point
        (substring afterpoint 0 (cdr bounds))))

(defun completion-basic-try-completion (string table pred point)
  (let* ((beforepoint (substring string 0 point))
         (afterpoint (substring string point))
         (bounds (completion-boundaries beforepoint table pred afterpoint)))
    (if (zerop (cdr bounds))
        ;; `try-completion' may return a subtly different result
        ;; than `all+merge', so try to use it whenever possible.
        (let ((completion (try-completion beforepoint table pred)))
          (if (not (stringp completion))
              completion
            (cons
             (concat completion
                     (completion--merge-suffix completion point afterpoint))
             (length completion))))
      (let* ((suffix (substring afterpoint (cdr bounds)))
             (prefix (substring beforepoint 0 (car bounds)))
             (pattern (completion-pcm--optimize-pattern
                       (completion-basic--pattern
                        beforepoint afterpoint bounds)))
             (all (completion-pcm--all-completions prefix pattern table pred)))
        (completion-pcm--merge-try pattern all prefix suffix)))))

(defun completion-basic-all-completions (string table pred point)
  (let* ((beforepoint (substring string 0 point))
         (afterpoint (substring string point))
         (bounds (completion-boundaries beforepoint table pred afterpoint))
         (prefix (substring beforepoint 0 (car bounds)))
         (pattern
          (delete "" (completion-basic--pattern beforepoint afterpoint bounds)))
         (all (completion-pcm--all-completions prefix pattern table pred)))
    (when all
      (nconc (completion-pcm--hilit-commonality pattern all)
             (car bounds)))))

(defun completion-regexp-try-completion (regexp table pred point)
  "Try completing REGEXP with respect to TABLE and PRED with point at POINT."
  (pcase (let ((completion-lazy-hilit t))
           (completion-regexp-all-completions regexp table pred))
    ('nil nil)
    (`(,sole) (or (string= sole regexp) (cons sole (length sole))))
    (_ (cons regexp point))))

(defun completion-regexp-all-completions (regexp table pred &optional _point)
  "Return all candidates in TABLE that match REGEXP and satisfy PRED."
  (let* ((c-f-s case-fold-search)
         (hilit-fn
          (lambda (str)
            (let ((case-fold-search c-f-s)) (string-match regexp str))
            (add-face-text-property (match-beginning 0) (match-end 0)
                                    'completions-common-part nil str)
            (let ((i 0)
                  (face nil))
              (while (and
                      (match-beginning (cl-incf i))
                      (facep
                       (setq face (intern-soft
                                   (format "completions-regexp-match-%d" i)))))
                (add-face-text-property (match-beginning i) (match-end i)
                                        face nil str)))
            str))
         (comps
          (let ((case-fold-search completion-ignore-case))
            (condition-case nil
                (all-completions
                 "" table
                 (if pred
                     (lambda (cand &rest more)
                       (and (apply pred cand more)
                            (string-match-p
                             regexp
                             (cond
                              ((stringp cand)              cand)
                              ((symbolp cand) (symbol-name cand))
                              (t              (car         cand))))))
                   (lambda (cand &optional _)
                     (string-match-p
                      regexp
                      (cond
                       ((stringp cand)              cand)
                       ((symbolp cand) (symbol-name cand))
                       (t              (car         cand)))))))
              (invalid-regexp nil)))))
    (if completion-lazy-hilit
        (prog1 comps (setq completion-lazy-hilit-fn hilit-fn))
      (setq completion-lazy-hilit-fn nil)
      (mapcar (compf [hilit-fn] copy-sequence) comps))))

;;; Partial-completion-mode style completion.

(defvar completion-pcm--delim-wild-regex nil
  "Regular expression matching delimiters controlling the partial-completion.
Typically, this regular expression simply matches a delimiter, meaning
that completion can add something at (match-beginning 0), but if it has
a submatch 1, then completion can add something at (match-end 1).
This is used when the delimiter needs to be of size zero (e.g. the transition
from lowercase to uppercase characters).")

(defun completion-pcm--prepare-delim-re (delims)
  (setq completion-pcm--delim-wild-regex (concat "[" delims "*]")))

(defcustom completion-pcm-word-delimiters "-_./:| ()"
  "A string of characters treated as word delimiters for completion.
Some arcane rules:
If `]' is in this string, it must come first.
If `^' is in this string, it must not come first.
If `-' is in this string, it must come first or right after `]'.
In other words, if S is this string, then `[S]' must be a valid Emacs regular
expression (not containing character ranges like `a-z')."
  :set (lambda (symbol value)
         (set-default symbol value)
         ;; Refresh other vars.
         (completion-pcm--prepare-delim-re value))
  :initialize 'custom-initialize-reset
  :type 'string)

(defcustom completion-pcm-complete-word-inserts-delimiters nil
  "This variable is obsolete and has no effect."
  :version "24.1"
  :type 'boolean)

(make-obsolete-variable 'completion-pcm-complete-word-inserts-delimiters
                        nil "30.1")

(defun completion-pcm--pattern-trivial-p (pattern)
  (and (stringp (car pattern))
       ;; It can be followed by `point' and "" and still be trivial.
       (let ((trivial t))
	 (dolist (elem (cdr pattern))
	   (unless (member elem '(point ""))
	     (setq trivial nil)))
	 trivial)))

(defun completion-pcm--string->pattern (string &optional point)
  "Split STRING into a pattern.
A pattern is a list where each element is either a string
or a symbol, see `completion-pcm--merge-completions'."
  (if (and point (< point (length string)))
      (let ((prefix (substring string 0 point))
            (suffix (substring string point)))
        (append (completion-pcm--string->pattern prefix)
                '(point)
                (completion-pcm--string->pattern suffix)))
    (let* ((pattern nil)
           (p 0)
           (p0 p)
           (pending nil))

      (while (setq p (string-match completion-pcm--delim-wild-regex string p))
        ;; Usually, completion-pcm--delim-wild-regex matches a delimiter,
        ;; meaning that something can be added *before* it, but it can also
        ;; match a prefix and postfix, in which case something can be added
        ;; in-between (e.g. match [[:lower:]][[:upper:]]).
        ;; This is determined by the presence of a submatch-1 which delimits
        ;; the prefix.
        (if (match-end 1) (setq p (match-end 1)))
        (unless (= p0 p)
          (if pending (push pending pattern))
          (push (substring string p0 p) pattern))
        (setq pending nil)
        (if (eq (aref string p) ?*)
            (progn
              (push 'star pattern)
              (setq p0 (1+ p)))
          (push 'any pattern)
          (if (match-end 1)
              (setq p0 p)
            (push (substring string p (match-end 0)) pattern)
            ;; `any-delim' is used so that "a-b" also finds "array->beginning".
            (setq pending 'any-delim)
            (setq p0 (match-end 0))))
        (setq p p0))

      (when (> (length string) p0)
        (if pending (push pending pattern))
        (push (substring string p0) pattern))
      (nreverse pattern))))

(defun completion-pcm--optimize-pattern (p)
  ;; Remove empty strings in a separate phase since otherwise a ""
  ;; might prevent some other optimization, as in '(any "" any).
  (setq p (delete "" p))
  (let ((n '()))
    (while p
      (pcase p
        (`(,(or 'any 'any-delim) ,(or 'any 'point) . ,_)
         (setq p (cdr p)))
        ;; This is not just a performance improvement: it turns a
        ;; terminating `point' into an implicit `any', which affects
        ;; the final position of point (because `point' gets turned
        ;; into a non-greedy ".*?" regexp whereas we need it to be
        ;; greedy when it's at the end, see bug#38458).
        (`(point) (setq p nil)) ;Implicit terminating `any'.
        (_ (push (pop p) n))))
    (nreverse n)))

(defun completion-pcm--pattern->regex (pattern &optional group)
  (let ((re
         (concat "\\`"
                 (mapconcat
                  (lambda (x)
                    (cond
                     ((stringp x) (regexp-quote x))
                     (t
                      (let ((re (if (eq x 'any-delim)
                                    (concat completion-pcm--delim-wild-regex "*?")
                                  "[^z-a]*?")))
                        (if (if (consp group) (memq x group) group)
                            (concat "\\(" re "\\)")
                          re)))))
                  pattern
                  ""))))
    ;; Avoid pathological backtracking.
    (while (string-match "\\.\\*\\?\\(?:\\\\[()]\\)*\\(\\.\\*\\?\\)" re)
      (setq re (replace-match "" t t re 1)))
    re))

(defun completion-pcm--pattern-point-idx (pattern)
  "Return index of subgroup corresponding to `point' element of PATTERN.
Return nil if there's no such element."
  (let ((idx nil)
        (i 0))
    (dolist (x pattern)
      (unless (stringp x)
        (incf i)
        (if (eq x 'point) (setq idx i))))
    idx))

(defun completion-pcm--all-completions (prefix pattern table pred)
  "Find all completions for PATTERN in TABLE obeying PRED.
PATTERN is as returned by `completion-pcm--string->pattern'."
  ;; (cl-assert (= (car (completion-boundaries prefix table pred ""))
  ;;            (length prefix)))
  ;; Find an initial list of possible completions.
  (if (completion-pcm--pattern-trivial-p pattern)

      ;; Minibuffer contains no delimiters -- simple case!
      (all-completions (concat prefix (car pattern)) table pred)

    ;; Use all-completions to do an initial cull.  This is a big win,
    ;; since all-completions is written in C!
    (let* (;; Convert search pattern to a standard regular expression.
	   (regex (completion-pcm--pattern->regex pattern))
           (case-fold-search completion-ignore-case)
	   (compl (all-completions
                   (concat prefix
                           (if (stringp (car pattern)) (car pattern) ""))
		   table pred)))
      (let ((poss ()))
	(dolist (c compl)
	  (when (string-match-p regex c) (push c poss)))
	(nreverse poss)))))

(defvar flex-score-match-tightness 3
  "Controls how the `flex' completion style scores its matches.

Value is a positive number.  A number smaller than 1 makes the
scoring formula reward matches scattered along the string, while
a number greater than one make the formula reward matches that
are clumped together.  I.e \"foo\" matches both strings
\"fbarbazoo\" and \"fabrobazo\", which are of equal length, but
only a value greater than one will score the former (which has
one large \"hole\" and a clumped-together \"oo\" match) higher
than the latter (which has two \"holes\" and three
one-letter-long matches).")

(defvar completion-lazy-hilit nil
  "If non-nil, request lazy highlighting of completion candidates.

Lisp programs (a.k.a. \"front ends\") that present completion
candidates may opt to bind this variable to a non-nil value when
calling functions (such as `completion-all-completions') which
produce completion candidates.  This tells the underlying
completion styles that they do not need to fontify (i.e.,
propertize with the `face' property) completion candidates in a
way that highlights the matching parts.  Then it is the front end
which presents the candidates that becomes responsible for this
fontification.  The front end does that by calling the function
`completion-lazy-hilit' on each completion candidate that is to be
displayed to the user.

Note that only some completion styles take advantage of this
variable for optimization purposes.  Other styles will ignore the
hint and fontify eagerly as usual.  It is still safe for a
front end to call `completion-lazy-hilit' in these situations.

To author a completion style that takes advantage of this variable,
see `completion-lazy-hilit-fn' and `completion-pcm--hilit-commonality'.")

(defvar completion-lazy-hilit-fn nil
  "Fontification function set by lazy-highlighting completions styles.
When a given style wants to enable support for `completion-lazy-hilit'
\(which see), that style should set this variable to a function of one
argument.  It will be called with each completion candidate, a string, to
be displayed to the user, and should destructively propertize these
strings with the `face' property.")

(defun completion-lazy-hilit (str)
  "Return a copy of completion candidate STR that is `face'-propertized.
See documentation of the variable `completion-lazy-hilit' for more
details."
  (if (and completion-lazy-hilit completion-lazy-hilit-fn)
      (funcall completion-lazy-hilit-fn (copy-sequence str))
    str))

(defun completion--hilit-from-re (string regexp &optional point-idx)
  "Fontify STRING using REGEXP POINT-IDX.
`completions-common-part' and `completions-first-difference' are
used.  POINT-IDX is the position of point in the presumed \"PCM\"
pattern that was used to generate derive REGEXP from."
(let* ((md (and regexp (string-match regexp string) (cddr (match-data t))))
       (pos (if point-idx (match-beginning point-idx) (match-end 0)))
       (me (and md (match-end 0)))
       (from 0))
  (while md
    (add-face-text-property from (pop md) 'completions-common-part nil string)
    (setq from (pop md)))
  (if (> (length string) pos)
      (add-face-text-property
       pos (1+ pos)
       'completions-first-difference
       nil string))
  (unless (or (not me) (= from me))
    (add-face-text-property from me 'completions-common-part nil string))
  string))

(defun completion--flex-score-1 (md-groups match-end len)
  "Compute matching score of completion.
The score lies in the range between 0 and 1, where 1 corresponds to
the full match.
MD-GROUPS is the \"group\"  part of the match data.
MATCH-END is the end of the match.
LEN is the length of the completion string."
  (let* ((from 0)
         ;; To understand how this works, consider these simple
         ;; ascii diagrams showing how the pattern "foo"
         ;; flex-matches "fabrobazo", "fbarbazoo" and
         ;; "barfoobaz":

         ;;      f abr o baz o
         ;;      + --- + --- +

         ;;      f barbaz oo
         ;;      + ------ ++

         ;;      bar foo baz
         ;;          +++

         ;; "+" indicates parts where the pattern matched.  A
         ;; "hole" in the middle of the string is indicated by
         ;; "-".  Note that there are no "holes" near the edges
         ;; of the string.  The completion score is a number
         ;; bound by (0..1] (i.e., larger than (but not equal
         ;; to) zero, and smaller or equal to one): the higher
         ;; the better and only a perfect match (pattern equals
         ;; string) will have score 1.  The formula takes the
         ;; form of a quotient.  For the numerator, we use the
         ;; number of +, i.e. the length of the pattern.  For
         ;; the denominator, it first computes
         ;;
         ;;     hole_i_contrib = 1 + (Li-1)^(1/tightness)
         ;;
         ;; , for each hole "i" of length "Li", where tightness
         ;; is given by `flex-score-match-tightness'.  The
         ;; final value for the denominator is then given by:
         ;;
         ;;    (SUM_across_i(hole_i_contrib) + 1) * len
         ;;
         ;; , where "len" is the string's length.
         (score-numerator 0)
         (score-denominator 0)
         (last-b 0))
    (while (and md-groups (car md-groups))
      (let ((a from)
            (b (pop md-groups)))
        (setq
         score-numerator   (+ score-numerator (- b a)))
        (unless (or (= a last-b)
                    (zerop last-b)
                    (= a len))
          (setq
           score-denominator (+ score-denominator
                                1
                                (expt (- a last-b 1)
                                      (/ 1.0
                                         flex-score-match-tightness)))))
        (setq
         last-b              b))
      (setq from (pop md-groups)))
    ;; If `pattern' doesn't have an explicit trailing any, the
    ;; regex `re' won't produce match data representing the
    ;; region after the match.  We need to account to account
    ;; for that extra bit of match (bug#42149).
    (unless (= from match-end)
      (let ((a from)
            (b match-end))
        (setq
         score-numerator   (+ score-numerator (- b a)))
        (unless (or (= a last-b)
                    (zerop last-b)
                    (= a len))
          (setq
           score-denominator (+ score-denominator
                                1
                                (expt (- a last-b 1)
                                      (/ 1.0
                                         flex-score-match-tightness)))))
        (setq
         last-b              b)))
    (/ score-numerator (* len (1+ score-denominator)) 1.0)))

(defvar completion--flex-score-last-md nil
  "Helper variable for `completion--flex-score'.")

(defun completion--flex-score (str re &optional dont-error)
  "Compute flex score of completion STR based on RE.
If DONT-ERROR, just return nil if RE doesn't match STR."
  (let ((case-fold-search completion-ignore-case))
    (cond ((string-match re str)
           (let* ((match-end (match-end 0))
                  (md (cddr
                       (setq
                        completion--flex-score-last-md
                        (match-data t completion--flex-score-last-md)))))
             (completion--flex-score-1 md match-end (length str))))
          ((not dont-error)
           (error "Internal error: %s does not match %s" re str)))))

(defvar completion-pcm--regexp nil
  "Regexp from PCM pattern in `completion-pcm--hilit-commonality'.")

(defun completion-pcm--hilit-commonality (pattern completions)
  "Show where and how well PATTERN matches COMPLETIONS.
PATTERN, a list of symbols and strings as seen
`completion-pcm--merge-completions', is assumed to match every
string in COMPLETIONS.

If `completion-lazy-hilit' is nil, return a deep copy of COMPLETIONS
where each string is propertized with faces `completions-common-part'
and `completions-first-difference' in the relevant segments.

Else, if `completion-lazy-hilit' is t, return COMPLETIONS
unchanged, but setup a suitable `completion-lazy-hilit-fn' (which
see) for later lazy highlighting."
  (setq completion-pcm--regexp nil
        completion-lazy-hilit-fn nil)
  (cond
   ((and completions (cl-loop for e in pattern thereis (stringp e)))
    (let* ((re (completion-pcm--pattern->regex pattern 'group))
           (point-idx (completion-pcm--pattern-point-idx pattern)))
      (setq completion-pcm--regexp re)
      (cond (completion-lazy-hilit
             (setq completion-lazy-hilit-fn
                   (lambda (str) (completion--hilit-from-re str re point-idx)))
             completions)
            (t
             (mapcar
              (lambda (str)
                (completion--hilit-from-re (copy-sequence str) re point-idx))
              completions)))))
   (t completions)))

(defun completion-pcm--find-all-completions (string table pred point
                                                    &optional filter)
  "Find all completions for STRING at POINT in TABLE, satisfying PRED.
POINT is a position inside STRING.
FILTER is a function applied to the return value, that can be used, e.g. to
filter out additional entries (because TABLE might not obey PRED)."
  (unless filter (setq filter 'identity))
  (let* ((beforepoint (substring string 0 point))
         (afterpoint (substring string point))
         (bounds (completion-boundaries beforepoint table pred afterpoint))
         (prefix (substring beforepoint 0 (car bounds)))
         (suffix (substring afterpoint (cdr bounds)))
         firsterror)
    (setq string (substring string (car bounds) (+ point (cdr bounds))))
    (let* ((relpoint (- point (car bounds)))
           (pattern (completion-pcm--optimize-pattern
                     (completion-pcm--string->pattern string relpoint)))
           (all (condition-case-unless-debug err
                    (funcall filter
                             (completion-pcm--all-completions
                              prefix pattern table pred))
                  (error (setq firsterror err) nil))))
      (when (and (null all)
                 (> (car bounds) 0)
                 (null (ignore-errors (try-completion prefix table pred))))
        ;; The prefix has no completions at all, so we should try and fix
        ;; that first.
        (pcase-let* ((substring (substring prefix 0 -1))
                     (`(,subpat ,suball ,subprefix ,_subsuffix)
                      (completion-pcm--find-all-completions
                       substring table pred (length substring) filter))
                     (sep (aref prefix (1- (length prefix))))
                     ;; Text that goes between the new submatches and the
                     ;; completion substring.
                     (between nil))
          ;; Eliminate submatches that don't end with the separator.
          (dolist (submatch (prog1 suball (setq suball ())))
            (when (eq sep (aref submatch (1- (length submatch))))
              (push submatch suball)))
          (when suball
            ;; Update the boundaries and corresponding pattern.
            ;; We assume that all submatches result in the same boundaries
            ;; since we wouldn't know how to merge them otherwise anyway.
            ;; FIXME: COMPLETE REWRITE!!!
            (let* ((newbeforepoint
                    (concat subprefix (car suball)
                            (substring string 0 relpoint)))
                   (leftbound (+ (length subprefix) (length (car suball))))
                   (newbounds (completion-boundaries
                               newbeforepoint table pred afterpoint)))
              (unless (or (and (eq (cdr bounds) (cdr newbounds))
                               (eq (car newbounds) leftbound))
                          ;; Refuse new boundaries if they step over
                          ;; the submatch.
                          (< (car newbounds) leftbound))
                ;; The new completed prefix does change the boundaries
                ;; of the completed substring.
                (setq suffix (substring afterpoint (cdr newbounds)))
                (setq string
                      (concat (substring newbeforepoint (car newbounds))
                              (substring afterpoint 0 (cdr newbounds))))
                (setq between (substring newbeforepoint leftbound
                                         (car newbounds)))
                (setq pattern (completion-pcm--optimize-pattern
                               (completion-pcm--string->pattern
                                string
                                (- (length newbeforepoint)
                                   (car newbounds))))))
              (dolist (submatch suball)
                (setq all (nconc
                           (mapcar
                            (lambda (s) (concat submatch between s))
                            (funcall filter
                                     (completion-pcm--all-completions
                                      (concat subprefix submatch between)
                                      pattern table pred)))
                           all)))
              ;; FIXME: This can come in handy for try-completion,
              ;; but isn't right for all-completions, since it lists
              ;; invalid completions.
              ;; (unless all
              ;;   ;; Even though we found expansions in the prefix, none
              ;;   ;; leads to a valid completion.
              ;;   ;; Let's keep the expansions, tho.
              ;;   (dolist (submatch suball)
              ;;     (push (concat submatch between newsubstring) all)))
              ))
          (setq pattern (append subpat (list 'any (string sep))
                                (if between (list between)) pattern))
          (setq prefix subprefix)))
      (if (and (null all) firsterror)
          (signal (car firsterror) (cdr firsterror))
        (list pattern all prefix suffix)))))

(defun completion-pcm-all-completions (string table pred point)
  (pcase-let ((`(,pattern ,all ,prefix ,_suffix)
               (completion-pcm--find-all-completions string table pred point)))
    (when all
      (nconc (completion-pcm--hilit-commonality pattern all)
             (length prefix)))))

(defun completion--common-suffix (strs)
  "Return the common suffix of the strings STRS."
  (nreverse (try-completion "" (mapcar #'reverse strs))))

(defun completion-pcm--merge-completions (strs pattern)
  "Extract the commonality in STRS, with the help of PATTERN.
PATTERN can contain strings and symbols chosen among `star', `any', `point',
and `prefix'.  They all match anything (aka \".*\") but are merged differently:
`any' only grows from the left (when matching \"a1b\" and \"a2b\" it gets
  completed to just \"a\").
`prefix' only grows from the right (when matching \"a1b\" and \"a2b\" it gets
  completed to just \"b\").
`star' grows from both ends and is reified into a \"*\"  (when matching \"a1b\"
  and \"a2b\" it gets completed to \"a*b\").
`point' is like `star' except that it gets reified as the position of point
  instead of being reified as a \"*\" character.
The underlying idea is that we should return a string which still matches
the same set of elements."
  ;; When completing while ignoring case, we want to try and avoid
  ;; completing "fo" to "foO" when completing against "FOO" (bug#4219).
  ;; So we try and make sure that the string we return is all made up
  ;; of text from the completions rather than part from the
  ;; completions and part from the input.
  ;; FIXME: This reduces the problems of inconsistent capitalization
  ;; but it doesn't fully fix it: we may still end up completing
  ;; "fo-ba" to "foo-BAR" or "FOO-bar" when completing against
  ;; '("foo-barr" "FOO-BARD").
  (cond
   ((null (cdr strs)) (list (car strs)))
   (t
    (let ((re (completion-pcm--pattern->regex pattern 'group))
          (ccs ()))                     ;Chopped completions.

      ;; First chop each string into the parts corresponding to each
      ;; non-constant element of `pattern', using regexp-matching.
      (let ((case-fold-search completion-ignore-case))
        (dolist (str strs)
          (unless (string-match re str)
            (error "Internal error: %s doesn't match %s" str re))
          (let ((chopped ())
                (last 0)
                (i 1)
                next)
            (while (setq next (match-end i))
              (push (substring str last next) chopped)
              (setq last next)
              (setq i (1+ i)))
            ;; Add the text corresponding to the implicit trailing `any'.
            (push (substring str last) chopped)
            (push (nreverse chopped) ccs))))

      ;; Then for each of those non-constant elements, extract the
      ;; commonality between them.
      (let ((res ())
            (fixed ""))
        ;; Make the implicit trailing `any' explicit.
        (dolist (elem (append pattern '(any)))
          (if (stringp elem)
              (setq fixed (concat fixed elem))
            (let ((comps ()))
              (dolist (cc (prog1 ccs (setq ccs nil)))
                (push (car cc) comps)
                (push (cdr cc) ccs))
              ;; Might improve the likelihood to avoid choosing
              ;; different capitalizations in different parts.
              ;; In practice, it doesn't seem to make any difference.
              (setq ccs (nreverse ccs))
              (let* ((prefix (try-completion fixed comps))
                     (unique (or (and (eq prefix t) (setq prefix fixed))
                                 (and (stringp prefix)
                                      (eq t (try-completion prefix comps))))))
                ;; If there's only one completion, `elem' is not useful
                ;; any more: it can only match the empty string.
                ;; FIXME: in some cases, it may be necessary to turn an
                ;; `any' into a `star' because the surrounding context has
                ;; changed such that string->pattern wouldn't add an `any'
                ;; here any more.
                (if unique
                    ;; If the common prefix is unique, it also is a common
                    ;; suffix, so we should add it for `prefix' elements.
                    (push prefix res)
                  ;; `prefix' only wants to include the fixed part before the
                  ;; wildcard, not the result of growing that fixed part.
                  (when (eq elem 'prefix)
                    (setq prefix fixed))
                  (push prefix res)
                  (push elem res)
                  ;; Extract common suffix additionally to common prefix.
                  ;; Don't do it for `any' since it could lead to a merged
                  ;; completion that doesn't itself match the candidates.
                  (when (and (memq elem '(star point prefix))
                             ;; If prefix is one of the completions, there's no
                             ;; suffix left to find.
                             (not (assoc-string prefix comps t)))
                    (let ((suffix
                           (completion--common-suffix
                            (if (zerop (length prefix)) comps
                              ;; Ignore the chars in the common prefix, so we
                              ;; don't merge '("abc" "abbc") as "ab*bc".
                              (let ((skip (length prefix)))
                                (mapcar (lambda (str) (substring str skip))
                                        comps))))))
                      (cl-assert (stringp suffix))
                      (unless (equal suffix "")
                        (push suffix res)))))
                (setq fixed "")))))
        ;; We return it in reverse order.
        res)))))

(defun completion-pcm--pattern->string (pattern)
  (mapconcat (lambda (x) (cond
                          ((stringp x) x)
                          ((eq x 'star) "*")
                          (t "")))           ;any, point, prefix.
             pattern
             ""))

(defun completion-pcm--merge-try (pattern all prefix suffix)
  (cond
   ((not (consp all)) all)
   ((and (not (consp (cdr all)))        ;Only one completion.
         ;; Ignore completion-ignore-case here.
         (equal (completion-pcm--pattern->string pattern) (car all)))
    t)
   (t
    (let* ((mergedpat (completion-pcm--merge-completions all pattern))
           ;; `mergedpat' is in reverse order.  Place new point (by
           ;; order of preference) either at the old point, or at
           ;; the last place where there's something to choose, or
           ;; at the very end.
           (pointpat (or (memq 'point mergedpat)
                         (memq 'any   mergedpat)
                         (memq 'star  mergedpat)
                         ;; Not `prefix'.
                         mergedpat))
           ;; New pos from the start.
	   (newpos (length (completion-pcm--pattern->string pointpat)))
           ;; Do it afterwards because it changes `pointpat' by side effect.
           (merged (completion-pcm--pattern->string (nreverse mergedpat))))

      (setq suffix (completion--merge-suffix
                    ;; The second arg should ideally be "the position right
                    ;; after the last char of `merged' that comes from the text
                    ;; to be completed".  But completion-pcm--merge-completions
                    ;; currently doesn't give us that info.  So instead we just
                    ;; use the "last but one" position, which tends to work
                    ;; well in practice since `suffix' always starts
                    ;; with a boundary and we hence mostly/only care about
                    ;; merging this boundary (bug#15419).
                    merged (max 0 (1- (length merged))) suffix))
      (cons (concat prefix merged suffix) (+ newpos (length prefix)))))))

(defun completion-pcm-try-completion (string table pred point)
  (pcase-let ((`(,pattern ,all ,prefix ,suffix)
               (completion-pcm--find-all-completions
                string table pred point)))
    (completion-pcm--merge-try pattern all prefix suffix)))

;;; Substring completion
;; Mostly derived from the code of `basic' completion.

(defun completion-substring--all-completions
    (string table pred point &optional transform-pattern-fn)
  "Match the presumed substring STRING to the entries in TABLE.
Respect PRED and POINT.  The pattern used is a PCM-style
substring pattern, but it be massaged by TRANSFORM-PATTERN-FN, if
that is non-nil."
  (let* ((beforepoint (substring string 0 point))
         (afterpoint (substring string point))
         (bounds (completion-boundaries beforepoint table pred afterpoint))
         (suffix (substring afterpoint (cdr bounds)))
         (prefix (substring beforepoint 0 (car bounds)))
         (basic-pattern (completion-basic--pattern
                         beforepoint afterpoint bounds))
         (pattern (if (not (stringp (car basic-pattern)))
                      basic-pattern
                    (cons 'prefix basic-pattern)))
         (pattern (completion-pcm--optimize-pattern
                   (if transform-pattern-fn
                       (funcall transform-pattern-fn pattern)
                     pattern)))
         (all (completion-pcm--all-completions prefix pattern table pred)))
    (list all pattern prefix suffix (car bounds))))

(defun completion-substring-try-completion (string table pred point)
  (pcase-let ((`(,all ,pattern ,prefix ,suffix ,_carbounds)
               (completion-substring--all-completions
                string table pred point)))
    (completion-pcm--merge-try pattern all prefix suffix)))

(defun completion-substring-all-completions (string table pred point)
  (pcase-let ((`(,all ,pattern ,prefix ,_suffix ,_carbounds)
               (completion-substring--all-completions
                string table pred point)))
    (when all
      (nconc (completion-pcm--hilit-commonality pattern all)
             (length prefix)))))

;;; "flex" completion, also known as flx/fuzzy/scatter completion
;; Completes "foo" to "frodo" and "farfromsober"

(defcustom completion-flex-nospace nil
  "Non-nil if `flex' completion rejects spaces in search pattern."
  :version "27.1"
  :type 'boolean)

(defun completion-flex--make-flex-pattern (pattern)
  "Convert PCM-style PATTERN into PCM-style flex pattern.

This turns
    (prefix \"foo\" point)
into
    (prefix \"f\" any \"o\" any \"o\" any point)
which is at the core of flex logic.  The extra
`any' is optimized away later on."
  (mapcan (lambda (elem)
            (if (stringp elem)
                (mapcan (lambda (char)
                          (list (string char) 'any))
                        elem)
              (list elem)))
          pattern))

(defun completion-flex-try-completion (string table pred point)
  "Try to flex-complete STRING in TABLE given PRED and POINT."
  (unless (and completion-flex-nospace (string-search " " string))
    (pcase-let ((`(,all ,pattern ,prefix ,suffix ,_carbounds)
                 (completion-substring--all-completions
                  string table pred point
                  #'completion-flex--make-flex-pattern)))
      ;; Try some "merging", meaning add as much as possible to the
      ;; user's pattern without losing any possible matches in `all'.
      ;; i.e this will augment "cfi" to "config" if all candidates
      ;; contain the substring "config".  FIXME: this still won't
      ;; augment "foo" to "froo" when matching "frodo" and
      ;; "farfromsober".
      (completion-pcm--merge-try pattern all prefix suffix))))

(defun completion-flex-all-completions (string table pred point)
  "Get flex-completions of STRING in TABLE, given PRED and POINT."
  (unless (and completion-flex-nospace (string-search " " string))
    (pcase-let ((`(,all ,pattern ,prefix ,_suffix ,_carbounds)
                 (completion-substring--all-completions
                  string table pred point
                  #'completion-flex--make-flex-pattern)))
      (when all
        (nconc (completion-pcm--hilit-commonality pattern all)
               (length prefix))))))

;; Initials completion
;; Complete /ums to /usr/monnier/src or lch to list-command-history.

(defun completion-initials-expand (str table pred)
  (let ((bounds (completion-boundaries str table pred "")))
    (unless (or (zerop (length str))
                ;; Only check within the boundaries, since the
                ;; boundary char (e.g. /) might be in delim-regexp.
                (string-match completion-pcm--delim-wild-regex str
                              (car bounds)))
      (if (zerop (car bounds))
          ;; FIXME: Don't hardcode "-" (bug#17559).
          (mapconcat 'string str "-")
        ;; If there's a boundary, it's trickier.  The main use-case
        ;; we consider here is file-name completion.  We'd like
        ;; to expand ~/eee to ~/e/e/e and /eee to /e/e/e.
        ;; But at the same time, we don't want /usr/share/ae to expand
        ;; to /usr/share/a/e just because we mistyped "ae" for "ar",
        ;; so we probably don't want initials to touch anything that
        ;; looks like /usr/share/foo.  As a heuristic, we just check that
        ;; the text before the boundary char is at most 1 char.
        ;; This allows both ~/eee and /eee and not much more.
        ;; FIXME: It sadly also disallows the use of ~/eee when that's
        ;; embedded within something else (e.g. "(~/eee" in Info node
        ;; completion or "ancestor:/eee" in bzr-revision completion).
        (when (< (car bounds) 3)
          (let ((sep (substring str (1- (car bounds)) (car bounds))))
            ;; FIXME: the above string-match checks the whole string, whereas
            ;; we end up only caring about the after-boundary part.
            (concat (substring str 0 (car bounds))
                    (mapconcat 'string (substring str (car bounds)) sep))))))))

(defun completion-initials-all-completions (string table pred _point)
  (let ((newstr (completion-initials-expand string table pred)))
    (when newstr
      (completion-pcm-all-completions newstr table pred (length newstr)))))

(defun completion-initials-try-completion (string table pred _point)
  (let ((newstr (completion-initials-expand string table pred)))
    (when newstr
      (completion-pcm-try-completion newstr table pred (length newstr)))))

;; Shorthand completion
;;
;; Iff there is a (("x-" . "string-library-")) shorthand setup and
;; string-library-foo is in candidates, complete x-foo to it.

(defun completion-shorthand-try-completion (string table pred point)
  "Try completion with `read-symbol-shorthands' of original buffer."
  (cl-loop with expanded
           for (short . long) in
           (with-current-buffer minibuffer--original-buffer
             read-symbol-shorthands)
           for probe =
           (and (> point (length short))
                (string-prefix-p short string)
                (try-completion (setq expanded
                                      (concat long
                                              (substring
                                               string
                                               (length short))))
                                table pred))
           when probe
           do (message "Shorthand expansion")
           and return (cons expanded (max (length long)
                                          (+ (- point (length short))
                                             (length long))))))

(defun completion-shorthand-all-completions (_string _table _pred _point)
  ;; no-op: For now, we don't want shorthands to list all the possible
  ;; locally active longhands.  For the completion categories where
  ;; this style is active, it could hide other more interesting
  ;; matches from subsequent styles.
  nil)

(defun minibuffer-kill-completions-buffer ()
  "Kill the *Completions* buffer for this minibuffer."
  (when-let ((buf (get-buffer completions-buffer-name))) (kill-buffer buf)))


(defvar completing-read-function #'completing-read-default
  "The function called by `completing-read' to do its work.
It should accept the same arguments as `completing-read'.")

(defcustom minibuffer-completions-buffer-name-format "*Minibuffer%{%d%} Completions*"
  "Format string for minibuffer completions list buffer names.

For nested minibuffers, the construct \"%d\" is substituted with the
current minibuffer depth, \"%{\" is substituted with \"<\", and \"%}?\"
is substituted with \">\".  For regular (non-nested) minibuffers, these
constructs are all substituted with the empty string.

The resulting string is the buffer name for the completions list buffer
of the current minibuffer."
  :version "30.1"
  :type 'string)

(defun completing-read-default (prompt collection &optional predicate
                                       require-match initial-input
                                       hist def inherit-input-method)
  "Default method for reading from the minibuffer with completion.
See `completing-read' for the meaning of the arguments."

  (when (consp initial-input)
    (setq initial-input
          (cons (car initial-input)
                ;; `completing-read' uses 0-based index while
                ;; `read-from-minibuffer' uses 1-based index.
                (1+ (cdr initial-input)))))

  (let* ((keymap (if require-match
                     minibuffer-local-must-match-map
                   minibuffer-local-completion-map))
         (buffer (current-buffer))
         (result
          (minibuffer-with-setup-hook
              (lambda ()
                (setq-local completions-buffer-name
                            (format-spec
                             minibuffer-completions-buffer-name-format
                             `((?d . ,(let ((md (minibuffer-depth)))
                                        (if (equal md 1)
                                            ""
                                          (number-to-string md))))
                               (?{ . ,(if (< 1 (minibuffer-depth)) "<" ""))
                               (?} . ,(if (< 1 (minibuffer-depth)) ">" "")))))
                (setq-local minibuffer-completion-table collection)
                (setq-local minibuffer-completion-predicate predicate)
                ;; FIXME: Remove/rename this var, see the next one.
                (setq-local minibuffer-completion-confirm
                            (unless (eq require-match t) require-match))
                (setq-local minibuffer--require-match require-match)
                (setq-local minibuffer--original-buffer buffer)
                (add-hook 'minibuffer-exit-hook
                          #'minibuffer-kill-completions-buffer nil t)
                ;; Show the completion help eagerly if
                ;; `completion-eager-display' is t or if eager display
                ;; has been requested by the completion table.
                (when completion-eager-display
                  (let* ((md (completion-metadata
                              (buffer-substring-no-properties
                               (minibuffer-prompt-end) (point))
                              collection predicate))
                         (fun (completion-metadata-get md 'eager-display)))
                    (when (or fun (eq completion-eager-display t))
                      (funcall (if (functionp fun)
                                   fun #'minibuffer-completion-help))))))
            (read-from-minibuffer prompt initial-input keymap
                                  nil hist def inherit-input-method))))
    (when (and (equal result "") def)
      (setq result (if (consp def) (car def) def)))
    result))

;; Miscellaneous

(defun minibuffer-insert-file-name-at-point ()
  "Get a file name at point in original buffer and insert it to minibuffer."
  (interactive "" minibuffer-mode)
  (let ((file-name-at-point
	 (with-current-buffer (window-buffer (minibuffer-selected-window))
	   (run-hook-with-args-until-success 'file-name-at-point-functions))))
    (when file-name-at-point
      (insert file-name-at-point))))

(defun minibuffer-insert-symbol-at-point ()
  "Get a symbol at point in original buffer and insert it to minibuffer."
  (interactive "" minibuffer-mode)
  (when-let ((sym (with-current-buffer
                      (window-buffer (minibuffer-selected-window))
                    (thing-at-point 'symbol t))))
    (insert sym)))

(defun minibuffer-beginning-of-buffer (&optional arg)
  "Move to the logical beginning of the minibuffer.
This command behaves like `beginning-of-buffer', but if point is
after the end of the prompt, move to the end of the prompt.
Otherwise move to the start of the buffer."
  (declare (interactive-only "use `(goto-char (point-min))' instead."))
  (interactive "^P" minibuffer-mode)
  (or (consp arg)
      (region-active-p)
      (push-mark))
  (goto-char (cond
              ;; We want to go N/10th of the way from the beginning.
              ((and arg (not (consp arg)))
	       (+ (point-min) 1
		  (/ (* (- (point-max) (point-min))
                        (prefix-numeric-value arg))
                     10)))
              ;; Go to the start of the buffer.
              ((or (null minibuffer-beginning-of-buffer-movement)
                   (<= (point) (minibuffer-prompt-end)))
	       (point-min))
              ;; Go to the end of the minibuffer.
              (t
               (minibuffer-prompt-end))))
  (when (and arg (not (consp arg)))
    (forward-line 1)))

(defmacro with-minibuffer-selected-window (&rest body)
  "Execute the forms in BODY from the minibuffer in its original window.
When used in a minibuffer window, select the window selected just before
the minibuffer was activated, and execute the forms."
  (declare (indent 0) (debug t))
  `(let ((window (minibuffer-selected-window)))
     (when window
       (with-selected-window window
         ,@body))))

(defun minibuffer-recenter-top-bottom (&optional arg)
  "Run `recenter-top-bottom' from the minibuffer in its original window."
  (interactive "P" minibuffer-mode)
  (with-minibuffer-selected-window
    (recenter-top-bottom arg)))

(defun minibuffer-scroll-up-command (&optional arg)
  "Run `scroll-up-command' from the minibuffer in its original window."
  (interactive "^P" minibuffer-mode)
  (with-minibuffer-selected-window
    (scroll-up-command arg)))

(defun minibuffer-scroll-down-command (&optional arg)
  "Run `scroll-down-command' from the minibuffer in its original window."
  (interactive "^P" minibuffer-mode)
  (with-minibuffer-selected-window
    (scroll-down-command arg)))

(defun minibuffer-scroll-other-window (&optional arg)
  "Run `scroll-other-window' from the minibuffer in its original window."
  (interactive "^P" minibuffer-mode)
  (with-minibuffer-selected-window
    (scroll-other-window arg)))

(defun minibuffer-scroll-other-window-down (&optional arg)
  "Run `scroll-other-window-down' from the minibuffer in its original window."
  (interactive "^P" minibuffer-mode)
  (with-minibuffer-selected-window
    (scroll-other-window-down arg)))

(defmacro with-minibuffer-completions-window (&rest body)
  "Execute the forms in BODY from the minibuffer in its completions window.
When used in a minibuffer window, select the window with completions,
and execute the forms."
  (declare (indent 0) (debug t))
  `(let ((window (or (get-buffer-window completions-buffer-name 0)
                     ;; Make sure we have a completions window.
                     (progn (minibuffer-completion-help)
                            (get-buffer-window completions-buffer-name 0)))))
     (when window
       (with-selected-window window
         ,@body))))

(defcustom minibuffer-completion-auto-choose t
  "Non-nil means to automatically insert completions to the minibuffer.
When non-nil, then `minibuffer-next-completion' and
`minibuffer-previous-completion' will insert the completion
selected by these commands to the minibuffer."
  :type 'boolean
  :version "29.1")

(defmacro minibuffer-completions-motion (&rest body)
  "Execute BODY in the minibuffer completions window.
When `minibuffer-completion-auto-choose' is non-nil in the minibuffer,
then also insert the completion candidate at point to the minibuffer
after executing BODY."
  (declare (indent 0) (debug t))
  `(let ((auto-choose minibuffer-completion-auto-choose))
     (with-minibuffer-completions-window
       (when completions-highlight-face
         (setq-local cursor-face-highlight-nonselected-window t))
       ,@body
       (when auto-choose
         (choose-completion nil t t)))))

(defun minibuffer-first-completion ()
  "Move to the first item in the completions window from the minibuffer.
When `minibuffer-completion-auto-choose' is non-nil, then also
insert the selected completion candidate to the minibuffer."
  (interactive "" minibuffer-mode)
  (minibuffer-completions-motion (first-completion)))

(defun minibuffer-last-completion ()
  "Move to the last item in the completions window from the minibuffer.
When `minibuffer-completion-auto-choose' is non-nil, then also
insert the selected completion candidate to the minibuffer."
  (interactive "" minibuffer-mode)
  (minibuffer-completions-motion (last-completion)))

(defun minibuffer-next-completion (&optional n vertical)
  "Move to the next item in the completions window from the minibuffer.
When the optional argument VERTICAL is non-nil, move vertically
to the next item on the next line using `next-line-completion'.
Otherwise, move to the next item horizontally using `next-completion'.
When `minibuffer-completion-auto-choose' is non-nil, then also
insert the selected completion candidate to the minibuffer."
  (interactive "p" minibuffer-mode)
  (minibuffer-completions-motion
    (funcall (if vertical #'next-line-completion #'next-completion) (or n 1))))

(defun minibuffer-previous-completion (&optional n)
  "Move to the previous item in the completions window from the minibuffer.
When `minibuffer-completion-auto-choose' is non-nil, then also
insert the selected completion candidate to the minibuffer."
  (interactive "p" minibuffer-mode)
  (minibuffer-next-completion (- (or n 1))))

(defun minibuffer-next-line-completion (&optional n)
  "Move to the next completion line from the minibuffer.
This means to move to the completion candidate on the next line
in the *Completions* buffer while point stays in the minibuffer.
When `minibuffer-completion-auto-choose' is non-nil, then also
insert the selected completion candidate to the minibuffer."
  (interactive "p" minibuffer-mode)
  (minibuffer-next-completion (or n 1) t))

(defun minibuffer-previous-line-completion (&optional n)
  "Move to the previous completion line from the minibuffer.
This means to move to the completion candidate on the previous line
in the *Completions* buffer while point stays in the minibuffer.
When `minibuffer-completion-auto-choose' is non-nil, then also
insert the selected completion candidate to the minibuffer."
  (interactive "p" minibuffer-mode)
  (minibuffer-next-completion (- (or n 1)) t))

(defun minibuffer-next-line-or-completion (&optional arg)
  "Move cursor down ARG lines, or to the next completion candidate."
  (interactive "^p" minibuffer-mode)
  (or arg (setq arg 1))
  (minibuffer-next-line-or-call #'minibuffer-next-completion arg))

(defun minibuffer-previous-line-or-completion (&optional arg)
  "Move cursor up ARG lines, or to the previous completion candidate."
  (interactive "^p" minibuffer-mode)
  (or arg (setq arg 1))
  (minibuffer-previous-line-or-call #'minibuffer-previous-completion arg))

(defun minibuffer-choose-completion (&optional no-exit no-quit)
  "Run `choose-completion' from the minibuffer in the completions window.
With prefix argument NO-EXIT, insert the completion candidate at point to
the minibuffer, but don't exit the minibuffer.  When the prefix argument
is not provided, then whether to exit the minibuffer depends on the value
of `completion-no-auto-exit'.
If NO-QUIT is non-nil, insert the completion candidate at point to the
minibuffer, but don't quit the completions window."
  (interactive "P" minibuffer-mode)
  (with-minibuffer-completions-window
    (choose-completion nil no-exit no-quit)))

(defun minibuffer-complete-history ()
  "Complete the minibuffer history as far as possible.
Like `minibuffer-complete' but completes on the history items
instead of the default completion table."
  (interactive "" minibuffer-mode)
  (let* ((history (symbol-value minibuffer-history-variable))
         (completions
          (if (listp history)
              ;; Support e.g. `C-x ESC ESC TAB' as
              ;; a replacement of `list-command-history'
              (mapcar (lambda (h)
                        (if (stringp h) h (format "%S" h)))
                      history)
            (user-error "No history available"))))
    ;; FIXME: Can we make it work for CRM?
    (completion-in-region
     (minibuffer--completion-prompt-end) (point-max)
     (completion-table-with-metadata
      completions '((sort-function   . identity))))))

(defun minibuffer-complete-defaults ()
  "Complete minibuffer defaults as far as possible.
Like `minibuffer-complete' but completes on the default items
instead of the completion table."
  (interactive "" minibuffer-mode)
  (when (and (not minibuffer-default-add-done)
             (functionp minibuffer-default-add-function))
    (setq minibuffer-default-add-done t
          minibuffer-default (funcall minibuffer-default-add-function)))
  (let ((completions (ensure-list minibuffer-default)))
    (completion-in-region
     (minibuffer--completion-prompt-end) (point-max)
     (completion-table-with-metadata
      completions '((sort-function . identity))))))

(define-key minibuffer-local-map [?\C-x up] 'minibuffer-complete-history)
(define-key minibuffer-local-map [?\C-x down] 'minibuffer-complete-defaults)

(defun minibuffer-narrow-completions-p ()
  "Return non-nil if there are restrictions on current completions list."
  (functionp minibuffer-completion-predicate)
  (let ((result nil))
    (advice-function-mapc
     (lambda (_ alist)
       (setq result (alist-get 'description alist)))
     minibuffer-completion-predicate)
    result))

(defvar minibuffer-narrow-completions-function
  #'minibuffer-completions-regexp-predicate
  "Function to use for restricting the list of completions candidates.

Minibuffer command `minibuffer-narrow-completions' calls this
function with no arguments.  This function should return a cons
cell (PRED . DESC) where PRED is a function that takes one
completion candidate and returns non-nil if it should appear in
the *Completions* buffer, and DESC is a string describing PRED.")

(defun minibuffer--completion-boundaries ()
  "Return boundaries for minibuffer completion as a pair of buffer positions."
  (let* ((prompt-end (minibuffer-prompt-end))
         (beg-end (completion-boundaries
                   (buffer-substring prompt-end (point))
                   minibuffer-completion-table
                   minibuffer-completion-predicate
                   (buffer-substring (point) (point-max)))))
    (cons (+ prompt-end (car beg-end)) (+ (point) (cdr beg-end)))))

(defun minibuffer-completions-regexp-predicate ()
  "Narrow completion candidates by matching a given regular expression.
This function is the default value of variable
`minibuffer-narrow-completions-function', which see."
  (let* ((beg-end (minibuffer--completion-boundaries))
         (beg (car beg-end)) (end (cdr beg-end))
         (rel-point (- (point) beg))
         (input (buffer-substring beg end))
         (regexp (minibuffer-with-setup-hook
                     (lambda ()
                       (insert input)
                       (goto-char (+ rel-point (minibuffer-prompt-end))))
                   (read-regexp "Keep candidates matching regexp: "))))
    (delete-region beg end)
    (cons (lambda (cand &rest _)
            (let ((string (cond
                           ((stringp cand)              cand)
                           ((symbolp cand) (symbol-name cand))
                           (t              (car         cand)))))
              (string-match-p regexp string)))
          (concat "match=" (prin1-to-string regexp)))))

(defun minibuffer--add-completions-predicate (pred desc)
  "Restrict minibuffer completions list to candidates satisfying PRED.
DESC is a string describing predicate PRED."
  (unless minibuffer-completion-predicate
    (setq-local minibuffer-completion-predicate #'always))
  (add-function :after-while (local 'minibuffer-completion-predicate)
                pred `((description . ,desc)))
  (when completion-auto-help (minibuffer-completion-help)))

(defun minibuffer-read-predicate-description (prompt &optional default)
  "Prompt with PROMPT for current completion predicate description.

Optional argument DEFAULT is the default minibuffer argument.  If
omitted or nil, it defaults to the description of the predicate you
added last.

If there is only one current completion predicate, return its
description without prompting."
  (let ((default (or default (minibuffer-latest-predicate-description)))
        (single nil))
    (advice-function-mapc
     (lambda (_a p)
       (when-let ((d (alist-get 'description p)))
         (setq single (if single t d))))
     minibuffer-completion-predicate)
    (if (stringp single) single
      (completing-read
       (format-prompt prompt default)
       (completion-table-with-metadata
        (completion-table-dynamic
         (let ((buf (current-buffer)))
           (lambda (&rest _)
             (with-current-buffer buf
               (let ((descs nil))
                 (advice-function-mapc
                  (lambda (_a p)
                    (when-let ((d (alist-get 'description p)))
                      (push d descs)))
                  minibuffer-completion-predicate)
                 ;; Put latest restriction first.
                 (reverse descs))))))
        '((category . predicate-description)))
       nil t nil nil default))))

(defun minibuffer-predicate-description-to-function (desc)
  "Return predicate function the DESC describes, or nil."
  (catch 'stop
    (advice-function-mapc
     (lambda (a p)
       (when (equal (alist-get 'description p) desc)
         (throw 'stop (cons (alist-get 'description p) a))))
     minibuffer-completion-predicate)
    nil))

(defun minibuffer-latest-predicate-description ()
  "Return the completion predicate you added most recently, or nil."
  (catch 'stop
    (advice-function-mapc
     (lambda (_ p)
       (when-let ((desc (alist-get 'description p)))
         (throw 'stop desc)))
     minibuffer-completion-predicate)
    nil))

(defun minibuffer-negate-completion-predicate (&optional desc)
  "Negate completion predicate with description DESC.

Optional argument DESC says which predicate to negate.  If it is a
string, negate the predicate that DESC describes.  Otherwise, negate the
conjunction of all current predicates together.

Interactively, prompt for DESC among curent predicates, unless there is
only one predicate, in which case DESC is the string describing that
predicate.  With a prefix argument, negate the conjunction of all
predicates together."
  (interactive (list (or current-prefix-arg
                         (minibuffer-read-predicate-description "Negate")))
               minibuffer-mode)
  (unless minibuffer-completion-predicate
    (setq-local minibuffer-completion-predicate #'always))
  (if (stringp desc)
      ;; Negate the conjuct with description DESC.
      (if-let* ((desc-fn (minibuffer-predicate-description-to-function desc))
                (desc (car desc-fn))
                (fn (cdr desc-fn)))
          (progn
            (remove-function (local 'minibuffer-completion-predicate) fn)
            (if-let ((neg (get-text-property 0 'negated desc)))
                (minibuffer--add-completions-predicate (cdr neg) (car neg))
              (minibuffer--add-completions-predicate
               (compf not [fn])
               (propertize (concat "-(" desc ")") 'negated (cons desc fn)))))
        (user-error "`%s' is not a description of a current predicate" desc))
    ;; Negate the entire predicate.
    (if (advice-function-member-p #'not minibuffer-completion-predicate)
        (remove-function (local 'minibuffer-completion-predicate) #'not)
      (add-function :filter-return (local 'minibuffer-completion-predicate)
                    #'not '((depth . -100)))))
  (when completion-auto-help (minibuffer-completion-help)))

(put 'minibuffer-negate-completion-predicate 'minibuffer-action
     (cons (lambda (d)
             (with-current-buffer minibuffer--original-buffer
               (minibuffer-negate-completion-predicate d)))
           "negate"))

(defun minibuffer-narrow-completions ()
  "Restrict completion candidates for current minibuffer interaction."
  (interactive "" minibuffer-mode)
  (let* ((filter-desc
          (funcall
           (or (when-let ((fun (completion-metadata-get
                                (completion--field-metadata
                                 (minibuffer-prompt-end))
                                'narrow-completions-function)))
                 (if (functionp fun) fun
                   (nth 3 (read-multiple-choice "Narrow completions by:" fun))))
               minibuffer-narrow-completions-function))))
    (minibuffer--add-completions-predicate (car filter-desc) (cdr filter-desc))))

(defun minibuffer-narrow-completions-by-regexp ()
  "Restrict completion candidates by matching a given regular expression."
  (interactive "" minibuffer-mode)
  (let* ((filter-desc (minibuffer-completions-regexp-predicate)))
    (minibuffer--add-completions-predicate (car filter-desc) (cdr filter-desc))))

(defun minibuffer-add-completion-predicate (pred)
  "Restrict completion candidates to those satisfying PRED."
  (interactive
   (list
    (completing-read "Predicate: "
                     (completion-table-with-metadata
                      obarray '((category . function)))
                     #'fboundp
                     nil nil 'minibuffer-completions-predicate-history
                     "minibuffer-collect"))
   minibuffer-mode)
  (when (stringp pred) (setq pred (read pred)))
  (minibuffer--add-completions-predicate pred (prin1-to-string pred)))

(put 'minibuffer-add-completion-predicate 'minibuffer-action
     (cons (lambda (p)
             (with-current-buffer minibuffer--original-buffer
               (minibuffer-add-completion-predicate p)))
            "add"))

(defun minibuffer-narrow-completions-to-current (arg)
  "Restrict completion candidates according to current minibuffer input.
ARG is the numeric prefix argument.  When ARG is negative,
exclude matches to current input from completions list."
  (interactive "p" minibuffer-mode)
  (let* ((table (make-hash-table :test #'equal))
         (beg-end (minibuffer--completion-boundaries))
         (beg (car beg-end)) (end (cdr beg-end))
         (current (buffer-substring-no-properties beg end))
         (start (minibuffer-prompt-end))
         (input (buffer-substring start (point-max)))
         (all (completion-all-completions
               input
               minibuffer-completion-table
               minibuffer-completion-predicate
               (- (point) start)
               (completion--field-metadata start)))
         (last (last all)))
    (unless all
      (user-error "No matching completion candidates"))
    (delete-region beg end)
    (setcdr last nil)
    (dolist (str all)
      (puthash str t table))
    (if (< arg 0)
        (minibuffer--add-completions-predicate
         (lambda (cand &rest _)
           (let ((key (cond
                       ((stringp cand)              cand)
                       ((symbolp cand) (symbol-name cand))
                       (t              (car         cand)))))
             (not (gethash key table))))
         (concat "remove=" (prin1-to-string current)))
      (minibuffer--add-completions-predicate
       (lambda (cand &rest _)
         (let ((key (cond
                     ((stringp cand)              cand)
                     ((symbolp cand) (symbol-name cand))
                     (t              (car         cand)))))
           (gethash key table)))
       (concat "narrow=" (prin1-to-string current))))))

(defun minibuffer-narrow-completions-to-history (&optional exclude)
  "EXCLUDE or keep only members of the minibuffer history as completions.

If EXCLUDE is nil, restrict completions to only those that are
also in the minibuffer history list.  In other words, keep only
your past inputs as completion candidates.  Otherwise, if EXCLUDE
is non-nil, keep only \"new\" completion candidates, excluding
members of the minibuffer history list."
  (interactive "P" minibuffer-mode)
  (if-let ((hist
            (mapcar
             (lambda (string)
               (substring string
                          (car (completion-boundaries
                                string
                                minibuffer-completion-table
                                minibuffer-completion-predicate
                                ""))))
             (and (not (eq minibuffer-history-variable t))
                  (symbol-value minibuffer-history-variable))))
           (func (if exclude (lambda (k h) (not (member k h))) #'member)))
      (minibuffer--add-completions-predicate
       (lambda (cand &rest _)
         (let* ((key (cond
                      ((stringp cand)              cand)
                      ((symbolp cand) (symbol-name cand))
                      (t              (car         cand)))))
           (funcall func key hist)))
       (concat "used=" (if exclude "n" "y")))))

(defun minibuffer-toggle-exceptional-candidates ()
  "Toggle display of exceptional completion candidates."
  (interactive "" minibuffer-mode)
  (setq-local completions-exclude-exceptional-candidates
              (not completions-exclude-exceptional-candidates))
  (when (get-buffer-window completions-buffer-name 0)
    (minibuffer-completion-help))
  (minibuffer-message "Completion now %scludes exceptional candidates"
                      (if completions-exclude-exceptional-candidates
                          "ex" "in")))

(defun minibuffer-toggle-completions-annotations ()
  "Toggle display of annotations for completion candidates."
  (interactive "" minibuffer-mode)
  (setq-local minibuffer-completion-annotations
              (not minibuffer-completion-annotations))
  (when (get-buffer-window completions-buffer-name 0)
    (minibuffer-completion-help))
  (minibuffer-message "Completion annotations %sabled"
                      (if minibuffer-completion-annotations "en" "dis")))

(defun minibuffer-widen-completions (&optional desc)
  "Remove restrictions on current minibuffer completions list.

Optional argument DESC says which restrictions to remove.  If it is a
string, remove the restriction that DESC describes.  Otherwise, remove
all current restrictions.

Interactively, prompt for DESC among curent restrictions, unless there
is only one restriction, in which case DESC is the string describing
that restriction.  With a prefix argument, remove all restrictions,
regardless of how many there are."
  (interactive (list (or current-prefix-arg
                         (minibuffer-read-predicate-description "Remove")))
               minibuffer-mode)
  (if (stringp desc)
      ;; Remove conjunct with description DESC.
      (if-let* ((fn (cdr (minibuffer-predicate-description-to-function desc))))
          (remove-function (local 'minibuffer-completion-predicate) fn)
        (user-error "`%s' is not a description of a current predicate" desc))
    ;; Remove all restrictions.
    (let ((preds nil))
      (advice-function-mapc
       (lambda (a p)
         (when (alist-get 'description p)
           (push a preds)))
       minibuffer-completion-predicate)
      (dolist (pred preds)
        (remove-function (local 'minibuffer-completion-predicate) pred))))
  (when completion-auto-help (minibuffer-completion-help)))

(put 'minibuffer-widen-completions 'minibuffer-action
     (cons (lambda (d)
             (with-current-buffer minibuffer--original-buffer
               (minibuffer-widen-completions d)))
           "remove"))

(defcustom minibuffer-default-prompt-format " (default %s)"
  "Format string used to output \"default\" values.
When prompting for input, there will often be a default value,
leading to prompts like \"Number of articles (default 50): \".
The \"default\" part of that prompt is controlled by this
variable, and can be set to, for instance, \" [%s]\" if you want
a shorter displayed prompt, or \"\", if you don't want to display
the default at all.

This variable is used by the `format-prompt' function."
  :version "28.1"
  :type 'string)

(defun format-prompt (prompt &optional default &rest format-args)
  "Format PROMPT with DEFAULT according to `minibuffer-default-prompt-format'.
If FORMAT-ARGS is nil, PROMPT is used as a plain string.  If
FORMAT-ARGS is non-nil, PROMPT is used as a format control
string, and FORMAT-ARGS are the arguments to be substituted into
it.  See `format' for details.

Both PROMPT and `minibuffer-default-prompt-format' are run
through `substitute-command-keys' (which see).  In particular,
this means that single quotes may be displayed by equivalent
characters, according to the capabilities of the terminal.

If DEFAULT is a list, the first element is used as the default.
If not, the element is used as is.

If DEFAULT is nil or an empty string, no \"default value\" string
is included in the return value."
  (concat
   (if (null format-args)
       (substitute-command-keys prompt)
     (apply #'format (substitute-command-keys prompt) format-args))
   (and default
        (or (not (stringp default))
            (length> default 0))
        (propertize
         (format (substitute-command-keys minibuffer-default-prompt-format)
                 (if (consp default)
                     (car default)
                   default))
         'minibuffer-default t))
   ": "))


;;; On screen keyboard support.
;; Try to display the on screen keyboard whenever entering the
;; mini-buffer, and hide it whenever leaving.

(defvar minibuffer-on-screen-keyboard-timer nil
  "Timer run upon exiting the minibuffer.
It will hide the on screen keyboard when necessary.")

(defvar minibuffer-on-screen-keyboard-displayed nil
  "Whether or not the on-screen keyboard has been displayed.
Set inside `minibuffer-setup-on-screen-keyboard'.")

(defun minibuffer-setup-on-screen-keyboard ()
  "Maybe display the on-screen keyboard in the current frame.
Display the on-screen keyboard in the current frame if the
last device to have sent an input event is not a keyboard.
This is run upon minibuffer setup."
  ;; Don't hide the on screen keyboard later on.
  (when minibuffer-on-screen-keyboard-timer
    (cancel-timer minibuffer-on-screen-keyboard-timer)
    (setq minibuffer-on-screen-keyboard-timer nil))
  (setq minibuffer-on-screen-keyboard-displayed nil)
  (when (and (framep last-event-frame)
             (not (memq (device-class last-event-frame
                                      last-event-device)
                        '(keyboard core-keyboard))))
    (setq minibuffer-on-screen-keyboard-displayed
          (frame-toggle-on-screen-keyboard (selected-frame) nil))))

(defun minibuffer-exit-on-screen-keyboard ()
  "Hide the on-screen keyboard if it was displayed.
Hide the on-screen keyboard in a timer set to run in 0.1 seconds.
It will be canceled if the minibuffer is displayed again within
that timeframe.

Do not hide the on screen keyboard inside a recursive edit.
Likewise, do not hide the on screen keyboard if point in the
window that will be selected after exiting the minibuffer is not
on read-only text.

The latter is implemented in `touch-screen.el'."
  (unless (or (not minibuffer-on-screen-keyboard-displayed)
              (> (recursion-depth) 1))
    (when minibuffer-on-screen-keyboard-timer
      (cancel-timer minibuffer-on-screen-keyboard-timer))
    (setq minibuffer-on-screen-keyboard-timer
          (run-with-timer 0.1 nil #'frame-toggle-on-screen-keyboard
                          (selected-frame) t))))

(add-hook 'minibuffer-setup-hook #'minibuffer-setup-on-screen-keyboard)
(add-hook 'minibuffer-exit-hook #'minibuffer-exit-on-screen-keyboard)

(defvar minibuffer-regexp-mode)

(defun minibuffer--regexp-propertize ()
  "In current minibuffer propertize parens and slashes in regexps.
Put punctuation `syntax-table' property on selected paren and
backslash characters in current buffer to make `show-paren-mode'
and `blink-matching-paren' more user-friendly."
  (let (in-char-alt-p)
    (save-excursion
      (with-silent-modifications
        (remove-text-properties (point-min) (point-max) '(syntax-table nil))
        (goto-char (point-min))
        (while (re-search-forward
                (rx (| (group "\\\\")
                       (: "\\" (| (group (in "(){}"))
                                  (group "[")
                                  (group "]")))
                       (group "[:" (+ (in "A-Za-z")) ":]")
                       (group "[")
                       (group "]")
                       (group (in "(){}"))))
	        (point-max) 'noerror)
	  (cond
           ((match-beginning 1))                ; \\, skip
           ((match-beginning 2)			; \( \) \{ \}
            (if in-char-alt-p
	        ;; Within character alternative, set symbol syntax for
	        ;; paren only.
                (put-text-property (1- (point)) (point) 'syntax-table '(3))
	      ;; Not within character alternative, set symbol syntax for
	      ;; backslash only.
              (put-text-property (- (point) 2) (1- (point)) 'syntax-table '(3))))
	   ((match-beginning 3)			; \[
            (if in-char-alt-p
                (progn
	          ;; Set symbol syntax for backslash.
                  (put-text-property (- (point) 2) (1- (point)) 'syntax-table '(3))
                  ;; Re-read bracket we might be before a character class.
                  (backward-char))
	      ;; Set symbol syntax for bracket.
	      (put-text-property (1- (point)) (point) 'syntax-table '(3))))
	   ((match-beginning 4)			; \]
            (if in-char-alt-p
                (progn
                  ;; Within character alternative, set symbol syntax for
	          ;; backslash, exit alternative.
                  (put-text-property (- (point) 2) (1- (point)) 'syntax-table '(3))
	          (setq in-char-alt-p nil))
	      ;; Not within character alternative, set symbol syntax for
	      ;; bracket.
	      (put-text-property (1- (point)) (point) 'syntax-table '(3))))
	   ((match-beginning 5))         ; POSIX character class, skip
	   ((match-beginning 6)          ; [
	    (if in-char-alt-p
	        ;; Within character alternative, set symbol syntax.
	        (put-text-property (1- (point)) (point) 'syntax-table '(3))
	      ;; Start new character alternative.
	      (setq in-char-alt-p t)
              ;; Looking for immediately following non-closing ].
	      (when (looking-at "\\^?\\]")
	        ;; Non-special right bracket, set symbol syntax.
	        (goto-char (match-end 0))
	        (put-text-property (1- (point)) (point) 'syntax-table '(3)))))
	   ((match-beginning 7)			; ]
            (if in-char-alt-p
                (setq in-char-alt-p nil)
              ;; The only warning we can emit before RET.
	      (message "Not in character alternative")))
	   ((match-beginning 8)                 ; (){}
	    ;; Plain parenthesis or brace, set symbol syntax.
	    (put-text-property (1- (point)) (point) 'syntax-table '(3)))))))))

;; The following variable is set by 'minibuffer--regexp-before-change'.
;; If non-nil, either 'minibuffer--regexp-post-self-insert' or
;; 'minibuffer--regexp-after-change', whichever comes next, will
;; propertize the minibuffer via 'minibuffer--regexp-propertize' and
;; reset this variable to nil, avoiding to propertize the buffer twice.
(defvar-local minibuffer--regexp-primed nil
  "Non-nil when minibuffer contents change.")

(defun minibuffer--regexp-before-change (_a _b)
  "`minibuffer-regexp-mode' function on `before-change-functions'."
  (setq minibuffer--regexp-primed t))

(defun minibuffer--regexp-after-change (_a _b _c)
  "`minibuffer-regexp-mode' function on `after-change-functions'."
  (when minibuffer--regexp-primed
    (setq minibuffer--regexp-primed nil)
    (minibuffer--regexp-propertize)))

(defun minibuffer--regexp-post-self-insert ()
  "`minibuffer-regexp-mode' function on `post-self-insert-hook'."
  (when minibuffer--regexp-primed
    (setq minibuffer--regexp-primed nil)
    (minibuffer--regexp-propertize)))

(defvar minibuffer--regexp-prompt-regexp
  "\\(?:Posix search\\|RE search\\|Search for regexp\\|Query replace regexp\\)"
  "Regular expression compiled from `minibuffer-regexp-prompts'.")

(defcustom minibuffer-regexp-prompts
  '("Posix search" "RE search" "Search for regexp" "Query replace regexp")
  "List of regular expressions that trigger `minibuffer-regexp-mode' features.
The features of `minibuffer-regexp-mode' will be activated in a minibuffer
interaction if and only if a prompt matching some regexp in this list
appears at the beginning of the minibuffer.

Setting this variable directly with `setq' has no effect; instead,
either use \\[customize-option] interactively or use `setopt'."
  :type '(repeat (string :tag "Prompt"))
  :set (lambda (sym val)
	 (set-default sym val)
         (when val
           (setq minibuffer--regexp-prompt-regexp
                 (concat "\\(?:" (mapconcat 'regexp-quote val "\\|") "\\)"))))
  :version "30.1")

(defun minibuffer--regexp-setup ()
  "Function to activate`minibuffer-regexp-mode' in current buffer.
Run by `minibuffer-setup-hook'."
  (if (and minibuffer-regexp-mode
           (save-excursion
             (goto-char (point-min))
             (looking-at minibuffer--regexp-prompt-regexp)))
      (progn
        (setq-local parse-sexp-lookup-properties t)
        (add-hook 'before-change-functions #'minibuffer--regexp-before-change nil t)
        (add-hook 'after-change-functions #'minibuffer--regexp-after-change nil t)
        (add-hook 'post-self-insert-hook #'minibuffer--regexp-post-self-insert nil t))
    ;; Make sure.
    (minibuffer--regexp-exit)))

(defun minibuffer--regexp-exit ()
  "Function to deactivate `minibuffer-regexp-mode' in current buffer.
Run by `minibuffer-exit-hook'."
  (with-silent-modifications
    (remove-text-properties (point-min) (point-max) '(syntax-table nil)))
  (setq-local parse-sexp-lookup-properties nil)
  (remove-hook 'before-change-functions #'minibuffer--regexp-before-change t)
  (remove-hook 'after-change-functions #'minibuffer--regexp-after-change t)
  (remove-hook 'post-self-insert-hook #'minibuffer--regexp-post-self-insert t))

(define-minor-mode minibuffer-regexp-mode
  "Minor mode for editing regular expressions in the minibuffer.
Highlight parens via `show-paren-mode' and `blink-matching-paren'
in a user-friendly way, avoid reporting alleged paren mismatches
and make sexp navigation more intuitive.

The list of prompts activating this mode in specific minibuffer
interactions is customizable via `minibuffer-regexp-prompts'."
  :global t
  :initialize 'custom-initialize-delay
  :init-value t
  (if minibuffer-regexp-mode
      (progn
        (add-hook 'minibuffer-setup-hook #'minibuffer--regexp-setup)
        (add-hook 'minibuffer-exit-hook #'minibuffer--regexp-exit))
    ;; Clean up - why is Vminibuffer_list not available in Lisp?
    (dolist (buffer (buffer-list))
      (when (and (minibufferp)
                 parse-sexp-lookup-properties
                 (with-current-buffer buffer
                   (save-excursion
                     (goto-char (point-min))
                     (looking-at minibuffer--regexp-prompt-regexp))))
        (with-current-buffer buffer
          (with-silent-modifications
            (remove-text-properties
             (point-min) (point-max) '(syntax-table nil)))
          (setq-local parse-sexp-lookup-properties t))))
    (remove-hook 'minibuffer-setup-hook #'minibuffer--regexp-setup)
    (remove-hook 'minibuffer-exit-hook #'minibuffer--regexp-exit)))

(defvar completion-regexp-list nil "Unused obsolete variable.")
(make-obsolete-variable 'completion-regexp-list nil "30.1")

(defvar minibuffer-help-form nil "Unused obsolete variable.")
(make-obsolete-variable 'minibuffer-help-form 'help-form "30.1")

(defvar minibuffer-allow-text-properties nil "Unused obsolete variable.")
(make-obsolete-variable 'minibuffer-allow-text-properties nil "30.1")

(defvar read-minibuffer-restore-windows nil "Unused obsolete variable.")
(make-obsolete-variable 'read-minibuffer-restore-windows nil "30.1")

(defvar enable-recursive-minibuffers nil "Unused obsolete variable.")
(make-obsolete-variable 'enable-recursive-minibuffers nil "31.1")

(defvar completion-tab-width nil "Unused obsolete variable.")
(make-obsolete-variable 'completion-tab-width nil "31.1")

(defcustom minibuffer-auto-completion-idle-time 0.4
  "Number for seconds to wait before auto-completion in the minibuffer."
  :type 'float
  :version "30.1")

(defcustom minibuffer-auto-completion-expand-common nil
  "Whether to auto-expand minibuffer input based on common part of completions."
  ;; TODO: Add option to only expand when completion is strict.
  ;; TODO: Add option to only expand match is exact.
  :type 'boolean
  :version "30.1")

(defvar-local minibuffer-auto-completion-timer nil)

(defun minibuffer-auto-completion ()
  "Try completing minibuffer input and indicate the results."
  ;; TODO: Optimize.
  (when minibuffer-auto-completion-expand-common
    (let* ((cont (minibuffer-contents))
           (comp (completion-try-completion cont
                                            minibuffer-completion-table
                                            minibuffer-completion-predicate
                                            (- (point)
                                               (minibuffer-prompt-end)))))
      (when (consp comp)
        (let* ((comp-pos (cdr comp))
               (completion (car comp)))
          (unless (string= completion cont)
            (minibuffer-record-completion-input
              ;; TODO: Pulse changed part.
              (completion--replace (minibuffer-prompt-end) (point-max)
                                   completion))
            (forward-char (- comp-pos (length completion))))))))
  (if (get-buffer-window completions-buffer-name 0)
      ;; Completions list is already displayed, refresh it.
      (minibuffer-completion-help)
    ;; Otherwise, display a concise summary inline.
    (if-let ((all (let ((completion-lazy-hilit t)
                        completion-all-sorted-completions)
                    (completion-all-sorted-completions))))
        (let ((minibuffer-message-timeout))
          (minibuffer-message
           (concat (if (string= (car all) (minibuffer-contents))
                       (propertize "Match" 'face 'success)
                     (propertize (car all) 'face 'bold))
                   (when (consp (cdr all))
                     (propertize
                      (concat
                       "/"
                       (number-to-string (let ((n 1))
                                           (while (consp (cdr all))
                                             (setq n (1+ n)
                                                   all (cdr all)))
                                           n)))
                      'face 'shadow)))))
      (completion--fail))))

(defun minibuffer-auto-completion-cancel-timer ()
  "Cancel `minibuffer-auto-completion-timer'."
  (when (timerp minibuffer-auto-completion-timer)
    (cancel-timer minibuffer-auto-completion-timer)
    (setq minibuffer-auto-completion-timer nil)))

(defun minibuffer-auto-completion-fn (minib)
  "Return a function that auto-completes in minibuffer MINIB."
  (lambda ()
    (when (equal (current-buffer) minib)
      ;; TODO: Consider wrapping this call with `while-no-input'.
      ;; TODO: Detect slow auto completion and disable/extend delay.
      (minibuffer-auto-completion))
    (when (buffer-live-p minib)
      (with-current-buffer minib (minibuffer-auto-completion-cancel-timer)))))

(defun minibuffer-auto-completion-start-timer ()
  "Start minibuffer auto-completion timer."
  (unless (timerp minibuffer-auto-completion-timer)
    (setq minibuffer-auto-completion-timer
          (run-with-idle-timer minibuffer-auto-completion-idle-time
                               nil (minibuffer-auto-completion-fn (current-buffer))))))

(define-minor-mode minibuffer-auto-completion-mode
  "Automatically complete minibuffer input and indicate the results."
  :interactive (minibuffer-mode)
  (if minibuffer-auto-completion-mode
      (if (not minibuffer-completion-table)
          (setq minibuffer-auto-completion-mode nil)
        (add-hook 'post-self-insert-hook #'minibuffer-auto-completion-start-timer nil t)
        (add-hook 'minibuffer-exit-hook #'minibuffer-auto-completion-cancel-timer nil t)
        (add-hook 'pre-command-hook #'minibuffer-auto-completion-cancel-timer nil t))
    (remove-hook 'post-self-insert-hook #'minibuffer-auto-completion-start-timer t)
    (remove-hook 'minibuffer-exit-hook #'minibuffer-auto-completion-cancel-timer t)
    (remove-hook 'pre-command-hook #'minibuffer-auto-completion-cancel-timer t)
    (minibuffer-auto-completion-cancel-timer)))

(define-minor-mode global-minibuffer-auto-completion-mode
  "Use `minibuffer-auto-completion-mode' in all minibuffers."
  :global t
  (if global-minibuffer-auto-completion-mode
      (add-hook 'minibuffer-setup-hook #'minibuffer-auto-completion-mode)
    (remove-hook 'minibuffer-setup-hook #'minibuffer-auto-completion-mode)))

(defun minibuffer-prompt ()
  "Return the current minibuffer prompt."
  (buffer-substring (point-min) (minibuffer-prompt-end)))

;; (defun minibuffer-set-prompt (prompt)
;;   "Set the current minibuffer prompt to PROMPT."
;;   ;; FIXME: Inhibit/adjust undo.
;;   (let ((inhibit-read-only t)
;;         (buffer-undo-list t)
;;         (tniop (- (point-max) (point))))
;;     (delete-region (point-min) (minibuffer-prompt-end))
;;     (goto-char (point-min))
;;     (insert prompt)
;;     (put-text-property (point-min) (point) 'front-sticky t)
;;     (put-text-property (point-min) (point) 'rear-nonsticky t)
;;     (put-text-property (point-min) (point) 'field t)
;;     (cl-loop for (prop val) on minibuffer-prompt-properties by #'cddr
;;              do (if (eq prop 'face)
;;                     (add-face-text-property (point-min) (point) val t)
;;                   (put-text-property (point-min) (point) prop val)))
;;     (goto-char (- (point-max) tniop))))

(defvar read-history-variable-history nil
  "History list for `read-history-variable-history'.")

(defun read-history-variable (prompt)
  "Prompt with PROMPT for a history variable and return its name as a string."
  (completing-read prompt obarray
                   (lambda (s)
                     (and (boundp s)
                          (consp (symbol-value s))
                          (stringp (car (symbol-value s)))
                          (string-match "-\\(history\\|ring\\)\\'"
                                        (symbol-name s))))
                   t nil 'read-history-variable-history))

(defun minibuffer-alternate-history (hist-var)
  "Set history variable of current minibuffer to HIST-VAR."
  (interactive (list (read-history-variable "Use history variable: "))
               minibuffer-mode)
  ;; TODO: Make `minibuffer-history-variable' (mini)buffer-local.
  (setq minibuffer-history-variable (if (stringp hist-var)
                                        (intern hist-var)
                                      hist-var)))

(defun minibuffer-kill-from-history (input)
  "Remove INPUT from current minibuffer history."
  (interactive
   (if (eq minibuffer-history-variable t)
       (user-error "No history available")
     (list
      (minibuffer-with-setup-hook
          (lambda ()
            (setq-local history-add-new-input nil
                        ;; All candidates are previous inputs by
                        ;; definition, so no need to highlight them.
                        completions-highlight-previous-inputs nil))
        (completing-read
         "Delete from history: "
         (completion-table-dynamic
          (lambda (&rest _)
            (mapcar
             (lambda (cand)
               (if (and (stringp cand) (not (string-empty-p cand)))
                   (propertize cand 'completion-identity cand)
                 cand))
             (symbol-value minibuffer-history-variable))))
         nil t nil
         ;; HACK: Use the history variable of the original minibuffer
         ;; also in the recursive minibuffer s.t. `minibuffer-apply' in
         ;; the recursive minibuffer deletes from the original history.
         minibuffer-history-variable))))
   minibuffer-mode)
  (if (eq minibuffer-history-variable t) (error "No history available")
    (let* ((hist (cons nil (symbol-value minibuffer-history-variable)))
           (temp hist)
           (res nil))
      ;; First check with `eq' in favor of `minibuffer-apply' on a
      ;; non-first occurrence of a history entry.
      (while (and temp (not res))
        (if (not (eq (get-text-property 0 'completion-identity input)
                     (cadr temp)))
            (setq temp (cdr temp))
          ;; It's a match, delete it.
          (setcdr temp (cddr temp))
          (setq res t)))
      ;; If `res' is still nil that means we found nothing when
      ;; comparing with `eq', so try again with `equal'.
      (setq temp hist)
      (while (and temp (not res))
        (if (not (equal input (cadr temp)))
            (setq temp (cdr temp))
          (setcdr temp (cddr temp))
          (setq res t)))
      (if res
          (progn
            (set minibuffer-history-variable (cdr hist))
            (message "Deleted \"%s\" from history `%s'"
                     input minibuffer-history-variable))
        (message "Input \"%s\" not in history `%s'"
                 input minibuffer-history-variable)))))

(put 'minibuffer-kill-from-history 'minibuffer-action "delete")

(defvar minibuffer-collect-completions nil)
(defvar minibuffer-collect-base nil)
(defvar minibuffer-collect-action nil)
(defvar minibuffer-collect-alt-action nil)

(defun minibuffer-collect-apply (&optional event alt)
  "Apply minibuffer action to the candidate at mouse EVENT or at point.

Non-nil optional argument ALT says to apply the alternative minibuffer
action instead."
  (interactive (list last-nonmenu-event) minibuffer-collect-mode)
  (with-current-buffer (window-buffer (posn-window (event-start event)))
    (funcall (car (if alt minibuffer-collect-alt-action minibuffer-collect-action))
             (concat minibuffer-collect-base
                     (get-text-property (posn-point (event-start event))
                                        'completion--string)))))

(defun minibuffer-collect-apply-alt (&optional event)
  "Apply alternative action to the candidate at mouse EVENT or at point."
  (interactive (list last-nonmenu-event) minibuffer-collect-mode)
  (minibuffer-collect-apply event t))

(defun minibuffer-collect-revert (&rest _)
  (let ((inhibit-read-only t))
    (erase-buffer)
    (delete-all-overlays)
    (completion--insert-one-column minibuffer-collect-completions nil))
  (goto-char (point-min)))

(defvar-keymap minibuffer-collect-mode-map
  :doc "Keymap for Minibuffer Collect mode."
  "n"   #'next-completion
  "p"   #'previous-completion
  "RET" #'minibuffer-collect-apply
  "<mouse-2>" #'minibuffer-collect-apply
  "S-RET" #'minibuffer-collect-apply-alt
  "S-<return>" #'minibuffer-collect-apply-alt
  "S-<mouse-1>" #'minibuffer-collect-apply-alt
  "<follow-link>" 'mouse-face)

(define-derived-mode minibuffer-collect-mode special-mode "Minibuffer Collect"
  "Major mode for minibuffer completion collection buffers."
  :interactive nil
  (cursor-face-highlight-mode)
  (setq-local revert-buffer-function #'minibuffer-collect-revert))

(defvar minibuffer-export-minibuffer nil)

(defun minibuffer--export-data ()
  (let* ((start (minibuffer-prompt-end))
         (end (point-max))
         (string (buffer-substring start end))
         (md (completion--field-metadata start))
         (completion-lazy-hilit t)
         (completions (completion-all-completions
                       string
                       minibuffer-completion-table
                       minibuffer-completion-predicate
                       (- (point) start)
                       md))
         (last (last completions))
         (base-size (or (cdr last) 0))
         (full-base (substring string 0 base-size))
         (base (funcall (or (alist-get 'adjust-base-function md) #'identity)
                        full-base)))
    (when last (setcdr last nil))
    (list completions base md)))

(defun minibuffer-collect (completions base md)
  (let ((buffer (generate-new-buffer "*Collection*"))
        (action (minibuffer-action))
        (altact (minibuffer-action t))
        (sort-fun (completion-metadata-get md 'sort-function))
        (aff-fun (completion-metadata-get md 'affixation-function))
        (ann-fun (completion-metadata-get md 'annotation-function)))
    (setq completions
          (cond
           (minibuffer-completions-sort-function
            (funcall minibuffer-completions-sort-function completions))
           (sort-fun (funcall sort-fun completions))
           (t
            (pcase completions-sort
              ('nil completions)
              ('alphabetical (minibuffer-sort-alphabetically completions))
              ('historical (minibuffer-sort-by-history completions))
              (_ (funcall completions-sort completions))))))
    (when minibuffer-completion-annotations
      (cond
       (aff-fun
        (setq completions
              (funcall aff-fun completions)))
       (ann-fun
        (setq completions
              (mapcar (lambda (s)
                        (let ((ann (funcall ann-fun s)))
                          (if ann (list s ann) s)))
                      completions)))))
    (with-current-buffer buffer
      (minibuffer-collect-mode)
      (let ((inhibit-read-only t))
        (erase-buffer)
        (delete-all-overlays)
        (completion--insert-one-column completions nil))
      (goto-char (point-min))
      (setq-local minibuffer-collect-completions completions
                  minibuffer-collect-base        base
                  minibuffer-collect-action      action
                  minibuffer-collect-alt-action  altact))
    buffer))

(defvar minibuffer-default-export-function #'minibuffer-collect)

(defun minibuffer-export-dired (files dir &optional _)
  "Create a Dired buffer listing FILES in DIR."
  (setq dir (file-name-as-directory (expand-file-name dir)))
  (dired-noselect
   (cons dir
         (mapcar (compf (lambda (file) (file-relative-name file dir))
                       directory-file-name)
                 (seq-filter #'file-exists-p
                             (mapcar (lambda (file) (expand-file-name file dir))
                                     files))))))

(defun minibuffer-export-list-buffers (buffer-names &rest _)
  (list-buffers-noselect nil (mapcar #'get-buffer buffer-names)))

(defvar minibuffer-export-history nil)

(defun minibuffer-export (&optional export-fn top-level-p)
  "Create a category-specific export buffer with current completion candidates.

Optional argument EXPORT-FN is a function that creates the export
buffer.  It should take three arguments: the list of completion
candidates, as strings; the common base part elided from all candidates,
also a string; and the completion metadata.  If omitted or nil,
EXPORT-FN defaults to the `export-function' entry in the completion
metadata.  If that is also nil, `minibuffer-default-export-function' is
used instead, which defaults to `minibuffer-collect'.  Interactively,
without a prefix argument, EXPORT-FN is nil.  With a prefix argument,
this command prompts for EXPORT-FN in a recursive minibuffer.

If second optional argument TOP-LEVEL-P is non-nil, this function
exports the current minibuffer.  Otherwise, it exports the minibuffer
that is the value of `minibuffer-export-minibuffer'.  Interactively,
TOP-LEVEL-P is non-nil."
  (interactive (list
                (when current-prefix-arg
                  (let ((minibuffer-export-minibuffer (current-buffer)))
                    (completing-read
                     (format-prompt "Export function" "minibuffer-collect")
                     (completion-table-with-metadata
                      obarray '((category . function)))
                     #'fboundp
                     nil nil 'minibuffer-export-history "minibuffer-collect")))
                t)
               minibuffer-mode)
  (when (stringp export-fn) (setq export-fn (read export-fn)))
  (with-current-buffer
      (if (or top-level-p (not minibuffer-export-minibuffer))
          (current-buffer)
        minibuffer-export-minibuffer)
    (seq-let (completions base md) (minibuffer--export-data)
      (display-buffer (funcall (or export-fn
                                   (completion-metadata-get md 'export-function)
                                   minibuffer-default-export-function)
                               completions base md)))))

(put 'minibuffer-export 'minibuffer-action "export")

(defcustom minibuffer-new-completion-input-hook nil
  "Hook run in minibuffer after pushing new input to `completion-history'."
  :type 'hook)

(defun completing-read-case-insensitive
    ( prompt collection &optional
      predicate require-match initial-input
      hist def inherit-input-method)
  "`completing-read' with `completion-ignore-case' set to t in the minibuffer."
  (minibuffer-with-setup-hook
      (lambda () (setq-local completion-ignore-case t))
    (completing-read
     prompt collection predicate require-match
     initial-input hist def inherit-input-method)))

(define-obsolete-function-alias 'completion--cycle-threshold 'ignore "31.1"
  "Obsolete function, defined only for compatibility.")

(define-obsolete-function-alias 'completion-pcm--filename-try-filter 'identity "31.1"
  "Obsolete function, defined only for compatibility.")

(provide 'minibuffer)
;;; minibuffer.el ends here
