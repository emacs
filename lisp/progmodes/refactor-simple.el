;;; refactor-simple.el --- A simple refactor backend   -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'project)
(require 'refactor)

(defun refactor-simple-backend () '(simple rename))

(defun refactor-simple-rename-edits-in-buffer (old new &optional buf)
  (with-current-buffer (or buf (current-buffer))
    (let ((edits nil)
          (case-fold-search nil))
      (save-excursion
        (without-restriction
          (goto-char (point-min))
          (while (search-forward old nil t)
            (push (list (match-beginning 0) (match-end 0) new) edits))))
      (cons (current-buffer) edits))))

(cl-defmethod refactor-backend-rename-edits ((_backend (eql simple)) old new
                                             (_scope (eql buffer)))
  (list (refactor-simple-rename-edits-in-buffer old new)))

(cl-defmethod refactor-backend-rename-edits ((_backend (eql simple)) old new
                                             (_scope (eql project)))
  (mapcar (apply-partially #'refactor-simple-rename-edits-in-buffer old new)
          (seq-filter (let ((mm major-mode))
                        (lambda (buf)
                          (with-current-buffer buf
                            (derived-mode-p mm))))
                      (project-buffers (project-current)))))

;;;###autoload
(define-minor-mode refactor-simple-mode
  "Simple backend for refactoring operations."
  :group 'refactor
  :global t
  (if refactor-simple-mode
      (add-hook 'refactor-backend-functions #'refactor-simple-backend)
    (remove-hook 'refactor-backend-functions #'refactor-simple-backend)))


(provide 'refactor-simple)
;;; refactor-simple.el ends here
