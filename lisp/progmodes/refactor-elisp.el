;;; refactor-elisp.el --- Refactoring backend for Emacs Lisp   -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'refactor)

;;;###autoload
(defun elisp-refactor-backend () '(elisp rename extract))

(cl-defmethod refactor-backend-read-scoped-identifier ((_backend (eql elisp)))
  (let* ((pos (point)))
    (save-excursion
      (beginning-of-defun-raw)
      (catch 'var-def
        (scope (lambda (_type beg len _id &optional def)
                 (when (and def (<= beg pos (+ beg len)))
                   (throw 'var-def
                          (list (propertize
                                 (buffer-substring-no-properties beg (+ beg len))
                                 'pos beg))))))
        (user-error "No local variable to rename at point")))))

(cl-defmethod refactor-backend-rename-edits
  ((_backend (eql elisp)) old new (_scope (eql nil)))
  (list (cons (current-buffer)
              (let (res)
                (pcase-dolist (`(,beg . ,len)
                               (elisp-local-references
                                (get-text-property 0 'pos old)))
                  (alist-set beg res (list (+ beg len) new)))
                res))))

(cl-defmethod refactor-backend-rename-highlight-regions
  ((_backend (eql elisp)) old (_scope (eql nil)))
  (let (res)
    (pcase-dolist (`(,beg . ,len)
                   (elisp-local-references
                    (get-text-property 0 'pos old)))
      (alist-set beg res (+ beg len)))
    res))

(cl-defmethod refactor-backend-extract-validate-region
  ((_backend (eql elisp)) beg end)
  (unless (ignore-errors (read (buffer-substring beg end)) t)
    "syntactically invalid"))

(cl-defmethod refactor-backend-extract-edits
  ((_backend (eql elisp)) beg end new)
  (save-excursion
    ;; TODO:
    ;; - Handle local function bindings (e.g. `named-let').
    ;; - Remove redundant `progn' around extracted forms.
    ;; - Find more occurrences of extracted form, maybe with el-search.
    (goto-char beg)
    (beginning-of-defun-raw)
    (let (bound)
      (scope (lambda (_type sbeg len _id &optional def)
               (let ((send (+ sbeg len)))
                 (and (<= beg sbeg send end) def (< def beg)
                      (unless (assoc def bound #'=)
                        (push (cons def (buffer-substring-no-properties
                                         sbeg send))
                              bound))))))
      (let* ((buf (current-buffer)) (pos (point))
             (vstr (mapconcat #'cdr (sort bound) " "))
             (beg (progn
                    (goto-char beg)
                    (skip-chars-forward "[:blank:]\n")
                    (point)))
             (end (progn
                    (goto-char end)
                    (skip-chars-backward "[:blank:]\n")
                    (point)))
             (newf (with-temp-buffer
                     (insert-buffer-substring buf)
                     (emacs-lisp-mode)
                     (goto-char pos)
                     (insert "\n\n(defun " new " (" vstr ")" "\n")
                     (insert (string-trim (buffer-substring beg end)))
                     (when (nth 4 (syntax-ppss)) (insert "\n"))
                     (insert ")")
                     (prog-indent-sexp 'defun)
                     (buffer-substring pos (point)))))
        `((,buf
           ;; Add `new' function definition at `pos'.
           (,pos ,pos ,newf)
           ;; Replace `beg'-`end' region with call to `new' function.
           (,beg ,end ,(concat "(" new (when bound " ") vstr ")"))))))))

(defvar scope-local-functions)

(cl-defmethod refactor-backend-extract-done ((_backend (eql elisp)) new)
  (let ((sym (intern new)))
    (unless (memq sym scope-local-functions)
      (setq-local scope-local-functions (cons sym scope-local-functions)))))

(provide 'refactor-elisp)
;;; refactor-elisp.el ends here
