;;; refactor.el --- Common interface for code refactoring   -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Eshel Yaron

;; Author: Eshel Yaron <me@eshelyaron.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Generic refactoring UI and API.

;;; TODO:

;; - Add a menu bar menu and a prefix keymap.

;;; Code:

(defgroup refactor nil
  "Refactor code."
  :group 'programming)

(defcustom refactor-apply-edits-function #'refactor-apply-edits-at-once
  "Function to use for applying edits during refactoring.

`refactor-apply-edits' calls this function with one argument, a list of
cons cells (BUFFER . REPS), where BUFFER is the buffer to edit, and REPS
is a list of replacements to perform in BUFFER.  Each element of REPS is
a list (BEG END STR TOKEN) describing a replacement in BUFFER, where BEG
and END are buffer positions to delete and STR is the string to insert
at BEG afterwards.  If this function applies a replacement, it should
run hook `refactor-replacement-functions' with the corresponding TOKEN
as an argument while BUFFER is current."
  :type '(choice (const :tag "Apply edits at once" refactor-apply-edits-at-once)
                 (const :tag "Query about each edit" refactor-query-apply-edits)
                 (const :tag "Display edits as diff" refactor-display-edits-as-diff)
                 (function :tag "Custom function")))

(defcustom refactor-read-operation-function
  #'refactor-read-operation-multiple-choice
  "Function to use for reading a refactor operation."
  :type '(choice (const :tag "One-key selection" refactor-read-operation-multiple-choice)
                 (const :tag "Minibuffer completion" refactor-completing-read-operation)
                 (function :tag "Custom function")))

(defcustom refactor-backend-rename-message-format
  "Renaming \"%o\" to \"%n\" in %s."
  "Message to display when renaming identifiers.

This can be nil, which says not to display any message, or a
string that `refactor-rename' displays when renaming.  If the
value is a string, it may include the following `%'-constructs:
`%o' is the old identifier name, `%n' is the new identifier name,
and `%s' is the scope of the renaming operation.

The default value is the string \"Renaming \\\"%o\\\" to \\\"%n\\\" in %s.\""
  :type '(choice (string :tag "Format string")
                 (const  :tag "Disable" nil)))

(defvar refactor-backend-functions nil
  "Special hook for choosing a refactor backend to use in the current context.

Each function on this hook is called in turn with no arguments, and
should return either nil to say that it is not applicable, or a cons
cell (BACKEND . OPS) where BACKEND is a refactor backend, a value used
for dispatching the generic functions, and OPS is a list of refactoring
operations that BACKEND supports.")

(defun refactor-backends ()
  "Return alist of refactor operations and backends that support them."
  (let ((op-be-alist nil))
    (run-hook-wrapped
     'refactor-backend-functions
     (lambda (be-fun &rest _)
       (pcase (funcall be-fun)
         (`(,be . ,ops)
          (dolist (op ops)
            (push be (alist-get op op-be-alist)))))))
    op-be-alist))

;;;###autoload
(defun refactor (operation backend)
  (interactive
   (let* ((op-be-alist (refactor-backends))
          (op (if op-be-alist
                  (funcall refactor-read-operation-function op-be-alist)
                (user-error "No refactor operations available"))))
     (list op (car (alist-get op op-be-alist)))))
  (pcase operation
    ('rename (refactor-rename backend))
    ('extract (refactor-extract backend))
    ;; TODO:
    ;; ('inline (refactor-inline backend))
    ;; ('organize (refactor-organize backend))
    ;; ('simplify (refactor-simplify backend))
    (_ (refactor-backend-custom-operation backend operation))))

(cl-defgeneric refactor-backend-custom-operation (backend operation)
  "Apply custom refactoring OPERATION provided by BACKEND.")

(defun refactor-backend-for-operation (op)
  (car (alist-get op (refactor-backends))))

;;;###autoload
(defun refactor-extract (backend)
  (interactive (list
                (or (refactor-backend-for-operation 'extract)
                    (user-error "No appropriate refactor backend available"))))
  (unless (use-region-p) (user-error "No active region"))
  (let ((beg (region-beginning)) (end (region-end)))
    (when-let ((err (refactor-backend-extract-validate-region backend beg end)))
      (user-error "Cannot extract region: %s" err))
    (deactivate-mark)
    (let ((new (refactor-backend-read-new-function-name backend)))
      (refactor-apply-edits
       (refactor-backend-extract-edits
        backend (region-beginning) (region-end) new))
      (refactor-backend-extract-done backend new))))

;;;###autoload
(defun refactor-rename (backend)
  (interactive (list
                (or (refactor-backend-for-operation 'rename)
                    (user-error "No appropriate refactor backend available"))))
  (pcase (refactor-backend-read-scoped-identifier backend)
    (`(,old . ,scope)
     (let ((new (refactor-backend-read-replacement backend old scope)))
       (message (format-spec refactor-backend-rename-message-format
                             (list (cons ?o old)
                                   (cons ?n new)
                                   (cons ?s (or scope "current scope")))))
       (refactor-apply-edits
        ;; TODO: Maybe `save-some-buffers' first?
        (refactor-backend-rename-edits backend old new scope))))))

(defun refactor-read-operation-multiple-choice (operations)
  (intern (cadr (read-multiple-choice "Refactor operation:"
                                      (mapcar (pcase-lambda (`(,op . ,_))
                                                (list nil (symbol-name op)))
                                              operations)))))

(defun refactor-completing-read-operation (operations)
  (intern (completing-read "Refactor operation: "
                           (mapcar (compf symbol-name cadr)
                                   operations)
                           nil t)))

(cl-defgeneric refactor-backend-read-scoped-identifier (_backend)
  "Read an identifier and its scope for refactoring using BACKEND.

Return a cons cell (IDENT . SCOPE), where IDENT is the identifier to
operate on and SCOPE is the scope of application.  The meaning of both
IDENT and SCOPE are BACKEND-specific, but SCOPE is conventionally one of
`expression', `defun', `buffer' or `project'."
  (when-let ((sym (symbol-at-point)))
    (cons (symbol-name sym) (if (project-current) 'project 'buffer))))

(cl-defgeneric refactor-backend-invalid-replacement (_backend _old _new _scope)
  "Check if NEW is a valid replacement for OLD in SCOPE according to BACKEND.

If it is invalid, for example if NEW conflicts with an identifier that
is already in use, return a string to display as feedback to the user.
Otherwise, if the replacement is valid, return nil."
  nil)

(cl-defgeneric refactor-backend-rename-highlight-regions (_backend old _scope)
  "Return regions to highlight while prompting for replacement for OLD."
  (let ((regions nil)
        (case-fold-search nil))
    (save-excursion
      (goto-char (point-min))
      (while (search-forward old nil t)
        (push (cons (match-beginning 0) (match-end 0)) regions)))
    regions))

(declare-function track-changes-register "track-changes")
(declare-function track-changes-fetch    "track-changes")

(cl-defgeneric refactor-backend-read-replacement (backend old scope)
  "Read a replacement for identifier OLD across SCOPE using BACKEND."
  (let (ovs)
    (pcase-dolist (`(,beg . ,end)
                   (refactor-backend-rename-highlight-regions backend old scope))
      (let ((ov (make-overlay beg end)))
        (overlay-put ov 'face 'lazy-highlight)
        (push ov ovs)))
    (unwind-protect
        (let ((new nil) (invalid nil))
          (while (not new)
            (setq new
                  (minibuffer-with-setup-hook
                      (lambda ()
                        (require 'track-changes)
                        (track-changes-register
                         (lambda (id)
                           (track-changes-fetch
                            id (lambda (&rest _)
                                 (dolist (ov ovs)
                                   (overlay-put ov 'display
                                                (minibuffer-contents))))))
                         :nobefore t))
                    (read-string (format "%sRename \"%s\" across %s to: "
                                         (or invalid "")
                                         old (or scope "current scope"))
                                 nil nil old)))
            (when-let ((inv (refactor-backend-invalid-replacement
                             backend old new scope)))
              (setq invalid (format "Invalid replacement \"%s\": %s\n" new inv)
                    new nil)))
          new)
      (mapc #'delete-overlay ovs))))

(cl-defgeneric refactor-backend-read-new-function-name (_backend)
  "Read a new function name."
  (let ((def (when-let ((d (which-function))) (concat d "-1"))))
    (read-string (format-prompt "Extract region to function called" def)
                 nil nil def)))

(cl-defgeneric refactor-backend-extract-validate-region (backend beg end)
  "Check if BEG to END region can be extracted to new function with BACKEND.

Return nil if the region is valid, or a string that explains why the
region is not valid.")

(cl-defgeneric refactor-backend-extract-edits (backend beg end new)
  "Return edits for extracting BEG to END region to NEW function using BACKEND.

See `refactor-apply-edits' for the format of the return value.")

(cl-defgeneric refactor-backend-extract-done (backend new))

(cl-defgeneric refactor-backend-rename-edits (backend old new scope)
  "Return alist of edits for renaming OLD to NEW across SCOPE using BACKEND.

See `refactor-apply-edits' for the format of the return value.")

(defvar refactor-replacement-functions nil
  "Abnormal hook for tracking text replacements in refactor operations.

The value of this variable is a list of functions.
`refactor-apply-edits' calls each of these functions with one argument
after applying a text replacement as part of a refactor operation.  The
argument is the token corresponding to that text replacement.")

(defun refactor--apply-replacements (reps)
  "Apply text replacements REPS in the current buffer."
  (pcase-dolist (`(,beg ,end ,str ,token) (sort reps :key #'cadr :reverse t))
    (let ((source (current-buffer)))
      (with-temp-buffer
        (insert str)
        (let ((temp (current-buffer)))
          (with-current-buffer source
            (save-excursion
              (save-restriction
                (narrow-to-region beg end)
                (replace-buffer-contents temp)))
            (run-hook-with-args 'refactor-replacement-functions token)))))))

(defun refactor--find-edit-buffers (edits)
  (dolist (edit edits)
    (let ((file-or-buffer (car edit)))
      (unless (bufferp file-or-buffer)
        (setcar edit (find-file-noselect file-or-buffer))))))

(defun refactor-apply-edits-at-once (edits)
  "Apply EDITS at once, without confirmation."
  (dolist (buffer-reps edits)
    (with-current-buffer (car buffer-reps)
      (atomic-change-group
        (let ((change-group (prepare-change-group)))
          (refactor--apply-replacements (cdr buffer-reps))
          (undo-amalgamate-change-group change-group))))))

(defun refactor-run-replacement-functions-from-diff (buf &rest _)
  (let ((tokens (get-text-property (point) 'refactor-replacement-tokens)))
    (with-current-buffer buf
      (dolist (token tokens)
        (run-hook-with-args 'refactor-replacement-functions token)))))

(defun refactor-display-edits-as-diff (edits)
  "Display EDITS as a diff."
  (with-current-buffer (get-buffer-create "*Refactor Diff*")
    (buffer-disable-undo (current-buffer))
    (setq buffer-read-only t)
    (diff-mode)
    (let ((inhibit-read-only t)
          (target (current-buffer)))
      (erase-buffer)
      (pcase-dolist (`(,buf . ,reps) edits)
        (when (and reps buf)
          (with-temp-buffer
            (let ((diff (current-buffer))
                  (tokens nil))
              (with-temp-buffer
                (insert-buffer-substring buf)
                (let ((refactor-replacement-functions
                       (list (lambda (token) (push token tokens)))))
                  (refactor--apply-replacements reps))
                (diff-no-select buf (current-buffer) nil t diff))
              (with-current-buffer target
                (let ((start (point)))
                  (insert-buffer-substring diff)
                  (put-text-property start (point)
                                     'refactor-replacement-tokens tokens)
                  (put-text-property start (point) 'diff-new buf))))))))
    (add-hook 'diff-apply-hunk-functions
              #'refactor-run-replacement-functions-from-diff nil t)
    (buffer-enable-undo (current-buffer))
    (goto-char (point-min))
    (pop-to-buffer (current-buffer))
    (font-lock-ensure)))

(defcustom refactor-query-apply-display-buffer-action nil
  "`display-buffer' action to show a buffer when querying about editing it."
  :type '(cons (choice function (repeat :tag "Functions" function)) alist))

(defun refactor--query-apply-buffer-reps (buffer reps)
  "Suggest applying replacements REPS in BUFFER."
  (if-let ((win (display-buffer buffer refactor-query-apply-display-buffer-action)))
      (with-selected-window win
        (dolist (rep reps)
          (setcar rep (copy-marker (car rep)))
          (setcar (cdr rep) (copy-marker (cadr rep)))
          (let ((ov (make-overlay (car rep) (cadr rep))))
            (overlay-put ov 'face '(obsolete diff-refine-removed))
            (overlay-put ov 'after-string (propertize (caddr rep) 'face 'diff-added))
            (overlay-put ov 'window (selected-window))
            (if-let ((cell (cdddr rep)))
                (setcdr cell ov)
              (setcdr (cddr rep) (cons nil ov)))))
        (unwind-protect
            (while reps
              (pcase (car reps)
                (`(,beg ,end ,str ,token . ,ov)
                 (save-excursion
                   (overlay-put ov 'after-string (propertize str 'face 'diff-refine-added))
                   (set-window-point (selected-window) end)
                   (when (prog1
                             (pcase (car (read-multiple-choice
                                          "Apply?"
                                          '((?y  "yes"  "Apply")
                                            (?n  "no"   "Skip")
                                            (?q  "quit" "Quit")
                                            (?!  "all"  "Apply to all"))))
                               (?y t)
                               (?n nil)
                               (?q (throw 'stop nil))
                               (?! (throw 'stop (cons 'all reps))))
                           (delete-overlay ov))
                     (let ((source (current-buffer)))
                       (with-temp-buffer
                         (insert str)
                         (let ((temp (current-buffer)))
                           (with-current-buffer source
                             (save-excursion
                               (save-restriction
                                 (narrow-to-region beg end)
                                 (replace-buffer-contents temp)))
                             (run-hook-with-args
                              'refactor-replacement-functions token)))))))))
              (setq reps (cdr reps)))
          (pcase-dolist (`(,_ ,_ ,_ ,_ . ,ov) reps)
            (when (overlayp ov) (delete-overlay ov)))))
    (error "Failed to display buffer `%s' for applying edits"
           (buffer-name buffer))))

(defun refactor-query-apply-edits (edits)
  "Suggest applying each edit in EDITS in turn."
  (let ((change-group (mapcan (compf prepare-change-group car) edits))
	(undo-outer-limit nil)
	(undo-limit most-positive-fixnum)
	(undo-strong-limit most-positive-fixnum)
	(success nil))
    (unwind-protect
	(progn
	  (activate-change-group change-group)
          (pcase (catch 'stop
                   (while edits
                     (let ((buffer-reps (car edits)))
                       (refactor--query-apply-buffer-reps
                        (car buffer-reps)
                        (sort (cdr buffer-reps) :key #'cadr))
                       (setq edits (cdr edits)))))
            (`(all . ,reps)
             (setcdr (car edits) reps)
             (refactor-apply-edits-at-once edits)))
          (setq success t))
      (funcall (if success #'accept-change-group #'cancel-change-group) change-group))))

;;;###autoload
(defun refactor-apply-edits (edits)
  "Apply EDITS.

Call the function specified by `refactor-apply-edits-function' to
do the work.

EDITS is list of cons cells (FILE-OR-BUFFER . REPS), where
FILE-OR-BUFFER is the file name or buffer to edit, and REPS is a list of
replacements to perform in FILE-OR-BUFFER.  Each element of REPS is a
list (BEG END STR TOKEN), where BEG and END are positions to delete and
STR is the string to insert at BEG afterwards.  TOKEN is an arbitrary
object that a refactor backend can provide in order to track
applications of this replacement via `refactor-replacement-functions',
which see."
  (refactor--find-edit-buffers edits)
  (funcall refactor-apply-edits-function edits))

(provide 'refactor)
;;; refactor.el ends here
