;;; scope-tests.el --- Tests for scope.el -*- lexical-binding: t -*-

;; Copyright (C) 2024 Eshel Yaron

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'scope)
(require 'ert)

(defmacro scope-test (given expected)
  `(should (equal ,expected (let (all)
                              (scope (lambda (_type beg len bin &optional def)
                                       (push (list beg len def) all))
                                     ,given)
                              (reverse all)))))

(ert-deftest scope-test-1 ()
  (scope-test "
(defun foo (bar baz)
  (let* ((baz baz)
         (baz baz))
    (when (and bar spam baz)
      (ignore bar baz)))
  (ignore baz))" '((2 5 nil)
                   (8 3 nil)
                   (13 3 13)
                   (17 3 17)
                   (25 4 nil)
                   (32 3 32)
                   (36 3 17)
                   (51 3 51)
                   (55 3 32)
                   (66 4 nil)
                   (72 3 nil)
                   (76 3 13)
                   (80 4 nil)
                   (85 3 51)
                   (97 6 nil)
                   (104 3 13)
                   (108 3 51)
                   (118 6 nil)
                   (125 3 17))))

(ert-deftest scope-test-2 ()
  (scope-test "
(defun refactor-backends ()
  \"Return alist of refactor operations and backends that support them.\"
  (let ((op-be-alist nil))
    (run-hook-wrapped
     'refactor-backend-functions
     (lambda (be-fun &rest _)
       (pcase (funcall be-fun)
         (`(,be . ,ops)
          (dolist (op ops)
            (push be (alist-get op op-be-alist)))))))
    op-be-alist))" '((2 5 nil)
                     (8 17 nil)
                     (104 3 nil)
                     (110 11 110)
                     (156 26 nil)
                     (133 16 nil)
                     (189 6 nil)
                     (197 6 197)
                     (204 5 nil)
                     (221 5 nil)
                     (228 7 nil)
                     (236 6 197)
                     (257 2 257)
                     (263 3 263)
                     (279 6 nil)
                     (290 3 263)
                     (287 2 287)
                     (308 4 nil)
                     (313 2 257)
                     (317 9 nil)
                     (327 2 287)
                     (330 11 110)
                     (353 11 110))))

(ert-deftest scope-test-3 ()
  (scope-test "
(defmacro erc--with-entrypoint-environment (env &rest body)
  \"Run BODY with bindings from ENV alist.\"
  (declare (indent 1))
  (let ((syms (make-symbol \"syms\"))
        (vals (make-symbol \"vals\")))
    `(let (,syms ,vals)
       (pcase-dolist (`(,k . ,v) ,env) (push k ,syms) (push v ,vals))
       (cl-progv ,syms ,vals
         ,@body))))" '((2 8 nil)
                       (11 32 nil)
                       (107 7 nil)
                       (116 6 nil)
                       (45 3 45)
                       (49 5 nil)
                       (55 4 55)
                       (130 3 nil)
                       (136 4 136)
                       (142 11 nil)
                       (172 4 172)
                       (178 11 nil)
                       (212 4 136)
                       (218 4 172)
                       (258 3 45)
                       (272 4 136)
                       (287 4 172)
                       (312 4 136)
                       (318 4 172)
                       (334 4 55))))

(ert-deftest scope-test-4 ()
  (scope-test "
(let ((foo 1))
  (cl-flet ((foo (bar) (* bar foo)))
    (cl-block foo
      (while (foo foo) (cl-return-from foo (foo foo))))))"
              '((2 3 nil)
                (8 3 8)
                (19 7 nil)
                (29 3 29)
                (34 3 34)
                (40 1 nil)
                (42 3 34)
                (46 3 8)
                (58 8 nil)
                (67 3 67)
                (78 5 nil)
                (85 3 29)
                (89 3 8)
                (95 14 nil)
                (110 3 67)
                (115 3 29)
                (119 3 8))))

(ert-deftest scope-test-5 ()
  (scope-test "(cl-loop if 1 return (+ it it))"
              '((1 7 nil)
                (22 1 nil)
                (24 2 9)
                (27 2 9))))

(ert-deftest scope-test-6 ()
  (scope-test "
(let-alist '((rose . red) (lily . white) (buttercup . yellow))
  (if (eq .rose 'red)
      (let-alist '((rose . red) (lily . white) (buttercup . yellow))
        (if (eq .rose 'red)
            .lily
          (+ .lily .rose)))
    (+ .lily .rose)))"
              '((2 9 nil)
                (67 2 nil)
                (71 2 nil)
                (74 5 2)
                (93 9 nil)
                (164 2 nil)
                (168 2 nil)
                (171 5 93)
                (195 5 93)
                (212 1 nil)
                (214 5 93)
                (220 5 93)
                (234 1 nil)
                (236 5 2)
                (242 5 2))))

;;; scope-tests.el ends here
